#pragma once

/**
 * @defgroup J1587 J1587 CAPL Functions
 */

/* Selectors */
#include "J1587ErrorMessage.h"
#include "J1587Message.h"
#include "J1587Param.h"

/* Event Handler */

/* General Functions */
#include "J1587AppendParameter.h"
#include "J1587GetParameter.h"
#include "J1587GetParameterByPID.h"
#include "J1587GetParameterCount.h"
#include "setJ1587TP.h"
