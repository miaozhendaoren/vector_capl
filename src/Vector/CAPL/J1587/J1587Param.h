#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Receipt of a J1587 parameter.
 *
 * Defines the event handler for a valid J1587 parameter, the this pointer is of type J1587Param.
 */
class J1587Param
{
public:
    /**
     * Time stamp in 10 µs resolution
     */
    dword TIME;

    /**
     * Length of the parameter
     */
    word DLC;

    /** @todo
    J1587_PID
     PID of the parameter

    J1587_MID
     MID of the message which contains the parameter

    J1587_ReceiverMID
     Receiver MID for proprietary messages

    BYTE( index )
     Byte access for the data

    WORD( index )
     Word access for the data

    DWORD( index )
     DWord access for the data
    */
};

}
