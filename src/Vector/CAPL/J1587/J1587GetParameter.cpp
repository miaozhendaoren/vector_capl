#include "J1587GetParameter.h"

#include <iostream>

namespace capl
{

byte J1587GetParameter(J1587Message msg, J1587Param param, word index)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
