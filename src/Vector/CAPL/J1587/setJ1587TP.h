#pragma once

#include "../DataTypes.h"
#include "J1587.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Selects the setting for the transport protocol.
 *
 * Selects the setting for the transport protocol to use when sending a J1587 parameter via the output function.
 *
 * @param param
 *   J1587 parameter for which the setting is used
 *
 * @param mode
 *   - 0 (default): select the transport protocol by means of the receiver (selector J1587_ReceiverMID)
 *     - MID 127: Multisection
 *     - other MID: CMP/CDP
 *   - 1: disable use of any transport protocol
 *   - 2: use Multisection
 *   - 3: use CMP/CDP
 */
void setJ1587TP(J1587Param param, dword mode);

}
