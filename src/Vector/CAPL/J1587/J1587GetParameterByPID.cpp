#include "J1587GetParameterByPID.h"

#include <iostream>

namespace capl
{

byte J1587GetParameterByPID(J1587Message msg, J1587Param param, dword dbPID)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
