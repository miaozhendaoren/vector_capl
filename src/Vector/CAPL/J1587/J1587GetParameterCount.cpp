#include "J1587GetParameterCount.h"

#include <iostream>

namespace capl
{

word J1587GetParameterCount(J1587Message msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
