#include "testWaitForScopeEvent.h"

#include <iostream>

namespace capl
{

long testWaitForScopeEvent(dword aTimeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long testWaitForScopeEvent(ScopeEventType scopeEvent, dword aTimeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
