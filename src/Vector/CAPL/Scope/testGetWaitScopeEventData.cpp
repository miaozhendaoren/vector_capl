#include "testGetWaitScopeEventData.h"

#include <iostream>

namespace capl
{

long testGetWaitScopeEventData(ScopeEvent aScopeEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
