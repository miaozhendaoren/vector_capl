#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Performs Deactivate Trigger action for Scope window.
 *
 * Performs Deactivate Trigger action for Scope window. This action is equivalent to deactivating the trigger via the GUI.
 *
 * The completion of this action is reported with an internal event which can be awaited via TFS-function testWaitForScopeEvent() in CAPL programs for test modules.
 *
 * @return
 *   - 2 (success): Trigger is already inactive. This might be a case when the trigger has been deactivated by a previous CAPL call or manually.
 *   - 1 (success): Trigger deactivation process started. On success an internal Scope event will be generated (see above). Failure can be recognized implicitly by not receiving the corresponding Scope event during certain time-out, e.g. during one second.
 *   - -1 (failure): Failure on stop capture request.
 */
long scopeDeactivateTrigger(void);

}
