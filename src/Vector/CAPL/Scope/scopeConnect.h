#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Performs Connect Scope action for Scope window.
 *
 * Performs Connect Scope action for Scope window. This action is equivalent to connecting Scope via the GUI. The Scope window will be opened automatically if not yet opened.
 *
 * The completion of this action is reported with an internal event which can be awaited via TFS-function testWaitForScopeEvent() in CAPL programs for test modules.
 *
 * @return
 *   - 2 (success): Connection is already established. This might be a case when connection has been established by a previous CAPL call or manually.
 *   - 1 (success): Connection process started. On success an internal Scope event will be generated (see above). Failure can be recognized implicitly by not receiving the corresponding Scope event during certain timeout, e.g. during one second.
 *   - -1 (failure): Failure on connection request.
 */
long scopeConnect(void);

}
