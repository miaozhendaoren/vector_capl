#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Performs Disconnect Scope action for Scope window.
 *
 * Performs Disconnect Scope action for Scope window. This action is equivalent to disconnecting Scope via the GUI.
 *
 * The completion of this action is reported with an internal event which can be awaited via TFS-function testWaitForScopeEvent() in CAPL programs for test modules.
 *
 * @return
 *   - 2 (success): Scope is already disconnected. This might be a case when the disconnection has been done by a previous CAPL call or manually.
 *   - 1 (success): Disconnection process started. On success an internal Scope event will be generated (see above). Failure can be recognized implicitly by not receiving the corresponding Scope event during certain timeout, e.g. during one second.
 *   - -1 (failure): Failure on disconnection request.
 */
long scopeDisconnect(void);

}
