#include "scopeDisconnect.h"

#include <iostream>

namespace capl
{

long scopeDisconnect(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
