#pragma once

/**
 * @defgroup Scope Scope CAPL Functions
 */

/* Selectors */
#include "ScopeEvent.h"

/* General Function */
#include "scopeActivateTrigger.h"
#include "scopeConnect.h"
#include "scopeDeactivateTrigger.h"
#include "scopeDisconnect.h"
#include "scopeTriggerNow.h"

/* Test Feature Set for Scope */
#include "testGetWaitScopeEventData.h"
#include "testWaitForScopeEvent.h"
