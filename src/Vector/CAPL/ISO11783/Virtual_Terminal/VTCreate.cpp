#include "VTCreate.h"

#include <iostream>

namespace capl
{

dword VTCreate(char * deviceName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword VTCreate(char * deviceName, long flags)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
