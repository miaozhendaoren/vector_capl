#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Virtual_Terminal
 *
 * @brief
 *   Creates a Virtual Terminal.
 *
 * The function creates a Virtual Terminal. Call this function from "on start".
 *
 * @param deviceName
 *   device name for the Virtual Terminal, array must have 8 Bytes
 *
 * @return
 *   - =0: success
 *   - >0: error
 */
dword VTCreate(char * deviceName);

/**
 * @ingroup Virtual_Terminal
 *
 * @brief
 *   Creates a Virtual Terminal.
 *
 * The function creates a Virtual Terminal. Call this function from "on start".
 *
 * @param deviceName
 *   device name for the Virtual Terminal, array must have 8 Bytes
 *
 * @param flags
 *   Bit 0 = 1: If a VT registers to the network, it is checked if the selected Function Instance (part of the device name) is already used by another VT before. In that case the VT changes its Function Instance to an unused value.
 *
 *
 * @return
 *   - =0: success
 *   - >0: error
 */
dword VTCreate(char * deviceName, long flags);

}
