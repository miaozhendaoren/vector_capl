#include "J1939TestNmtRefresh.h"

#include <iostream>

namespace capl
{

long J1939TestNmtRefresh(dword waitTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
