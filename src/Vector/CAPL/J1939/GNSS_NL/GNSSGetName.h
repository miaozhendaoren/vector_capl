#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup GNSS_NL
 *
 * @brief
 *   Returns the J1939 device name of a control device.
 *
 * The function returns the J1939 device name of a control device that was logged on to the network with addr. The function works with a global table containing all node names and addresses logged on to the network.
 *
 * @param addr
 *   address of the device
 *
 * @param name
 *   name of the device
 *
 * @return
 *   error code
 */
dword GNSSGetName(dword addr, byte * name);

}
