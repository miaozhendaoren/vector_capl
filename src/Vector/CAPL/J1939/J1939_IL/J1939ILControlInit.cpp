#include "J1939ILControlInit.h"

#include <iostream>

namespace capl
{

long J1939ILControlInit(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long J1939ILControlInit(byte * deviceName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
