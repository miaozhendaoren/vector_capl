#include "SysSetVariableString.h"

#include <iostream>

namespace capl
{

long SysSetVariableString(char * namespace_, char * variable, char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableString(sysvarString & SysVarName, char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
