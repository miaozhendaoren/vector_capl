#include "SysSetVariableData.h"

#include <iostream>

namespace capl
{

long SysSetVariableData(char * namespace_, char * variable, byte * data, long size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableData(sysvarData & SysVarName, byte * data, long size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
