#include "SysFilterAddVariable.h"

#include <iostream>

namespace capl
{

long sysFilterAddVariable(sysvar & variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterAddVariable(sysvar & variable, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterAddVariable(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterAddVariable(char * namespace_, char * variable, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
