#include "SysGetVariableInt.h"

#include <iostream>

namespace capl
{

long SysGetVariableInt(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableInt(sysvarInt & SysVarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
