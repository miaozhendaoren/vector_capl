#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Activates or deactivates a variable filter.
 * If no filterName is given, the default filter is affected.
 *
 * @param active
 *   Specifies the new filter status:
 *   - 0: deactivated.
 *   - All other values: activated.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The filter does not exist. The filter status cannot be changed.
 */
long sysSetVariableFilterActive(dword * active);

/**
 * @ingroup System_Variables
 *
 * Activates or deactivates a variable filter.
 * If no filterName is given, the default filter is affected.
 *
 * @param active
 *   Specifies the new filter status:
 *   - 0: deactivated.
 *   - All other values: activated.
 *
 * @param filterName
 *   The name of the filter that should be affected.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The filter does not exist. The filter status cannot be changed.
 */
long sysSetVariableFilterActive(dword * active, char * filterName);

}
