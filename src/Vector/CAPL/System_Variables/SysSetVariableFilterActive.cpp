#include "SysSetVariableFilterActive.h"

#include <iostream>

namespace capl
{

long sysSetVariableFilterActive(dword * active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysSetVariableFilterActive(dword * active, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
