#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Retrieves the physical value of a specific element of a variable of type struct or generic array.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variableAndMemberName
 *   Name of the variable and the element of the struct/array.
 *
 * @param value
 *   Receives the current physical value of the element.
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable or element were not found
 *   - 4: the element has no suitable type for the function
 */
long SysGetVariableMemberPhys(char * namespace_, char * variableAndMemberName, double & value);

/**
 * @ingroup System_Variables
 *
 * Retrieves the physical value of a specific element of a variable of type struct or generic array.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable element, including all name spaces, separated by "::". The name must be preceded by "sysVarMember::".
 *
 * @param value
 *   Receives the current physical value of the element.
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable or element were not found
 *   - 4: the element has no suitable type for the function
 */
long SysGetVariableMemberPhys(sysvar & SysVarMemberName, double & value);

}
