#pragma once

#include "../DataTypes.h"
#include "sysvarFloat.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the value of a variable of the float type. (Form 1)
 *
 * Returns the value of a variable of the float type.
 *
 * @param namespace_
 *   Name of the name space (form 1)
 *
 * @param variable
 *   Name of the variable (form 1)
 *
 * @return
 *   Value of the variable or 0 in case of error
 */
double SysGetVariableFloat(char * namespace_, char * variable);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the value of a variable of the float type. (Form 2)
 *
 * Returns the value of a variable of the float type.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::". The name must be preceded by "sysVar::". (form 2)
 *
 * @return
 *   Value of the variable or 0 in case of error
 */
double SysGetVariableFloat(sysvarFloat & SysVarName);

}
