#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * System variable
 */
class sysvar
{
public:
    sysvar();
    virtual ~sysvar();
};

}
