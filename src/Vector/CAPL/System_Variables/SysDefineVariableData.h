#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Defines a variable of data type.
 *
 * @brief namespace_
 *   Name of the name space.
 *
 * @brief variable
 *   Name of the variable.
 *
 * @brief initialData
 *   Initial value of the variable.
 *
 * @brief initialSize
 *   Initial size (in bytes) of the variable. Must not exceed the length of the initialData array.
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysDefineVariableData(char * namespace_, char * variable, byte * initialData, long initialSize);

}
