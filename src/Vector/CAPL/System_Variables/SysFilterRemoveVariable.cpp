#include "SysFilterRemoveVariable.h"

#include <iostream>

namespace capl
{

long sysFilterRemoveVariable(sysvar & variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterRemoveVariable(sysvar & variable, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterRemoveVariable(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterRemoveVariable(char * namespace_, char * variable, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
