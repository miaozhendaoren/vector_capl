#include "SysEndVariableStructUpdate.h"

#include <iostream>

namespace capl
{

long SysEndVariableStructUpdate(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysEndVariableStructUpdate(sysvar & sysvarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
