#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Ends the update of several elements of a system variable of type struct or generic array.
 *
 * Use this function and the corresponding SysBeginVariableStructUpdate to change several elements
 * at the same time without the variable having an intermediate value where only some elements are changed.
 *
 * @param namespace
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable SysVarName: Name of the fully qualified name of the system variable, including all name spaces, separated by "::". The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - -1: variable was not found
 */
long SysEndVariableStructUpdate(char * namespace_, char * variable);

/**
 * @ingroup System_Variables
 *
 * Ends the update of several elements of a system variable of type struct or generic array.
 *
 * Use this function and the corresponding SysBeginVariableStructUpdate to change several elements
 * at the same time without the variable having an intermediate value where only some elements are changed.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::". The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - -1: variable was not found
 */
long SysEndVariableStructUpdate(sysvar & sysvarName);

}
