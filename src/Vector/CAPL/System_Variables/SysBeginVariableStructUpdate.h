#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Starts the update of several elements of a system variable of type struct or generic array.
 *
 * Starts the update of several elements of a system variable of type struct or generic array.
 *
 * Use this function and the corresponding SysEndVariableStructUpdate to change several elements at the same time without the variable having an intermediate value where only some elements are changed.
 * Each call must be followed by a call to SysEndVariableStructUpdate, else the variable value will not change.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable SysVarName: Name of the fully qualified name of the system variable, including all name spaces, separated by "::". The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - -1: variable was not found
 */
long SysBeginVariableStructUpdate(char * namespace_, char * variable);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Starts the update of several elements of a system variable of type struct or generic array.
 *
 * Starts the update of several elements of a system variable of type struct or generic array.
 *
 * Use this function and the corresponding SysEndVariableStructUpdate to change several elements at the same time without the variable having an intermediate value where only some elements are changed.
 * Each call must be followed by a call to SysEndVariableStructUpdate, else the variable value will not change.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::". The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - -1: variable was not found
 */
long SysBeginVariableStructUpdate(sysvar & sysvarName);

}
