#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Removes a namespace from the variable filter. If no filterName is given, the namespace is removed from the default filter.
 *
 * @param namespace_
 *   The namespace to remove.
 *
 * @return
 *   - 0 : No error, function successful.
 *   - 2: The variable filter does not exist. The namespace cannot be removed.
 *   - 3: The namespace is not filtered by the filter. The namespace cannot be removed.
 */
long sysFilterRemoveNamespace(char * namespace_);

/**
 * @ingroup System_Variables
 *
 * Removes a namespace from the variable filter. If no filterName is given, the namespace is removed from the default filter.
 *
 * @param namespace_
 *   The namespace to remove.
 *
 * @param filterName
 *   The name of the filter the namespace should be removed from.
 *
 * @return
 *   - 0 : No error, function successful.
 *   - 2: The variable filter does not exist. The namespace cannot be removed.
 *   - 3: The namespace is not filtered by the filter. The namespace cannot be removed.
 */
long sysFilterRemoveNamespace(char * namespace_, char * filterName);

}
