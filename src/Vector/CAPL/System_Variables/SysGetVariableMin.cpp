#include "SysGetVariableMin.h"

#include <iostream>

namespace capl
{

long sysGetVariableMin(sysvar & SysVarName, long& minimum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysGetVariableMin(sysvar & SysVarName, double& minimum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysGetVariableMin(char * namespace_, char * variable, long& minimum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysGetVariableMin(char * namespace_, char * variable, double& minimum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
