#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Removes a variable from a variable filter. If no filterName is given, the variable will be removed from the default filter.
 *
 * @param variable
 *   The variable to remove.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be removed.
 *   - 5: The variable is not filtered by the filter. The variable cannot be removed.
 *   - 6: The variable does not exist. The variable cannot be removed.
 */
long sysFilterRemoveVariable(sysvar & variable);

/**
 * @ingroup System_Variables
 *
 * Removes a variable from a variable filter. If no filterName is given, the variable will be removed from the default filter.
 *
 * @param variable
 *   The variable to remove.
 *
 * @param filterName
 *   The name of the filter the variable should be removed from.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be removed.
 *   - 5: The variable is not filtered by the filter. The variable cannot be removed.
 *   - 6: The variable does not exist. The variable cannot be removed.
 */
long sysFilterRemoveVariable(sysvar & variable, char * filterName);

/**
 * @ingroup System_Variables
 *
 * Removes a variable from a variable filter. If no filterName is given, the variable will be removed from the default filter.
 *
 * @param namespace_
 *   The namespace that contains the variable.
 *
 * @param variable
 *   The name of the variable.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be removed.
 *   - 5: The variable is not filtered by the filter. The variable cannot be removed.
 *   - 6: The variable does not exist. The variable cannot be removed.
 */
long sysFilterRemoveVariable(char * namespace_, char * variable);

/**
 * @ingroup System_Variables
 *
 * Removes a variable from a variable filter. If no filterName is given, the variable will be removed from the default filter.
 *
 * @param filterName
 *   The name of the filter the variable should be removed from.
 *
 * @param namespace_
 *   The namespace that contains the variable.
 *
 * @param variable
 *   The name of the variable.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be removed.
 *   - 5: The variable is not filtered by the filter. The variable cannot be removed.
 *   - 6: The variable does not exist. The variable cannot be removed.
 */
long sysFilterRemoveVariable(char * namespace_, char * variable, char * filterName);

}
