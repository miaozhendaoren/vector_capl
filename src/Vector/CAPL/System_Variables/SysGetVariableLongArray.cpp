#include "SysGetVariableLongArray.h"

#include <iostream>

namespace capl
{

long SysGetVariableLongArray(char * namespace_, char * variable, long * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableLongArray(sysvarIntArray & SysVarName, long * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
