#pragma once

#include "../DataTypes.h"
#include "sysvarData.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Sets the value of a variable of the type data, struct or generic array.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable.
 *
 * @param data
 *   New values for the variable.
 *
 * @param size
 *   New size for the variable.
 *   Must not exceed the length of the data array.
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysSetVariableData(char * namespace_, char * variable, byte * data, long size);

/**
 * @ingroup System_Variables
 *
 * Sets the value of a variable of the type data, struct or generic array.
 *
 * @param data
 *   New values for the variable.
 *
 * @param size
 *   New size for the variable.
 *   Must not exceed the length of the data array.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::".
 *   The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysSetVariableData(sysvarData & SysVarName, byte * data, long size);

}
