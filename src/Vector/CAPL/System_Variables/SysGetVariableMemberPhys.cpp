#include "SysGetVariableMemberPhys.h"

#include <iostream>

namespace capl
{

long SysGetVariableMemberPhys(char * namespace_, char * variableAndMemberName, double & value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableMemberPhys(sysvar & SysVarMemberName, double & value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
