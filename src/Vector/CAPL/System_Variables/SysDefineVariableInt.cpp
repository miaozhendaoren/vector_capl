#include "SysDefineVariableInt.h"

#include <iostream>

namespace capl
{

long SysDefineVariableInt(char * namespace_, char * variable, long initialValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysDefineVariableInt(char * namespace_, char * variable, long initialValue, long minimum, long maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
