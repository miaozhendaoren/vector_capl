#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Adds a namespace to the variable filter.
 * If no filterName is given, the namespace is added to the default filter.
 *
 * @param namespace_
 *   The namespace to add.
 *   The existence of the namespace is not checked, and adding a namespace that is not defined is not an error.
 *   If the namespace gets defined after it is added to the filter, it is affected by the filter normally.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The namespace cannot be added.
 *   - 3: The variable is already added to the filter. The namespace cannot be added.
 */
long sysFilterAddNamespace(char * namespace_);

/**
 * @ingroup System_Variables
 *
 * Adds a namespace to the variable filter.
 * If no filterName is given, the namespace is added to the default filter.
 *
 * @param namespace_
 *   The namespace to add.
 *   The existence of the namespace is not checked, and adding a namespace that is not defined is not an error.
 *   If the namespace gets defined after it is added to the filter, it is affected by the filter normally.
 *
 * @param filterName
 *   The name of the filter the namespace should be added to.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The namespace cannot be added.
 *   - 3: The variable is already added to the filter. The namespace cannot be added.
 */
long sysFilterAddNamespace(char * namespace_, char * filterName);

}
