#include "SysGetVariableMax.h"

#include <iostream>

namespace capl
{

long sysGetVariableMax(sysvar & SysVarName, long& maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysGetVariableMax(sysvar & SysVarName, double& maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysGetVariableMax(char * namespace_, char * variable, long& maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysGetVariableMax(char * namespace_, char * variable, double& maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
