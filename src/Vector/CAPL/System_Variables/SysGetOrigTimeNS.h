#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the original time stamp of the last update to the variable value.
 *
 * Returns the original time stamp of the last update to the variable value, before time
 * synchronization may have changed it.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::". The name must be preceded by "sysVar::".
 *
 * @return
 *   Original time stamp of the last update to the variable value in nanoseconds since
 *   measurement start.
 */
int64 SysGetOrigTimeNS(sysvar & SysVarName);

}
