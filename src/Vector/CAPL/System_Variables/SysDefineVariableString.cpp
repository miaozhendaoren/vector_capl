#include "SysDefineVariableString.h"

#include <iostream>

namespace capl
{

long SysDefineVariableString(char * namespace_, char * variable, char * initialValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
