#include "SysGetVariableData.h"

#include <iostream>

namespace capl
{

long SysGetVariableData(char * namespace_, char * variable, byte * buffer, long & copiedBytes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableData(sysvarData & SysVarName, byte * buffer, long & copiedBytes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
