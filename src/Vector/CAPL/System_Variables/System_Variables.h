#pragma once

/**
 * @defgroup System_Variables System Variables CAPL Functions
 */

/* General Functions */
#include "SysDefineNamespace.h"
#include "SysUndefineNamespace.h"
#include "SysUndefineVariable.h"

/* Functions to Define Variables */
#include "SysDefineVariableData.h"
#include "SysDefineVariableFloat.h"
#include "SysDefineVariableFloatArray.h"
#include "SysDefineVariableInt.h"
#include "SysDefineVariableIntArray.h"
#include "SysDefineVariableString.h"

/* Functions to Return the Value of a Variable */
#include "SysGetOrigTimeNS.h"
#include "SysGetVariableArrayLength.h"
#include "SysGetVariableData.h"
#include "SysGetVariableDescriptionForValue.h"
#include "SysGetVariableFloat.h"
#include "SysGetVariableFloatArray.h"
#include "SysGetVariableValueForDescription.h"
#include "SysGetVariableInt.h"
#include "SysGetVariableLongArray.h"
#include "SysGetVariableString.h"
#include "SysGetVariableMax.h"
#include "SysGetVariableMin.h"
#include "SysGetVariableTimeNS.h"

/* Functions to Set the Value of a Variable */
#include "SysSetAnalysisOnlyVariable.h"
#include "SysSetVariableData.h"
#include "SysSetVariableDescriptionForValue.h"
#include "SysSetVariableFloat.h"
#include "SysSetVariableFloatArray.h"
#include "SysSetVariableInt.h"
#include "SysSetVariableLongArray.h"
#include "SysSetVariableString.h"

/* Functions for Structs of System Variables */
#include "SysBeginVariableStructUpdate.h"
#include "SysEndVariableStructUpdate.h"
#include "SysGetVariableMemberPhys.h"
#include "SysSetVariableMemberPhys.h"

/* Functions for variable filters */
#include "SysCreateVariableFilter.h"
#include "SysFilterAddNamespace.h"
#include "SysFilterAddVariable.h"
#include "SysFilterRemoveNamespace.h"
#include "SysFilterRemoveVariable.h"
#include "SysSetVariableFilterActive.h"

/* Undocumented */
#include "sysvar.h"
#include "sysvarData.h"
#include "sysvarInt.h"
#include "sysvarFloat.h"
#include "sysvarString.h"
#include "sysvarIntArray.h"
#include "sysvarFloatArray.h"
