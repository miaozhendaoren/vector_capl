#include "SysGetVariableValueForDescription.h"

#include <iostream>

namespace capl
{

long SysGetVariableValueForDescription(sysvar & SysVarName, char * description, long & value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableValueForDescription(char * namespace_, char * variable, char * description, long & value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
