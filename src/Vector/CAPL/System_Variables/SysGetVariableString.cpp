#include "SysGetVariableString.h"

#include <iostream>

namespace capl
{

long SysGetVariableString(sysvarString & namespace_, char * variable, char * buffer, long bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableString(sysvarString & SysVarName, char * buffer, long bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
