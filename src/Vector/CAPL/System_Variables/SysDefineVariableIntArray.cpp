#include "SysDefineVariableIntArray.h"

#include <iostream>

namespace capl
{

long SysDefineVariableIntArray(char * namespace_, char * variable, int * initialValues, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysDefineVariableIntArray(char * namespace_, char * variable, int * initialValues, long arraySize, long minimum, long maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
