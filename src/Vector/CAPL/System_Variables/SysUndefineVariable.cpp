#include "SysUndefineVariable.h"

#include <iostream>

namespace capl
{

long SysUndefineVariable(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
