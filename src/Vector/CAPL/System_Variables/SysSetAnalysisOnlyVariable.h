#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Defines whether the variable shall be used only in the analysis part of CANoe.
 *
 * Determines whether the variable is meant to be used for analysis purposes.
 *
 * If this is true (anlyzLocal is set to 1), and the variable is changed in a CAPL program in the
 * Measurement setup, the value change is not transmitted to the real time part of CANoe,
 * but used immediately in the analysis part. This is the default.
 *
 * If it is false (anlyzLocal is set to 0), value changes are always transmitted to the real time
 * part.
 *
 * @param anlyzLocal
 *   Defines whether the variable shall be used only in the analysis part of CANoe.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 */
void SysSetAnalysisOnlyVariable(sysvar & SysVarName, long anlyzLocal);

}
