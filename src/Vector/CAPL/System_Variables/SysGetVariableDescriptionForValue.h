#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Retrieves a description for a value of a system variable of type long or long array.
 * In this way, you can access the value table of the system variable.
 *
 * @param value
 *   Value for which the description shall be retrieved.
 *
 * @param buffer
 *   Buffer which receives the description.
 *
 * @param bufferSize
 *   Size of the buffer.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::".
 *   The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable was not found or second try to define the same variable
 *   - 5: there is no description of this value for the variable
 *   - 6: the buffer is not large enough for the description
 */
long SysGetVariableDescriptionForValue(sysvar & SysVarName, long value, char * buffer, long bufferSize);

/**
 * @ingroup System_Variables
 *
 * Retrieves a description for a value of a system variable of type long or long array.
 * In this way, you can access the value table of the system variable.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable.
 *
 * @param value
 *   Value for which the description shall be retrieved.
 *
 * @param buffer
 *   Buffer which receives the description.
 *
 * @param bufferSize
 *   Size of the buffer.
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable was not found or second try to define the same variable
 *   - 5: there is no description of this value for the variable
 *   - 6: the buffer is not large enough for the description
 */
long SysGetVariableDescriptionForValue(char * namespace_, char * variable, long value, char * buffer, long bufferSize);

}
