#include "SysSetVariableDescriptionForValue.h"

#include <iostream>

namespace capl
{

long SysSetVariableDescriptionForValue(char * namespace_, char * variable, long value, char * description)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
