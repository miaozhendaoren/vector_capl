#include "SysFilterAddNamespace.h"

#include <iostream>

namespace capl
{

long sysFilterAddNamespace(char * namespace_)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterAddNamespace(char * namespace_, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
