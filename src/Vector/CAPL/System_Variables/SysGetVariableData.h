#pragma once

#include "../DataTypes.h"
#include "sysvarData.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Returns the value of a variable of the type data, struct or generic array.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable.
 *
 * @param buffer
 *   Buffer which takes the value of the variable.
 *
 * @param copiedBytes
 *   Receives the number of bytes which were copied into the buffer.
 *   If the buffer is too small, no bytes will be copied; else the parameter will contain the current size of the variable.
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 *   - 5: the buffer is too small for the value
 */
long SysGetVariableData(char * namespace_, char * variable, byte * buffer, long & copiedBytes);

/**
 * @ingroup System_Variables
 *
 * Returns the value of a variable of the type data, struct or generic array.
 *
 * @param buffer
 *   Buffer which takes the value of the variable.
 *
 * @param copiedBytes
 *   Receives the number of bytes which were copied into the buffer.
 *   If the buffer is too small, no bytes will be copied; else the parameter will contain the current size of the variable.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::".
 *   The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 *   - 5: the buffer is too small for the value
 */
long SysGetVariableData(sysvarData & SysVarName, byte * buffer, long & copiedBytes);

}
