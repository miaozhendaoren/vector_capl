#include "SysGetOrigTimeNS.h"

#include <iostream>

namespace capl
{

int64 SysGetOrigTimeNS(sysvar & SysVarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
