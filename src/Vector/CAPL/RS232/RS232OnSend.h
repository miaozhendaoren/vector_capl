#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Callback handler for completion of send operation to a serial port.
 *
 * Callback handler for completion of send operation to a serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param buffer
 *   The buffer given to the send call.
 *
 * @param number
 *   The number of bytes which have been sent.
 *
 * @return
 *   - 0: error
 *        The error occurs if no send operation will be used.
 *   - 1: success
 */
dword RS232OnSend(dword port, byte * buffer, dword number);

}
