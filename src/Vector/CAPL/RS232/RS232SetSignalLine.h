#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Sets signal lines on serial port.
 *
 * Sets signal lines on serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param line
 *   Specifies signal line of which the state shall be set.
 *   - 0: RTS
 *   - 1: DTR
 *   Data-Terminal-Ready (from sender).
 *   RTS: Request-To-Send (from sender).
 *
 * @param state
 *   - 0: disabled
 *   - 1: enabled
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *   - 1: success
 */
dword RS232SetSignalLine(dword port, dword line, dword state); // Sets signal line on serial port.

}
