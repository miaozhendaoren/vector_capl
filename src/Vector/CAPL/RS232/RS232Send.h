#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Sends a block of bytes to a serial port.
 *
 * Sends a block of bytes to a serial port.
 *
 * - The operation starts the sending of a block.
 * - The callback handler RS232OnSend will notify the node of completion.
 * - To get informed about errors occurring in later stages of the operation use
 *   RS232OnError. There are no automatic retrials in case of error.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param buffer
 *   An array of bytes of which number will be sent.
 *
 * @param number
  *   Number of bytes to send.
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *        another non-blocking send operation (this one, RS232WriteByte or RS232WriteBlock
 *        made by non-blocking CAN.INI switch) has been used shortly before, then the
 *        previous send operation may not have finished which leads to an error. Use the
 *        RS232OnSend callback handler to synchronize operations.
 *        An error is only given if a problem is issued directly.
 *        To get informed about errors occurring in later stages of the operation use RS232OnError.
 *   - 1: success
 *        The operation may fail in later stages. Success means here, that the send operation could
 *        be started properly.
 */
dword RS232Send(dword port, byte * buffer, dword number); // Sends block of data asynchronously on serial port.

}
