#include "RS232SetCommState.h"

#include <iostream>

namespace capl
{

dword RS232SetCommState(dword port, dword baudrate, dword numberOfDataBits, dword numberOfStopBits, dword parity)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
