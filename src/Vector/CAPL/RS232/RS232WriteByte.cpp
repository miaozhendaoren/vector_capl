#include "RS232WriteByte.h"

#include <iostream>

namespace capl
{

dword RS232WriteByte(dword port, dword datum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
