#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Opens and configures serial port.
 *
 * @deprecated
 *   Replaced by RS232Open and RS232Configure
 *
 * Opens and configures a serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param baudrate
 *   The symbol rate to use for reception and transmission.
 *   Typically, 9600 is used. There is a list of possible values which depends on the serial port. Normally, 115.200 is the allowed maximum.
 *
 * @param numberOfDataBits
 *   The number of data bits within a transmission frame.
 *   8 is used at most. Only values between 5 and 8 are possible. Not all values and not all combinations with other frame parameters may be supported by the serial port.
 *
 * @param numberOfStopBits
 *   A code which sets the number of stop bits within a transmission frame.
 *   - 0 means 1 stop bit is used. it is the typical and most reasonable value
 *   - 1 means 1.5 stop bits are used
 *   - 2 means 2 stop bits are used
 *
 * @param parity
 *   A code which identifies the parity mode to use.
 *   - 0: no parity used, i.e. frame contains no parity bit
 *   - 1: odd parity
 *   - 2: even parity
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        - the serial port with the given number does not exist on the system
 *        - the port has not been opened
 *        - another program uses the serial port according to the port number
 *   - 1: success
 */
dword RS232SetCommState(dword port, dword baudrate, dword numberOfDataBits, dword numberOfStopBits, dword parity);

}
