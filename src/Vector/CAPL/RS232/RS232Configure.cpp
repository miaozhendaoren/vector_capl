#include "RS232Configure.h"

#include <iostream>

namespace capl
{

dword RS232Configure(dword port, dword baudrate, dword numberOfDataBits, dword numberOfStopBits, dword parity)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword RS232Configure(dword port, dword baudrate, dword numberOfDataBits, dword numberOfStopBits, dword parity, dword enableParityCheck)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
