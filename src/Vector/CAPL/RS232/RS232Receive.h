#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Receive blocks of bytes from a serial port.
 *
 * Receive blocks of bytes from a serial port.
 *
 * - The operation works continuous if issued once.
 * - If any data has been received, the node will be notified by the callback handler
 *   RS232OnReceive.
 * - If another receive operation will be given, the result buffer will change to that one
 *   given by the last receive operation.
 * - To get informed about errors occurring in later stages of the operation use
 *   RS232OnError. Retrials to receive will be started continuously but you won't be
 *   notified if an error happens again.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param buffer
 *   An array of bytes.
 *
 * @param size
 *   Maximum number of bytes which can be received.
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *        An error is only given if a problem is issued directly by the system.
 *        To get informed about errors occurring in later stages of the operation use RS232OnError.
 *   - 1: success
 *        Success means here, that the receive operation could be started properly.
 */
dword RS232Receive(dword port, byte * buffer, dword size); // Sets receiver buffer for serial port.

}
