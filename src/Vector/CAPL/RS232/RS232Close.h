#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Closes a serial port.
 *
 * Closes a serial port.
 *
 * The serial port will be closed for all nodes. One and the same port may be closed by
 * several nodes (or repetitively). After closure other programs may use the serial port.
 *
 * After closing the serial port the configuration of the port is lost. The next time the port is
 * opened it will be configured with the system default.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @return
 *   - 0: error.
 *        The error occurs if the serial port with the given number does not exist on the system.
 *   - 1: success
 */
dword RS232Close(dword port);

}
