#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Callback handler for reception of data at a serial port.
 *
 * Callback handler for reception of data at a serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param buffer
 *   The buffer given to the start receive call.
 *
 * @param number
 *   The number of bytes which have been received.
 *
 * @return
 *   - 0: error
 *        The error occurs if no start receive operation has been.
 *   - 1: success
 */
dword RS232OnReceive(dword port, byte * buffer, dword number);

}
