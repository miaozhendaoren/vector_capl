#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Writes byte to serial port.
 *
 * @deprecated
 *   Replaced by RS232Send
 *
 * Sends one bytes to a serial port.
 * - The operation starts sending a byte.
 * - By default the function works blocking, i.e. it waits for completion.
 * - A CAN.INI switch allows for non-blocking behavior. The non-blocking behavior does not block the RT kernel and allows accurate and timely operation of the RT kernel. Therefore, it is very recommendable to use non-blocking behavior.
 *   INI entry:
 *   [RS232]
 *   BlockingWrite=0
 *   Values: integer
 *   0=non-Blocking, 1=blocking
 * - The callback handler RS232OnSend will notify the node of completion.
 * - To get informed about errors occurring in later stages of the operation use RS232OnError. There are no automatic retrials in case of error.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param datum
 *   Byte to be sent (lowest 8 bits).
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        - if the serial port with the given number does not exist on the system, then the call will fail.
 *        - if the port has not been opened, then the call will fail.
 *        - only relevant for non-blocking usage:
 *          if another send operation (this one,RS232WriteBlock or RS232Send) has been used shortly before, then the previous send operation may not have finished which leads to an error. Use the RS232OnSend callback handler to synchronize operations.
 *        - For non-blocking usage, see accoding section under RS232Send.
 *   - 1: success
 *        In contrast to RS232Send success means here that the operation has really succeeded to transmit data.
 *   - 2: time out, i.e. write access could not be completed till timeout (only relevant for blocking variant, see RS232SetHandshake for setting timeout.
 */
dword RS232WriteByte(dword port, dword datum);

}
