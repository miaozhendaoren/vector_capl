#include "RS232Send.h"

#include <iostream>

namespace capl
{

dword RS232Send(dword port, byte * buffer, dword number)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
