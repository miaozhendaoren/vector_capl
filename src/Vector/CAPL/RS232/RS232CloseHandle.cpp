#include "RS232CloseHandle.h"

#include <iostream>

namespace capl
{

dword RS232CloseHandle(dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
