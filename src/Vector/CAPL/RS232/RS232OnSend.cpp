#include "RS232OnSend.h"

#include <iostream>

namespace capl
{

dword RS232OnSend(dword port, byte * buffer, dword number)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
