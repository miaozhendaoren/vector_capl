#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Handler will be called if a byte has been received.
 *
 * @deprecated
 *   Replaced by RS232Receive and RS232OnReceive
 *
 * Callback handler for reception of data from serial ports.
 *
 * Each node which implements this handler will receive data.
 * The handler receives data from all opened ports (i.e. opened by CANoe/CANalyzer).
 *
 * It is cumbersome and slower by design since it gets one byte time by time.
 * Block handlers receive blocks of data at an instant of time.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param datum
 *   Byte which has been received (lowest 8 bits).
 *
 * @param note
 *   Constant 1 (for sake of interface compatibility).
 *
 * @return
 *   - 0: error.
 *        The error occurs if no serial port has been opened.
 *   - 1: success
 */
void RS232ByteCallback(dword port, dword datum, dword note);

}
