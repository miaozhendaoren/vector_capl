#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

#include <unordered_set>

/**
 * @defgroup Associative_Fields Associative Fields CAPL Functions
 */

namespace capl
{

/**
 * @ingroup Associative_Fields
 */
class VECTOR_CAPL_EXPORT Associative_Fields
{
public:
    /**
     * @brief
     *   Removes all elements from the field.
     *
     * Removes all elements from the field.
     */
    void clear(void);

    /**
     * @brief
     *   Returns the information if a key is available in the field.
     *
     * Returns the information if a key is available in the field.
     *
     * @param key
     *   Key that should be checked.
     *
     * @return
     *   1 if the key was available - otherwise 0
     */
    int containsKey(unsigned int key);

    /**
     * @brief
     *   Removes one element from the field.
     *
     * Removes one element from the field.
     *
     * @param key
     *   Key of the element.
     *
     * @return
     *   1 if the key was available - otherwise 0
     */
    int remove(unsigned int key);

    /**
     * @brief
     *   Returns the number of elements in the field.
     *
     * Returns the number of elements in the field.
     *
     * @return
     *   Number of elements in the field.
     */
    int size(void);

private:
    std::unordered_set<unsigned int> keys;
};

}
