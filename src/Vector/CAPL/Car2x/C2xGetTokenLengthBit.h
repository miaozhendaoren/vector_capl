#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the length of a token in bit
 *
 * The function returns the length of a token in bit.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "header"
 *
 * @return
 *   length of the token in bit
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenLengthBit(long packet, char * protocolDesignator, char * tokenDesignator);

}
