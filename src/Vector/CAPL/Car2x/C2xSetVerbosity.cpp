#include "C2xSetVerbosity.h"

#include <iostream>

namespace capl
{

long C2xSetVerbosity(long verbosity)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
