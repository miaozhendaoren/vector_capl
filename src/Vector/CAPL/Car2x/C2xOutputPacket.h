#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   sends a WLAN packet
 *
 * This function sends a WLAN packet.
 *
 * @param packet
 *   handle of the packet to send
 *
 * @return
 *   0 or error code
 */
long C2xOutputPacket(long packet);

}
