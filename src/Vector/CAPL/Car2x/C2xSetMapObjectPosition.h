#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the GPS position of a map object.
 */
long C2xSetMapObjectPosition(long handle, double longitude1, double latitude1);

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the GPS position of a map object.
 */
long C2xSetMapObjectPosition(long handle, double longitude1, double latitude1, double longitude2, double latitude2);

}
