#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the data of a token (Variant 1)
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * buffer);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the data of a token (Variant 2)
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * buffer);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the data of a token (Variant 3)
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param bufferStruct
 *   struct in which the data are copied
 *
 * @return
 */
long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * bufferStruct);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the data of a token (Variant 4)
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * buffer);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the data of a token (Variant 5)
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * buffer);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the data of a token (Variant 6)
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param bufferStruct
 *   struct in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * bufferStruct);

}
