#include "C2xGetThisValue8.h"

#include <iostream>

namespace capl
{

long C2xGetThisValue8(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
