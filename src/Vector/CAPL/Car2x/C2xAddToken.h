#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   adds a token at the end of a protocol header
 *
 * The function adds an additional token at the end of a protocol header
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol
 *
 * @param tokenDesignator
 *   name of the token
 *
 * @return
 *   0 or error code
 */
long C2xAddToken(long packet, char * protocolDesignator, char * tokenDesignator);

}
