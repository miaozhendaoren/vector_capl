#include "C2xCompletePacket.h"

#include <iostream>

namespace capl
{

long C2xCompletePacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
