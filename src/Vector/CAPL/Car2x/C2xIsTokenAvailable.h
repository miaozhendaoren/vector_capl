#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Checks if a token is part of a packet.
 */
long C2xIsTokenAvailable(long packet, char * protocolDesignator, char * tokenDesignator);

}
