#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   registers a CAPL handler function to receive WLAN packets
 *
 * Use this function to register a CAPL callback function to receive WLAN packets. The callback has
 * a packet handle as parameter and the functions to access the tokens can be used. The
 * C2xGetThis-functions can be used to access the payload of the highest interpretable protocol.
 *
 * The callback must have the following signature:
 *
 * void OnC2xPacket ( LONG channel, LONG dir, LONG radioChannel, LONG signalStrength, LONG signalQuality, LONG packet )
 *
 * @param onPacketCallback
 *   name of CAPL callback function
 *
 * @return
 *   0 or error code
 */
long C2xReceivePacket(char * onPacketCallback);

}
