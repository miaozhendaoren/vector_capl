#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the string value of a token (Variant 1)
 *
 * The function copies the string value to the token without terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param data
 *   data as string
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, char * data);

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the string value of a token (Variant 2)
 *
 * The function copies the string value to the token without terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param data
 *   data as string
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, char * data);

}
