#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief OnC2xPacket
 *   CAPL handler to receive data of WLAN packets
 *
 * The function is called when a registered WLAN packet is received.
 *
 * Within this callback function the following functions can be used to retrieve the received packet
 * data:
 *
 * - C2xGetThisData
 * - C2xGetThisTimeNS
 * - C2xGetThisValue8
 * - C2xGetThisValue16
 * - C2xGetThisValue32
 * - C2xGetThisValue64
 * - C2xGetThisMotorolaValue16
 * - C2xGetThisMotorolaValue32
 * - C2xGetThisMotorolaValue64
 *
 * The functions access the payload of the highest interpretable protocol. Offset 0 starts at the
 * beginning of the payload.
 *
 * @param channel
 *   WLAN channel on which the packet was received
 *
 * @param dir
 *   direction of the packet
 *   - 0 - Rx
 *   - 1 - Tx
 *
 * @param radioChannel
 *   radio channel, i.e. 176 or 180
 *
 * @param signalStrength
 *   signal strength of the received packet in [dBm]
 *
 * @param signalQuality
 *   signal quality of the received packet
 *
 * @param packet
 *  handle of the received packet
 */
void OnC2xPacket(long channel, long dir, long radioChannel, long signalStrength, long signalQuality, long packet);

}
