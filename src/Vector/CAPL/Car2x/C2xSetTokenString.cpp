#include "C2xSetTokenString.h"

#include <iostream>

namespace capl
{

long C2xSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
