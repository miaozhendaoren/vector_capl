#include "C2xInitProtocol.h"

#include <iostream>

namespace capl
{

long C2xInitProtocol(long packet, char * protocolDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xInitProtocol(long packet, char * protocolDesignator, char * packetTypeDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
