#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Returns the MAC-ID of a WLAN interface.
 */
long C2xGetDefaultMacId(byte * macId);

}
