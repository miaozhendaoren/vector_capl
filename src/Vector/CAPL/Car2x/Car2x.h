#pragma once

/**
 * @defgroup Car2x Car2x CAPL Functions
 */

/* Callback Functions */
#include "OnC2xPacket.h"

/* General Functions */
#include "C2xGetDefaultMacId.h"
#include "C2xGetLastError.h"
#include "C2xGetLastErrorText.h"
#include "C2xSetVerbosity.h"

/* Packet API */
#include "C2xInitPacket.h"
#include "C2xCompletePacket.h"
#include "C2xOutputPacket.h"
#include "C2xReleasePacket.h"
#include "C2xReceivePacket.h"
#include "C2xIsPacketValid.h"
#include "C2xIsTokenAvailable.h"
#include "C2xInitProtocol.h"
#include "C2xAddToken.h"
#include "C2xRemoveToken.h"
#include "C2xResizeToken.h"
#include "C2xGetThisData.h"
#include "C2xGetThisTimeNS.h"
#include "C2xGetThisValue8.h"
#include "C2xGetThisValue16.h"
#include "C2xGetThisValue16.h"
#include "C2xGetThisValue32.h"
#include "C2xGetThisValue64.h"
#include "C2xGetThisMotorolaValue16.h"
#include "C2xGetThisMotorolaValue32.h"
#include "C2xGetThisMotorolaValue64.h"
#include "C2xGetTokenData.h"
#include "C2xGetTokenInt.h"
#include "C2xGetTokenInt64.h"
#include "C2xGetTokenLengthBit.h"
#include "C2xGetTokenString.h"
#include "C2xGetTokenSubString.h"
#include "C2xGetTokenBitOfBitString.h"
#include "C2xSetTokenData.h"
#include "C2xSetTokenInt.h"
#include "C2xSetTokenInt64.h"
#include "C2xSetTokenString.h"
#include "C2xSetTokenBitOfBitString.h"

/* Map Window API */
#include "C2xCreateMapObject.h"
#include "C2xDeleteMapObject.h"
#include "C2xDrawMapObject.h"
#include "C2xSetMapObjectBmpCount.h"
#include "C2xSetMapObjectBmpFilePath.h"
#include "C2xSetMapObjectBmpIndex.h"
#include "C2xSetMapObjectFillColor.h"
#include "C2xSetMapObjectHeading.h"
#include "C2xSetMapObjectLineColor.h"
#include "C2xSetMapObjectPenStyle.h"
#include "C2xSetMapObjectPenWidth.h"
#include "C2xSetMapObjectPosition.h"
#include "C2xSetMapObjectSize.h"
#include "C2xSetMapObjectText.h"
