#include "C2xGetThisMotorolaValue32.h"

#include <iostream>

namespace capl
{

long C2xGetThisMotorolaValue32(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
