#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Gets the 64 bit integer value of a token (Variant 1)
 */
int64 C2xGetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator);

/**
 * @ingroup Car2x
 *
 * @brief
 *   Gets the 64 bits integer value of a token (Variant 2)
 */
int64 C2xGetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder);

}
