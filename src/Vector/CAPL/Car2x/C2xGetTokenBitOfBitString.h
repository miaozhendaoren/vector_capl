#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Gets the value of a bit in a bit string.
 */
long C2xGetTokenBitOfBitString(long packet, char * protocolDesignatorl, char * tokenDesignator, char * namedBit);

/**
 * @ingroup Car2x
 *
 * @brief
 *   Gets the value of a bit in a bit string.
 */
long C2xGetTokenBitOfBitString(long packet, char * protocolDesignatorl, char * tokenDesignator, long bitPosition);

}
