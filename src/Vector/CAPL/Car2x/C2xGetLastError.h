#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the last error code
 *
 * Returns the error code of the last called C2x... function.
 *
 * @return
 *   error code
 */
long C2xGetLastError(void);

}
