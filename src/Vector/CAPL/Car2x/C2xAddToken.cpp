#include "C2xAddToken.h"

#include <iostream>

namespace capl
{

long C2xAddToken(long packet, char * protocolDesignator, char * tokenDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
