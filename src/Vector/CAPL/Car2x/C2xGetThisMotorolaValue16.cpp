#include "C2xGetThisMotorolaValue16.h"

#include <iostream>

namespace capl
{

long C2xGetThisMotorolaValue16(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
