#include "C2xGetLastError.h"

#include <iostream>

namespace capl
{

long C2xGetLastError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
