#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the fill color of a map object.
 */
long C2xSetMapObjectFillColor(long handle, double fillColor);

}
