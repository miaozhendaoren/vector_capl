#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the pen width of a map object.
 */
long C2xSetMapObjectPenWidth(long handle, double penWidth);

}
