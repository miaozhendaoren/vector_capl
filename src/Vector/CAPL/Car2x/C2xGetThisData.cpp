#include "C2xGetThisData.h"

#include <iostream>

namespace capl
{

long C2xGetThisData(long offset, long bufferSize, byte * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetThisData(long offset, long bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetThisData(long offset, long bufferSize, void * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
