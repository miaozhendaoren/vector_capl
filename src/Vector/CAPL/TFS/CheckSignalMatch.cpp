#include "CheckSignalMatch.h"

#include <iostream>

namespace capl
{

long CheckSignalMatch(dbSignal & aSignal, double aCompareValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long CheckSignalMatch(dbEnvVar & aEnvVar, double aCompareValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long CheckSignalMatch(sysvar & aSysVar, double aCompareValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
