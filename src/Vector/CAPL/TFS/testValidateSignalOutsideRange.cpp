#include "testValidateSignalOutsideRange.h"

#include <iostream>

namespace capl
{

long testValidateSignalOutsideRange(char * aTestStep, dbSignal & aSignal, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long testValidateSignalOutsideRange(char * aTestStep, envvar & EnvVarName, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long testValidateSignalOutsideRange(char * aTestStep, sysvar & aSysVar, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
