#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"
#include "../General/envvar.h"
#include "../System_Variables/sysvar.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * Checks the signal value, the environment variable value or the system variable value against the condition:
 * - Value < aLowLimit
 * or
 * - Value > aHighLimit
 * The test step is evaluated as passed or failed depending on the results.
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param aSignal
 *   The signal to be polled
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalOutsideRange(char * aTestStep, dbSig & aSignal, char * aTxNode, double aLowLimit, double aHighLimit);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * Checks the signal value, the environment variable value or the system variable value against the condition:
 * - Value < aLowLimit
 * or
 * - Value > aHighLimit
 * The test step is evaluated as passed or failed depending on the results.
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param EnvVarName
 *   Environment variable to be queried
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalOutsideRange(char * aTestStep, envvar & EnvVarName, char * aTxNode, double aLowLimit, double aHighLimit);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * Checks the signal value, the environment variable value or the system variable value against the condition:
 * - Value < aLowLimit
 * or
 * - Value > aHighLimit
 * The test step is evaluated as passed or failed depending on the results.
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param aSysVar
 *   System Variable to be queried
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalOutsideRange(char * aTestStep, sysvar & aSysVar, char * aTxNode, double aLowLimit, double aHighLimit);

}
