#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Registers the given callback as a 'signal driver' for the signal. (Form 1)
 *
 * Registers the given callback as a 'signal driver' for the signal.
 *
 * @param aSignal
 *   DB signal to be queried.
 *
 * @param aCallbackFunction
 *   Name of a callback function that should be defined as follows: void function(double value).
 *
 * @return
 *   - 1: Correct functionality
 *   - 0: CAPL 'signal driver' could not be registered
 */
long RegisterSignalDriver(dbSignal & aSignal, char * aCallbackFunction);

}
