#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * @deprecated
 *   This function replaces the function testValidateSignalInRangeGM.
 *   Version 7.1: Replaced by testValidateSignalInRange.
 *
 * Checks the signal value against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * The test step is then evaluated as passed or failed, depending on the results
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param aSignal
 *   The signal to be polled
 *
 * @param aTxNode
 *   Send node of the message whose signal should be polled
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalInRangeByTxNode(char * aTestStep, char * aSignal, char * aTxNode, double aLowLimit, double aHighLimit);

}
