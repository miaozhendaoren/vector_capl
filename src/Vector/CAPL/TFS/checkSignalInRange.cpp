#include "checkSignalInRange.h"

#include <iostream>

namespace capl
{

long checkSignalInRange(dbSig & aSignal, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long checkSignalInRange(envvar & EnvVarName, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long checkSignalInRange(sysvar & aSysVar, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
