#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Indicates the period of time for which the last wait function executed had to wait until being triggered.
 *
 * Indicates the period of time for which the last wait function executed had to wait until being triggered.
 *
 * @return
 *   - Last wait duration (see above)
 *   - -1.0 for call without a previous call to a wait function.
 */
double TestGetLastWaitElapsedTimeNS(void);

}
