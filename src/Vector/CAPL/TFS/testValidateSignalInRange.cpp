#include "testValidateSignalInRange.h"

#include <iostream>

namespace capl
{

long testValidateSignalInRange(char * aTestStep, dbSignal & aSignal, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long testValidateSignalInRange(char * aTestStep, envvar & EnvVarName, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long testValidateSignalInRange(char * aTestStep, sysvar & aSysVar, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
