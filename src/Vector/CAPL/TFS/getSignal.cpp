#include "getSignal.h"

#include <iostream>

namespace capl
{

double getSignal(dbSignal & aSignal)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

double getSignal(char * signalName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
