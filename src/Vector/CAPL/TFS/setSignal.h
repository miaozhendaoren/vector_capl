#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Sets the transmitted signal to the accompanying value. (Form 1)
 *
 * Sets the transmitted signal to the accompanying value.
 *
 * If no suitable signal driver exists and thus no signal can be stimulated, then in the test
 * module the verdict of the test module is set to "fail". In the simulation module the
 * measurement is stopped and an error message is displayed.
 *
 * @param aSignal
 *   Signal to be set.
 *
 * @param aValue
 *   Physical value to be accepted.
 */
void setSignal(dbSignal & aSignal, double aValue);

/**
 * @ingroup TFS
 *
 * @brief
 *   Sets the transmitted signal to the accompanying value. (Form 2)
 *
 * Sets the transmitted signal to the accompanying value.
 *
 * If no suitable signal driver exists and thus no signal can be stimulated, then in the test
 * module the verdict of the test module is set to "fail". In the simulation module the
 * measurement is stopped and an error message is displayed.
 *
 * @param aValue
 *   Physical value to be accepted.
 *
 * @param signalName
 *   Name of the signal. May be qualified with channel, database name, node name and
 *   message name (see example).
 */
void setSignal(char * signalName, double aValue);

}
