#include "TestWaitForSignals.h"

#include <iostream>

namespace capl
{

long TestWaitForSignals(dbNode & aNode, dword aTimeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
