#include "setSignal.h"

#include <iostream>

namespace capl
{

void setSignal(dbSignal & aSignal, double aValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void setSignal(char * signalName, double aValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
