#include "TestGetLastWaitElapsedTimeNS.h"

#include <iostream>

namespace capl
{

double TestGetLastWaitElapsedTimeNS(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
