#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"
#include "../General/envvar.h"
#include "../System_Variables/sysvar.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks a given value against the value of the signal.
 *
 * Checks the given value against the value of signal. The resolution of the signal is
 * considered.
 *
 * @param aSignal
 *   The signal to be polled.
 *
 * @param aCompareValue
 *   Value which is compared to the signal value.
 *
 * @return
 *   -  1: If the condition is TRUE
 *   -  0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 *   - -1: General error
 */
long CheckSignalMatch(dbSignal & aSignal, double aCompareValue);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks a given value against the value of the signal.
 *
 * Checks the given value against the value of signal. The resolution of the signal is
 * considered.
 *
 * @param aEnvVar
 *   The environment variable to be polled.
 *
 * @param aCompareValue
 *   Value which is compared to the signal value.
 *
 * @return
 *   -  1: If the condition is TRUE
 *   -  0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 *   - -1: General error
 */
long CheckSignalMatch(dbEnvVar & aEnvVar, double aCompareValue);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks a given value against the value of the signal.
 *
 * Checks the given value against the value of signal. The resolution of the signal is
 * considered.
 *
 * @param aSysVar
 *   The system variable to be polled.
 *
 * @param aCompareValue
 *   Value which is compared to the signal value.
 *
 * @return
 *   -  1: If the condition is TRUE
 *   -  0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 *   - -1: General error
 */
long CheckSignalMatch(sysvar & aSysVar, double aCompareValue);

}
