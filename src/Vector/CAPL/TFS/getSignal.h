#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Gets the value of a signal. (Form 1)
 *
 * Gets the value of a signal.
 *
 * @param aSignal
 *   The signal to be polled.
 *
 * @return
 *   Value of the signal; 0.0 if the signal is not found (only form 2).
 */
double getSignal(dbSignal & aSignal);

/**
 * @ingroup TFS
 *
 * @brief
 *   Gets the value of a signal. (Form 2)
 *
 * Gets the value of a signal.
 *
 * @param signalName
 *   Name of the signal. May be qualified with channel, database name, node name and
 *   message name (see example).
 *
 * @return
 *   Value of the signal; 0.0 if the signal is not found (only form 2).
 */
double getSignal(char * signalName);

}
