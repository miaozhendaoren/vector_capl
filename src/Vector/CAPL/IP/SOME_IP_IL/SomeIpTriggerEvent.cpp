#include "SomeIpTriggerEvent.h"

#include <iostream>

namespace capl
{

long SomeIpTriggerEvent(dword pevHandle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
