#pragma once

/**
 * @ingroup IP
 * @{
 * @defgroup SOME_IP_IL SOME/IP Interaction Layer
 * @}
 */

/* Callback Functions */
// @todo

/* General Functions */
#include "SomeIpGetLastError.h"
#include "SomeIpGetLastErrorText.h"
#include "SomeIpSetProperty.h"

/* Endpoints */
#include "SomeIpCloseLocalApplicationEndpoint.h"
#include "SomeIpOpenLocalApplicationEndpoint.h"

/* Control API */
#include "SomeIpControlInit.h"
#include "SomeIpControlResume.h"
#include "SomeIpControlStart.h"
#include "SomeIpControlStop.h"
#include "SomeIpControlWait.h"

/* Service Discovery */
#include "SomeIpSDDesubscribeEventGroup.h"
#include "SomeIpSDReleaseService.h"
#include "SomeIpSDRequireService.h"
#include "SomeIpSDSetServiceStatus.h"
#include "SomeIpSDSubscribeEventGroup.h"

/* Low-level API */
#include "SomeIpCreateMessage.h"
#include "SomeIpOutputMessage.h"
#include "SomeIpPostMessage.h"
#include "SomeIpReleaseMessage.h"

/* Value Access */
#include "SomeIpGetDestinationAddress.h"
#include "SomeIpGetDestinationPort.h"
#include "SomeIpGetInterfaceVersion.h"
#include "SomeIpGetLength.h"
#include "SomeIpGetMessageId.h"
#include "SomeIpGetMessageType.h"
#include "SomeIpGetProtocol.h"
#include "SomeIpGetProtocolVersion.h"
#include "SomeIpGetRequestId.h"
#include "SomeIpGetReturnCode.h"
#include "SomeIpGetSourceAddress.h"
#include "SomeIpGetSourcePort.h"
#include "SomeIpGetValueDWord.h"
#include "SomeIpGetValueFloat.h"
#include "SomeIpGetValueInt64.h"
#include "SomeIpGetValueLong.h"
#include "SomeIpGetValueQWord.h"
#include "SomeIpGetValueString.h"
#include "SomeIpSetRequestId.h"
#include "SomeIpSetReturnCode.h"
#include "SomeIpSetValueDWord.h"
#include "SomeIpSetValueFloat.h"
#include "SomeIpSetValueInt64.h"
#include "SomeIpSetValueLong.h"
#include "SomeIpSetValueQWord.h"
#include "SomeIpSetValueString.h"

/* Raw Data Access */
#include "SomeIpGetData.h"
#include "SomeIpSetData.h"

/* Client-Side API */
#include "SomeIpAddConsumedEventGroup.h"
#include "SomeIpCallMethod.h"
#include "SomeIpCreateConsumedServiceInstance.h"
#include "SomeIpCreateEventConsumer.h"
#include "SomeIpCreateMethodCall.h"
#include "SomeIpReleaseConsumedServiceInstance.h"
#include "SomeIpRemoveConsumedEventGroup.h"
#include "SomeIpRemoveEventConsumer.h"
#include "SomeIpRemoveMethodCall.h"

/* Server-Side API */
#include "SomeIpAddEvent.h"
#include "SomeIpAddEventToEventgroup.h"
#include "SomeIpAddMethod.h"
#include "SomeIpAddProvidedEventGroup.h"
#include "SomeIpCreateProvidedServiceInstance.h"
#include "SomeIpReleaseProvidedServiceInstance.h"
#include "SomeIpRemoveEvent.h"
#include "SomeIpRemoveEventFromEventgroup.h"
#include "SomeIpRemoveMethod.h"
#include "SomeIpRemoveProvidedEventGroup.h"
#include "SomeIpTriggerEvent.h"
