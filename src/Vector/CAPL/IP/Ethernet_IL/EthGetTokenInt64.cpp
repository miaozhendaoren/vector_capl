#include "EthGetTokenInt64.h"

#include <iostream>

namespace capl
{

int64 EthGetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

int64 EthGetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
