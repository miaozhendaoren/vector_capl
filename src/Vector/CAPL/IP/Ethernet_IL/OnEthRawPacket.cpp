#include "OnEthRawPacket.h"

#include <iostream>

namespace capl
{

void OnEthRawPacket(long channel, long dir, long packetLength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
