#include "EthOutputPacket.h"

#include <iostream>

namespace capl
{

long EthOutputPacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
