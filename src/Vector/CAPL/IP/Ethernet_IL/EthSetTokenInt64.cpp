#include "EthSetTokenInt64.h"

#include <iostream>

namespace capl
{

long EthSetTokenInt64(long packet, char * protocolDesignatorl, char * tokenDesignator, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthSetTokenInt64(long packet, char * protocolDesignatorl, char * tokenDesignator, long byteOffset, long length, long networkByteOrder, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
