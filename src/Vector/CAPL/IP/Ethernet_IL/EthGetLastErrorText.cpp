#include "EthGetLastErrorText.h"

#include <iostream>

namespace capl
{

long EthGetLastErrorText(dword bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
