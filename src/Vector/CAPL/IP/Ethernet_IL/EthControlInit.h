#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Initializes the Ethernet Interaction Layer.
 *
 * This function initializes the Ethernet Interaction Layer.
 *
 * The function call is only possible in on prestart.
 * Then the Ethernet Interaction Layer has to be started with EthControlStart.
 * If it is not called, the Ethernet Interaction Layer automatically starts with the properties defined in the node attributes.
 *
 * @return
 *   0 or error code
 */
long EthControlInit();

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Initializes the Ethernet Interaction Layer.
 *
 * This function initializes the Ethernet Interaction Layer.
 *
 * The function call is only possible in on prestart.
 * Then the Ethernet Interaction Layer has to be started with EthControlStart.
 * If it is not called, the Ethernet Interaction Layer automatically starts with the properties defined in the node attributes.
 *
 * @param protocolDesignator
 *   protocol, which should be used with the IL - use IPv4 address (other values are not supported yet)
 *
 * @return
 *   0 or error code
 */
long EthControlInit(char * protocolDesignator);

}
