#include "EthGetMacId.h"

#include <iostream>

namespace capl
{

long EthGetMacId(byte * macId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
