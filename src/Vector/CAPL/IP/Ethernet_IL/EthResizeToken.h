#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Modifies the length of a token.
 *
 * The function sets the length of a token.
 *
 * In case the token length is increased by full bytes, the additional bytes are initialized with 0.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param newBitLength
 *   new length of the token in bits
 *
 * @return
 *   0 or error code
 */
long EthResizeToken(long packet, char * protocolDesignator, char * tokenDesignator, long newBitLength);

}
