#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sets the 64 bit integer value of a token. (form 1)
 *
 * The function sets the integer value of a token.
 *
 * Variant (2) with byte offset sets a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param value
 *   new integer value
 *
 * @return
 *   0 or error code
 */
long EthSetTokenInt64(long packet, char * protocolDesignatorl, char * tokenDesignator, int64 value);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sets the 64 bit integer value of a token. (form 2)
 *
 * The function sets the integer value of a token.
 *
 * Variant (2) with byte offset sets a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   length of the integer value, up to 8 byte
 *
 * @param networkByteOrder
 *   - 0 = INTEL (little-endian)
 *   - 1 = MOTOROLA (big-endian)
 *
 * @param value
 *   new integer value
 *
 * @return
 *   0 or error code
 */
long EthSetTokenInt64(long packet, char * protocolDesignatorl, char * tokenDesignator, long byteOffset, long length, long networkByteOrder, int64 value);

}
