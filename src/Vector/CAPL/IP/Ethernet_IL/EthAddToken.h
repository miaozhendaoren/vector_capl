#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Adds a token at the end of a protocol header.
 *
 * The function adds an additional token at the end of a protocol header or at a specific position (for details see protocol help).
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "dhcpv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "option1"
 *
 * @return
 *   0 or error code
 */
long EthAddToken(long packet, char * protocolDesignator, char * tokenDesignator);

}
