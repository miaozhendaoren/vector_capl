#include "EthSetTokenString.h"

#include <iostream>

namespace capl
{

long EthSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
