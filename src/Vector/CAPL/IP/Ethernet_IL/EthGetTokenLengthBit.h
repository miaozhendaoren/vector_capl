#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the token length in bit.
 *
 * The function returns the length of a token in bit.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @return
 *   length of the token in bit.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenLengthBit(long packet, char * protocolDesignator, char * tokenDesignator);

}
