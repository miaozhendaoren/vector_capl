#include "EthGetThisMotorolaValue64.h"

#include <iostream>

namespace capl
{

long EthGetThisMotorolaValue64(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
