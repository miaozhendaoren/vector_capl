#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Registers a CAPL handler function to receive raw Ethernet packets.
 *
 * Use this function to register a CAPL function to receive Ethernet packets.
 * The callback function is called, if a packet with the specified MAC addresses and Ethernet type is received.
 * Use the flags to ignore the Ethernet type or the MAC address, e.g. flag = 7 receives all packets.
 *
 * The callback must have the following signature:
 *
 * void OnEthRawPacket(long channel, long dir, long packetLength);
 *
 * @param flags
 *   - Bit 1 = ignore Source MAC address
 *   - Bit 2 = ignore Destination MAC address
 *   - Bit 3 = ignore Ethernet type
 *
 * @param srcMacId
 *   source MAC address
 *
 * @param dstMacId
 *   destination MAC address
 *
 * @param ethernetType
 *   Ethernet type (16 Bit)
 *
 * @param callback
 *   CAPL callback function name
 *
 * @return
 *   0 or error code
 */
long EthReceiveRawPacket(long flags, byte srcMacId[6], byte dstMacId[6], long ethernetType, char * callback);

}
