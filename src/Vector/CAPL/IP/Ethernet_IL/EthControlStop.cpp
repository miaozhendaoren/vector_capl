#include "EthControlStop.h"

#include <iostream>

namespace capl
{

long EthControlStop()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
