#include "EthGetThisMotorolaValue32.h"

#include <iostream>

namespace capl
{

long EthGetThisMotorolaValue32(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
