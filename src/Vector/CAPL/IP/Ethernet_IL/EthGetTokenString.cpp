#include "EthGetTokenString.h"

#include <iostream>

namespace capl
{

long EthGetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
