#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets received 8 bit value.
 *
 * This function reads the data of a received packet.
 *
 * It is only usable in a CAPL callback function that had been registered with EthReceiveRawPacket or EthReceivePacket. It depends on the callback function, which part of the packet data is accessed by the function:
 *
 *   - Callback of EthReceiveRawPacket:
 *     The function handles the raw Ethernet packet data.
 *     The offset 0 is the beginning of the Ethernet packet.
 *   - Callback of EthReceivePacket:
 *     The function handles the payload data of the highest interpretable protocol, i.e. in an UDP packet the functions handles the payload data of the UDP protocol.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @return
 *   read value
 */
long EthGetThisValue8(long offset);

}
