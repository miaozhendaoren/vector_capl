#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the integer value of a token. (form 1)
 *
 * The function requests the integer value of a token.
 *
 * Variant (2) with byte offset returns a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @return
 *   integer value of the token or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the integer value of a token. (form 2)
 *
 * The function requests the integer value of a token.
 *
 * Variant (2) with byte offset returns a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   length of the integer value, must be 1, 2, 3 or 4 byte
 *
 * @param networkByteOrder
 *   - 0 = INTEL (little-endian)
 *   - 1 = MOTOROLA (big-endian)
 *
 * @return
 *   integer value of the token or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder);

}
