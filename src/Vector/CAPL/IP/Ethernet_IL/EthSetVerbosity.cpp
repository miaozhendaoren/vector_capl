#include "EthSetVerbosity.h"

#include <iostream>

namespace capl
{

long EthSetVerbosity(long verbosity)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
