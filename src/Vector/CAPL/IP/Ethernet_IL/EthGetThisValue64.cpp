#include "EthGetThisValue64.h"

#include <iostream>

namespace capl
{

long EthGetThisValue64(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
