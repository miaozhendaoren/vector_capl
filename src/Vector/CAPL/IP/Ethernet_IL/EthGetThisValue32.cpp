#include "EthGetThisValue32.h"

#include <iostream>

namespace capl
{

long EthGetThisValue32(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
