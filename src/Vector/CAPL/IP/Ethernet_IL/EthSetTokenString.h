#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sets the string value of a token. (form 1)
 *
 * The function copies the string value to the token without terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "udp"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param data
 *   data as string
 *
 * @return
 *   0 or error code
 */
long EthSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, char * data);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sets the string value of a token. (form 2)
 *
 * The function copies the string value to the token without terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "udp"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param data
 *   data as string
 *
 * @return
 *   0 or error code
 */
long EthSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, char * data);


}
