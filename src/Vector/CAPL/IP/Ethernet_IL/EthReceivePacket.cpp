#include "EthReceivePacket.h"

#include <iostream>

namespace capl
{

long EthReceivePacket(char * onPacketCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
