#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sends a raw Ethernet packet.
 *
 * The function sends a raw Ethernet packet.
 * The packet data must contain the destination (byte 1..6) and source (byte 7..11) MAC address and the Ethernet type (byte 12..13 in Motorola format).
 * After that the user data are contained.
 *
 * @param packetLength
 *   size of the Ethernet packet in byte (see Packet Length)
 *
 * @param packetData
 *   packet data as byte or char array including destination and source MAC address, Ethernet type and user data (without checksum)
 *
 * @param packetDataStruct
 *   CAPL structure containing the packet data
 *
 * @return
 *   0 or error code
 */
long EthOutputRawPacket(long packetLength, char packetData);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sends a raw Ethernet packet.
 *
 * The function sends a raw Ethernet packet.
 * The packet data must contain the destination (byte 1..6) and source (byte 7..11) MAC address and the Ethernet type (byte 12..13 in Motorola format).
 * After that the user data are contained.
 *
 * @param packetLength
 *   size of the Ethernet packet in byte (see Packet Length)
 *
 * @param packetData
 *   packet data as byte or char array including destination and source MAC address, Ethernet type and user data (without checksum)
 *
 * @param packetDataStruct
 *   CAPL structure containing the packet data
 *
 * @return
 *   0 or error code
 */
long EthOutputRawPacket(long packetLength, byte packetData);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sends a raw Ethernet packet.
 *
 * The function sends a raw Ethernet packet.
 * The packet data must contain the destination (byte 1..6) and source (byte 7..11) MAC address and the Ethernet type (byte 12..13 in Motorola format).
 * After that the user data are contained.
 *
 * @param packetLength
 *   size of the Ethernet packet in byte (see Packet Length)
 *
 * @param packetData
 *   packet data as byte or char array including destination and source MAC address, Ethernet type and user data (without checksum)
 *
 * @param packetDataStruct
 *   CAPL structure containing the packet data
 *
 * @return
 *   0 or error code
 */
long EthOutputRawPacket(long packetLength, void * packetDataStruct);

}
