#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Checks if an Ethernet packet has protocol errors.
 *
 * The function checks, if a received packet has a protocol error.
 * Packets with a protocol error are marked with an error icon in the trace window.
 *
 * @param packet
 *   handle of a packet that should be checked
 *
 * @return
 *   - 0 - packet is valid
 *   - !0 - packet has protocol errors
 */
long EthIsPacketValid(long packet);

}
