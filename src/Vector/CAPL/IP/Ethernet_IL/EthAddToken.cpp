#include "EthAddToken.h"

#include <iostream>

namespace capl
{

long EthAddToken(long packet, char * protocolDesignator, char * tokenDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
