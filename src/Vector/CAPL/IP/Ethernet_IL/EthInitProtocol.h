#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Initializes a protocol for an Ethernet packet. (form 1)
 *
 * The function initializes the protocol for a packet.
 * If necessary further needed lower protocols are initialized, e.g. IPv4.
 * Already initialized higher protocols are deleted.
 *
 * Protocol fields that are marked as InitProtocol in the protocol overview are initialized.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, taken from the protocol overview
 *
 * @return
 *   0 or error code
 */
long EthInitProtocol(long packet, char * protocolDesignator);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Initializes a protocol for an Ethernet packet. (form 2)
 *
 * The function initializes the protocol for a packet.
 * If necessary further needed lower protocols are initialized, e.g. IPv4.
 * Already initialized higher protocols are deleted.
 *
 * Protocol fields that are marked as InitProtocol in the protocol overview are initialized.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, taken from the protocol overview
 *
 * @param packetTypeDesignator
 *   type of the packet, taken from the protocol overview
 *
 * @return
 *   0 or error code
 */
long EthInitProtocol(long packet, char * protocolDesignator, char * packetTypeDesignator);

}
