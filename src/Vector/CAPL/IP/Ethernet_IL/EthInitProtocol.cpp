#include "EthInitProtocol.h"

#include <iostream>

namespace capl
{

long EthInitProtocol(long packet, char * protocolDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitProtocol(long packet, char * protocolDesignator, char * packetTypeDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
