#include "EthCompletePacket.h"

#include <iostream>

namespace capl
{

long EthCompletePacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
