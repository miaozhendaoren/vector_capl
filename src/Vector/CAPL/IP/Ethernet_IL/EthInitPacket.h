#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 1)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket();

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 2)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param protocolDesignator
 *   designator of the protocol, which should be used for initialization
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket(char * protocolDesignator);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 3)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param protocolDesignator
 *   designator of the protocol, which should be used for initialization
 *
 * @param packetTypeDesignator
 *   designator of the packet type
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket(char * protocolDesignator, char * packetTypeDesignator);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 4)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param srcMacIdStruct
 *   source MAC address as struct
 *
 * @param dstMacIdStruct
 *   destination MAC address as struct
 *
 * @param ethernetType
 *   Ethernet type (16 Bit)
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket(void * srcMacIdStruct, void * dstMacIdStruct, long ethernetType);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 5)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param srcMacId
 *   source MAC address
 *
 * @param dstMacId
 *   destination MAC address
 *
 * @param ethernetType
 *   Ethernet type (16 Bit)
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket(byte srcMacId[6], byte dstMacId[6], long ethernetType);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 6)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param packetToCopy
 *   handle of a packet which was created with EthInitPacket before or handle of a packet which has been received within a callback function (OnEthPacket).
 *   The header and the data of this packet are copied to the new created packet.
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket(long packetToCopy);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Creates a new Ethernet packet. (form 7)
 *
 * This function creates a new Ethernet packet.
 * Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawData
 *   raw data of an Ethernet packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthInitPacket(long rawDataLength, byte * rawData);

}
