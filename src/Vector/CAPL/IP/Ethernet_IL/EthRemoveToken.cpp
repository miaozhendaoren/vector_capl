#include "EthRemoveToken.h"

#include <iostream>

namespace capl
{

long EthRemoveToken(long packet, char * protocolDesignator, char * tokenDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
