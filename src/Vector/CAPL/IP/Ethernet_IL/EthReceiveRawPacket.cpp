#include "EthReceiveRawPacket.h"

#include <iostream>

namespace capl
{

long EthReceiveRawPacket(long flags, byte srcMacId[6], byte dstMacId[6], long ethernetType, char * callback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
