#include "FRSSoftwareTrigger.h"

#include <iostream>

namespace capl
{

long FRSSoftwareTrigger(int iEnable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
