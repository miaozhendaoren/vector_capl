#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *    Sets the device number for FRstress.
 *
 * Sets the device number for FRstress. All subsequent calls will be addressed to this unit.
 *
 * @param iDeviceNo
 *   Device number: 1 or 2
 */
void FRSSetDevice(int iDeviceNo);

}
