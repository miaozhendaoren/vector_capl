#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Sets the input port for the trigger condition in digital mode.
 *
 * Adds a bit sequence to the the respective trigger condition as a trigger value.
 * A complex trigger condition can be created through multiple calls.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param portMask
 *   Values: 3= both ports, 1= Port A (Channel1), 2= Port B (Channel2)
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetTrigPort (int triggerCondition, int portMask);

}
