#include "FRSActivateCRCCalc.h"

#include <iostream>

namespace capl
{

long FRSActivateCRCCalc(int index, int calculateCRC)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
