#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Orders the FRstress COM server to end the current disturbance activity of the hardware.
 *
 * Orders the FRstress COM server to end the current disturbance activity of the hardware.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSStop();

}
