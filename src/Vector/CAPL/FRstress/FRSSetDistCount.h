#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Configures the number of disturbances.
 *
 * Configures the number of disturbances.
 *
 * @param index
 *   Values: 1-4
 *
 * @param disturbanceCount
 *   Values: 0-254, -1 = infinite
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetDistCount(int index, int disturbanceCount);

}
