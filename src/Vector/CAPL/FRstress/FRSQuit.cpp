#include "FRSQuit.h"

#include <iostream>

namespace capl
{

void FRSQuit()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
