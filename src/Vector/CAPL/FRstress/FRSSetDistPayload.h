#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Adds frame Payload areas to a disturbance.
 *
 * Modifies the Payload with bit accuracy.
 * The disturbance is defined as a bit sequence.
 * A complex disturbance can be configured through multiple calls.
 * The associated trigger condition must not overlap with the disturbance.
 * If the function is called in analog mode, it has no effect.
 *
 * @param triggerCondition
 *   Values: 1...4
 *
 * @param disturbancePayloadElement
 *   Values: 0=Payload Byte, 1=BSS
 *
 * @param byteIndex
 *   Values: 0...254 start index in the payload
 *
 * @param disturbanceValue
 *   Bit sequence of the respective payload byte.
 *   The possible elements of the sequence are 0,1,x (don't care).
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetDistPayload(int triggerCondition, int disturbancePayloadElement, int byteIndex, char * disturbanceValue);

}
