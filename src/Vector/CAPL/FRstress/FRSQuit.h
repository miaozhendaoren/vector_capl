#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Ends the FRstress software.
 *
 * Ends the FRstress software.
 */
void FRSQuit();

}
