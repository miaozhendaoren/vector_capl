#include "FRSSetScopeMode.h"

#include <iostream>

namespace capl
{

long FRSSetScopeMode(double baudrate, int channel, int payloadLength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
