#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Adds frame elements (numeric input) to a bit accuracy disturbance.
 *
 * Modifies the specified frame element with bit accuracy.
 * A complex disturbance can be configured through multiple calls.
 *
 * The associated trigger condition must not overlap with the disturbance.
 *
 * If the function is called in analog mode, it has no effect.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param disturbanceElement
 *   Values:
 *   - 0: reservedBit
 *   - 1: PPI
 *   - 2: NFI
 *   - 3: SFI
 *   - 4: SUPFI
 *   - 5: FrameId
 *   - 6: PayloadLength
 *   - 7: HeaderCRC
 *   - 8: cycleCount
 *   - 9: BSS1
 *   - 10: BSS2
 *   - 11: BSS3
 *   - 12: BSS4
 *   - 13: BSS5
 *   - 14: TrailerBSS1
 *   - 15: TrailerBSS2
 *   - 16: TrailerBSS3
 *   - 17: TrailerCRCByte1
 *   - 18: TrailerCRCByte2
 *   - 19: TrailerCRCByte3
 *   - 20: FES
 *
 * @param disturbanceValue
 *   Numerical value of disturbance. e.g.: FrameId=5
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetFrmDistElem(int triggerCondition, int disturbanceElement, int disturbanceValue);

}
