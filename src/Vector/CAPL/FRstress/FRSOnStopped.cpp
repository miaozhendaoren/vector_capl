#include "FRSOnStopped.h"

#include <iostream>

namespace capl
{

long FRSOnStopped(char * CallbackName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
