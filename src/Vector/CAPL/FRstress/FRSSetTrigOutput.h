#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Configures the trigger output.
 *
 * Activates the trigger output of FRsstress.
 * The trigger output can react to individual trigger lowerings or to a combination of sources.
 *
 * @param triggermask
 *   Values:
 *   - 1: Trigger1
 *   - 2: Trigger2
 *   - 4: Trigger3
 *   - 8: Trigger4
 *   - 16: SW Trigger
 *   - 32: External Trigger
 *
 * @param triggerlevel
 *   Values: 0=Low, 1=High
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetTrigOutput(int triggermask, int triggerlevel);

}
