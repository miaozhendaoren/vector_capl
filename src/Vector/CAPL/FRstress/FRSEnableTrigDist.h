#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Activates a trigger/disturbance page of FRstress.
 *
 * Activates a trigger/disturbance page of FRstress.
 * This function can be called out of a measurement session.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param iEnable
 *   - Enable = 1
 *   - Disable = 0
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSEnableTrigDist(int triggerCondition, int iEnable);

}
