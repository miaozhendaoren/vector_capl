#include "FRSStart.h"

#include <iostream>

namespace capl
{

long FRSStart()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
