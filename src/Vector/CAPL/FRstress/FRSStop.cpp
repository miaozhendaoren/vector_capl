#include "FRSStop.h"

#include <iostream>

namespace capl
{

long FRSStop()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
