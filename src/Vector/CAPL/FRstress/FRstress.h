#pragma once

/**
 * @defgroup FRstress FRstress CAPL Functions
 */

/* Event Procedures */
#include "FRSOnFinished.h"
#include "FRSOnStopped.h"

/* Control */
#include "FRSEnableTrigDist.h"
#include "FRSInit.h"
#include "FRSQuit.h"
#include "FRSStart.h"
#include "FRSStop.h"
#include "FRSSoftwareTrigger.h"

/* General Configuration */
#include "FRSClear.h"
#include "FRSGetDevice.h"
#include "FRSOpen.h"
#include "FRSSetDevice.h"

/* Operating Mode Configuration */
#include "FRSSetAnalogMode.h"
#include "FRSSetDigitalMode.h"
#include "FRSSetScopeMode.h"

/* Trigger Configuration */
#include "FRSSetTrig.h"
#include "FRSSetTrigElem.h"
#include "FRSSetTrigPayload.h"
#include "FRSSetTrigPort.h"

/* Disturbance Configuration */
#include "FRSActivateCRCCalc.h"
#include "FRSSetBitstreamDist.h"
#include "FRSSetDistCount.h"
#include "FRSSetDistPayload.h"
#include "FRSSetFrmDist.h"
#include "FRSSetFrmDistElem.h"

/* Trigger Output Configuration */
#include "FRSSetTrigOutput.h"

/* Obsolete Functions */
#include "FRSSetDistMode.h"
