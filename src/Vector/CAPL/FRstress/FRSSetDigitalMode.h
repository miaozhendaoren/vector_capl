#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Activates the digital disturbance mode.
 *
 * Activates the digital disturbance mode of FRstress.
 * Configuration functions that belong to another operating mode are ignored.
 *
 * @param baudrate
 *   Values: 10, 5, 2.5, 1.25 Mbit/s
 *
 * @param channel
 *   Values: 1=A, 2=B
 *
 * @param payloadLength
 *   Values: 0-254 bytes
 *
 * @param macrotickLengthUs
 *   Values: 1-6 us
 *
 * @param TSSLengthBit
 *   Values3-15 Bit
 *
 * @param cycleLengthUs
 *   Values: 26 – 16000 us
 *
 * @param numberStaticSlots
 *   Values: 2-1023
 *
 * @param actionPointOffsetMT
 *   Values: 1-63 Macrotick
 *
 * @param staticSlotLengthMT
 *   Values: 4 – 661 Macrotick
 *
 * @param TSSExtension
 *   Values: 0-14 Bit
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetDigitalMode(double baudrate, int channel, int payloadLength, double macrotickLengthUs, int TSSLengthBit, int cycleLengthUs, int numberStaticSlots, int actionPointOffsetMT, int staticSlotLengthMT, int TSSExtension);

}
