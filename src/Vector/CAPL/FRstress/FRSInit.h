#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Initializes FRstress and starts the COM server.
 *
 * Initializes FRstress and starts the COM server.
 * The function must be called once in on prestart.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSInit();

}
