#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Configures a bit sequence as disturbance.
 *
 * Generates a bit sequence that is applied to the bus as a disturbance after the trigger is detected.
 * In addition, a delay between the trigger and disturbance can be set.
 * The autoincrement value increases the delay by the specified amount at each additional trigger.
 *
 * @param triggerCondition
 *   Values: 1...4
 *
 * @param disturbanceSequence
 *   Bit sequence of length disturbanceLength.
 *   Possible value in the sequence: 0,1,u (undisturbed).
 *
 * @param disturbanceLength
 *   Number of bits to be disturbed.
 *
 * @param delayAfterTrigger
 *   Values: 0...16777 bit times.
 *
 * @param autoincrement
 *   Values: 0...4095 bit times.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetBitstreamDist(int triggerCondition, char * disturbanceSequence, int disturbanceLength, int delayAfterTrigger, int autoincrement);

}
