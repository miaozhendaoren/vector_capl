#include "FRSGetDevice.h"

#include <iostream>

namespace capl
{

long FRSGetDevice()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
