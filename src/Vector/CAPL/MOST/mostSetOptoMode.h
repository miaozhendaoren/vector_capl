#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the operation mode of the Optolyzer to the specified value.
 *
 * @deprecated
 *   No replacement
 *
 * Sets the operation mode of the Optolyzer to the specified value.
 *
 * @param channel
 *   Channel of the connected interface
 *
 * @param optoMode
 *   - 1: Spy Mode
 *   - 4: Slave Mode
 *   - 6: Master Mode
 *
 * @return
 *   - <0: See error codes
 */
long mostSetOptoMode(long channel, long optoMode);

}
