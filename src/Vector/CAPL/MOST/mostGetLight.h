#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Gets the Ring state.
 *
 * @deprecated
 *   Please use mostGetRxLight instead.
 *
 * Get the Ring state.
 *
 * @param channel
 *   Channel of the interface to be requested or to be accessed
 *
 * @return
 *   - 1: light on
 *   - 0: light out
 *   - <0: See error codes
 */
long mostGetLight(long channel);

}
