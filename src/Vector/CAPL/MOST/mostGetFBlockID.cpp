#include "mostGetFBlockID.h"

#include <iostream>

namespace capl
{

long mostGetFBlockID(mostMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long mostGetFBlockID(mostAMSMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
