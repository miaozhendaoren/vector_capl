#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the FBlockID of the message.
 *
 * @deprecated
 *   The following functions are obsolete!
 *   Please use the mostMessage selector FBlockID instead.
 *
 * Set the FBlockID of the message.
 *
 * @param msg
 *   Variable of type mostMessage.
 *
 * @param value
 *   Value to be set for FBlockID.
 */
void mostSetFBlockID(mostMessage msg, long value);

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the FBlockID of the message.
 *
 * @deprecated
 *   The following functions are obsolete!
 *   Please use the mostAMSMessage selector FBlockID instead.
 *
 * Set the FBlockID of the message.
 *
 * @param msg
 *   Variable of type mostAMSMessage.
 *
 * @param value
 *   Value to be set for FBlockID.
 */
void mostSetFBlockID(mostAMSMessage msg, long value);

}
