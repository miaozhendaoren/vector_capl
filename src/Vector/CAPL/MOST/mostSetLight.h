#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the Ring state.
 *
 * @deprecated
 *   Please use mostSetTxLight instead.
 *
 * Set the Ring state.
 *
 * @param channel
 *   Channel of the interface to be requested or to be accessed
 *
 * @param mode
 *   ?
 *
 * @return
 *   - 1: light on
 *   - 0: light out
 *   - <0: See error codes
 */
long mostSetLight(long channel, long mode);

}
