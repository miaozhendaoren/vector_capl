#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the FunctionID of the message.
 *
 * @deprecated
 *   Please use the mostMessage selector FunctionID instead.
 *
 * These functions return the FunctionID of the message.
 *
 * @param msg
 *   Message variable of type mostMessage
 *
 * @return
 *   FunctionId
 */
long mostGetFunctionID(mostMessage msg);

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the FunctionID of the message.
 *
 * @deprecated
 *   Please use the mostAMSMessage selector FunctionID instead.
 *
 * These functions return the FunctionID of the message.
 *
 * @param msg
 *   Message variable of type mostAMSMessage
 *
 * @return
 *   FunctionId
 */
long mostGetFunctionID(mostAMSMessage msg);

}
