#include "mostSetFunctionID.h"

#include <iostream>

namespace capl
{

void mostSetFunctionID(mostMessage msg, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void mostSetFunctionID(mostAMSMessage msg, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
