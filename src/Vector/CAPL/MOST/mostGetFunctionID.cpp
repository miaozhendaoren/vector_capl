#include "mostGetFunctionID.h"

#include <iostream>

namespace capl
{

long mostGetFunctionID(mostMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long mostGetFunctionID(mostAMSMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
