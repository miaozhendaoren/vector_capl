#include "mostGetOpType.h"

#include <iostream>

namespace capl
{

long mostGetOpType(mostMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long mostGetOpType(mostAMSMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
