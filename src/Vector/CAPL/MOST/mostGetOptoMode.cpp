#include "mostGetOptoMode.h"

#include <iostream>

namespace capl
{

long mostGetOptoMode(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
