#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the OpType of the message.
 *
 * @deprecated
 *   Please use the mostMessage selector OpType instead.
 *
 * These functions return the OpType of the message.
 *
 * @param msg
 *   Message variable of type mostMessage
 *
 * @return
 *   OpType
 */
long mostGetOpType(mostMessage msg);

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the OpType of the message.
 *
 * @deprecated
 *   Please use the mostAMSMessage selector OpType instead.
 *
 * These functions return the OpType of the message.
 *
 * @param msg
 *   Message variable of type mostAMSMessage
 *
 * @return
 *   OpType
 */
long mostGetOpType(mostAMSMessage msg);

}
