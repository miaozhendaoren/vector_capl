#include "mostSetFBlockID.h"

#include <iostream>

namespace capl
{

void mostSetFBlockID(mostMessage msg, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void mostSetFBlockID(mostAMSMessage msg, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
