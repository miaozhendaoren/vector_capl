#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the Continuous disturbance (time limited) mode.
 *
 * Sets the Continuous disturbance (time limited) mode.
 * If the disturbance has not previously been terminated by means of an explicit command such as CANstressStop,
 * the end of the test module or the end of the measurement, it will prevail for the period of time set in duration.
 *
 * @param duration
 *   Defines the maximum length of the continuous disturbance. The length is specified in ms and must be greater than 0.
 *
 * @param type
 *   Defines the type of continuous disturbance.
 *   The disturbance can be associated with dominant or recessive bits or it can be an analog disturbance.
 *   2 indicates a dominant disturbance,
 *   3 a recessive disturbance and
 *   4 an analog disturbance (only in conjunction with CANstress DR).
 */
void CANstressSetContinuousDisturbanceTimeLimited(dword duration, dword type);

}
