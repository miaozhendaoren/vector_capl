#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Ends the CANstress software.
 *
 * Ends the CANstress software.
 * All following CANstress CAPL functions cause error messages (if previously a CANstress server was not created a new).
 */
dword CANstressQuit(void);

}
