#include "CANstressIsFinished.h"

#include <iostream>

namespace capl
{

long CANstressIsFinished(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
