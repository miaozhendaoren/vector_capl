#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the Continuous disturbance (until stop) mode.
 *
 * Sets the Continuous disturbance (until stop) mode.
 * Once the trigger has been activated, the continuous disturbance will prevail until the CANstress device is deactivated
 * (e.g. by means of the CANstressStop function) and/or the measurement or test module is terminated.
 *
 * @param type
 *   Defines the type of continuous disturbance.
 *   The disturbance can be associated with dominant or recessive bits or it can be an analog disturbance.
 *   2 indicates a dominant disturbance,
 *   3 a recessive disturbance and
 *   4 an analog disturbance (only in conjunction with CANstress DR).
 */
void CANstressSetContinuousDisturbanceUntilStop(dword type);

}
