#include "CANstressWaitForPending.h"

#include <iostream>

namespace capl
{

long CANstressWaitForPending(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
