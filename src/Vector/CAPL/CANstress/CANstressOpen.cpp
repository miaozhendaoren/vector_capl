#include "CANstressOpen.h"

#include <iostream>

namespace capl
{

void CANstressOpen(char * fileName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
