#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Delivers information about CANstress software and the connected CAN hardware.
 *
 * Delivers information about CANstress software and the connected CAN hardware.
 *
 * @param softwareVersion
 *   Buffer in which the version of the software is written (recommended buffer size: 20 Byte).
 *
 * @param swVersionBufLen
 *   Size of the buffer in which the software version is written (in bytes).
 *
 * @param firmwareVersion
 *   Buffer in which the version of the firmware is written (recommended buffer size: 10 Byte).
 *
 * @param fwVersionBufLen
 *   Size of the buffer in which the firmware version is written (in bytes).
 *
 * @param serialNumber
 *   Buffer in which the serial number of the CANstress hardware is written (recommended buffer size: 20 Byte).
 *
 * @param snBufLen
 *   Size of the buffer in which the hardware serial number is written (in bytes).
 *
 * @param canInterface1
 *   Buffer in which the type of the CAN interface 1 of the CANstress hardware is written (recommended buffer size: 40 Byte).
 *
 * @param if1BufLen
 *   Size of the buffer in which the type of the CAN interface1 is written (in bytes).
 *
 * @param canInterface2
 *   Buffer in which the type of the CAN interface 2 of the CANstress hardware is written (recommended buffer size: 40 Byte).
 *
 * @param if2BufLen
 *   Size of the buffer in which the type of the CAN interface 2 is written (in bytes).
 *
 * @return
 *   - 0: On successful call.
 *   - -1: In case of error.
 */
long CANstressGetInfo(char * softwareVersion, long swVersionBufLen, char * firmwareVersion, long fwVersionBufLen, char * serialNumber, long snBufLen, char * canInterface1, long if1BufLen, char * canInterface2, long if2BufLen);

}
