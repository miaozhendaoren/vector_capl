#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Activates the CANstress software trigger.
 *
 * Activates the CANstress software trigger.
 *
 * @return
 *   0: On successful call.
 */
long CANstressTrigger(void);

}
