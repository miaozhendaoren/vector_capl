#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the Continuous disturbance (while trigger) mode.
 *
 * Sets the Continuous disturbance (while trigger) mode.
 * The continuous disturbance will prevail for as long as the trigger is active.
 * Please note that this mode is generally only useful in conjunction with a software trigger.
 *
 * @param type
 *   Defines the type of continuous disturbance.
 *   The disturbance can be associated with dominant or recessive bits or it can be an analog disturbance.
 *   2 indicates a dominant disturbance,
 *   3 a recessive disturbance and
 *   4 an analog disturbance (only in conjunction with CANstress DR).
 */
void CANstressSetContinuousDisturbanceWhileTrigger(dword type);

}
