cmake_minimum_required(VERSION 2.8)

set(function_group CANstress)

set(source_files
  CANstressAvailableDevices.cpp
  CANstressConnect.cpp
  CANstressCreateServer.cpp
  CANstressGetDevice.cpp
  CANstressGetInfo.cpp
  CANstressGetPerformedDisturbances.cpp
  CANstressIsFinished.cpp
  CANstressIsIdle.cpp
  CANstressIsPending.cpp
  CANstressOnFinished.cpp
  CANstressOnIdle.cpp
  CANstressOnPending.cpp
  CANstressOpen.cpp
  CANstressQuit.cpp
  CANstressSetBTR.cpp
  CANstressSetContinuousDisturbanceTimeLimited.cpp
  CANstressSetContinuousDisturbanceUntilStop.cpp
  CANstressSetContinuousDisturbanceWhileTrigger.cpp
  CANstressSetDevice.cpp
  CANstressSetDisturbanceSequence.cpp
  CANstressSetLimitedDisturbanceNumber.cpp
  CANstressSetResistor.cpp
  CANstressSetTriggerId.cpp
  CANstressSetTriggerRange.cpp
  CANstressSetUnlimitedDisturbanceNumber.cpp
  CANstressStart.cpp
  CANstressStop.cpp
  CANstressStopTrigger.cpp
  CANstressTrigger.cpp
  CANstressWaitForFinished.cpp
  CANstressWaitForIdle.cpp
  CANstressWaitForPending.cpp)

set(header_files
  CANstressAvailableDevices.h
  CANstressConnect.h
  CANstressCreateServer.h
  CANstressGetDevice.h
  CANstressGetInfo.h
  CANstressGetPerformedDisturbances.h
  CANstress.h
  CANstressIsFinished.h
  CANstressIsIdle.h
  CANstressIsPending.h
  CANstressOnFinished.h
  CANstressOnIdle.h
  CANstressOnPending.h
  CANstressOpen.h
  CANstressQuit.h
  CANstressSetBTR.h
  CANstressSetContinuousDisturbanceTimeLimited.h
  CANstressSetContinuousDisturbanceUntilStop.h
  CANstressSetContinuousDisturbanceWhileTrigger.h
  CANstressSetDevice.h
  CANstressSetDisturbanceSequence.h
  CANstressSetLimitedDisturbanceNumber.h
  CANstressSetResistor.h
  CANstressSetTriggerId.h
  CANstressSetTriggerRange.h
  CANstressSetUnlimitedDisturbanceNumber.h
  CANstressStart.h
  CANstressStop.h
  CANstressStopTrigger.h
  CANstressTrigger.h
  CANstressWaitForFinished.h
  CANstressWaitForIdle.h
  CANstressWaitForPending.h)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_library(${PROJECT_NAME}_${function_group} OBJECT ${source_files} ${header_files})

if(OPTION_USE_GCOV)
  target_link_libraries(${PROJECT_NAME}_${function_group} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

install(
  FILES ${header_files}
  DESTINATION include/Vector/CAPL/${function_group})
