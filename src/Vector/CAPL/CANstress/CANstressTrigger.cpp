#include "CANstressTrigger.h"

#include <iostream>

namespace capl
{

long CANstressTrigger(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
