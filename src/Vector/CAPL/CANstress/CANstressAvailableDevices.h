#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Indicates the number of configured CANstress devices.
 *
 * Indicates the number of configured CANstress devices.
 * This number is defined in the canstress.ini file, which is located in the CANstress installation directory.
 *
 * @return
 *   Number of configured CANstress devices.
 */
dword CANstressAvailableDevices(void);

}
