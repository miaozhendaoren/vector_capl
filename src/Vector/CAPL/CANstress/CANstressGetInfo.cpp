#include "CANstressGetInfo.h"

#include <iostream>

namespace capl
{

long CANstressGetInfo(char * softwareVersion, long swVersionBufLen, char * firmwareVersion, long fwVersionBufLen, char * serialNumber, long snBufLen, char * canInterface1, long if1BufLen, char * canInterface2, long if2BufLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
