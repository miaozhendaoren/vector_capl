#include "CANstressQuit.h"

#include <iostream>

namespace capl
{

dword CANstressQuit(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
