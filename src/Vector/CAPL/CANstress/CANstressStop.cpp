#include "CANstressStop.h"

#include <iostream>

namespace capl
{

void CANstressStop(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
