#include "CANstressOnFinished.h"

#include <iostream>

namespace capl
{

long CANstressOnFinished(char * fnctCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
