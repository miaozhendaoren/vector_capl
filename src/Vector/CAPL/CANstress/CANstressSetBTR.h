#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the value of the Bus Timing Register.
 *
 * Sets the value of the Bus Timing Register.
 * This setting affects the baud rate of the CAN bus set in CANstress.
 * The CAN controller data sheet will list valid pairs of values.
 *
 * @param btr0
 *   New value for BTR0.
 *
 * @param btr1
 *   New value for BTR1.
 */
void CANstressSetBTR(dword btr0, dword btr1);

}
