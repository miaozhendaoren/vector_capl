#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets a range for message IDs, which will activate triggers.
 *
 * Sets a range for message IDs, which will activate triggers.
 * Both simple and complex CAN message IDs can be specified.
 * However, the corresponding mode must be set in the basic configuration!
 *
 * @param fromId
 *   Sets the lower limit of the trigger range.
 *
 * @param toId
 *   Sets the upper limit of the trigger range.
 */
void CANstressSetTriggerRange(dword fromId, dword toId);

}
