#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the disturbance sequence.
 *
 * Sets the disturbance sequence.
 *
 * @param sequence
 *   Describes the sequence as a string of 0, 1, a, and u characters, whereby
 *   0 stands for dominant,
 *   1 for recessive,
 *   a for an analog disturbance and
 *   u for undisturbed.
 *   A disturbance sequence can contain up to 2048 characters.
 *
 * @param resolution
 *   Sets the resolution of the disturbance sequence. Possible values are 0 for bit times and 1 for BTL cycles.
 */
void CANstressSetDisturbanceSequence(char * sequence, dword resolution);

}
