#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Serves to query whether the CANstress hardware is in the state Idle.
 *
 * Serves to query whether the CANstress hardware is in the state Idle.
 *
 * @return
 *   - 1: If the CANstress hardware is in the state Idle.
 *   - 0: If the hardware is in another state (Pending or Finished).
 *   - -1: On occurrence of an error.
 */
long CANstressIsIdle(void);

}
