#include "CANstressStart.h"

#include <iostream>

namespace capl
{

long CANstressStart(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
	return 0;
}

}
