#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the Unlimited number of disturbances disturbance mode.
 *
 * Sets the Unlimited number of disturbances disturbance mode.
 * In this mode, a disturbance occurs on every trigger event.
 * There is no limit to the total number of disturbances.
 */
void CANstressSetUnlimitedDisturbanceNumber(void);

}
