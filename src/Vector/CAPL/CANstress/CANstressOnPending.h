#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Registers a CAPL function as callback that is called if CANstress is switched into the state Pending.
 *
 * Registers a CAPL function as callback that is called if CANstress is switched into the state Pending.
 *
 * @param fnctCallback
 *   Name of the function that should be used as callback.
 *
 * @return
 *   0: On successful call.
 */
long CANstressOnPending(char * fnctCallback);

}
