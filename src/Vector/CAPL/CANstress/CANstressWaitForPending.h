#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Waits until the CANstress hardware is in the state Pending.
 *
 * Waits until the CANstress hardware is in the state Pending.
 *
 * @param timeout
 *   Specifies a maximal time in milliseconds that is waited. At the latest after the expiration of this time, the function returns.
 *
 * @return
 *   - 0: If successful.
 *   - 1: On occurrence of an internal error.
 *   - -2: On return of the function due to a timeout.
 */
long CANstressWaitForPending(dword timeout);

}
