#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Wakes up the LIN bus without sending any wakeup frames.
 *
 * Wakes up the LIN bus without sending any wakeup frames.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSilentWakeup();

}
