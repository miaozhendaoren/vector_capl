#include "linStopDisturbance.h"

#include <iostream>

namespace capl
{

long linStopDisturbance()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
