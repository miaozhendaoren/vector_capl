#include "linDisturbHeaderWithHeader.h"

#include <iostream>

namespace capl
{

dword linDisturbHeaderWithHeader(dword byteIndex, dword bitIndex, long disturbingFrameId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
