#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified number of 1/16th bits at the specified position in the next LIN frame's response with the specified frame ID.
 *
 * Inverts the specified number of 1/16th bits at the specified position in the next LIN frame’s response with the specified frame ID.
 *
 * @param frameID
 *   ID of the bus event to be manipulated. Value range: 0..63
 *
 * @param byteIndex
 *   Index of the byte.
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @param bitOffsetInSixteenthBits
 *   The offset in 1/16th bits into the bit specified in bitIndex.
 *   Value range: 0..15
 *
 * @param distLengthInSixteenthBits
 *   The length of the disturbance in units of 1/16th bit.
 *   Value range: 0..65535
 *
 * @param level
 *   Level of the disturbance.
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertRespBitEx(long frameID, dword byteIndex, dword bitIndex, dword bitOffsetInSixteenthBits, dword distLengthInSixteenthBits, dword level);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified number of 1/16th bits at the specified position in the next LIN frame's response with the specified frame ID.
 *
 * Inverts the specified number of 1/16th bits at the specified position in the next LIN frame’s response with the specified frame ID.
 *
 * @param frameID
 *   ID of the bus event to be manipulated. Value range: 0..63
 *
 * @param byteIndex
 *   Index of the byte.
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @param bitOffsetInSixteenthBits
 *   The offset in 1/16th bits into the bit specified in bitIndex.
 *   Value range: 0..15
 *
 * @param distLengthInSixteenthBits
 *   The length of the disturbance in units of 1/16th bit.
 *   Value range: 0..65535
 *
 * @param level
 *   Level of the disturbance.
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param numberOfExecutions
 *   The number of consecutive headers in which the bit inversion will be executed.
 *   The default is 1 (single shot).
 *
 * @param reportFallingEdges
 *   Flag indicating whether time stamps of the falling edges in the resulting pseudo-byte have to be reported.
 *   The time stamps can be retrieved by calling linGetFallingEdgesOfDisturbedByte.
 *   0: Deactivate report
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertRespBitEx(long frameID, dword byteIndex, dword bitIndex, dword bitOffsetInSixteenthBits, dword distLengthInSixteenthBits, dword level, dword numberOfExecutions, dword reportFallingEdges);

}
