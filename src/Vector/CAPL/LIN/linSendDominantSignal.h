#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends a dominant signal of a specified length.
 *
 * Sends a dominant signal of the specified length.
 *
 * @param lengthInMicroseconds
 *   The length of the dominant signal in microseconds.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSendDominantSignal(dword lengthInMicroseconds);

}
