#include "linActivateFlashMode.h"

#include <iostream>

namespace capl
{

dword linActivateFlashMode(byte activate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
