#include "linGetSyncBreakLength.h"

#include <iostream>

namespace capl
{

dword linGetSyncBreakLength(linMessage busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncBreakLength(linCsError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncBreakLength(linReceiveError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncBreakLength(linTransmError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncBreakLength(linSyncError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
