#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Cancels a previously activated bits inversion for LIN header or response.
 *
 * With this function it is possible to cancel a previously activated bits inversion for LIN header or response.
 * This function is useful when after calling linInvertHeaderBit() no header occurred yet on the bus or
 * when after calling linInvertRespBit() no frame occurred yet.
 *
 * @return
 *   On success a value unequal to zero, otherwise zero.
 */
long LINDeactivateBitInversion();

}
