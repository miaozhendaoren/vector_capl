#include "linGetStartOfFrame.h"

#include <iostream>

namespace capl
{

dword linGetStartOfFrame(linMessage busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetStartOfFrame(linCsError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetStartOfFrame(linReceiveError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetStartOfFrame(linTransmError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetStartOfFrame(linSyncError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetStartOfFrame(linWakeupFrame busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
