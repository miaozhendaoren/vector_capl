#include "linActivateSlot.h"

#include <iostream>

namespace capl
{

long linActivateSlot(dword tableIndex, dword slotIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
