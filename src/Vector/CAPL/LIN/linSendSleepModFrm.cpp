#include "linSendSleepModFrm.h"

#include <iostream>

namespace capl
{

long linSendSleepModFrm(long silent, long restartScheduler, long wakeupIdentifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
