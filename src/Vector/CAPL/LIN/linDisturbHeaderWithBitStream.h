#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Configures the LIN hardware to disturb the next header with a bit stream.
 *
 * Configures the LIN hardware to disturb the next header with a bit stream.
 *
 * @param byteIndex
 *   Start disturbance in byte with index byteIndex.
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit where the interrupting bit stream will start.
 *   An index in the range 0-7 specifies a data bit, while the index 8 specifies the stop bit.
 *   Higher index values specify the interbyte-space after the indexed data byte.
 *   In which case, the user should make sure that the interbyte space is large enough.
 *   Value range: 0..255
 *
 * @param bitStream
 *   The bit stream to be used for the interruption.
 *   Maximum number of bits: 2^31-1.
 *
 * @param numberOfBits
 *   The number of bits in the bitStream-array.
 *
 * @param timeoutPrevention
 *   - 0: deactivates the timeout prevention for the 7259- or 7269-transceiver.
 *   - 1: activates the timeout prevention for the 7259- or 7269-transceiver.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linDisturbHeaderWithBitStream(dword byteIndex, dword bitIndex, byte * bitStream, dword numberOfBits, dword timeoutPrevention);

}
