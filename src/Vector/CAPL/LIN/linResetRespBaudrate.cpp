#include "linResetRespBaudrate.h"

#include <iostream>

namespace capl
{

long linResetRespBaudrate(long frameId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
