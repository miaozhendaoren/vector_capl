#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit when the next bus event for the specified ID occurs.
 *
 * This function inverts the specified bit when the next bus event for the specified ID occurs,
 * provided that the bus event contains the specified data byte.
 *
 * @param frameID
 *   ID of the bus event to be manipulated.
 *   Value range: 0..63
 *
 * @param byteIndex
 *   The index of the byte to be manipulated (use 0 for the first byte).
 *   If the index is equal to the frame’s length, then the checksum byte will be manipulated.
 *   An index larger than the frame length is invalid.
 *   Value range: 0..N, where N is frame length
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit when the next bus event for the specified ID occurs.
 *
 * This function inverts the specified bit when the next bus event for the specified ID occurs,
 * provided that the bus event contains the specified data byte.
 *
 * @param frameID
 *   ID of the bus event to be manipulated.
 *   Value range: 0..63
 *
 * @param byteIndex
 *   The index of the byte to be manipulated (use 0 for the first byte).
 *   If the index is equal to the frame’s length, then the checksum byte will be manipulated.
 *   An index larger than the frame length is invalid.
 *   Value range: 0..N, where N is frame length
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex, dword level);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit when the next bus event for the specified ID occurs.
 *
 * This function inverts the specified bit when the next bus event for the specified ID occurs,
 * provided that the bus event contains the specified data byte.
 *
 * @param frameID
 *   ID of the bus event to be manipulated.
 *   Value range: 0..63
 *
 * @param byteIndex
 *   The index of the byte to be manipulated (use 0 for the first byte).
 *   If the index is equal to the frame’s length, then the checksum byte will be manipulated.
 *   An index larger than the frame length is invalid.
 *   Value range: 0..N, where N is frame length
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param numberOfExecutions
 *   The number of consecutive frames with the specified identifier in which the bit inversion will be executed.
 *   Default: 1 (single shot).
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit when the next bus event for the specified ID occurs.
 *
 * This function inverts the specified bit when the next bus event for the specified ID occurs,
 * provided that the bus event contains the specified data byte.
 *
 * @param frameID
 *   ID of the bus event to be manipulated.
 *   Value range: 0..63
 *
 * @param byteIndex
 *   The index of the byte to be manipulated (use 0 for the first byte).
 *   If the index is equal to the frame’s length, then the checksum byte will be manipulated.
 *   An index larger than the frame length is invalid.
 *   Value range: 0..N, where N is frame length
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param numberOfExecutions
 *   The number of consecutive frames with the specified identifier in which the bit inversion will be executed.
 *   Default: 1 (single shot).
 *
 * @param reportFallingEdges
 *   Flag indicating whether time stamps of the falling edges in the resulting pseudo-byte have to be reported. The time stamps can be retrieved by calling linGetFallingEdgesOfDisturbedByte.
 *   - 0: Deactivate report
 *   - 1: Activate report
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions, dword reportFallingEdges);

}
