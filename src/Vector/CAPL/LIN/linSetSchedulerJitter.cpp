#include "linSetSchedulerJitter.h"

#include <iostream>

namespace capl
{

long linSetSchedulerJitter(long mode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSetSchedulerJitter(long mode, long jitterInMicSecs)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
