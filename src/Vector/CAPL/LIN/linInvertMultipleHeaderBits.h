#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the multiple bits in specified locations in the next LIN header.
 *
 * Inverts the multiple bits in specified locations in the next LIN header.
 *
 * @param byteIndices
 *   An array of the index of the bytes.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndices
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param numberOfDisturbedBits
 *   An array specifying how many consecutive bits shall be inverted at the specified locations.
 *
 * @param levels
 *   An array of the levels of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param arrSize
 *   The number of elements in the above arrays. This value should not be greater than the size of the smallest array.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertMultipleHeaderBits(dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the multiple bits in specified locations in the next LIN header.
 *
 * Inverts the multiple bits in specified locations in the next LIN header.
 *
 * @param byteIndices
 *   An array of the index of the bytes.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndices
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param numberOfDisturbedBits
 *   An array specifying how many consecutive bits shall be inverted at the specified locations.
 *
 * @param levels
 *   An array of the levels of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param arrSize
 *   The number of elements in the above arrays. This value should not be greater than the size of the smallest array.
 *
 * @param numberOfExecutions
 *   The number of consecutive headers in which the bit inversions will be executed.
 *   Default: 1 (single shot).
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertMultipleHeaderBits(dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize, dword numberOfExecutions);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the multiple bits in specified locations in the next LIN header.
 *
 * Inverts the multiple bits in specified locations in the next LIN header.
 *
 * @param byteIndices
 *   An array of the index of the bytes.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndices
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param numberOfDisturbedBits
 *   An array specifying how many consecutive bits shall be inverted at the specified locations.
 *
 * @param levels
 *   An array of the levels of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param arrSize
 *   The number of elements in the above arrays. This value should not be greater than the size of the smallest array.
 *
 * @param numberOfExecutions
 *   The number of consecutive headers in which the bit inversions will be executed.
 *   Default: 1 (single shot).
 *
 * @param disturbAfterHeaderID
 *   With this parameter and the next one it is possible to define exactly which header will be disturbed.
 *   The LIN hardware will first wait for a header with ID = disturbAfterHeaderID before additionally awaiting the number of headers defined by waitForHeaders.
 *   The next header following these headers will then be disturbed.
 *   For example: To disturb the next header directly after a header with the ID=5, set the disturbAfterHeaderID parameter to 5 and the waitForHeaders to 0.
 *
 * @param waitForHeaders
 *   See explanation for disturbAfterHeaderID.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertMultipleHeaderBits(dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize, dword numberOfExecutions, long disturbAfterHeaderID, dword waitForHeaders);

}
