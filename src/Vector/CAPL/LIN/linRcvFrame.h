#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   A function defined in CAPL with this signature receives all LIN frames that are occurring on the bus and are not filtered out by a global message filter.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
