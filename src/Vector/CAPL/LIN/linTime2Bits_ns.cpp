#include "linTime2Bits_ns.h"

#include <iostream>

namespace capl
{

dword linTime2Bits_ns(int64 time)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linTime2Bits_ns(dword channel, int64 time)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
