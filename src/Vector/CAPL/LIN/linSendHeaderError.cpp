#include "linSendHeaderError.h"

#include <iostream>

namespace capl
{

dword linSendHeaderError(byte syncByte, byte idWithParity, byte StopAfterError)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
