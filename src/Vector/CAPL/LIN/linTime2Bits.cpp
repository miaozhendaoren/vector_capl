#include "linTime2Bits.h"

#include <iostream>

namespace capl
{

dword linTime2Bits(dword time)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linTime2Bits(dword channel, dword time)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
