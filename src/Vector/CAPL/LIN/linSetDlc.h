#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Initializes the Data Length Code of a LIN frame.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
