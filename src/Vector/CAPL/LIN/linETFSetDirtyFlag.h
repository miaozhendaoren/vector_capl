#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets/clears the dirty flag of an associated frame.
 *
 * With this function it is possible to set/clear the dirty flag of an associated frame.
 * If the dirty flag of an associated frame is set when the corresponding event-triggered frame is being requested, then the LIN hardware will try to transmit the associated frame's data.
 * The dirty flag gets reset automatically when the associated frame has been sent successfully – either via the event-triggered frame or unconditionally.
 *
 * @param assocId
 *   Identifier of unconditional frame
 *   LIN frame identifier of associated (unconditional) frame whose dirty flag should be set or cleared.
 *   Value range: 0 .. 63
 *
 * @param dirty
 *   - 1: Set the dirty flag.
 *   - 0: Clear the dirty flag.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linETFSetDirtyFlag(long assocId, long dirty);

}
