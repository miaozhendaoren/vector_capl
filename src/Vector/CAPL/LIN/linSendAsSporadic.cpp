#include "linSendAsSporadic.h"

#include <iostream>

namespace capl
{

long linSendAsSporadic(long frameID)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
