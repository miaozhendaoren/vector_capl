#include "linCheckOEMSleepInd.h"

#include <iostream>

namespace capl
{

long linCheckOEMSleepInd()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
