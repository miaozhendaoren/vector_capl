#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates the Slave with the passed identifier for simulation.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
