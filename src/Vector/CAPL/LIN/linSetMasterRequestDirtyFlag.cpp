#include "linSetMasterRequestDirtyFlag.h"

#include <iostream>

namespace capl
{

dword linSetMasterRequestDirtyFlag(dword dirty)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
