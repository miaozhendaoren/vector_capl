#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the transmission speed in baud on the LIN bus.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
