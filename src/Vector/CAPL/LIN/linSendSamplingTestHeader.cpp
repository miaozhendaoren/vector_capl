#include "linSendSamplingTestHeader.h"

#include <iostream>

namespace capl
{

long linSendSamplingTestHeader(dword startBitLengthInMicSec)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
