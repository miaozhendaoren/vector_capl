#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Calls up the error content if LIN Transmission Error event is the last event that triggers a wait instruction.
 *
 * @todo
 */

}
