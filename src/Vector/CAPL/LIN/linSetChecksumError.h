#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets/resets a checksum error of a LIN frame.
 *
 * Sets/resets a checksum error of a LIN frame.
 * The activated checksum error is then constant and will not be affected by changes in the frame data.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63.
 *
 * @param activate
 *   - 0: deactivate checksum error
 *   - 1: activate checksum error
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetChecksumError(long frameId, long activate);

}
