#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Calls up the error content if LIN Receive Error event is the last event that triggers a wait instruction.
 *
 * @todo
 */

}
