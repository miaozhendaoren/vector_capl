#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the response tolerance for a specified frame in percent.
 *
 * Sets the response tolerance for a specified frame in percent.
 *
 * @param frameId
 *   The identifier of the frame for which the response tolerance shall be set.
 *
 * @param toleranceInPercent
 *   The response tolerance to be used for the specified frame.
 *   Value range: 0-255
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetRespTolerance(long frameId, long toleranceInPercent);

}
