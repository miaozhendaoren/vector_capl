#include "linResetScopeTrigger.h"

#include <iostream>

namespace capl
{

long linResetScopeTrigger()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
