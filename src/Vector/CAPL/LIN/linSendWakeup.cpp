#include "linSendWakeup.h"

#include <iostream>

namespace capl
{

long linSendWakeup()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSendWakeup(long ttobrk, long count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSendWakeup(long ttobrk, long count, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
