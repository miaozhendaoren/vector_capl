#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets/resets the jitter mode and the jitter of the LIN hardware scheduler.
 *
 * Sets/resets the jitter mode and the jitter of the LIN hardware scheduler. (the channel is determined by the CAPL program context).
 *
 * @param mode
 *   - 0: deactivates the jitter;
 *   - 1: sets the min-/max-mode; the minimum (0) and the maximum (specified value) jitter will be used alternatively
 *   - 2: sets the random jitter-mode; the jitter-offset will be equally distributed in the min/max range
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetSchedulerJitter(long mode);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets/resets the jitter mode and the jitter of the LIN hardware scheduler.
 *
 * Sets/resets the jitter mode and the jitter of the LIN hardware scheduler. (the channel is determined by the CAPL program context).
 *
 * @param mode
 *   - 0: deactivates the jitter;
 *   - 1: sets the min-/max-mode; the minimum (0) and the maximum (specified value) jitter will be used alternatively
 *   - 2: sets the random jitter-mode; the jitter-offset will be equally distributed in the min/max range
 *
 * @param jitterInMicSecs
 *   The jitter in microseconds to be used.
 *   Default: Jitter value from LDF.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetSchedulerJitter(long mode, long jitterInMicSecs);

}
