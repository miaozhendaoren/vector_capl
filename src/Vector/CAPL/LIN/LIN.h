#pragma once

/** @todo to be implemented */

/**
 * @defgroup LIN LIN CAPL Functions
 */

/* Selectors */
#include "linBaudrateEvent.h"
#include "linCsError.h"
#include "linDlcInfo.h"
#include "linHeader.h"
#include "linLongDominantSignal.h"
#include "linMessage.h"
#include "linReceiveError.h"
#include "linSchedulerModeChange.h"
#include "linSlaveTimeout.h"
#include "linSleepModeEvent.h"
#include "linSpikeEvent.h"
#include "linSyncError.h"
#include "linTransmError.h"
#include "linWakeupFrame.h"

/* Event Procedures */
#include "linMessage.h"

/* Configuring and Controlling a Simulated LIN Master */
#include "getSignal.h"
#include "linActivateCollisionResolution.h"
#include "linActivateSlot.h"
#include "linChangeSchedTable.h"
#include "linChangeWakeupSettings.h"
#include "linCheckRespError.h"
#include "linDeactivateSlot.h"
#include "linSendAsSporadic.h"
#include "linSendSleepModFrm.h"
#include "linSendWakeup.h"
#include "linSetBreakLength.h"
#include "linSetGlobalInterByteSpace.h"
#include "linSetInterByteSpace.h"
#include "linSetInterByteSpaces.h"
#include "linSetInterframeSpace.h"
#include "linSetMasterRequestDirtyFlag.h"
#include "linSetOEMDataInd.h"
#include "linSetOEMSleepInd.h"
#include "linSetOEMWakeupInd.h"
#include "linSetSchedulerJitter.h"
#include "linSetWakeupParams.h"
#include "linSimulateETFCollision.h"
#include "linStartScheduler.h"
#include "linStopScheduler.h"
#include "output.h"
#include "setSignal.h"

/* Configuring and Controlling a Simulated LIN Slave */
#include "getSignal.h"
#include "linActivateResps.h"
#include "linActivateGlobalNetworkManagement.h"
#include "linChangeDlc.h"
#include "linDeactivateResps.h"
#include "linETFSendOnSignalUpdate.h"
#include "linETFSetDirtyFlag.h"
#include "linGetBusIdleTimeout.h"
#include "linGetOEMDataInd.h"
#include "linGetOEMSleepInd.h"
#include "linGetOEMWakeupInd.h"
#include "linGetRespError.h"
#include "linResetRespBaudrate.h"
#include "linResetNAD.h"
#include "linResetSlave.h"
#include "linSendWakeup.h"
#include "linSetRespBaudrate.h"
#include "linSetBusIdleTimeout.h"
#include "linSetGlobalInterByteSpace.h"
#include "linSetInterByteSpace.h"
#include "linSetInterByteSpaces.h"
#include "linSetNAD.h"
#include "linSetOEMDataInd.h"
#include "linSetOEMDataIndTime.h"
#include "linSetOEMSleepInd.h"
#include "linSetOEMWakeupInd.h"
#include "linSetRespCounter.h"
#include "linSetRespError.h"
#include "linSetRespLength.h"
#include "linSetValidBreakLimits.h"
#include "output.h"
#include "setSignal.h"

/* Configuring and Controlling Network Management */
#include "linActivateGlobalNetworkManagement.h"
#include "linActivateSlaveNetworkManagement.h"
#include "linBusIsAwake.h"
#include "linChangeWakeupSettings.h"
#include "linCheckOEMDataInd.h"
#include "linCheckOEMSleepInd.h"
#include "linCheckOEMWakeupInd.h"
#include "linCheckRespError.h"
#include "linGetOEMDataInd.h"
#include "linGetOEMSleepInd.h"
#include "linGetOEMWakeupInd.h"
#include "linGetRespError.h"
#include "linSendSleepModFrm.h"
#include "linSendWakeup.h"
#include "linSetOEMDataInd.h"
#include "linSetOEMDataIndTime.h"
#include "linSetOEMSleepInd.h"
#include "linSetOEMWakeupInd.h"
#include "linSetRespError.h"
#include "linSetWakeupParams.h"
#include "linSilentWakeup.h"

/* LIN Analysis Feature Set */
#include "getSignal.h"
#include "linBits2Time_ns.h"
#include "linBusIsAwake.h"
#include "linGetChecksum.h"
#include "linGetDlc.h"
#include "linGetHWReceiveAccuracy.h"
#include "linGetHWTransmitAccuracy.h"
#include "linGetMeasBaudrate.h"
#include "linGetMeasEdgeTimeDiffs.h"
#include "linGetProtectedID.h"
#include "linMeasHeaderBaudrate.h"
#include "linMeasEdgeTimeDiffs.h"
#include "linMeasRespBaudrate.h"
#include "linResetMaxHeaderLength.h"
#include "linResetScopeTrigger.h"
#include "linResetStatistics.h"
#include "linSetBaudrateDetectionRange.h"
#include "linSetMaxHeaderLength.h"
#include "linSetRespTolerance.h"
#include "linSetScopeTrigger.h"
#include "linSetValidBreakLimits.h"
#include "linTime2Bits_ns.h"

/* LIN Disturbance Feature Set */
#include "linDetectMultipleErrors.h"
#include "linDisturbHeaderWithBitStream.h"
#include "linDisturbHeaderWithHeader.h"
#include "linDisturbHeaderWithVariableBitStream.h"
#include "linDisturbRespWithBitStream.h"
#include "linDisturbRespWithHeader.h"
#include "linDisturbRespWithVariableBitStream.h"
#include "linGetDominantTimeout.h"
#include "linInvertHeaderBit.h"
#include "linInvertHeaderBitEx.h"
#include "linInvertMultipleHeaderBits.h"
#include "linInvertRespBit.h"
#include "linInvertRespBitEx.h"
#include "linInvertMultipleRespBits.h"
#include "linDeactivateBitInversion.h"
#include "linSendDominantSignal.h"
#include "linSetGlobalTimeoutPrevention.h"
#include "linSetRespDisturbance.h"
#include "linResetRespDisturbance.h"
#include "linStartDisturbance.h"
#include "linStopDisturbance.h"

/* LIN Stress Feature Set */
#include "linActivateFlashMode.h"
#include "linGetFallingEdgesOfDisturbedByte.h"
#include "linIsFlashModeActive.h"
#include "linSendHeaderError.h"
#include "linSetBaudrate.h"
#include "linSetSchedulerJitter.h"
#include "linSetBreakLength.h"
#include "linSendSamplingTestHeader.h"
#include "linSetChecksumError.h"
#include "linSetManualChecksum.h"
#include "linResetManualChecksum.h"
#include "linSetInterByteSpace.h"
#include "linSetInterByteSpaces.h"
#include "linSetGlobalInterByteSpace.h"
#include "linSetInterframeSpace.h"
#include "linSetRespCounter.h"
#include "linSetRespLength.h"
#include "linResetRespBaudrate.h"
#include "linSetRespBaudrate.h"
#include "linSendBitStream.h"
#include "linSendVariableBitStream.h"
#include "linSetRespBitStream.h"
#include "linResetRespBitStream.h"

/* Test Feature Set for LIN */
#include "TestGetWaitEventMsgData.h"
#include "TestGetWaitLinCSErrorData.h"
#include "TestGetWaitLinETFSingleResponseData.h"
#include "TestGetWaitLinHdrData.h"
#include "TestGetWaitLinReceiveErrData.h"
#include "TestGetWaitLinSyncErrorData.h"
#include "TestGetWaitLinTransmErrData.h"
#include "TestGetWaitLinWakeupData.h"
#include "TestJoinMessageEvent.h"
#include "TestJoinLinCSErrorEvent.h"
#include "TestJoinLinETFSingleResponseEvent.h"
#include "TestJoinLinHeaderEvent.h"
#include "TestJoinLinReceiveErrorEvent.h"
#include "TestJoinLinTransmErrorEvent.h"
#include "TestJoinLinWakeupEvent.h"
#include "TestJoinLinSyncErrorEvent.h"
#include "TestWaitForLinCSError.h"
#include "TestWaitForLinETFSingleResponse.h"
#include "TestWaitForLinHeader.h"
#include "TestWaitForLinReceiveError.h"
#include "TestWaitForLinSyncError.h"
#include "TestWaitForLinTransmError.h"
#include "TestWaitForLinWakeupFrame.h"
#include "TestWaitForMessage.h"

/* Test Service Library for LIN */
#include "ChkStart_AllNodesDead.h"
#include "ChkStart_NodeDead.h"
#include "ChkStart_MsgDistViolation.h"
#include "ChkStart_LINDiagDelayTimesViolation.h"
#include "ChkStart_LINETFViolation.h"
#include "ChkStart_LINHeaderToleranceViolation.h"
#include "ChkStart_LINMasterBaudrateViolation.h"
#include "ChkStart_LINMasterInitTimeViolation.h"
#include "ChkStart_LINReconfRequestFormatViolation.h"
#include "ChkStart_LINRespErrorSignal.h"
#include "ChkStart_LINRespToleranceViolation.h"
#include "ChkStart_LINSchedTableViolation.h"
#include "ChkStart_LINSyncBreakTimingViolation.h"
#include "ChkStart_LINSyncDelTimingViolation.h"
#include "ChkStart_LINWakeupReqLengthViolation.h"
#include "ChkStart_LINWakeupRetryViolation.h"
#include "ChkStart_MsgSignalValueRangeViolation.h"
#include "ChkStart_MsgSignalValueInvalid.h"
#include "ChkStart_SignalCycleTimeViolation.h"
#include "ChkStart_SignalValueChange.h"

/* Obsolete Functions */
#include "getChecksum.h"
#include "linBits2Time.h"
#include "linCalcChecksum.h"
#include "linCalcCRC.h"
#include "linGetByteEndTime.h"
#include "linGetEndOfHeader.h"
#include "linGetStartOfFrame.h"
#include "linGetSyncBreakLength.h"
#include "linGetSyncDelLength.h"
#include "linGetWakeupLength.h"
#include "linInitBegin.h"
#include "linInitEnd.h"
#include "linInitGetRs232Baud.h"
#include "linInitSetBaseBaud.h"
#include "linInitSetMaster.h"
#include "linMrSchedGetMode.h"
#include "linMrSchedSetGlobal.h"
#include "linMrSchedSetMode.h"
#include "linMrSchedSetRqId.h"
#include "linMrSchedSetSyncT.h"
#include "linMrSendRequest.h"
#include "linRcvFrame.h"
#include "linSetDlc.h"
#include "linSetHeaderError.h"
#include "linSetResponseData.h"
#include "linSetResponseMsg.h"
#include "linSleepModeEvent.h"
#include "linSlFSMSetGlobal.h"
#include "linSlFSMSetState.h"
#include "linSlFSMSetStBegin.h"
#include "linSlFSMSetStEnd.h"
#include "linSlFSMSetStMFUp.h"
#include "linSlFSMSetStSFUp.h"
#include "linSlFSMSetStTO.h"
#include "linSlSimulate.h"
#include "linTime2Bits.h"
#include "linWakeupFrame.h"
#include "resetManualChecksum.h"
#include "setManualChecksum.h"
