#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the minimum inter-frame space.
 *
 * This function sets the minimum inter-frame space.
 *
 * With this function it is possible to change the inter-frame space for all frames.
 *
 * The inter-frame space is the time from the end of the frame until start of the next frame.
 *
 * By default the minimum inter-frame space is 0.
 *
 * @param bitTimes
 *   Length of the inter-frame space to set [in bit times].
 *   Value range: 0..255
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetInterframeSpace(dword bitTimes);

}
