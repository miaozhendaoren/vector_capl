#pragma once

#include "../DataTypes.h"
#include "LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the correct checksum of a LIN frame.
 *
 * Sets the correct checksum of a LIN frame, whose checksum has been changed by using linSetManualChecksum() function.
 * The checksum is calculated using the frame data.
 *
 * @param linFrame
 *   LIN frame for which the correct checksum will be used again.
 */
void linResetManualChecksum(linMessage linFrame);

}
