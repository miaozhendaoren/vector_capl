#include "linSendBitStream.h"

#include <iostream>

namespace capl
{

long linSendBitStream(byte * bitStream, long numberOfBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSendBitStream(byte * bitStream, long numberOfBits, long timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
