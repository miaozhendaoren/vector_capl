#include "linDisturbRespWithVariableBitStream.h"

#include <iostream>

namespace capl
{

dword linDisturbRespWithVariableBitStream(long disturbedFrameId, dword byteIndex, dword bitIndex, byte * bitStream, dword numberOfBits, dword timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
