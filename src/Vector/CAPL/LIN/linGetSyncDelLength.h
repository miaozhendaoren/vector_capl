#pragma once

#include "../DataTypes.h"
#include "../LIN/LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves the synch delimiter (recessive bits) length of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linMessage selectors
 *
 * This function can be used to retrieve the synch delimiter (recessive bits) length of a LIN bus event.
 * The resulting length is in units of 10 µsec (microseconds).
 * To get the result in bit times linTime2Bits() function can be used.
 *
 * @param busEvent
 *   LIN bus event of type frame.
 *
 * @return
 *   Returns the retrieved length [in units of 10 µsec] or 0 on failure.
 */
dword linGetSyncDelLength(linMessage busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves the synch delimiter (recessive bits) length of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linCsError selectors
 *
 * This function can be used to retrieve the synch delimiter (recessive bits) length of a LIN bus event.
 * The resulting length is in units of 10 µsec (microseconds).
 * To get the result in bit times linTime2Bits() function can be used.
 *
 * @param busEvent
 *   LIN bus event of type checksum error.
 *
 * @return
 *   Returns the retrieved length [in units of 10 µsec] or 0 on failure.
 */
dword linGetSyncDelLength(linCsError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves the synch delimiter (recessive bits) length of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linReceiveError selectors
 *
 * This function can be used to retrieve the synch delimiter (recessive bits) length of a LIN bus event.
 * The resulting length is in units of 10 µsec (microseconds).
 * To get the result in bit times linTime2Bits() function can be used.
 *
 * @param busEvent
 *   LIN bus event of type receive error.
 *
 * @return
 *   Returns the retrieved length [in units of 10 µsec] or 0 on failure.
 */
dword linGetSyncDelLength(linReceiveError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves the synch delimiter (recessive bits) length of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linTransmError selectors
 *
 * This function can be used to retrieve the synch delimiter (recessive bits) length of a LIN bus event.
 * The resulting length is in units of 10 µsec (microseconds).
 * To get the result in bit times linTime2Bits() function can be used.
 *
 * @param busEvent
 *   LIN bus event of type transmission error.
 *
 * @return
 *   Returns the retrieved length [in units of 10 µsec] or 0 on failure.
 */
dword linGetSyncDelLength(linTransmError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves the synch delimiter (recessive bits) length of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linSyncError selectors
 *
 * This function can be used to retrieve the synch delimiter (recessive bits) length of a LIN bus event.
 * The resulting length is in units of 10 µsec (microseconds).
 * To get the result in bit times linTime2Bits() function can be used.
 *
 * @param busEvent
 *   LIN bus event of type synchronization error.
 *
 * @return
 *   Returns the retrieved length [in units of 10 µsec] or 0 on failure.
 */
dword linGetSyncDelLength(linSyncError busEvent);


}
