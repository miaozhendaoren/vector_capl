#include "linResetNAD.h"

#include <iostream>

namespace capl
{

long linResetNAD()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
