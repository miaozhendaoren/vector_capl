#include "linSetBreakLength.h"

#include <iostream>

namespace capl
{

dword linSetBreakLength(dword syncBreakLen, dword syncDelLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linSetBreakLength(double syncBreakLen, double syncDelLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
