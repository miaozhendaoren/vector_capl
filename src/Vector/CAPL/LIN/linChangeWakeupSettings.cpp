#include "linChangeWakeupSettings.h"

#include <iostream>

namespace capl
{

dword linChangeWakeupSettings(byte restartScheduler, long wakeupIdentifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
