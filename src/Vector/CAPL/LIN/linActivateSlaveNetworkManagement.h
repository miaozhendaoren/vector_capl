#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates/deactivates network management for the entire LIN network.
 *
 * Activates/deactivates network management for the calling Slave node.
 *
 * Network management is responsible for the automatic setting and resetting of response error signals in the simulated Slave nodes.
 *
 * @param active
 *   - 0: deactivate
 *   - 1: activate
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linActivateSlaveNetworkManagement(long active);

}
