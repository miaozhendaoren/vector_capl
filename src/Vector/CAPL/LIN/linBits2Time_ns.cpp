#include "linBits2Time_ns.h"

#include <iostream>

namespace capl
{

int64 linBits2Time_ns(dword bitTimes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

int64 linBits2Time_ns(dword channel, dword bitTimes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
