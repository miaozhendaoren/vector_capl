#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Reactivates a schedule table slot defined in the LIN database file.
 *
 * Reactivates a schedule table slot defined in the LIN database file (LDF),
 * after having been previously deactivated by linDeactivateSlot().
 *
 * Schedule slots containing MasterRequests are automatically sent only if their data is updated i.e. using output().
 *
 * @param tableIndex
 *   Schedule table index according to the LIN database file (LDF).
 *   First table in the LDF has the index 0.
 *
 * @param slotIndex
 *   Slot index. First slot in a table has the index 0.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linActivateSlot(dword tableIndex, dword slotIndex);

}
