#include "linSimulateETFCollision.h"

#include <iostream>

namespace capl
{

dword linSimulateETFCollision(dword etfId, dword respLength, dword numCollisions, byte * dataBytes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
