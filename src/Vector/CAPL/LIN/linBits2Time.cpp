#include "linBits2Time.h"

#include <iostream>

namespace capl
{

dword linBits2Time(dword bitTimes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linBits2Time(dword channel, dword bitTimes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
