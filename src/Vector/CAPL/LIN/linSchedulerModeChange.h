#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Detailed description of the linSchedulerModeChange selectors.
 *
 * The event procedure on linSchedulerModeChange is called when modeled LIN Master switches from the current schedule table to another one.
 * This only occurs when the LIN hardware is in Master mode.
 *
 * The keyword this and the selectors can be used to access the data of the event that has just been received.
 */
class linSchedulerModeChange
{
public:
    /**
     * Time stamp synchronized with the global time base on the PC (CAN hardware or PC system clock).
     *
     * Unit: 10µs
     */
    dword Time;

    /**
     * Time stamp synchronized with the global time base on the PC (CAN hardware or PC system clock).
     *
     * Unit: nanoseconds
     */
    int64 Time_ns;

    /**
     * Time stamp generated by the LIN hardware.
     * This time stamp is unsynchronized with the global time base on the PC and thus still original.
     * It can be used to compare the time of two LIN hardware generated events.
     *
     * Unit: 10µs
     */
    dword MsgOrigTime;

    /**
     * Status of MsgOrigTime:
     * - Bit 0 set: Time stamp is valid
     * - Bit 0 not set: Time stamp is invalid
     * - Bit 1 set: Time stamp was generated by software
     * - Bit 1 not set: Time stamp was generated by hardware
     * - Bit 4: Has a bus-specific meaning; not currently in use for LIN
     */
    byte MsgTimeStampStatus;

    /**
     * Channel through which the event was received.
     *
     * Value range: 1..32
     */
    word MsgChannel;

    /**
     * Index of a previously active schedule table.
     *
     * Value range: 0..255
     */
    byte lin_OldMode;

    /**
     * Index of the newly activated schedule table.
     *
     * Value range: 0..255
     */
    byte lin_NewMode;
};

}
