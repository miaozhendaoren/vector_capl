#include "linSetOEMDataInd.h"

#include <iostream>

namespace capl
{

long linSetOEMDataInd(long active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
