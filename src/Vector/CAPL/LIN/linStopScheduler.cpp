#include "linStopScheduler.h"

#include <iostream>

namespace capl
{

void linStopScheduler()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
