#include "linDisturbHeaderWithBitStream.h"

#include <iostream>

namespace capl
{

dword linDisturbHeaderWithBitStream(dword byteIndex, dword bitIndex, byte * bitStream, dword numberOfBits, dword timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
