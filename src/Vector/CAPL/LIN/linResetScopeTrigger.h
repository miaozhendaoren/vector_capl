#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets the oscilloscope trigger condition(s).
 *
 * Resets the oscilloscope trigger condition(s) defined with linSetScopeTrigger.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linResetScopeTrigger();

}
