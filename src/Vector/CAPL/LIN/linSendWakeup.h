#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends Wakeup frames.
 *
 * This command is used to send Wakeup frames.
 * Wakeup frames can only be sent while the LIN hardware is in Sleep mode.
 * If no parameters are given, the default values of the parameters are used.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendWakeup();

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends Wakeup frames.
 *
 * This command is used to send Wakeup frames.
 * Wakeup frames can only be sent while the LIN hardware is in Sleep mode.
 * If no parameters are given, the default values of the parameters are used.
 *
 * @param ttobrk
 *   This parameter specifies the time difference between the transmissions of two consecutive Wakeup frames,
 *   i.e. the time between end of one wakeup frame and start of the next one.
 *   Units of this parameter as well as default value depend on the hardware settings (see Hardware Configuration: LIN).
 *   Value range (for units expected in bit times): 20 .. 50000
 *   Value range (for units expected in ms): 1 .. 65536
 *
 * @param count
 *   Sets the number of Wakeup frame retransmissions.
 *   Value range: 1…255
 *   Default value depends on the hardware settings: see Hardware Configuration: LIN
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendWakeup(long ttobrk, long count);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends Wakeup frames.
 *
 * This command is used to send Wakeup frames.
 * Wakeup frames can only be sent while the LIN hardware is in Sleep mode.
 * If no parameters are given, the default values of the parameters are used.
 *
 * @param ttobrk
 *   This parameter specifies the time difference between the transmissions of two consecutive Wakeup frames,
 *   i.e. the time between end of one wakeup frame and start of the next one.
 *   Units of this parameter as well as default value depend on the hardware settings (see Hardware Configuration: LIN).
 *   Value range (for units expected in bit times): 20 .. 50000
 *   Value range (for units expected in ms): 1 .. 65536
 *
 * @param count
 *   Sets the number of Wakeup frame retransmissions.
 *   Value range: 1…255
 *   Default value depends on the hardware settings: see Hardware Configuration: LIN
 *
 * @param length
 *   This parameter sets the length of the wakeup frame to be sent in microseconds.
 *   The resolution is 50 µs.
 *   This parameter is only used by LIN2.x slave nodes.
 *   Value range: 250...5000 µs
 *   Default value depends on the hardware settings: see Hardware Configuration: LIN
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendWakeup(long ttobrk, long count, dword length);

}
