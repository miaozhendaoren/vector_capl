#include "linGetByteEndTime.h"

#include <iostream>

namespace capl
{

dword linGetByteEndTime(linMessage busEvent, long index)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetByteEndTime(linCsError busEvent, long index)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetByteEndTime(linReceiveError busEvent, long index)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
