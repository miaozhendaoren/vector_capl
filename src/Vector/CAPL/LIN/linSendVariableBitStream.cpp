#include "linSendVariableBitStream.h"

#include <iostream>

namespace capl
{

long linSendVariableBitStream(byte * dataBuffer, int64 * lengthInNS, dword numberOfBits, dword roundUp)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSendVariableBitStream(byte * dataBuffer, int64 * lengthInNS, dword numberOfBits, dword roundUp, dword timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
