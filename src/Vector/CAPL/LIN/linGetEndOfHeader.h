#pragma once

#include "../DataTypes.h"
#include "../LIN/LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a time stamp of the header part for a certain LIN bus event.
 *
 * @deprecated
 *   Replaced by linMessage selectors
 *
 * This function can be used to retrieve a time stamp of the header part for a certain LIN bus event.
 * The resulting time stamp is a time elapsed since measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type frame.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetEndOfHeader(linMessage busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a time stamp of the header part for a certain LIN bus event.
 *
 * @deprecated
 *   Replaced by linCsError selectors
 *
 * This function can be used to retrieve a time stamp of the header part for a certain LIN bus event.
 * The resulting time stamp is a time elapsed since measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type checksum error.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetEndOfHeader(linCsError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a time stamp of the header part for a certain LIN bus event.
 *
 * @deprecated
 *   Replaced by linReceiveError selectors
 *
 * This function can be used to retrieve a time stamp of the header part for a certain LIN bus event.
 * The resulting time stamp is a time elapsed since measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type receive error.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetEndOfHeader(linReceiveError busEvent);

}
