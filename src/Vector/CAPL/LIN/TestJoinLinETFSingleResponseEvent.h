#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Adds an event of type LIN event-triggered Frame with a single response for the specified associated frame to the set of joined events.
 *
 * @todo
 */

}
