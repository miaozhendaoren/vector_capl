#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit in the next LIN header.
 *
 * Inverts the specified bit in the next LIN header.
 *
 * @param byteIndex
 *   Index of the byte.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertHeaderBit(dword byteIndex, dword bitIndex);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit in the next LIN header.
 *
 * Inverts the specified bit in the next LIN header.
 *
 * @param byteIndex
 *   Index of the byte.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit in the next LIN header.
 *
 * Inverts the specified bit in the next LIN header.
 *
 * @param byteIndex
 *   Index of the byte.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param numberOfExecutions
 *   The number of consecutive headers in which the bit inversion will be executed.
 *   Default: 1 (single shot).
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit in the next LIN header.
 *
 * Inverts the specified bit in the next LIN header.
 *
 * @param byteIndex
 *   Index of the byte.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param numberOfExecutions
 *   The number of consecutive headers in which the bit inversion will be executed.
 *   Default: 1 (single shot).
 *
 * @param reportFallingEdges
 *   Flag indicating whether time stamps of the falling edges in the resulting pseudo-byte have to be reported.
 *   The time stamps can be retrieved by calling linGetFallingEdgesOfDisturbedByte.
 *   - 0: Deactivate report
 *   - 1: Activate report
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions, dword reportFallingEdges);

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the specified bit in the next LIN header.
 *
 * Inverts the specified bit in the next LIN header.
 *
 * @param byteIndex
 *   Index of the byte.
 *   - -1: Sync Break
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   The index of the bit to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Since the bit index is based on a UART-byte, index 0 will not disturb the very first bit (i.e. the start bit) but the first data bit.
 *   Note that it is technically impossible to disturb the first bit of a dominant signal since by the time the falling edge has been seen,
 *   it is already to late to disturb that bit (i.e. you cannot undo the falling edge).
 *   This type of indexing means that in order to disturb the first bit in the sync delimiter you have to use the index length of sync break - 1.
 *   I.e. in case of an 18 bit break, index 17 will disturb the first bit in the sync delimiter.
 *   Value range: 0..255
 *
 * @param level
 *   Level of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param numberOfExecutions
 *   The number of consecutive headers in which the bit inversion will be executed.
 *   Default: 1 (single shot).
 *
 * @param reportFallingEdges
 *   Flag indicating whether time stamps of the falling edges in the resulting pseudo-byte have to be reported.
 *   The time stamps can be retrieved by calling linGetFallingEdgesOfDisturbedByte.
 *   - 0: Deactivate report
 *   - 1: Activate report
 *
 * @param disturbAfterHeaderID
 *   With this parameter and the next one it is possible to define exactly which header will be disturbed.
 *   The LIN hardware will first wait for a header with ID = disturbAfterHeaderID before additionally awaiting the number of headers defined by waitForHeaders.
 *   The next header following these headers will then be disturbed.
 *   For example: To disturb the next header directly after a header with the ID=5, set the disturbAfterHeaderID parameter to 5 and the waitForHeaders to 0.
 *
 * @param waitForHeaders
 *   See explanation for disturbAfterHeaderID.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions, dword reportFallingEdges, long disturbAfterHeaderID, dword waitForHeaders);

}
