#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Checks the data indication bits of all slave nodes defined in the LIN network.
 *
 * This function checks the data indication bits of all slave nodes defined in the LIN network (the channel is determined by the CAPL program context).
 *
 * @return
 *   Returns non-zero if all queried data indication bits are set, otherwise zero.
 */
long linCheckOEMDataInd();

}
