#pragma once

#include "../DataTypes.h"
#include "LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Applies frame header on the bus or reconfigures response data of LIN frame.
 *
 * This function can be used for two purposes:
 * - To apply frame header on the bus, i.e. to transmit the frame.
 *   In that case RTR selector has to be set to 1.
 * - To reconfigure response data of LIN frame. In that case RTR selector has to be set to 0.
 *   The LIN hardware responds to the next request of the specified frame with the newly configured data.
 * - If working without a database it is necessary to configure a message in the handler on prestart.
 *   The configuration is done via the output function.
 *
 * @param msg
 *   Variable of type linmessage.
 *   List of available selectors for this type of objects can be found under linMessage selectors.
 */
void output(linMessage msg);

}
