#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets the NAD of the modeled Slave node.
 *
 * This function resets the NAD (Node Address for Diagnostic) of the modeled Slave node and marks all its protected identifiers as invalid or
 * it will load the last saved configuration if resetToLastConfiguration is set.
 *
 * See Notes to the way initial NAD is determined to understand how initial NAD is determined in CANoe.
 *
 * @return
 *   On success this function returns a value unequal to -1, otherwise -1.
 */
long linResetSlave();

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets the NAD of the modeled Slave node.
 *
 * This function resets the NAD (Node Address for Diagnostic) of the modeled Slave node and marks all its protected identifiers as invalid or
 * it will load the last saved configuration if resetToLastConfiguration is set.
 *
 * See Notes to the way initial NAD is determined to understand how initial NAD is determined in CANoe.
 *
 * @param resetToLastConfiguration
 *   - 0: perform a complete reset
 *   - non-zero: load last saved configuration
 *
 * @return
 *   On success this function returns a value unequal to -1, otherwise -1.
 */
long linResetSlave(dword resetToLastConfiguration);

}
