#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Terminates the LIN initialization procedure and starts transmission of initialization data to the hardware.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
