#include "linInvertMultipleHeaderBits.h"

#include <iostream>

namespace capl
{

dword linInvertMultipleHeaderBits(dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertMultipleHeaderBits(dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize, dword numberOfExecutions)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertMultipleHeaderBits(dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize, dword numberOfExecutions, long disturbAfterHeaderID, dword waitForHeaders)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
