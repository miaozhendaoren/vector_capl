#include "linSetBaudrate.h"

#include <iostream>

namespace capl
{

void linSetBaudrate(long baudrate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void linSetBaudrate(long minBaudrate, long maxBaudrate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
