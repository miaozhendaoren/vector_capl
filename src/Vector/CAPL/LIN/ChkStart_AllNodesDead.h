#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   This check reports a problem, if the node has not send any of its TX messages within a given time-interval.
 *
 * @todo
 */

}
