#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Causes collisions for next coming header(s) of an event-triggered frame.
 *
 * This function can be used to cause collisions for next coming header(s) of an event-triggered frame.
 *
 * @param etfId
 *   LIN frame identifier in the range 0 .. 59
 *
 * @param respLength
 *   Number of databytes to be sent as the response on the header of the event-triggered frame.
 *   Value range: 1..9. Default: 1
 *
 * @param numCollisions
 *   Number of times the specified response has to be applied.
 *   Value range: 1..255. Default: 1
 *
 * @param dataBytes
 *   Array of databytes to be sent as the response.
 *   The size of the array shall correspond to the respLength parameter.
 *   Default: NULL
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSimulateETFCollision(dword etfId, dword respLength = 1, dword numCollisions = 1, byte * dataBytes = nullptr);

}
