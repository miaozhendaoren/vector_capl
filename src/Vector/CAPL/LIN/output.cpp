#include "output.h"

#include <iostream>

namespace capl
{

void output(linMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
