#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends a slave response header.
 *
 * Sends a slave response header with the start bit of the id byte set to the specified length.
 *
 * @param startBitLengthInMicSec
 *   The length of the start bit in microseconds.
 *   The first data bit will be adjusted to ensure that the start bit and the first data bit have a total length of two bit times.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendSamplingTestHeader(dword startBitLengthInMicSec);

}
