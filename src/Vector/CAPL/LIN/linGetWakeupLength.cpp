#include "linGetWakeupLength.h"

#include <iostream>

namespace capl
{

int linGetWakeupLength(linWakeupFrame wakeupFrame)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
