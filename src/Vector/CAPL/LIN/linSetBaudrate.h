#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes the baud rate during the measurement.
 *
 * With this function it is possible to change the baud rate during the measurement.
 *
 * It is also possible to activate automatic baud rate detection in a specified range.
 *
 * @param baudrate
 *   Baudrate to be set [in bit/sec].
 *   Value range: 200 Baud – 30500 Baud
 */
void linSetBaudrate(long baudrate);

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes the baud rate during the measurement.
 *
 * With this function it is possible to change the baud rate during the measurement.
 *
 * It is also possible to activate automatic baud rate detection in a specified range.
 *
 * @param minBaudrate
 *   The lower border of the automatic baud rate detection range.
 *   Value range: 200 Baud – maxBaudrate
 *
 * @param maxBaudrate
 *   The upper border of the automatic baud rate detection range.
 *   Value range: minBaudrate – 30500 Baud
 */
void linSetBaudrate(long minBaudrate, long maxBaudrate);

}
