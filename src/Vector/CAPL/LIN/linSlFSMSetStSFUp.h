#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Causes the Slave to respond to the transmit request for the message.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
