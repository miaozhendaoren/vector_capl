#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Returns the dominant timeout of the current channel's transceiver in nanoseconds.
 *
 * Returns the dominant timeout of the current channel’s transceiver in nanoseconds.
 * If zero, the transceiver does not have any dominant timeout.
 *
 * @return
 *   Returns the dominant timeout of the LINcab/LINpiggy or zero, if the transceiver does not have any dominant timeout.
 */
int64 linGetDominantTimeout();

}
