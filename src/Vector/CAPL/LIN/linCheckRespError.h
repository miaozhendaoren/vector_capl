#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Queries the response_error flags of all Slave nodes defined in the LIN network.
 *
 * Queries the response_error flags of all Slave nodes defined in the LIN network.
 *
 * @return
 *   Returns one if at least one of the Slave nodes has the response_error flag set, otherwise zero.
 */
long linCheckRespError();

}
