#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Returns the currently set bus idle timeout.
 *
 * This function returns the currently set bus idle timeout.
 * Depending on the protocol version, the timeout will be specified in bit times (LIN 1.x and Cooling) or in milliseconds (all others).
 *
 * @return
 *   On success, returns the bus idle timeout, otherwise zero.
 */
dword linGetBusIdleTimeout();

}
