#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the inter-byte spaces for all data byte fields of all published frames of the calling LIN Slave node.
 *
 * With the first function it is possible to set the inter-byte spaces [in bit times] for all data byte fields of all published frames (except for diagnostic frame 0x3D) of the calling LIN Slave node.
 *
 * With the second function it is possible to set the inter-byte spaces [in bit times] for all data byte fields of a specified frame.
 *
 * Inter-byte space is the period between the end of the stop bit of the preceding byte and the start bit of the following byte.
 *
 * By default the inter-byte space between all byte fields is 0.
 *
 * @param arrayOfSixteenthBits
 *   Array of 9 elements for inter-byte space lengths, when the length of the inter-byte space is in units of 1/16th of bit time.
 *   The first element specifies the inter-byte space before the first data byte.
 *   It continues in the same manner until Checksum byte. All not used elements must be set to 0.
 *   Value range for inter-byte lengthes: 0..65535. This corresponds to 0..4095.93 bit times.
 *   For a LIN standard compliance: The maximum space between the bytes cannot exceed 40% duration compared to nominal transmission time.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetInterByteSpaces(dword * arrayOfSixteenthBits);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the inter-byte spaces for all data byte fields of all published frames of the calling LIN Slave node.
 *
 * With the first function it is possible to set the inter-byte spaces [in bit times] for all data byte fields of all published frames (except for diagnostic frame 0x3D) of the calling LIN Slave node.
 *
 * With the second function it is possible to set the inter-byte spaces [in bit times] for all data byte fields of a specified frame.
 *
 * Inter-byte space is the period between the end of the stop bit of the preceding byte and the start bit of the following byte.
 *
 * By default the inter-byte space between all byte fields is 0.
 *
 * @param frameID
 *   LIN frame identifier whose inter-byte space should be changed
 *   Value range: 0 .. 63
 *
 * @param arrayOfSixteenthBits
 *   Array of 9 elements for inter-byte space lengths, when the length of the inter-byte space is in units of 1/16th of bit time.
 *   The first element specifies the inter-byte space before the first data byte.
 *   It continues in the same manner until Checksum byte. All not used elements must be set to 0.
 *   Value range for inter-byte lengthes: 0..65535. This corresponds to 0..4095.93 bit times.
 *   For a LIN standard compliance: The maximum space between the bytes cannot exceed 40% duration compared to nominal transmission time.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetInterByteSpaces(long frameID, dword * arrayOfSixteenthBits);

}
