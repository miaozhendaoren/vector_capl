#include "linETFSendOnSignalUpdate.h"

#include <iostream>

namespace capl
{

long linETFSendOnSignalUpdate(long etfId, long active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
