#pragma once

#include "../DataTypes.h"
#include "../LIN/LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a length of an occurred Wakeup frame.
 *
 * @deprecated
 *   Replaced by linWakeupFrame selectors
 *
 * With this function it is possible to retrieve a length of an occurred Wakeup frame.
 *
 * @param wakeupFrame
 *   Bus-event of type LIN Wakeup frame.
 *
 * @return
 *   Returns the retrieved length [in units of 10 µsec] or zero on failure.
 */
int linGetWakeupLength(linWakeupFrame wakeupFrame);

}
