#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes the inter-byte space for all frame responses.
 *
 * With this function it is possible to change the inter-byte space for all frame responses.
 * Inter-byte space is the period between the end of the stop bit of the preceding byte and the start bit of the following byte.
 *
 * By default the inter-byte space is 0.
 *
 * @param sixteenthBits
 *   Length of the inter-byte space to set [in units of 1/16th of bit time].
 *   Value range: 0..65535. This corresponds to 0..4095.93 bit times.
 *   For a LIN standard compliance: The maximum space between the bytes cannot exceed 40% duration compared to nominal transmission time.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetGlobalInterByteSpace(dword sixteenthBits);

}
