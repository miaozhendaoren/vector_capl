#include "linDisturbRespWithHeader.h"

#include <iostream>

namespace capl
{

dword linDisturbRespWithHeader(long disturbedFrameId, dword byteIndex, dword bitIndex, long disturbingFrameId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
