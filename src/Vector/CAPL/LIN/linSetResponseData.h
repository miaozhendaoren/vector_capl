#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the response data for a transmit request.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
