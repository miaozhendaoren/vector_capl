#include "linResetRespBitStream.h"

#include <iostream>

namespace capl
{

long linResetRespBitStream(long frameId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
