#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates or deactivates the Master mode and the terminating resistor on the LIN hardware.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
