#include "linSetBusIdleTimeout.h"

#include <iostream>

namespace capl
{

dword linSetBusIdleTimeout(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
