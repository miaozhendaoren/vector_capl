#include "linCheckOEMDataInd.h"

#include <iostream>

namespace capl
{

long linCheckOEMDataInd()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
