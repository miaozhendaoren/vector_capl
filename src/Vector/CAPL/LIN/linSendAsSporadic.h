#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Configures an associated frame as being ready for transmission.
 *
 * This function can be used to configure an associated frame as being ready for transmission.
 * At the next time slot, corresponding to its sporadic frame, the associated frame will be sent once.
 *
 * @param frameID
 *   Identifier of the associated LIN frame to be transmitted in the next time slot of a corresponding sporadic frame.
 *   Value range: 0..59, 62
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendAsSporadic(long frameID);

}
