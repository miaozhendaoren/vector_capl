#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Increases/decreases the baud rate detection range.
 *
 * With this function the baud rate detection range can be increased or decreased.
 * The default range is +/- 15%. Note that the detection range will always be symmetrical.
 *
 * @param rangeInPercent
 *   The detection range in percent.
 *   Value range: 0-30
 *   Default: 15
 *
 * @return
 *   If successful a value unequal to zero.
 */
long linSetBaudrateDetectionRange(long rangeInPercent = 15);

}
