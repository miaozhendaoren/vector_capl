#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the time in milliseconds after which a simulated slave automatically sets its data indication bit.
 *
 * Sets the time in milliseconds after which a simulated slave automatically sets its data indication bit.
 *
 * @param timeInMS
 *   Time value.
 *   Unit: milliseconds
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetOEMDataIndTime(long timeInMS);

}
