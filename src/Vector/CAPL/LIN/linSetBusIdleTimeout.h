#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets a new bus idle timeout.
 *
 * This function sets a new bus idle timeout.
 * Depending on the protocol version, the timeout will be specified in bit times (LIN 1.x and Cooling) or in milliseconds (all others).
 *
 * @param timeout
 *   The timeout in bit times (LIN 1.x and Cooling) or milliseconds (all other LIN protocols).
 *   Value range: 1 - 32767
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetBusIdleTimeout(dword timeout);

}
