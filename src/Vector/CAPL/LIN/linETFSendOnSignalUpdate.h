#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates/deactivates the automatic responses on a certain event-triggered frame request in the case of a signal update on its associated frame(s).
 *
 * With this function it is possible to activate/deactivate the automatic responses on a certain event-triggered frame request in the case of a signal update on its associated frame(s).
 * This concerns all Slave tasks publishing the responses to the specified event-triggered frame.
 *
 * @param etfId
 *   Identifier of event-triggered frame.
 *   Value range: 0-59
 *
 * @param active
 *   - 1: Activate
 *   - 0: Deactivate
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linETFSendOnSignalUpdate(long etfId, long active);

}
