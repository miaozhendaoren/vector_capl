#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Configures the LIN hardware to disturb the next header with a new header.
 *
 * Configures the LIN hardware to disturb the next header with a new header (id=disturbingFrameId).
 *
 * @param byteIndex
 *   Starts disturbance in byte with index byteIndex.
 *   - 0: Sync Byte
 *   - 1: ProtectedId Byte
 *
 * @param bitIndex
 *   Starts disturbance at bit position bitIndex.
 *   An index in the range 0-7 specifies a data bit, while the index 8 specifies the stop bit.
 *   Higher index values specify the interbyte-space after the indexed data byte.
 *   In which case, the user should make sure that the interbyte space is large enough.
 *   Value range: 0..255
 *
 * @param disturbingFrameId
 *   Frame ID to be used for the header disturbance.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linDisturbHeaderWithHeader(dword byteIndex, dword bitIndex, long disturbingFrameId);

}
