#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates or deactivates the Master node's automatic collision resolution of an event-triggered frame.
 *
 * Activates or deactivates the Master node's automatic collision resolution of an event-triggered frame.
 *
 * Per default the automatic collision resolution is active. If a collision occurs for any event-triggered frame,
 * the Master resolves this collision by sending headers for all associated frames using the event-triggered frame slot.
 * After all associated frames have sent new data, the Master sends the event-triggered frame header until the next collision occurs.
 *
 * If the automatic collision resolution is deactivated, the Master always sends the event-triggered frame's header.
 *
 * @param etfId
 *   Identifier of event-triggered frame.
 *   Value range: 0-59
 *
 * @param activate
 *   - 0: Disable
 *   - 1: Enable
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linActivateCollisionResolution(long etfId, long activate);

}
