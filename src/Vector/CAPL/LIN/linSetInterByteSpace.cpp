#include "linSetInterByteSpace.h"

#include <iostream>

namespace capl
{

dword linSetInterByteSpace(long frameID, long index, dword sixteenthBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
