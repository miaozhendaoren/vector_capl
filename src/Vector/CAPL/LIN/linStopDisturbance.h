#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Stops the disturbance on the bus.
 *
 * Stops the disturbance on the bus which is started with linStartDisturbance().
 *
 * Note that disturbances shorter than 64 bit times cannot be stopped.
 *
 * @return
 *   On success a value unequal to zero, otherwise zero.
 */
long linStopDisturbance();

}
