#pragma once

#include "../DataTypes.h"
#include "../LIN/LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a data byte time stamp of a certain LIN bus event.
 *
 * @deprecated
 *   Replaced by linMessage selectors
 *
 * This function can be used to retrieve a data byte time stamp of a certain LIN bus event.
 * The resulting time stamp is a time elapsed since measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type frame.
 *
 * @param index
 *   Data byte index.
 *   Value range: 1..N, where N = data bytes count of the current bus event
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetByteEndTime(linMessage busEvent, long index);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a data byte time stamp of a certain LIN bus event.
 *
 * @deprecated
 *   Replaced by linCsError selectors
 *
 * This function can be used to retrieve a data byte time stamp of a certain LIN bus event.
 * The resulting time stamp is a time elapsed since measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type checksum error.
 *
 * @param index
 *   Data byte index.
 *   Value range: 1..N, where N = data bytes count of the current bus event
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetByteEndTime(linCsError busEvent, long index);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a data byte time stamp of a certain LIN bus event.
 *
 * @deprecated
 *   Replaced by linReceiveError selectors
 *
 * This function can be used to retrieve a data byte time stamp of a certain LIN bus event.
 * The resulting time stamp is a time elapsed since measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type receive error.
 *
 * @param index
 *   Data byte index.
 *   Value range: 1..N, where N = data bytes count of the current bus event
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetByteEndTime(linReceiveError busEvent, long index);

}
