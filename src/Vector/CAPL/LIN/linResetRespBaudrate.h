#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets a response baud rate for a specified frame to the master baud rate.
 *
 * Resets a response baud rate for a specified frame to the master baud rate.
 *
 * @param frameId
 *   The identifier of the frame for which the response baud rate will be reset.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linResetRespBaudrate(long frameId);

}
