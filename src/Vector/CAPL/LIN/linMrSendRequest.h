#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends a transmit request on the LIN bus for the specified LIN frame identifer.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
