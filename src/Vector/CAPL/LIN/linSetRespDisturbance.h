#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates a disturbance in the response space of the specified LIN frame.
 *
 * Activates a disturbance in the response space of the specified LIN frame.
 * A normal response by a simulated slave will be deactivated as long as the disturbance is active.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63.
 *
 * @param lengthInBits
 *   The duration of the disturbance [in bit times].
 *
 * @param level
 *   The level of the disturbance.
 *   - 0: dominant disturbance  (inverts recessive bits)
 *   - 1: recessive disturbance (inverts dominant bits - requires mag-Cab/Piggy and external power supply)
 *
 * @param offsetInSixteenthBits
 *   - The offset [in 1/16th bits] of the disturbance relative to the end of the header, i.e. to the PID byte.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespDisturbance (long frameId, long lengthInBits, long level, long offsetInSixteenthBits);

}
