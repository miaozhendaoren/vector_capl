#include "linActivateCollisionResolution.h"

#include <iostream>

namespace capl
{

long linActivateCollisionResolution(long etfId, long activate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
