#include "linSetRespBaudrate.h"

#include <iostream>

namespace capl
{

long linSetRespBaudrate(long frameId, long baudrate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
