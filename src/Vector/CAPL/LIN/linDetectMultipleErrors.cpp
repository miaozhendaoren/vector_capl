#include "linDetectMultipleErrors.h"

#include <iostream>

namespace capl
{

long linDetectMultipleErrors(long activate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
