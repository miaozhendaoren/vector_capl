#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Defines trigger conditions for an oscilloscope.
 *
 * With these functions it is possible to define trigger conditions for an oscilloscope.
 * If the trigger conditions are satisfied, the LIN hardware will send an impulse on the sync cable.
 *
 * The first function defines a single trigger condition for a valid frame or an error in a response.
 *
 * The second function defines a set of trigger conditions.
 *
 * @param ID
 *   Identifier of the frame to be triggered on. The parameter is only evaluated for trigger conditions after the ID field.
 *
 * @param Position
 *   - 0: Trigger on end of header
 *   - 1-9: Trigger on databyte 1 - 8 or checksum (5 will trigger on the checksum if the length of the frame is 4)
 *   - 10: Trigger on detection of receive error in response
 *   - 11: Trigger on detection of checksum error
 *   - 12: Trigger on detection of transmission error
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetScopeTrigger(long ID, long Position);

/**
 * @ingroup LIN
 *
 * @brief
 *   Defines trigger conditions for an oscilloscope.
 *
 * With these functions it is possible to define trigger conditions for an oscilloscope.
 * If the trigger conditions are satisfied, the LIN hardware will send an impulse on the sync cable.
 *
 * The first function defines a single trigger condition for a valid frame or an error in a response.
 *
 * The second function defines a set of trigger conditions.
 *
 * @param eventCount
 *   Number of occurred/fulfilled trigger conditions, which activate the triggering.
 *   E.g. a number of 5 will activate the triggering, after the trigger condition was fulfilled 5 times.
 *   A number of 0 is not allowed.
 *
 * @param triggerCount
 *   Number n, how often the trigger will be activated.
 *   For every trigger activation the number of fulfilled trigger conditions (EventCount) has been occurred before,
 *   i.e. the triggering starts after 1 time of EventCount, 2 times of EventCount...n times of EventCount.
 *   If the triggerCount is 0, the trigger will run indefinitely.
 *
 * @param triggerMask
 *   Definition of trigger conditions.
 *   Meaning of the bits in the mask:
 *   - 0: Sync break detection (about 9.6 bit times after start of sync break)
 *   - 1: Start of sync delimiter
 *   - 2: Start of sync field (beginning of start bit if sync break could not have been a 2.0 wakeup frame, end of start bit otherwise)
 *   - 3: End of sync field
 *   - 4: Start of lin identifie
 *   - 5: End of lin identifier
 *   - 6: End of any data or checksum byte
 *   - 7-15: End of data byte 1 - 8 or checksum byte (depending on dlc)
 *   - 16: Detection of sync error
 *   - 17: Detection of receive error in header/bus idle phase/sleep mode
 *   - 18: Detection of receive error in response
 *   - 19: Detection of checksum error
 *   - 20: Detection of transmission error
 *   - 21: Detection of sleep mode frame
 *   - 22: Detection of sleep mode event
 *   - 23: Detection of wakeup frame
 *
 * @param idMask
 *   An array specifying the LIN identifiers on which to trigger.
 *   Bit 0 in byte 0 specifies id 0, bit 0 in byte 1 specifies id 8, bit 3 in byte 5 specifies id 5 * 8 + 3 = 43, etc.
 *   The parameter is only evaluated for trigger conditions after the ID field.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetScopeTrigger(long eventCount, long triggerCount, long triggerMask, byte * idMask);

}
