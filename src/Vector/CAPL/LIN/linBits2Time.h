#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified bit time to an absolute time.
 *
 * @deprecated
 *   Replaced by linBits2Time_ns
 *
 * Converts specified bit time to an absolute time.
 *
 * The absolute time is calculated using the current baud rate on the channel determined by the CAPL program context.
 *
 * @param bitTimes
 *   Time in bits.
 *
 * @return
 *   Absolute time in 10 µs.
 */
dword linBits2Time(dword bitTimes);

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified bit time to an absolute time.
 *
 * @deprecated
 *   Replaced by linBits2Time_ns
 *
 * Converts specified bit time to an absolute time.
 *
 * The absolute time is calculated using the current baud rate on the channel determined by the CAPL program context.
 *
 * @param bitTimes
 *   Time in bits.
 *
 * @param channel
 *   Channel number, whose baud rate will be used.
 *
 * @return
 *   Absolute time in 10 µs.
 */
dword linBits2Time(dword channel, dword bitTimes);

}
