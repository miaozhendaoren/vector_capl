#include "linMeasEdgeTimeDiffs.h"

#include <iostream>

namespace capl
{

dword linMeasEdgeTimeDiffs(dword numIndices, long * indices)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linMeasEdgeTimeDiffs(dword numIndices, long * indices, dword id)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
