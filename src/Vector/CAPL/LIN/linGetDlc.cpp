#include "linGetDlc.h"

#include <iostream>

namespace capl
{

long linGetDlc(long frameID)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
