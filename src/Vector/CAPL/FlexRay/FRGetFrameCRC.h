#pragma once

#include "../DataTypes.h"
#include "FlexRay.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Returns the CRC of a received FlexRay frame.
 *
 * This function returns the CRC of a received FlexRay frame.
 *
 * The Header CRC can be determined with a special selector of the event procedure.
 *
 * @param _this
 *   The function can only be used in the context of the following event procedures:
 *     - on FRSlot
 *     - on FRFrame
 *     - on FRNullFrame
 *     - on FRFrameError
 *   this references the corresponding receive object in the event procedure.
 *
 * @return
 *   Frame CRC
 *   The return value is only valid if the frame was received by a Vector FlexRay hardware interface in asynchronous monitor mode.
 *   In any other case 0 will be sent back.
 */
dword FrGetFrameCRC(FRSlot * _this);

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Returns the CRC of a received FlexRay frame.
 *
 * This function returns the CRC of a received FlexRay frame.
 *
 * The Header CRC can be determined with a special selector of the event procedure.
 *
 * @param _this
 *   The function can only be used in the context of the following event procedures:
 *     - on FRSlot
 *     - on FRFrame
 *     - on FRNullFrame
 *     - on FRFrameError
 *   this references the corresponding receive object in the event procedure.
 *
 * @return
 *   Frame CRC
 *   The return value is only valid if the frame was received by a Vector FlexRay hardware interface in asynchronous monitor mode.
 *   In any other case 0 will be sent back.
 */
dword FrGetFrameCRC(FRFrame * _this);

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Returns the CRC of a received FlexRay frame.
 *
 * This function returns the CRC of a received FlexRay frame.
 *
 * The Header CRC can be determined with a special selector of the event procedure.
 *
 * @param _this
 *   The function can only be used in the context of the following event procedures:
 *     - on FRSlot
 *     - on FRFrame
 *     - on FRNullFrame
 *     - on FRFrameError
 *   this references the corresponding receive object in the event procedure.
 *
 * @return
 *   Frame CRC
 *   The return value is only valid if the frame was received by a Vector FlexRay hardware interface in asynchronous monitor mode.
 *   In any other case 0 will be sent back.
 */
dword FrGetFrameCRC(FRNullFrame * _this);

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Returns the CRC of a received FlexRay frame.
 *
 * This function returns the CRC of a received FlexRay frame.
 *
 * The Header CRC can be determined with a special selector of the event procedure.
 *
 * @param _this
 *   The function can only be used in the context of the following event procedures:
 *     - on FRSlot
 *     - on FRFrame
 *     - on FRNullFrame
 *     - on FRFrameError
 *   this references the corresponding receive object in the event procedure.
 *
 * @return
 *   Frame CRC
 *   The return value is only valid if the frame was received by a Vector FlexRay hardware interface in asynchronous monitor mode.
 *   In any other case 0 will be sent back.
 */
dword FrGetFrameCRC(FRFrameError * _this);

}
