#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Is called up in each cycle after the Slot is past.
 *
 * @todo
 */
class FRSlot
{
public:
};

}
