#include "FRGetFrameCRC.h"

#include <iostream>

namespace capl
{

dword FrGetFrameCRC(FRSlot * _this)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword FrGetFrameCRC(FRFrame * _this)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword FrGetFrameCRC(FRNullFrame * _this)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword FrGetFrameCRC(FRFrameError * _this)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
