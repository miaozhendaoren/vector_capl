#pragma once

#include "../DataTypes.h"
#include "FlexRay.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Updates the FlexRay Communication Controller's (CC) send buffer with the current data from the send object.
 *
 * This function updates the FlexRay Communication Controller's (CC) send buffer with the current data from the send object.
 * This corresponds to a request to send.
 *
 * Only frames in the dynamic segment can be sent using this function!
 *
 * @param frame
 *   Name of the variable referenced by the frame object.
 *   The variable name was defined when the object was created using FrFrame.
 *
 * @return
 *   - 0: Ok, the request to send the frame is forwarded to the interface.
 *        This does not guarantee that the frame is really been sent.
 *        Refer to the FlexRay protocol rules.
 *   - -1: Error, either the channel is not available or the slot ID is not in the dynamic segment.
 */
int FROutputDynFrame(FRFrame & frame);

}
