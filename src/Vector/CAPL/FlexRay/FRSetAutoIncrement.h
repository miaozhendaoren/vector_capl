#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Part of the payload of a frame will be incremented automatically on each transmission.
 *
 * Part of the payload of a frame will be incremented automatically on each transmission.
 * The number of bytes used can be set to 1, 2 or 4.
 *
 * The byte offset can also be set. The only format supported is Intel (Little Endian).
 *
 * @param channel
 *   Channel number (or cluster number)
 *
 * @param slotId
 *   Slot number of the frame used.
 *
 * @param channelMask
 *   FlexRay channel mask
 *   Possible values:
 *   - 1: Channel A
 *   - 2: Channel B
 *   - 3: Channel A+B
 *
 * @param cycleStart
 *   Cycle multiplexing parameter
 *
 * @param cycleRepetition
 *   Cycle multiplexing parameter
 *
 * @param flags
 *   - 1: Payload increment is switched on.
 *   - All other values are reserved and must not be used.
 *
 * @param increment_size
 *   Number of bits used for the increment.
 *   8, 16 and 32 are valid values.
 *
 * @param increment_offset
 *   Byte position after which the payload is incremented.
 *   Possible values:
 *   - increment_size=8: increment_offset=0,1,2,3...
 *   - increment_size=16: increment_offset=0,2,4,6,...
 *   - increment_size=32: increment_offset=0,4,8,...
 */
void FRSetAutoIncrement(int channel, int slotId, int channelMask, int cycleStart, int cycleRepetition, dword flags, int increment_size, int increment_offset);

}
