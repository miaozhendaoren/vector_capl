#include "FROutputDynFrame.h"

#include <iostream>

namespace capl
{

int FROutputDynFrame(FRFrame & frame)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
