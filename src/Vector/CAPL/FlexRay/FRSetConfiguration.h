#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Writes the FlexRay protocol parameters from the FlexRay parameter object to the FlexRay interface's Communication Controller.
 *
 * @todo
 */

}
