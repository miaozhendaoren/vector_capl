#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Initializes the FlexRay Communication Controller (CC) and begins a new startup phase for the cluster or a new integrations phase in the cluster.
 *
 * This function initializes the FlexRay Communication Controller (CC) and begins a new startup phase for the cluster or a new integrations phase in the cluster - depending on whether a startup frame is to be sent or not.
 *
 * @param channel
 *   FlexRay channel (cluster number).
 */
void ResetFlexRayCC(int channel);

}
