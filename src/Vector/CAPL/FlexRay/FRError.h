#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Is called in the event of a general error being detected on the FlexRay bus.
 *
 * The event procedure is called in the event of a general error being detected on the FlexRay bus.
 */
class FRError
{
public:
    /**
     * The error time stamp that has been synchronized with the global time base in the PC (CAN hardware or PC system clock).
     *
     * The time stamp must be used if time relations should be regarded with events from other sources. This time stamp is also output in the Trace window when receiving a symbol.
     *
     * Unit: 10 microseconds
     */
    dword Time;

    /**
     * Channel in the tool that the FlexRay CC determines.
     */
    dword msgChannel;

    /**
     * Identifies the type of the FlexRay hardware:
     *   - 0: HW independent
     *   - 1: Invalid
     *   - 2: FlexCard Cyclone I
     *   - 3: BusDoctor
     *   - 4: FlexCard Cyclone II
     *   - 5: Vector FlexRay hardware
     */
    dword FR_HWTag;

    /**
     * Identifies the error type
     *   - -1: Unknown error
     *   - 0: NO error
     *   - 1: FlexCard Overflow
     *   - 2: POC Error Mode Changed
     *   - 3: Sync Frames Below Minimum
     *   - 4: Sync Frame Overflow
     *   - 5: Clock Correction Failure
     *   - 6: Parity Error
     *   - 7: Receive FIFO Overrun
     *   - 8: Empty FIFO Access
     *   - 9: Illegal Input Buffer Access
     *   - 10: Illegal Output Buffer Access
     *   - 11: Syntax Error
     *   - 12: Content Error
     *   - 13: Slot Boundary Violation
     *   - 14: Transmission Across Boundary Channel A
     *   - 15: Transmission Across Boundary Channel B
     *   - 16: Latest Transmit Violation Channel A
     *   - 17: Latest Transmit Violation Channel B
     *   - 18: Error Detection on Channel A
     *   - 19: Error Detection on Channel B
     *   - 20: Message Handler Constraints Flag Error
     *   - 21: NIT SENA (Syntax Error during NIT Channel A)
     *   - 22: NIT SBNA (Slot Boundary Violation during NIT Channel A)
     *   - 23: NIT SENB (Syntax Error during NIT Channel B)
     *   - 24: NIT SBNB (Slot Boundary Violation during NIT Channel B)
     *   - 25: Internal Error Overflow
     *   - 26: Wrong Frame
     *   - 27: Bus Guardian Error
     *   - 28: CHI Error
     *   - 29: Error Handling Level Changed
     *   - 30: Symbol Received
     */
    long FR_Code;

    /**
     * Contains extended information:
     *
     * Not used.
     */
    dword FR_Data0;

    /**
     * Contains extended information: Not used as yet.
     */
    dword FR_Data1;

    /**
     * Contains extended information: Not used as yet.
     */
    dword FR_Data2;

    /**
     * Contains extended information: Not used as yet.
     */
    dword FR_Data3;

    /**
     * Contains extended information: Not used as yet.
     */
    dword FR_Data4;
};

}
