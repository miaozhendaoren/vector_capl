#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   If a valid FlexRay PDU is the last event that triggers a wait instruction, the PDU’s content can be called up.
 *
 * @todo
 */

}
