#include "FrEnableGateway.h"

#include <iostream>

namespace capl
{

long FrEnableGateway(long channelA, long channelB)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
