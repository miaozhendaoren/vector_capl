#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Generates a FlexRay frame and sends it to the hardware.
 *
 * @deprecated
 *
 * @todo
 */

}
