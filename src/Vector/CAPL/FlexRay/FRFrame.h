#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Creates a FlexRay frame object.
 *   Is called up after a frame has been received in the specified slot and cycle.
 *
 * @todo
 */
class FRFrame
{
public:
};

}
