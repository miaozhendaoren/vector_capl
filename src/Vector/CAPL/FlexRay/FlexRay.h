#pragma once

/**
 * @defgroup FlexRay FlexRay CAPL Functions
 */

/* Object */
#include "FRFrame.h"
#include "FRConfiguration.h"
#include "FrPDU.h"

/* Event Procedures */
#include "FRError.h"
#include "FRFrameError.h"
#include "FRNullFrame.h"
#include "FRPOCState.h"
#include "FRSlot.h"
#include "FRStartCycle.h"
#include "FRSymbol.h"

/* Callbacks */
#include "FlexRayRcvStatusEvent.h"

/* General Functions */
#include "FrEnableGateway.h"
#include "FRGetConfiguration.h"
#include "FRGetFrameCRC.h"
#include "FrGwBypassDynamic.h"
#include "FrGwBypassStatic.h"
#include "FROutputDynFrame.h"
#include "frResetStatistics.h"
#include "FRSendFrame.h"
#include "FRSendSymbol.h"
#include "FRSetAutoIncrement.h"
#include "FRSetConfiguration.h"
#include "FRSetKeySlot.h"
#include "FRSetMode.h"
#include "FRSetPayloadLengthInByte.h"
#include "FRSetPOCState.h"
#include "FRSetSendFrame.h"
#include "FRSetSendGroup.h"
#include "FRSetSendPDU.h"
#include "FRSetTrigger.h"
#include "FRUpdatePDU.h"
#include "FRUpdateStatFrame.h"
#include "MessageTimeNS.h"
#include "output.h"
#include "ResetFlexRayCC.h"
#include "ResetFlexRayCCEx.h"

/* Test Feature Set for FlexRay */
#include "TestWaitForFrFrame.h"
#include "TestGetWaitFrFrameData.h"
#include "TestJoinFrFrameEvent.h"
#include "TestWaitForFrNullFrame.h"
#include "TestGetWaitFrNullFrameData.h"
#include "TestJoinFrNullFrameEvent.h"
#include "TestWaitForFrStartCycle.h"
#include "TestGetWaitFrStartCycleData.h"
#include "TestJoinFrStartCycleEvent.h"
#include "TestWaitForFrPDU.h"
#include "TestGetWaitFrPDUData.h"
#include "TestJoinFrPDUEvent.h"
#include "TestWaitForFrPOCState.h"
#include "TestGetWaitFrPOCStateData.h"
#include "TestJoinFrPOCState.h"
#include "TestWaitForFrFrameError.h"
#include "TestGetWaitFrFrameErrorData.h"
#include "TestJoinFrFrameErrorEvent.h"
#include "TestWaitForFrSymbol.h"
#include "TestGetWaitFrSymbolData.h"
#include "TestJoinFrSymbolEvent.h"

/* Test Service Library for FlexRay */
// @todo TestCheck::

/* Obsolete Functions */
#include "FlexRayRcvV6Frame.h"
#include "FlexRayRcvV6StartCycle.h"
#include "FrSendMsg.h"
#include "FrSendV6Frame.h"
#include "FrSendV6Msg.h"
#include "FrSetSendMsg.h"
#include "FrSetSendV6Frame.h"
#include "FrSetSendV6Msg.h"
