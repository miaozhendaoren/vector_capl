#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Initializes the FlexRay bus transceivers.
 *
 * This function initializes the FlexRay bus drivers.
 * Essentially, it defines whether the drivers are set to normal mode or sleep mode.
 *
 * @param channel
 *   FlexRay channel (cluster number).
 *
 * @param channelMask
 *   Determines which bus driver is being programmed.
 *   Value:
 *   - 1: Channel A
 *   - 2: Channel B
 *   - 3: Channel A+B
 *
 * @param mode
 *   Defines the bus driver state:
 *   - 0x0000: Normal mode, when starting the Communication Controller a Wakeup will be sent if this is defined in the hardware configuration dialog.
 *   - 0x0001: Sleep mode
 *   - 0x0002: Normal mode, when starting the Communication Controller no Wakeup will be sent.
 */
void FRSetMode(int channel, int channelMask, dword mode);

}
