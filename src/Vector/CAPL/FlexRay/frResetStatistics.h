#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Resets FlexRay cluster statistics.
 *
 * Resets FlexRay cluster statistics. Both A and B channels are concerned.
 */
void frResetStatistics();

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Resets FlexRay cluster statistics.
 *
 * Resets FlexRay cluster statistics. Both A and B channels are concerned.
 *
 * @param channel
 *   FlexRay cluster number (1-based)
 */
void frResetStatistics(long channel);

}
