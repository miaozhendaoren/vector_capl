#include "FRSetPOCState.h"

#include <iostream>

namespace capl
{

long FrSetPOCState(long channel, long ccNumber, long pocState)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
