#include "FRSendSymbol.h"

#include <iostream>

namespace capl
{

void FRSendSymbol(long type, long param, int channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
