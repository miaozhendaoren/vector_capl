#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Is called if a erroneous frame is received in the specified slot and cycle.
 *
 * @todo
 */
class FRFrameError
{
public:
};

}
