#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief Create a FlexRay parameter object.
 *
 * Can be used to create a FlexRay parameter object. The data content of this object is all
 * protocol parameters relevant to FlexRay in the context of initializing a FlexRay
 * Communication Controller. The object data can be manipulated or read out by selectors
 * associated with this object.
 *
 * Initially, all protocol parameters are set to a value of 0.
 *
 * The FRGetConfiguration or FRSetConfiguration functions are used to read or set parameters
 * from or in the FlexRay interface's Communication Controller.
 */
class FRConfiguration
{
public:
    /**
     * Baud rate in kBit/s.
     */
    unsigned int FRBaudrate;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    double gdMacrotick;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gMacroPerCycle;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdNIT;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    double gdSampleClockPeriod;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdTSSTransmitter;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gPayloadLengthStatic;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdActionPointOffset;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdStaticSlot;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gNumberOfStaticSlots;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdMinislotActionPointOffset;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdMinislot;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gNumberOfMinislots;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gClusterDriftDamping;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gListenNoise;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gColdstartAttempts;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gSyncNodeMax;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gOffsetCorrectionStart;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdDynamicSlotIdlePhase;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdSymbolWindow;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdCASRxLowMax;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    //unsigned int gdCASRxLowMax;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdWakeupSymbolRxIdle;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdWakeupSymbolRxLow;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdWakeupSymbolRxWindow;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdWakeupSymbolTxIdle;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gdWakeupSymbolTxLow;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gMaxWithoutClockCorrectionFatal;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gMaxWithoutClockCorrectionPassive;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int gNetworkManagementVectorLength;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pChannels;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pMicroPerCycle;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pSamplesPerMicrotick;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pPayloadLengthDynMax;

    /**
     * Size of the maximum data length that can be received via the FIFO buffer in 16-bit words.
     */
    unsigned int pPayloadLengthFIFO;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pLatestTx;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pdMaxDrift;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pdAcceptedStartupRange;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pClusterDriftDamping;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pDecodingCorrection;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pDelayCompensation_A;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pDelayCompensation_B;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pOffsetCorrectionOut;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pRateCorrectionOut;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pExternOffsetCorrection;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pExternRateCorrection;

    /**
     * Defines the addition mode for the external rate and offset correction values:
     * - 0: Offset=Disable, Rate=Disable
     * - 1: Offset=Disable, Rate=Disable
     * - 2: Offset=Disable, Rate=-
     * - 3: Offset=Disable, Rate=*
     * - 4: Offset=Disable, Rate=Disable
     * - 5: Offset=Disable, Rate=Disable
     * - 6: Offset=Disable, Rate=-
     * - 7: Offset=Disable, Rate=*
     * - 8: Offset=-, Rate=Disable
     * - 9: Offset=-, Rate=Disable
     * - 10: Offset=-, Rate=-
     * - 11: Offset=-, Rate=*
     * - 12: Offset=*, Rate=Disable
     * - 13: Offset=*, Rate=Disable
     * - 14: Offset=*, Rate=-
     * - 15: Offset=*, Rate=*
     *
     * Possible values are: 0, 5, 10, 11, 14 and 15.
     */
    unsigned int pExternCorrectionMode;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pMacroInitialOffset_A;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pMacroInitialOffset_B;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pMicroInitialOffset_A;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pMicroInitialOffset_B;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pWakeupChannel;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pWakeupPattern;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pAllowHaltDueToClock;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pAllowPassiveToActive;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pBGTick;

    /**
     * Defines the physical layer to be used:
     * - 0: RS485_0
     * - 1: RS485_1
     * - 2: FlexRay Electrical
     */
    unsigned int pPhysicalLayer;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pSingleSlotEnabled;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pBGEnable;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pDynamicSegmentEnable;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     */
    unsigned int pChannelsMTS;

    /**
     * Indicates the selected FlexRay interface type:
     * - 2: BusDoctor
     * - 3: FlexCard Cyclone
     * - 4: FlexCard Cyclone II
     * - 5: VN
     *
     * Write protected!
     */
    unsigned int pCCVersion;

    /**
     * E-Ray parameter. See E-Ray specification.
     */
    unsigned int pStrobePointPosition;

    /**
     * FlexRay protocol parameter. See FlexRay specification.
     *
     * In function FRSetConfiguration this parameter is ignored!
     */
    double pdMicrotick;
};

}
