#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   A function defined in CAPL with this signature receives all FlexRay StartCycle events.
 *
 * @deprecated
 *
 * @todo
 */

}
