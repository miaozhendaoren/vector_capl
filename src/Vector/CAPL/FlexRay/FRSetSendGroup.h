#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   All frames in the group are sent in the current cycle, or all frames are not sent until the next possible cycle.
 *
 * @todo
 */

}
