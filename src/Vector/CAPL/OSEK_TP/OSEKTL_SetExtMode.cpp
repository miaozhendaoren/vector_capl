#include "OSEKTL_SetExtMode.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetExtMode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
