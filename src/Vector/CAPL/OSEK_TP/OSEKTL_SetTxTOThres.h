#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the Ar/As timeout threshold value [ms].
 */
void OSEKTL_SetTxTOThres(dword threshold_ms);

}
