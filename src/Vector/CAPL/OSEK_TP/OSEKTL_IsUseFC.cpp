#include "OSEKTL_IsUseFC.h"

#include <iostream>

namespace capl
{

long OSEKTL_IsUseFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
