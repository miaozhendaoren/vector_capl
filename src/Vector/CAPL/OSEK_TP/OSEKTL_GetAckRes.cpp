#include "OSEKTL_GetAckRes.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetAckRes(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
