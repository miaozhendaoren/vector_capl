#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the maximum acceptable length (<= 4095 byte) of a
 *   TP message. The reception of longer messages will be
 *   aborted by sending an overflow (FC.OVFLW) frame.
 */
void OSEKTL_SetMaxMsgLen(long len);

}
