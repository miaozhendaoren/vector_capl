#include "OSEKTL_SetAckRes.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetAckRes(dword result)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
