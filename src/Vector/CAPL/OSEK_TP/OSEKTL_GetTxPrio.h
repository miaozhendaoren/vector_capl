#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   reads the priority of transmission in normal fixed and
 *   mixed addressing mode
 */
long OSEKTL_GetTxPrio(void);

}
