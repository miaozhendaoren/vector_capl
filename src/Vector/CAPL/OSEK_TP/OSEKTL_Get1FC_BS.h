#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the BS with the special meaning explained
 *   above.
 */
byte OSEKTL_Get1FC_BS(void);

}
