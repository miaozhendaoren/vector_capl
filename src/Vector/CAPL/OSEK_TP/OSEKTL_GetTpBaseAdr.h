#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active base address
 */
long OSEKTL_GetTpBaseAdr(void);

}
