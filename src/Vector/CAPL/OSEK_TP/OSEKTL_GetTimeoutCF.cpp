#include "OSEKTL_GetTimeoutCF.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutCF(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
