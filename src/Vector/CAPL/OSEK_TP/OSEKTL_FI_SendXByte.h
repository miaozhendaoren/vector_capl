#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Abort transmission after the specified
 *   number of bytes has been sent.
 *   See below for more detailed information.
 */
void OSEKTL_FI_SendXByte(
        long NumberOfBytesToSendTillStop,
        unsigned long MinLenLastFrame,
        long FillValue);

}
