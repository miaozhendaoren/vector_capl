#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Double a Consecutive Frame, where 1 is
 *   the first one. The doubled frame will be
 *   sent with the indicated delay after the
 *   first one.
 */
void OSEKTL_FI_DoubleCF(long frameNo, dword delay_ms);

}
