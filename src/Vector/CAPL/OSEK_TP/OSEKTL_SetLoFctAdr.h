#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the lowest functional address (Diagnosis)
 */
void OSEKTL_SetLoFctAdr(long adr);

}
