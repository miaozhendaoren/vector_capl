#include "OSEKTL_SetTimeoutFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetTimeoutFC(long t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
