#include "OSEKTL_GetHiRxId.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetHiRxId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
