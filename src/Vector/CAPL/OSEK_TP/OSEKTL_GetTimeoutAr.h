#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Reads the timeout value Ar [ms]
 *   (described in ISO/TF2)
 */
long OSEKTL_GetTimeoutAr(void);

}
