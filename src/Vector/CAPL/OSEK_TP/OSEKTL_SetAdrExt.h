#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the address extension to ae
 *   (has only effects in mixed addressing mode)
 */
void OSEKTL_SetAdrExt(byte ae);

}
