#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets STmin
 */
void OSEKTL_SetSTMIN(long stmin);

}
