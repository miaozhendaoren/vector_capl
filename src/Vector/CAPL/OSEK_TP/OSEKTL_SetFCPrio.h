#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the priority used in FC frames
 */
void OSEKTL_SetFCPrio(long prio);

}
