#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sequence number used for first CF
 */
long OSEKTL_GetStartSN(void);

}
