#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the delay of the message that will be
 *   sent next. NOTE: the delay is given in
 *   micro seconds (us).
 */
void OSEKTL_FI_SetDelay(dword MICROseconds);

}
