#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the timeout value As
 *   (described in ISO/TF2) to val ms
 */
void OSEKTL_SetTimeoutAs(long val);

}
