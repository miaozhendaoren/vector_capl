#include "OSEKTL_IsAdrFromId.h"

#include <iostream>

namespace capl
{

long OSEKTL_IsAdrFromId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
