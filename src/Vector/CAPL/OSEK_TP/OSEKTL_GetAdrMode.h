#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active addressing mode
 *
 * @return
 *   - 0: normal
 *   - 1: extended
 *   - 2: normalfixed
 *   - 3: mixed
 *   - 4: 11 bit mixed
 */
long OSEKTL_GetAdrMode(void);

}
