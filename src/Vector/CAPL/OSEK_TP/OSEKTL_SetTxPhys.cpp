#include "OSEKTL_SetTxPhys.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetTxPhys(long)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
