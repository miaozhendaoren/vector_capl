#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Gets the Ar/As timeout threshold value [ms].
 */
long OSEKTL_GetTxTOThres(void);

}
