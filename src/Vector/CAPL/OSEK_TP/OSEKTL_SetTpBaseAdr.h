#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the base address
 */
void OSEKTL_SetTpBaseAdr(long b);

}
