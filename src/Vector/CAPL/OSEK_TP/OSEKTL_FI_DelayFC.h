#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Delay a FlowControl frame for Delay-
 *   Time ms, where 0 is the first FC etc.
 */
void OSEKTL_FI_DelayFC(
        long NumberOfDelayedFlowControl,
        unsigned long DelayTime);

}
