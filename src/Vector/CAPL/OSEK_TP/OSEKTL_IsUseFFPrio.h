#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Priority for FC is used
 *
 * @return
 *   - 0: as configured
 *   - 1: as received in FF
 */
long OSEKTL_IsUseFFPrio(void);

}
