#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set Flow Control
 *
 * @param val
 *   - 1: used
 *   - 0: not used
 *
 */
void OSEKTL_SetUseFC(long val);

}
