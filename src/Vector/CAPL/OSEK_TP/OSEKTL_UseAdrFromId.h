#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Usually the source address (address where the FC
 *   frames for this transmission shall be sent) is calculated
 *   from the received CAN identifier.
 *
 * @param val
 *   - 1: target address calculated from CAN identifier
 *   - 0: target address from OSEKTL_SetTgtAdr(ta)
 */
void OSEKTL_UseAdrFromId(long val);

}
