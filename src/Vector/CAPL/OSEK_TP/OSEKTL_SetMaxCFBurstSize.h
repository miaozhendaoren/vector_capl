#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the maximum number of ConsecutiveFrames
 *   that are forwarded to the network layer in a burst
 *   when a STmin of 0 is used. Default: 64.
 */
void OSEKTL_SetMaxCFBurstSize(dword maxCFBurstSize);

}
