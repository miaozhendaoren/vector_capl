#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Treat all FC frames as CTS if parameter
 *   is not 0. This function may be called any
 *   time and will take effect for all following
 *   transmissions. (default: 0)
 */
void OSEKTL_FI_AllCTS(long flag);

}
