#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   The STmin value sent by the receiver in a FC.CTS is
 *   only used if it is larger than this value. (default: 0)
 */
void OSEKTL_SetMinSTmin(dword milliseconds);

}
