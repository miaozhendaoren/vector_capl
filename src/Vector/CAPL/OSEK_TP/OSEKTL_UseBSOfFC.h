#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   As sender, use the BS sent in the FC.CTS (default)
 */
void OSEKTL_UseBSOfFC(void);

}
