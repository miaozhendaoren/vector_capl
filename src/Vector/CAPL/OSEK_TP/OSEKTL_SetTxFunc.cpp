#include "OSEKTL_SetTxFunc.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetTxFunc(long)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
