#include "OSEKTL_SetNrFixMode.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetNrFixMode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
