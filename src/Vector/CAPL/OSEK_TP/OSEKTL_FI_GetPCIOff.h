#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the index of the PCI byte in the
 *   message data.
 */
dword OSEKTL_FI_GetPCIOff(void);

}
