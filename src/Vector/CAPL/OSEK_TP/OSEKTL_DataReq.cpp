#include "OSEKTL_DataReq.h"

#include <iostream>

namespace capl
{

void OSEKTL_DataReq(byte* txData, int length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
