#include "OSEKTL_GetRecentAE.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetRecentAE(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
