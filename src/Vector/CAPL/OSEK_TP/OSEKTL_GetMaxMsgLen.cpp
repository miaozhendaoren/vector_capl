#include "OSEKTL_GetMaxMsgLen.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetMaxMsgLen(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
