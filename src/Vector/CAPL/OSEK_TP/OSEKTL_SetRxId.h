#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the receive ID
 */
void OSEKTL_SetRxId(long id);

}
