#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns 1 if the message is a FC frame.
 */
long OSEKTL_FI_IsFC(void);

}
