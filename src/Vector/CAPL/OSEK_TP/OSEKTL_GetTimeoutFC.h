#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the time up to which a Flow Control message
 *   (FC) must have arrived
 */
long OSEKTL_GetTimeoutFC(void);

}
