#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Indicates that a Tx timeout (i.e. Ar or As) occurred.
 *
 * @return
 *   - 0: make the DLL abort the data transfer
 *   - 1: special handling
 */
long OSEKTL_TxTimeoutInd(word * frameLen, byte * frameData);

}
