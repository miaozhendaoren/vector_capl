#include "OSEKTL_GetSrcAdr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetSrcAdr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
