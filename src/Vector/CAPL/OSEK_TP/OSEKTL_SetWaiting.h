#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the wait state of the node. The node will send wait
 *   frames to a sender if set to 1.
 */
void OSEKTL_SetWaiting(long state);

}
