#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   The length of the CAN frames is adapted to the data
 *   length. For instance a flow control frame has dlc=3 (4 if
 *   extended Addressing is used). The length of first frames
 *   and the last consecutive frame of a segmented message
 *   is also adapted.
 */
void OSEKTL_SetDlcVar(void);

}
