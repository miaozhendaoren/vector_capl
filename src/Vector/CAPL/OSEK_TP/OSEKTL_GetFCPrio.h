#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the priority used in FC frames
 */
long OSEKTL_GetFCPrio(void);

}
