#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Allow fault injection
 */
void OSEKTL_FI_Enable(void);

}
