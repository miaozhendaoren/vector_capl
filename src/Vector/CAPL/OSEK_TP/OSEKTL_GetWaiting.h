#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the wait state of the node. If set, the node will
 *   send FC.WT frames instead of FC.CTS frames to a
 *   sender
 */
long OSEKTL_GetWaiting(void);

}
