#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Places a transmit request
 */
void OSEKTL_DataReq(byte* txData, int length);

}
