#include "OSEKTL_FI_Enable.h"

#include <iostream>

namespace capl
{

void OSEKTL_FI_Enable(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
