#include "OSEKTL_UseIdFromAdr.h"

#include <iostream>

namespace capl
{

void OSEKTL_UseIdFromAdr(long val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
