#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the J1939 fields PF (protocol, 8 bit), DP (data
 *   page, 1 bit) and R (reserved, 1 bit).
 */
void OSEKTL_SetJ1939Flds(dword protocol, dword dataPage, dword reserved);

}
