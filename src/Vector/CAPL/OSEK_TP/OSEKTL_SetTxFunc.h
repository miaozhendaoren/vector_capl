#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the addressing mode of the following transmissions
 *   to functional addressing
 */
void OSEKTL_SetTxFunc(long);

}
