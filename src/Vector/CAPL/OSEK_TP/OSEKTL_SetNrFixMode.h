#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Toggles to normalfixed addressing
 */
void OSEKTL_SetNrFixMode(void);

}
