#include "OSEKTL_IsUseExtId.h"

#include <iostream>

namespace capl
{

long OSEKTL_IsUseExtId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
