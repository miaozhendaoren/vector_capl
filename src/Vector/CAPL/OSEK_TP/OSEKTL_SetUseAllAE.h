#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   If set to a value other than 0, the node will accept
 *   transmissions in (11 bit) Mixed mode using any address
 *   extension value. Otherwise the value has to match (default).
 *
 */
void OSEKTL_SetUseAllAE(long state);

}
