#include "OSEKTL_GetTgtAdr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTgtAdr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
