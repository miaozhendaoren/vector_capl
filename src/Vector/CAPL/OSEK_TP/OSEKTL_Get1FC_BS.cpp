#include "OSEKTL_Get1FC_BS.h"

#include <iostream>

namespace capl
{

byte OSEKTL_Get1FC_BS(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
