#include "OSEKTL_GetTimeoutAs.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutAs(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
