#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Only messages with ids in the interval [base-address;
 *   base-address + n –1] will be considered.
 */
void OSEKTL_SetMsgCount(dword n);

}
