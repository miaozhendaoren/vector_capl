#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the lowest functional address (Diagnosis)
 */
long OSEKTL_GetLoFctAdr(void);

}
