#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the controller address
 */
void OSEKTL_SetEcuNumber(long nr);

}
