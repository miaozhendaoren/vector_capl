#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns 0 if the address extension has to match exactly
 *   in the mixed addressing modes,
 *   1 if the node accepts any AE value.
 */
long OSEKTL_GetUseAllAE(void);

}
