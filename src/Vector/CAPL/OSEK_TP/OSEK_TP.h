#pragma once

/** @todo detailed documentation is missing */

/**
 * @defgroup OSEK_TP OSEK TP CAPL Functions
 */

/* Functions for Data Transfer */
#include "OSEKTL_GetRxData.h"
#include "OSEKTL_DataReq.h"
#include "OSEKTL_GetSrcAdr.h"

/* Addressing Mode */
#include "OSEKTL_SetExtMode.h"
#include "OSEKTL_SetNrmlMode.h"
#include "OSEKTL_SetMixedMode.h"
#include "OSEKTL_SetNrFixMode.h"
#include "OSEKTL_Set11Mixed.h"
#include "OSEKTL_GetAdrMode.h"

/* Protocol Parameters for all addressing modes */
#include "OSEKTL_GetBS.h"
#include "OSEKTL_SetBS.h"
#include "OSEKTL_UseBSOfFC.h"
#include "OSEKTL_SetFixedBS.h"
#include "OSEKTL_GetSTMIN.h"
#include "OSEKTL_SetSTMIN.h"
#include "OSEKTL_SetFixedST.h"
#include "OSEKTL_UseSTminOfFC.h"
#include "OSEKTL_SetMinSTmin.h"
#include "OSEKTL_SetStartSN.h"
#include "OSEKTL_SetDlcVar.h"
#include "OSEKTL_SetDLC8.h"
#include "OSEKTL_SetEvalOneFC.h"
#include "OSEKTL_SetEvalAllFC.h"
#include "OSEKTL_GetEvalOneFC.h"
#include "OSEKTL_Set0Pattern.h"
#include "OSEKTL_Get0Pattern.h"
#include "OSEKTL_Set0Padding.h"
#include "OSEKTL_Clr0Padding.h"
#include "OSEKTL_Set1FC_BS.h"
#include "OSEKTL_Get1FC_BS.h"
#include "OSEKTL_UseExtId.h"
#include "OSEKTL_IsUseExtId.h"
#include "OSEKTL_GetLoRxId.h"
#include "OSEKTL_GetHiRxId.h"
#include "OSEKTL_GetStartSN.h"
#include "OSEKTL_GetFixedDLC.h"
#include "OSEKTL_GetCAN.h"
#include "OSEKTL_SetCAN.h"
#include "OSEKTL_IsUseFC.h"
#include "OSEKTL_SetUseFC.h"
#include "OSEKTL_SetFCDelay.h"
#include "OSEKTL_GetMaxMsgLen.h"
#include "OSEKTL_GetWaiting.h"
#include "OSEKTL_SetWaiting.h"
#include "OSEKTL_SetWFTMax.h"

/* Times */
#include "OSEKTL_GetTimeoutAr.h"
#include "OSEKTL_GetTimeoutBr.h"
#include "OSEKTL_GetTimeoutCr.h"
#include "OSEKTL_GetTimeoutAs.h"
#include "OSEKTL_GetTimeoutBs.h"
#include "OSEKTL_SetTimeoutAr.h"
#include "OSEKTL_SetTimeoutBr.h"
#include "OSEKTL_SetTimeoutCr.h"
#include "OSEKTL_SetTimeoutAs.h"
#include "OSEKTL_SetTimeoutBs.h"
#include "OSEKTL_GetTimeoutFC.h"
#include "OSEKTL_SetTimeoutFC.h"
#include "OSEKTL_GetTimeoutCF.h"
#include "OSEKTL_SetTimeoutCF.h"

/* Protocol Parameters for Extended Addressing */
#include "OSEKTL_GetTgtAdr.h"
#include "OSEKTL_SetTgtAdr.h"
#include "OSEKTL_GetTpBaseAdr.h"
#include "OSEKTL_SetTpBaseAdr.h"
#include "OSEKTL_GetEcuNumber.h"
#include "OSEKTL_SetEcuNumber.h"
#include "OSEKTL_GetLoFctAdr.h"
#include "OSEKTL_SetLoFctAdr.h"
#include "OSEKTL_GetHiFctAdr.h"
#include "OSEKTL_SetHiFctAdr.h"
#include "OSEKTL_UseIdFromAdr.h"
#include "OSEKTL_IsIdFromAdr.h"
#include "OSEKTL_UseAdrFromId.h"
#include "OSEKTL_IsAdrFromId.h"
#include "OSEKTL_SetMsgCount.h"
#include "OSEKTL_GetMsgCount.h"

/* Protocol Parameters for Normal Addressing */
#include "OSEKTL_GetTxId.h"
#include "OSEKTL_SetTxId.h"
#include "OSEKTL_GetRxId.h"
#include "OSEKTL_SetRxId.h"
#include "OSEKTL_GetLoFctAdr.h"
#include "OSEKTL_SetLoFctAdr.h"
#include "OSEKTL_GetHiFctAdr.h"
#include "OSEKTL_SetHiFctAdr.h"

/* Protocol Parameters for Normal Fixed Addressing */
#include "OSEKTL_SetTxPhys.h"
#include "OSEKTL_SetTxFunc.h"
#include "OSEKTL_IsTxModePhys.h"
#include "OSEKTL_SetTxPrio.h"
#include "OSEKTL_GetTxPrio.h"
#include "OSEKTL_IsUseFFPrio.h"
#include "OSEKTL_SetUseFFPrio.h"
#include "OSEKTL_GetFCPrio.h"
#include "OSEKTL_SetFCPrio.h"
#include "OSEKTL_GetEcuNumber.h"
#include "OSEKTL_SetEcuNumber.h"
#include "OSEKTL_GetTgtAdr.h"
#include "OSEKTL_SetTgtAdr.h"
#include "OSEKTL_SetJ1939Flds.h"
#include "OSEKTL_GetJ1939Flds.h"

/* Protocol Parameters for Mixed Addressing */
#include "OSEKTL_SetAdrExt.h"
#include "OSEKTL_GetAdrExt.h"
#include "OSEKTL_SetUseAllAE.h"
#include "OSEKTL_GetUseAllAE.h"
#include "OSEKTL_GetRecentAE.h"

/* CAN transmission timeout handling */
#include "OSEKTL_GetTxTOThres.h"
#include "OSEKTL_SetTxTOThres.h"

/* Transfer acceleration */
#include "OSEKTL_SetMaxCFBurstSize.h"

/* Callback Functions and error codes */
#include "OSEKTL_ErrorInd.h"
#include "OSEKTL_DataInd.h"
#include "OSEKTL_DataCon.h"
#include "OSEKTL_FirstFrameIndication.h"
#include "OSEKTL_TxTimeoutInd.h"

/* Acknowledged data transfer (extension) */
#include "OSEKTL_ActivateAck.h"
#include "OSEKTL_SetAckRes.h"
#include "OSEKTL_GetAckRes.h"

/* Fault injection support */
#include "OSEKTL_FI_Enable.h"
#include "OSEKTL_FI_Disable.h"
#include "OSEKTL_FI_SendXByte.h"
#include "OSEKTL_FI_DropCF.h"
#include "OSEKTL_FI_DoubleTx.h"
#include "OSEKTL_FI_DelayCF.h"
#include "OSEKTL_FI_DoubleCF.h"
#include "OSEKTL_FI_DropFC.h"
#include "OSEKTL_FI_DelayFC.h"
#include "OSEKTL_FI_DoubleFC.h"
#include "OSEKTL_FI_AllCTS.h"
#include "OSEKTL_FI_AbortRx.h"
#include "OSEKTL_FI_AbortTx.h"

/* Callbacks */
#include "OSEKTL_FI_PreSend.h"
#include "OSEKTL_FI_SetDelay.h"
#include "OSEKTL_FI_GetDelay.h"
#include "OSEKTL_FI_IsFC.h"
#include "OSEKTL_FI_GetPCIOff.h"
#include "OSEKTL_FI_SetId.h"
#include "OSEKTL_FI_GetId.h"
