#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   All FCs are evaluated. This is the default. Same behavior
 *   as DLL 2.4 and older
 */
void OSEKTL_SetEvalAllFC(void);

}
