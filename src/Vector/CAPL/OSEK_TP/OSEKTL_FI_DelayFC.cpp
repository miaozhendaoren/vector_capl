#include "OSEKTL_FI_DelayFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_FI_DelayFC(
        long NumberOfDelayedFlowControl,
        unsigned long DelayTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
