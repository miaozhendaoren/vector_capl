#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   sn is the sequence number used for the first consecutive
 *   frame in a segmented message. Default value is 1.
 *   Some manufacturers specifiy 0. All other values are
 *   incorrect, but not rejected by the DLL.
 */
void OSEKTL_SetStartSN(long sn);

}
