#include "OSEKTL_SetUseFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetUseFC(long val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
