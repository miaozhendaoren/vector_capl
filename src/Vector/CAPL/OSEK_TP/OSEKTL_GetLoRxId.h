#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Lowest CAN ID which passes Rx Filter
 */
long OSEKTL_GetLoRxId(void);

}
