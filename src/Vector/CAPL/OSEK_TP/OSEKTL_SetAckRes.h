#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Value of AckRes to send to sender (8 bit)
 */
void OSEKTL_SetAckRes(dword result);

}
