#include "OSEKTL_GetAdrMode.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetAdrMode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
