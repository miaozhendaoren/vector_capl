#include "OSEKTL_GetSTMIN.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetSTMIN(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
