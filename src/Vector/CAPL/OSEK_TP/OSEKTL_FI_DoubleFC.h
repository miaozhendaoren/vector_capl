#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Send a FC two times, where 0 is the first
 *   one etc.
 */
void OSEKTL_FI_DoubleFC(long NumberOfFCToBeDoubled);

}
