#include "OSEKTL_SetEvalOneFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetEvalOneFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
