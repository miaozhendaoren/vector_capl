#include "OSEKTL_Get0Pattern.h"

#include <iostream>

namespace capl
{

long OSEKTL_Get0Pattern(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
