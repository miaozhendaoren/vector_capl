#include "OSEKTL_IsIdFromAdr.h"

#include <iostream>

namespace capl
{

long OSEKTL_IsIdFromAdr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
