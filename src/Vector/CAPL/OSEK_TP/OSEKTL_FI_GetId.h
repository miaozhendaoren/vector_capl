#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   The message will be sent with this id.
 */
dword OSEKTL_FI_GetId(void);

}
