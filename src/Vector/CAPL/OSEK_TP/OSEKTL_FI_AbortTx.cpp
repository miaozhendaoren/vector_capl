#include "OSEKTL_FI_AbortTx.h"

#include <iostream>

namespace capl
{

void OSEKTL_FI_AbortTx(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
