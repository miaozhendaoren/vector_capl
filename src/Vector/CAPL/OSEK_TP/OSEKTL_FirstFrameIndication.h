#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Indicates, that a FF has been received. The meaning
 *   of the parameters depends on the addressing
 *   mode.
 *
 * @param source
 *   - normal: Tx Identifier of source node
 *   - extended: ECU (of source node) + Base
 *   - normal fixed: SA
 *   - mixed: SA
 *
 * @param target
 *   - normal: Tx Identifier of destination node
 *   - extended: ECU (of destination node) + Base
 *   - normal fixed: TA
 *   - mixed: TA
 *
 * @param length
 *   the length of the message for both
 *   addressing modes.
 */
void OSEKTL_FirstFrameIndication(long source, long target, long length);

}
