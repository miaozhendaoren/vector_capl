#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Stop the reception of data immediately.
 */
void OSEKTL_FI_AbortRx(void);

}
