#include "OSEKTL_GetStartSN.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetStartSN(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
