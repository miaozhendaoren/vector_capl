#include "OSEKTL_GetEcuNumber.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetEcuNumber(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
