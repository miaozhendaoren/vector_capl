#include "OSEKTL_GetFCPrio.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetFCPrio(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
