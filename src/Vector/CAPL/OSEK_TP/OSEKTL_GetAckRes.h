#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Value of last AckRes received, only
 *   valid during FinishTransmission callback
 */
long OSEKTL_GetAckRes(void);

}
