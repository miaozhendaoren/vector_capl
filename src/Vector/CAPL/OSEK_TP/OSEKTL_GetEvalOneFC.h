#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   True, if only one FC is evaluated
 */
long OSEKTL_GetEvalOneFC(void);

}
