#include "OSEKTL_IsUseFFPrio.h"

#include <iostream>

namespace capl
{

long OSEKTL_IsUseFFPrio(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
