#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   sets priority of transmission in normal fixed and
 *   mixed addressing mode
 */
void OSEKTL_SetTxPrio(long val);

}
