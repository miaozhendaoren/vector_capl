#include "OSEKTL_GetTimeoutCr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutCr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
