#include "OSEKTL_SetMixedMode.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetMixedMode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
