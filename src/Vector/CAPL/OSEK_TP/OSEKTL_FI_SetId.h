#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the message id to this value.
 *   -1 will tell the DLL to drop the frame.
 */
void OSEKTL_FI_SetId(dword id);

}
