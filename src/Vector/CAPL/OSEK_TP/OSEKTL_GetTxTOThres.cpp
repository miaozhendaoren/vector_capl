#include "OSEKTL_GetTxTOThres.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTxTOThres(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
