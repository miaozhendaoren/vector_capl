#include "OSEKTL_GetTimeoutAr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutAr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
