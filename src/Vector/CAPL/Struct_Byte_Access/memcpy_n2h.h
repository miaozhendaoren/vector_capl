#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Fills the struct with bytes from the array. (Form 1)
 *
 * Fills the struct with bytes from the array, and translates the byte order of the elements
 * from big-endian to little-endian (n2h stands for "network to host").
 *
 * @param source
 *   Array whose bytes shall be copied
 *
 * @param dest
 *   Struct into which the bytes shall be copied
 */
void memcpy_n2h(void * dest, byte * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Fills the struct with bytes from the array. (Form 2)
 *
 * Fills the struct with bytes from the array, and translates the byte order of the elements
 * from big-endian to little-endian (n2h stands for "network to host").
 *
 * @param source
 *   Array whose bytes shall be copied
 *
 * @param dest
 *   Struct into which the bytes shall be copied
 *
 * @param offset
 *   Offset in the array where the data begins
 */
void memcpy_n2h(void * dest, byte * source, int offset);

}
