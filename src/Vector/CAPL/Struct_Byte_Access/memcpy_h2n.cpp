#include "memcpy_h2n.h"

#include <iostream>

namespace capl
{

void memcpy_h2n(byte * dest, void * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_h2n(byte * dest, int offset, void * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
