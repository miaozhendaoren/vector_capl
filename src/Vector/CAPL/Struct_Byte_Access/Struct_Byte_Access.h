#pragma once

/**
 * @defgroup Struct_Byte_Access Struct Byte Access CAPL Functions
 */

#include "memcmp.h"
#include "memcpy.h"
#include "memcpy_h2n.h"
#include "memcpy_n2h.h"
#include "memcpy_off.h"
