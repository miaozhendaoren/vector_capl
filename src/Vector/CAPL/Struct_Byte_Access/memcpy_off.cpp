#include "memcpy_off.h"

#include <iostream>

namespace capl
{

void memcpy_off(void * dest, dword destOffset, byte * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(void * dest, dword destOffset, char * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(byte * dest, dword destOffset, void * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(char * dest, dword destOffset, void * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(byte * dest, dword destOffset, byte * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(char * dest, dword destOffset, byte * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(byte * dest, dword destOffset, char * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_off(char * dest, dword destOffset, char * source, dword sourceOffset, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
