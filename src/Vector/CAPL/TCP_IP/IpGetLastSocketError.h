#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Returns the Winsock 2 error code of the last operation that failed on a specified socket.
 *
 * The function returns the Winsock 2 error code of the last operation that failed on the
 * specified socket.
 *
 * @param socket
 *   The socket handle.
 *
 * @return
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - Any other value: The error code as provided by the Winsock 2 WSAGetLastError function.
 */
long IpGetLastSocketError(dword socket);

}
