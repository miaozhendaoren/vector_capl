#include "UdpSocket.h"

#include <iostream>

namespace capl
{

long UdpSocket::bind(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long UdpSocket::close(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long UdpSocket::getLastSocketError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long UdpSocket::getLastSocketErrorAsString(char * text, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

void UdpSocket::open(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

long UdpSocket::receiveFrom(char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long UdpSocket::sendTo(dword address, dword port, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long UdpSocket::setSocketOption(long level, long name, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
