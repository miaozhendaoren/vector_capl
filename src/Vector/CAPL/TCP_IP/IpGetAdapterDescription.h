#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the description of a specified network interface.
 *
 * The function retrieves the description of the specified network interface.
 *
 * @param index
 *   The 1-based network interface index.
 *
 * @param name
 *   The buffer used to store the description.
 *
 * @param count
 *   The size of the name buffer.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The name buffer was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified network interface index was invalid.
 */
long IpGetAdapterDescription(dword index, char * name, dword count);

}
