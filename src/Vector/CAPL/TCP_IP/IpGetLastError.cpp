#include "IpGetLastError.h"

#include <iostream>

namespace capl
{

long IpGetLastError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
