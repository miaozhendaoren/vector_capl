#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Establishes a connection with a specified location.
 *
 * The function establishes a connection with the specified location. If the connect
 * operation doesn't complete immediately the operation is performed asynchronously and
 * the function will return SOCKET_ERROR (-1). Use IpGetLastSocketError to get a more
 * specific error code. If the specific error code is WSAWOULDBLOCK (10035), the CAPL
 * callback OnTcpConnect will be called on completion (successful or not), provided it is
 * implemented in the same CAPL program.
 *
 * @param socket
 *   The socket handle.
 *
 * @param address
 *   The address of the destination in network-byte order.
 *
 * @param port
 *   The port of the destination in host-byte order.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
 *     error code.
 */
long TcpConnect(dword socket, dword address, dword port);

}
