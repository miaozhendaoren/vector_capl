#include "UdpSendTo.h"

#include <iostream>

namespace capl
{

long UdpSendTo(dword socket, dword address, dword port, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
