#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Causes the socket to listen for incoming connection requests.
 *
 * The function causes the socket to listen for incoming connection requests, which will be
 * provided in the CAPL callback OnTcpListen, if it is implemented in the same CAPL
 * program. If the operation fails, the function will return SOCKET_ERROR (-1). Use
 * IpGetLastSocketError to get a more specific error code.
 *
 * @param socket
 *   The socket handle.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
 *     error code. */
long TcpListen(dword socket);

}
