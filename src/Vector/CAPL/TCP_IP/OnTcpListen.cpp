#include "OnTcpListen.h"

#include <iostream>

namespace capl
{

void OnTcpListen(dword socket, long result)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
