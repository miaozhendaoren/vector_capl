/**
 * This class is used to implement TCP network communications.
 */

#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 */
class TcpSocket
{
public:
    /**
     * @brief
     *   Accepts an incoming connection request on a specified socket resulting in a new socket.
     *
     * The function accepts an incoming connection request on the specified socket resulting in
     * a new socket . If the operation fails, the function will return INVALID_SOCKET (~0).
     */
    void accept(void);

    /**
     * @brief
     *   Creates a TCP socket for use in connection-based, message-oriented communications.
     *
     * The function creates a TCP socket for use in connection-based, message-oriented
     * communications. All parameters may be zero. If the port parameter is non-zero the
     * socket is implicitly bound to the given port.
     *
     * @param address
     *   The local address in network-byte order to be used with the socket.
     *
     * @param port
     *   The port in host-byte order to be used with the socket.
     */
    void open(dword address, dword port);

    /**
     * @brief
     *   Closes the TCP socket.
     *
     * The function closes the TCP socket. Upon successful completion the passed socket is no
     * longer valid.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastError to get a more specific error
     *     code.
     */
    long close(void);

    /**
     * @brief
     *   Associates an address and a port with a specified socket.
     *
     * The function associates an address and a port with the specified socket.
     *
     * @param address
     *   The local address in network-byte order.
     *
     * @param port
     *   The local port in host-byte order.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastError to get a more specific error code.
     */
    long bind(dword address, dword port);

    /**
     * @brief
     *   Establishes a connection with a specified location.
     *
     * The function establishes a connection with the specified location. If the connect
     * operation doesn't complete immediately the operation is performed asynchronously and
     * the function will return SOCKET_ERROR (-1). Use IpGetLastSocketError to get a more
     * specific error code. If the specific error code is WSAWOULDBLOCK (10035), the CAPL
     * callback OnTcpConnect will be called on completion (successful or not), provided it is
     * implemented in the same CAPL program.
     *
     * @param address
     *   The address of the destination in network-byte order.
     *
     * @param port
     *   The port of the destination in host-byte order.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
     *     error code.
     */
    long connect(dword address, dword port);

    /**
     * @brief
     *   Returns the Winsock 2 error code of the last operation that failed on a specified socket.
     *
     * The function returns the Winsock 2 error code of the last operation that failed on the
     * specified socket.
     *
     * @return
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - Any other value: The error code as provided by the Winsock 2 WSAGetLastError function.
     */
    long getLastSocketError(void);

    /**
     * @brief
     *   Retrieves the error message of the last operation that failed on a specified socket.
     *
     * The function retrieves the error message of the last operation that failed on the specified
     * socket (see Winsock 2 error code).
     *
     * @param text
     *   The buffer used to store the error message.
     *
     * @param count
     *   The size of the text buffer.
     *
     * @return
     *   - 0: The error message was written into the text buffer. In case of an invalid error code,
     *        the error message has the format "Unknown error: x" assuming the last error code x for
     *        the specified socket.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     */
    long getLastSocketErrorAsString(char * text, dword count);

    /**
     * @brief
     *   Causes the socket to listen for incoming connection requests.
     *
     * The function causes the socket to listen for incoming connection requests, which will be
     * provided in the CAPL callback OnTcpListen, if it is implemented in the same CAPL
     * program. If the operation fails, the function will return SOCKET_ERROR (-1). Use
     * IpGetLastSocketError to get a more specific error code.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
     *     error code.
     */
    long listen(void);

    /**
     * @brief
     *   Receives data into a specified buffer.
     *
     * The function receives data into the specified buffer. If the receive operation doesn't
     * complete immediately the operation is performed asynchronously and the function will
     * return SOCKET_ERROR (-1). Use IpGetLastSocketError to get a more specific error code. If
     * the specific error code is WSA_IO_PENDING (997) the CAPL callback OnTcpReceive will be
     * called on completion (successful or not), provided it is implemented in the same CAPL
     * program.
     *
     * @param buffer
     *   The buffer used to store the incoming data.
     *
     * @param size
     *   The size of the data buffer.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
     *     error code.
     */
    long receive(char * buffer, dword size);

    /**
     * @brief
     *   Sends data on a specified socket.
     *
     * The function sends data on the specified socket. If the send operation doesn't complete
     * immediately the operation is performed asynchronously and the function will return
     * SOCKET_ERROR (-1). Use IpGetLastSocketError to get a more specific error code. If the
     * specific error code is WSA_IO_PENDING (997) the CAPL callback OnTcpSend will be called
     * on completion (successful or not), provided it is implemented in the same CAPL program.
     *
     * @param buffer
     *   The buffer containing the data to be sent.
     *
     * @param size
     *   The size of the data to be sent.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
     *     error code.
     */
    long send(char * buffer, dword size);

    /**
     * @brief
     *   Modifies a socket option.
     *
     * The function modifies a socket option.
     *
     * @param level
     *   The level at which the option is defined, e.g. SOL_SOCKET (0xFFFF).
     *
     * @param name
     *   The socket option name to be modified, e.g. SO_BROADCAST (0x0020).
     *
     * @param value
     *   The value to be set for the socket option.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastError to get a more specific error
     *     code.
     */
    long setSocketOption(long level, long name, long value);

    /**
     * @brief
     *   Disables send operations on a specified socket.
     *
     * The function disables send operations on the specified socket. This function may be used
     * to shutdown a TCP connection.
     *
     * @return
     *   - 0: The function completed successfully.
     *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
     *     error code.
     */
    long shutdown(void);

private:
    dword socket;
};

}
