#include "TcpReceive.h"

#include <iostream>

namespace capl
{

long TcpReceive(dword socket, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
