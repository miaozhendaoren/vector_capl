#include "OnTcpReceive.h"

#include <iostream>

namespace capl
{

void OnTcpReceive(dword socket, long result, dword address, dword port, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
