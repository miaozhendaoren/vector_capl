#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   It is dispatched when an asynchronous receive operation on a TCP socket completes.
 *
 * Provided the CAPL program implements this callback it is dispatched when an
 * asynchronous receive operation on a TCP socket completes.
 *
 * @param socket
 *   The socket handle.
 *
 * @param result
 *   The specific result code of the operation. If the operation completed successfully the
 *   value is zero. Otherwise the value is non-zero.
 *
 * @param address
 *   The remote address of the location which sent the data in network-byte order.
 *
 * @param port
 *   The remote port of the location which sent the data in host-byte order.
 *
 * @param buffer
 *   The buffer into which the data was stored.
 *
 * @param size
 *   The size of the received data.
 */
void OnTcpReceive(dword socket, long result, dword address, dword port, char * buffer, dword size);

}
