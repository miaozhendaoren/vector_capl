#include "IpGetAdapterGatewayAsString.h"

#include <iostream>

namespace capl
{

long IpGetAdapterGatewayAsString(dword index, char * address, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
