#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Modifies a socket option.
 *
 * The function modifies a socket option.
 *
 * @param socket
 *   The socket handle.
 *
 * @param level
 *   The level at which the option is defined, e.g. SOL_SOCKET (0xFFFF).
 *
 * @param name
 *   The socket option name to be modified, e.g. SO_BROADCAST (0x0020).
 *
 * @param value
 *   The value to be set for the socket option.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastError to get a more specific error
 *     code.
 */
long IpSetSocketOption(dword socket, long level, long name, long value);

}
