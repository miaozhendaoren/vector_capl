#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   It is dispatched when a TCP socket receives a close notification.
 *
 * Provided the CAPL program implements this callback it is dispatched when a TCP socket
 * receives a close notification.
 *
 * @param socket
 *   The socket handle.
 *
 * @param result
 *   The specific result code of the operation. If the operation completed successfully the
 *   value is zero. Otherwise the value is non-zero.
 */
void OnTcpClose(dword socket, long result);

}
