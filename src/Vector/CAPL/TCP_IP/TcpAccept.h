#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Accepts an incoming connection request on a specified socket resulting in a new socket.
 *
 * The function accepts an incoming connection request on the specified socket resulting in
 * a new socket . If the operation fails, the function will return INVALID_SOCKET (~0).
 *
 * @param socket
 *   The socket handle.
 *
 * @return
 *   - INVALID_SOCKET (~0): The function failed. Call IpGetLastError to get a more specific error
 *     code. If this error code is WSA_INVALID_PARAMETER (87), the specified socket was
 *     invalid. Otherwise use IpGetLastSocketError to get the reason for the failing.
 *   - Any other value: A valid socket handle identifying the created socket.
 */
dword TcpAccept(dword socket);

}
