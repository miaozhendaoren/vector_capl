#include "IpSetSocketOption.h"

#include <iostream>

namespace capl
{

long IpSetSocketOption(dword socket, long level, long name, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
