#include "IpGetAddressAsString.h"

#include <iostream>

namespace capl
{

long IpGetAddressAsString(dword numericAddress, char * address, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
