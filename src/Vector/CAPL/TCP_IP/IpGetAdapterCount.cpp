#include "IpGetAdapterCount.h"

#include <iostream>

namespace capl
{

dword IpGetAdapterCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
