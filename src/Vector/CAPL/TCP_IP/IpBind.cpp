#include "IpBind.h"

#include <iostream>

namespace capl
{

long IpBind(dword socket, dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
