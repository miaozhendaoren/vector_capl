#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 */
dword IpGetAddressAsArray(char * address, byte * ipv6Address);

}
