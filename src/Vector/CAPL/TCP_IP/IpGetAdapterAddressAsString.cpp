#include "IpGetAdapterAddressAsString.h"

#include <iostream>

namespace capl
{

long IpGetAdapterAddressAsString(dword index, char * address, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
