#include "OnTcpSend.h"

#include <iostream>

namespace capl
{

void OnTcpSend(dword socket, long result, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
