#include "IpGetLastSocketError.h"

#include <iostream>

namespace capl
{

long IpGetLastSocketError(dword socket)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
