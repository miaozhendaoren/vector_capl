#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Closes the UDP socket.
 *
 * The function closes the UDP socket. Upon successful completion the passed socket is no
 * longer valid.
 *
 * @param socket
 *   The socket handle.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastError to get a more specific error
 *     code.
 */
long UdpClose(dword socket);

}
