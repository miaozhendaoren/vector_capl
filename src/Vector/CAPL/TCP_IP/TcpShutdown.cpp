#include "TcpShutdown.h"

#include <iostream>

namespace capl
{

long TcpShutdown(dword socket)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
