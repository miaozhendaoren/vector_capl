#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Converts a numeric address in network-byte order to a address string in dot notation.
 *
 * The function converts a numeric address in network-byte order to a address string in dot
 * notation as in "192.168.0.10".
 *
 * @param numericAddress
 *   The address to be converted.
 *
 * @param address
 *   The buffer used to store the converted address.
 *
 * @param count
 *   The size of the address buffer.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The address buffer was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified address was invalid.
 */
long IpGetAddressAsString(dword numericAddress, char * address, dword count);

}
