#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Converts an address string in dot notation to it's numerical value in network-byte order.
 *
 * The function converts an address string in dot notation to it's numerical value in networkbyte
 * order.
 *
 * @param address
 *   The address to be converted.
 *
 * @return
 *   - 4294967295 (0xFFFFFFF, the equivalent of "255.255.255.255"): The specified address string was invalid.
 *   - Any other value: The numeric equivalent of the given address string.
 */
dword IpGetAddressAsNumber(char * address);

}
