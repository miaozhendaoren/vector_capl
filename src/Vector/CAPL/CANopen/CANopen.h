#pragma once

/**
 * @defgroup CANopen CANopen CAPL Functions
 */

/* CANopen Node Layer */
#include "CANopen_NL/CANopen_NL.h"

/* CANopen Test Feature Set Node Layer */
#include "CANopen_TFS_NL/CANopen_TFS_NL.h"
