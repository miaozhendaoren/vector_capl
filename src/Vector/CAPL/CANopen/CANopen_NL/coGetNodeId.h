#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the node-ID.
 *
 * The function returns the node-ID. If the function coStartUp was not executed yet, the function returns 0.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   node-ID
 */
dword coGetNodeId(dword * errCode);

}
