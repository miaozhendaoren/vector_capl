#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Allows several communication state changes during the start procedure of a CANopen node.
 *
 * If a simulated node is configured as NMT master (Bit 0 in 1F80 set), there are various states during the start procedure in which the application is signaled by the event function coOnConfigReady. In these states, the simulated node waits for the release of the application in order to be able to continue the start procedure. The various events and the associated information can be read in the description of coOnConfigReady.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coAllowStart(dword * errCode);

}
