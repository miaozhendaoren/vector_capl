#include "coOnUploadResponse.h"

#include <iostream>

namespace capl
{

void coOnUploadResponse(dword nodeId, dword index, dword subIndex, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
