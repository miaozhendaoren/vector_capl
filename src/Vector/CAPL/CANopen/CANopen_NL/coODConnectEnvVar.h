#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Connects an object with an environment variable.
 *
 * Connects an object from the local object dictionary with an environment variable.
 *
 * @param index
 *   index of the object, value range 1..65535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param envVar
 *   name of the environment variable
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODConnectEnvVar(dword index, dword subIndex, char * envVar, dword * errCode);

}
