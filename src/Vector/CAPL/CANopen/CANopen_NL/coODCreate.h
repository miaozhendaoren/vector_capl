#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates a new object in the local object dictionary of the node. (form 1)
 *
 * Generates a new object in the local object dictionary.
 * If the object already exists, it is replaced by the new object.
 *
 * Generates a new object of the specified type and without a start value.
 * The value of the object is initialized with null.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..254
 *
 * @param dataType
 *   data type of the object
 *
 * @param access
 *   access type of the object
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreate(dword index, dword subIndex, dword dataType, dword access, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates a new object in the local object dictionary of the node. (form 1)
 *
 * Generates a new object in the local object dictionary.
 * If the object already exists, it is replaced by the new object.
 *
 * Generates a new object of the specified type with the specified initial data.
 * If the length of the available start data is smaller than the size of the object, the remaining part is initialized with null.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..254
 *
 * @param dataType
 *   data type of the object
 *
 * @param access
 *   access type of the object
 *
 * @param initData
 *   initial data of the object
 *
 * @param dataSize
 *   length of the initial data in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreate(dword index, dword subIndex, dword dataType, dword access, byte * initData, dword dataSize, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates a new object in the local object dictionary of the node. (form 1)
 *
 * Generates a new object in the local object dictionary.
 * If the object already exists, it is replaced by the new object.
 *
 * Generates a new special object, with the internally assumed data type Domain.
 * This object reads from and/or writes to a file on the local file system.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..254
 *
 * @param access
 *   access type of the object, if form (3) is used, the access type is limited to 0..2
 *
 * @param filename
 *   path and name of the file (absolute or relative to the CANoe configuration)
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreate(dword index, dword subIndex, dword access, char * filename, dword * errCode);

}
