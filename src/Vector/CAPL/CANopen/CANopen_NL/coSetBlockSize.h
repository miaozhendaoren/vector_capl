#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the block size used for SDO block transfers.
 *
 * This function sets the block size that is used for SDO block transfers.
 * The default value is 7 and applies globally for all SDO transmission.
 *
 * @param blockSize
 *   block size to be used for SDO block transfers, 1..127
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetBlockSize(dword blockSize, dword * errCode);

}
