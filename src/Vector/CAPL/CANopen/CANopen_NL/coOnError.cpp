#include "coOnError.h"

#include <iostream>

namespace capl
{

void coOnError(dword errorCode, dword errorClass, dword nodeId, dword index, dword subIndex, dword param)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
