#include "coODCreate.h"

#include <iostream>

namespace capl
{

void coODCreate(dword index, dword subIndex, dword dataType, dword access, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coODCreate(dword index, dword subIndex, dword dataType, dword access, byte * initData, dword dataSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coODCreate(dword index, dword subIndex, dword access, char * filename, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
