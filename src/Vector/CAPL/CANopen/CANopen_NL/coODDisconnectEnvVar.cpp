#include "coODDisconnectEnvVar.h"

#include <iostream>

namespace capl
{

void coODDisconnectEnvVar(dword index, dword subIndex, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coODDisconnectEnvVar(char * envVar, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
