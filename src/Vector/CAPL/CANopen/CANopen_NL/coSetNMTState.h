#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the state of a node on the network.
 *
 * Sets the state of a node on the network.
 * The node for which this function is called must previously have been started with coStartUp().
 * Furthermore, the node must be configured as NMT master (Bit 0 in 1F80 set) and the object 1F82 must exist in the object dictionary.
 *
 * @param nodeId
 *   node-ID, value range 0..127, 0 - all nodes
 *
 * @param newState
 *   desired state of the node
 *   - 4 - stopped
 *   - 5 - operational
 *   - 6 - reset node
 *   - 7 - reset communication
 *   - 127 - pre-operational
 *
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetNMTState(dword nodeId, dword newState, dword * errCode);

}
