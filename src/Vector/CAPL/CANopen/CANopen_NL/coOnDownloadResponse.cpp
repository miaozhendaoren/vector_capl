#include "coOnDownloadResponse.h"

#include <iostream>

namespace capl
{

void coOnDownloadResponse(dword nodeId, dword index, dword subIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
