#include "coGetNMTState.h"

#include <iostream>

namespace capl
{

dword coGetNMTState(dword nodeId, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
