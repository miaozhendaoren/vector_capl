#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if a boot-up message was registered on the bus.
 *
 * This function is called if a boot-up message was registered on the bus.
 * Each CANopen node sends this message after the reset during the transition into the state pre-operational.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 */
void coOnBootUp(dword nodeId);

}
