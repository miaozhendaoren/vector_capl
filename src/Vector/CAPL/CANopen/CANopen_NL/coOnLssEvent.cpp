#include "coOnLssEvent.h"

#include <iostream>

namespace capl
{

void coOnLssEvent(dword evType, dword param, dword param2)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
