#include "coLssInqSerialNo.h"

#include <iostream>

namespace capl
{

void coLssInqSerialNo(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
