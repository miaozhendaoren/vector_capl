#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Stops the CANopen communication of the node.
 *
 * Stops the CANopen communication of the node.
 *
 * @param deleteOD
 *   specifies whether the object dictionary should be deleted
 *   - 0 - object dictionary is not deleted (can also be used again after renewed coStartUp)
 *   - 1 - object dictionary is deleted (must be generated again)
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coShutDown(dword deleteOD, dword * errCode);

}
