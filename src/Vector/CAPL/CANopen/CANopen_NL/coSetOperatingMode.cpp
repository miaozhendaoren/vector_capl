#include "coSetOperatingMode.h"

#include <iostream>

namespace capl
{

void coSetOperatingMode(dword msgTriggered, dword cycleTime, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
