#include "coGetLocalState.h"

#include <iostream>

namespace capl
{

dword coGetLocalState(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
