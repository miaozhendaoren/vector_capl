#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Reads an entry from the object dictionary of another node by using the SDO upload.
 *
 * Reading of an entry from the object dictionary of another node.
 *
 * The function triggers a SDO upload.
 * The response of the node is returned in the event function coOnUploadResponse or coOnError.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param flags
 *   bit field with options:
 *   - 0 - block transfer off
 *   - 1 - block transfer on
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coUpload(dword nodeId, dword index, dword subIndex, dword flags, dword * errCode);

}
