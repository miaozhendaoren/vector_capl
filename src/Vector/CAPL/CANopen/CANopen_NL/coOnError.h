#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if an error occurs.
 *
 * This function is called if an error occurs.
 *
 * @param errorCode
 *   error code (see error codes of coOnError)
 *
 * @param errorClass
 *   error class
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param param
 *   additional error parameter (see error codes of coOnError)
 */
void coOnError(dword errorCode, dword errorClass, dword nodeId, dword index, dword subIndex, dword param);

}
