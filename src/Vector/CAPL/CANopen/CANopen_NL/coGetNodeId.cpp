#include "coGetNodeId.h"

#include <iostream>

namespace capl
{

dword coGetNodeId(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
