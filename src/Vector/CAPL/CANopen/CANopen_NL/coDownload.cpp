#include "coDownload.h"

#include <iostream>

namespace capl
{

void coDownload(dword nodeId, dword index, dword subIndex, char * data, dword dataSize, dword flags, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coDownload(dword nodeId, dword index, dword subIndex, byte * data, dword dataSize, dword flags, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coDownload(dword nodeId, dword index, dword subIndex, double value, dword flags, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
