#include "coLssInqVendorId.h"

#include <iostream>

namespace capl
{

void coLssInqVendorId(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
