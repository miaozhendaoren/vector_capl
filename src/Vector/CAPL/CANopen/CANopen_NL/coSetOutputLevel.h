#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the output level of the used node layer.
 *
 * The function sets the output level of the node layer.
 *
 * @param level
 *   output level, value range 0..10
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetOutputLevel(dword level, dword * errCode);

}
