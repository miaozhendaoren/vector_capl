#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Starts the CANopen communication of the node.
 *
 * Starts the CANopen communication of the node.
 *
 * The start procedure occurs according to the specifications in the object dictionary.
 * If the node is configured as NMT master (Bit 0 in 1F80 set) and if additional nodes are assigned to this NMT master,
 * then these are started automatically (depending on the configuration).
 * If the node was able to send its boot-up message, then it changes automatically to the state pre-operational.
 * Only after this function has been executed successfully it is possible to access the object dictionary of this node via the network.
 *
 * The event function coOnNodeChangedState is called several times during the start procedure.
 *
 * @param nodeId
 *  node-ID that should be used by the node
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coStartUp(dword nodeId, dword * errCode);

}
