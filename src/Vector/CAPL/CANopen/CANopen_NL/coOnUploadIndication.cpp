#include "coOnUploadIndication.h"

#include <iostream>

namespace capl
{

void coOnUploadIndication(dword index, dword subIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
