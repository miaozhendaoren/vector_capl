#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates the object dictionary by reading an EDS or DCF file.
 *
 * The function loads an EDS or DCF file and generates the local object dictionary from it.
 * The given file have to match the format described in /7/.
 * Already existing objects will be deleted on call of that function.
 *
 * All errors that occur and indications about this procedure are reported to the write window.
 *
 * @param filename
 *   file name of the EDS or DCF file
 *
 * @param nodeId
 *   node-ID, value range 1..127 (only required if values in the EDS or DCF file depend on the node-ID)
 *
 * @param defaults
 *   specifies whether the default or parameter values from a DCF file are used:
 *   - 0 - parameter values are used
 *   - 1 - default values are used
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreateFromFile(char * filename, dword nodeId, dword defaults, dword * errCode);

}
