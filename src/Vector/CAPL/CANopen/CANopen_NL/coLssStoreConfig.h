#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   The configured parameters are written into non-volatile memory with this service.
 *
 * The configured parameters are written into non-volatile memory with this service.
 * Only one LSS slave may be in configuration mode.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssStoreConfig(dword * errCode);

}
