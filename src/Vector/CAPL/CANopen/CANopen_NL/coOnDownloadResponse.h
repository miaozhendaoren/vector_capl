#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if the response to a SDO download was received.
 *
 * This function is called if the response to a SDO download was received (see coDownload and coDownloadExpedited).
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 */
void coOnDownloadResponse(dword nodeId, dword index, dword subIndex);

}
