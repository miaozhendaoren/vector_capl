#include "coOnEmergency.h"

#include <iostream>

namespace capl
{

void coOnEmergency(dword nodeId, dword errorCode, dword errorRegister)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
