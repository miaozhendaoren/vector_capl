#include "coGetLastError.h"

#include <iostream>

namespace capl
{

dword coGetLastError(char * buffer, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
