#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the size of the data of an SDO transfer.
 *
 * The function returns the size of the data of a SDO transfer in the event function coOnUploadResponse or coOnDownloadIndication.
 * The call is only allowed in this event function.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   size of the read object in bytes
 */
dword coThisGetSize(dword * errCode);

}
