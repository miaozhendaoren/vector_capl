#include "coODGetData.h"

#include <iostream>

namespace capl
{

dword coODGetData(dword index, dword subIndex, char * buffer, dword bufferSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword coODGetData(dword index, dword subIndex, byte * buffer, dword bufferSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
