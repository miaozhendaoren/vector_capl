#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   The LSS master commands all LSS slaves whose node-ID is not configured (0xFF) to identify themselves.
 *
 * With this service, the LSS master commands all LSS slaves whose node-ID is not configured (0xFF) to identify themselves.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssIdentNonConfigSlave(dword * errCode);

}
