#include "coLssInqProdCode.h"

#include <iostream>

namespace capl
{

void coLssInqProdCode(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
