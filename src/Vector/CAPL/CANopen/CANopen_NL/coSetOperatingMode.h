#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the operating mode of the node.
 *
 * The function sets the work mode of the node.
 * As default setting the node operates at a cycle time of 5 ms not triggered by messages.
 * This means it is processed in a fixed grid. In extreme cases, 5 ms occur until an SDO request of another node is answered.
 * If the node is set to message-triggered mode using this function now, it reacts as fast as possible to the messages sent to it.
 * The indicated cycle time specifies in which time interval the node is executed.
 * For example, this determines how fast given commands are processed or how occurring events (boot-up of a node) are reported to the application.
 * This function allows the behavior of real ECUs to be better simulated because a fixed cycle time is often used here for the communication section of the software.
 *
 * A short cycle time can be used simultaneously with message-triggered mode in order to obtain the fastest possible reaction of the node.
 *
 * @param msgTriggered
 *   specifies whether the node should work in message-triggered mode:
 *   - 0 - deactivate message triggering
 *   - 1 - activate message triggering
 *
 * @param cycleTime
 *   cycle time in ms for processing the node, value range 1..100
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetOperatingMode(dword msgTriggered, dword cycleTime, dword * errCode);

}
