#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if a SDO upload is executed.
 *
 * This function is called if an SDO upload is executed on an object that was created with the access type 7.
 * The transfer can be confirmed with coODUploadResponse or aborted with coODAbortTransfer.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 */
void coOnUploadIndication(dword index, dword subIndex);

}
