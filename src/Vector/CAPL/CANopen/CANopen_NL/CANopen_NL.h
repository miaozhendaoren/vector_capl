#pragma once

/**
 * @ingroup CANopen
 * @{
 * @defgroup CANopen_NL CANopen Node Layer CAPL Functions
 * @}
 */

/* Node Control */
#include "coAllowStart.h"
#include "coStartUp.h"
#include "coShutDown.h"
#include "coGetNodeId.h"
#include "coGetLocalState.h"
#include "coSetLocalState.h"
#include "coEmergency.h"
#include "coSetOperatingMode.h"

/* Network Management */
#include "coGetNMTState.h"
#include "coSetNMTState.h"

/* Generate the local object dictionary */
#include "coODCreate.h"
#include "coODCreateSigned.h"
#include "coODCreateUnsigned.h"
#include "coODCreateFloat.h"
#include "coODCreateArray.h"
#include "coODCreateFromFile.h"

/* Access to the local object dictionary */
#include "coODSetSigned.h"
#include "coODGetSigned.h"
#include "coODSetUnsigned.h"
#include "coODGetUnsigned.h"
#include "coODSetFloat.h"
#include "coODGetFloat.h"
#include "coODSetData.h"
#include "coODGetData.h"
#include "coODGetSize.h"
#include "coODGetBitSize.h"
#include "coODGetType.h"
#include "coODConnectEnvVar.h"
#include "coODDisconnectEnvVar.h"
#include "coODUploadResponse.h"
#include "coODDownloadResponse.h"
#include "coODAbortTransfer.h"

/* Access to the object dictionary of other nodes */
#include "coDownload.h"
#include "coDownloadExpedited.h"
#include "coUpload.h"
#include "coSetSDOTimeout.h"
#include "coSetBlockSize.h"

/* PDO Control */
#include "coTriggerTPDO.h"

/* Layer Setting Services */
#include "coLssSwitchModeGlob.h"
#include "coLssSwitchModeSel.h"
#include "coLssConfigNodeId.h"
#include "coLssStoreConfig.h"
#include "coLssInqNodeId.h"
#include "coLssInqVendorId.h"
#include "coLssInqProdCode.h"
#include "coLssInqRevNo.h"
#include "coLssInqSerialNo.h"
#include "coLssIdentRemoteSlave.h"
#include "coLssIdentNonConfigSlave.h"

/* Event functions */
#include "coOnBootUp.h"
#include "coOnConfigReady.h"
#include "coOnDownloadIndication.h"
#include "coOnDownloadResponse.h"
#include "coOnUploadIndication.h"
#include "coOnUploadResponse.h"
#include "coOnNodeChangedState.h"
#include "coOnRPDOMissing.h"
#include "coOnLssEvent.h"
#include "coOnEmergency.h"
#include "coOnError.h"

/* Data access in event functions */
#include "coThisGetSigned.h"
#include "coThisGetUnsigned.h"
#include "coThisGetFloat.h"
#include "coThisGetData.h"
#include "coThisGetSize.h"

/* Error handling */
#include "coGetLastError.h"

/* Other functions */
#include "coGetNodeLabel.h"
#include "coSetNodeLabel.h"
#include "coGetVersionInfo.h"
#include "coSetOutputLevel.h"
#include "coPutValue.h"
#include "coGetValue.h"
