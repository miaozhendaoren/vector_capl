#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Writes an entry into the object dictionary of another node by using the SDO download. (form 1)
 *
 * Writing of an entry into the object dictionary of another node.
 *
 * The function triggers a SDO download. The response of the node is returned in the event function coOnDownloadResponse or coOnError.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param data
 *   data of the object
 *
 * @param dataSize
 *   number of data bytes
 *
 * @param flags
 *   bit field with options:
 *   - 0 - block transfer off
 *   - 1 - block transfer on
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coDownload(dword nodeId, dword index, dword subIndex, char * data, dword dataSize, dword flags, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Writes an entry into the object dictionary of another node by using the SDO download. (form 2)
 *
 * Writing of an entry into the object dictionary of another node.
 *
 * The function triggers a SDO download. The response of the node is returned in the event function coOnDownloadResponse or coOnError.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param data
 *   data of the object
 *
 * @param dataSize
 *   number of data bytes
 *
 * @param flags
 *   bit field with options:
 *   - 0 - block transfer off
 *   - 1 - block transfer on
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coDownload(dword nodeId, dword index, dword subIndex, byte * data, dword dataSize, dword flags, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Writes an entry into the object dictionary of another node by using the SDO download. (form 3)
 *
 * Writing of an entry into the object dictionary of another node.
 *
 * The function triggers a SDO download. The response of the node is returned in the event function coOnDownloadResponse or coOnError.
 *
 * If it is used with the float parameter value, it always initiates a transfer with the length 8 bytes. This corresponds to the data type Real64.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param value
 *   value of the object
 *
 * @param flags
 *   bit field with options:
 *   - 0 - block transfer off
 *   - 1 - block transfer on
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coDownload(dword nodeId, dword index, dword subIndex, double value, dword flags, dword * errCode);

}
