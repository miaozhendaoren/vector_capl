#include "coODGetSigned.h"

#include <iostream>

namespace capl
{

long coODGetSigned(dword index, dword subIndex, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
