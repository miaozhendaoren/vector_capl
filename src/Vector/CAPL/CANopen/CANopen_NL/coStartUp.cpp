#include "coStartUp.h"

#include <iostream>

namespace capl
{

void coStartUp(dword nodeId, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
