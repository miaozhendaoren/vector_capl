#include "coOnConfigReady.h"

#include <iostream>

namespace capl
{

void coOnConfigReady(dword event, dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
