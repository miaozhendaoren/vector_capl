#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   The LSS master commands all LSS slaves whose LSS address matches to the transmitted LSS address parameters to identify themselves.
 *
 * With this service the LSS master commands all LSS slaves whose LSS address matches the transmitted LSS address parameters to identify themselves.
 *
 * @param vendorId
 *   vendor-Id
 *
 * @param productCode
 *   product code
 *
 * @param revisionNoLow
 *   revision number (lower limit)
 *
 * @param revisionNoHigh
 *   revision number (upper limit)
 *
 * @param serialNoLow
 *   serial number (lower limit)
 *
 * @param serialNoHigh
 *   serial number (upper limit)
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssIdentRemoteSlave(dword vendorId, dword productCode, dword revisionNoLow, dword revisionNoHigh, dword serialNoLow, dword serialNoHigh, dword * errCode);

}
