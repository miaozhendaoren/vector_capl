#include "coAllowStart.h"

#include <iostream>

namespace capl
{

void coAllowStart(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
