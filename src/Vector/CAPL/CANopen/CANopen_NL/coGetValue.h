#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the current value of an environment variable of type integer.
 *
 * The function returns the current value of an environment variable of the type integer.
 *
 * @param envVar
 *   name of the environment variable
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   value of the environment variable
 */
long coGetValue(char * envVar, dword * errCode);

}
