#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called when the response to an SDO upload was received.
 *
 * This function is called when the response to an SDO upload was received (see coUpload).
 * Now the data can be accessed with coThisGetSigned, coThisGetUnsigned, coThisGetFloat, coThisGetData, and coThisGetSize.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param size
 *   size of the data byte, value range 0..4.294.967.295
 */
void coOnUploadResponse(dword nodeId, dword index, dword subIndex, dword size);

}
