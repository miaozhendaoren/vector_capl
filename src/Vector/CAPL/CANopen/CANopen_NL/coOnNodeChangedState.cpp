#include "coOnNodeChangedState.h"

#include <iostream>

namespace capl
{

void coOnNodeChangedState(dword newState)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
