#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the current communication state of the node.
 *
 * This function returns the current communication state of the node.
 * It is not necessary to determine the state permanently since a state change is signaled by the event function coOnNodeChangedState.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   current communication state of the node
 *   - 4 - stopped
 *   - 5 - operational
 *   - 6 - reset node
 *   - 7 - reset communication
 *   - 127 - pre-operational
 *   The states reset node and reset communication are transient states.
 *   This means they are not static and are only run through.
 *   The node then automatically goes to state pre-operational.
 */
dword coGetLocalState(dword * errCode);

}
