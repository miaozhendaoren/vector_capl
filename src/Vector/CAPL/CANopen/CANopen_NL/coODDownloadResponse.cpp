#include "coODDownloadResponse.h"

#include <iostream>

namespace capl
{

void coODDownloadResponse(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
