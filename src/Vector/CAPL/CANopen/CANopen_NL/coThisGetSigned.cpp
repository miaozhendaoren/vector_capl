#include "coThisGetSigned.h"

#include <iostream>

namespace capl
{

long coThisGetSigned(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
