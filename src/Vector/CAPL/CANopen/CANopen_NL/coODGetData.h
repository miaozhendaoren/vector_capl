#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the data of an object.
 *
 * Returns the data of an object in the local object dictionary.
 * This function can be applied to all objects.
 * For objects with a size up to 32 bits or floating point objects, however, the functions coODGetSigned, coODGetUnsigned, and coODGetFloat are recommended.
 *
 * For the use of this function, a note on handling should be considered.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param buffer
 *   buffer for the read data
 *
 * @param bufferSize
 *   size of the buffer in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   number of read data bytes
 */
dword coODGetData(dword index, dword subIndex, char * buffer, dword bufferSize, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the data of an object.
 *
 * Returns the data of an object in the local object dictionary.
 * This function can be applied to all objects.
 * For objects with a size up to 32 bits or floating point objects, however, the functions coODGetSigned, coODGetUnsigned, and coODGetFloat are recommended.
 *
 * For the use of this function, a note on handling should be considered.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param buffer
 *   buffer for the read data
 *
 * @param bufferSize
 *   size of the buffer in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   number of read data bytes
 */
dword coODGetData(dword index, dword subIndex, byte * buffer, dword bufferSize, dword * errCode);

}
