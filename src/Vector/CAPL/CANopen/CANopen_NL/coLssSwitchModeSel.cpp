#include "coLssSwitchModeSel.h"

#include <iostream>

namespace capl
{

void coLssSwitchModeSel(dword vendorId, dword productCode, dword revisionNo, dword serialNo, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
