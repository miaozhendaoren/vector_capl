#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   The function is called if an event timer of a RPDO is activated and the PDO is not received in the defined time.
 *
 * The function is called if an event timer of a RPDO is activated and the PDO is not received in the defined time.
 *
 * @param pdoNumber
 *   RPDO number 1..512
 */
void coOnRPDOMissing(dword pdoNumber);

}
