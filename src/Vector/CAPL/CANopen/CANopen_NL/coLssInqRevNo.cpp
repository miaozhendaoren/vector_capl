#include "coLssInqRevNo.h"

#include <iostream>

namespace capl
{

void coLssInqRevNo(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
