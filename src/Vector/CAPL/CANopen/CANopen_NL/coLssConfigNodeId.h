#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   The LSS master configures the node-ID of a LSS slave.
 *
 * With this service, the LSS master configures the node-ID of a LSS slave.
 * Only one LSS slave may be in configuration mode.
 *
 * @param nodeId
 *   node-ID that should be given to the LSS slave, value range 1..127
 *   If the value 0xFF is specified, then the previous node-ID is invalidated.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssConfigNodeId(dword nodeId, dword * errCode);

}
