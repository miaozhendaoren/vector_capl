#include "coLssIdentRemoteSlave.h"

#include <iostream>

namespace capl
{

void coLssIdentRemoteSlave(dword vendorId, dword productCode, dword revisionNoLow, dword revisionNoHigh, dword serialNoLow, dword serialNoHigh, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
