#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates a new array object in the local object dictionary.
 *
 * Generates a new field object in the local object dictionary.
 * If the object already exists, then it is replaced by the new object.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param dataType
 *   data type of the object
 *
 * @param accessSub0
 *   access type of the first sub object
 *
 * @param access
 *   access type of all additional sub objects
 *
 * @param elementCount
 *   number of elements of the field, value range 1..254
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreateArray(dword index, dword dataType, dword accessSub0, dword access, dword elementCount, dword * errCode);

}
