#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if an emergency message was received.
 *
 * This function is called if an emergency message was received.
 * Data of manufacturer emergency codes can be read with coThisGetData.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param errorCode
 *   emergency error code, value range 0..65.535
 *
 * @param errorRegister
 *   content of the error register
 */
void coOnEmergency(dword nodeId, dword errorCode, dword errorRegister);

}
