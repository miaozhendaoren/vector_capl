#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the time-out value for an abort of a SDO transfer.
 *
 * The function sets the time after which a SDO transmission is aborted if the communication partner does not response.
 * The default value is 2000 ms and applies globally for all SDO transmission.
 *
 * @param time
 *   time value in ms, value range 0..60000
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetSDOTimeout(dword time, dword * errCode);

}
