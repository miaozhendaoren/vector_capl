#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Deletes the connection of an object to an environment variable.
 *
 * Deletes the connection between an object of the local object dictionary and an environment variable.
 *
 * @param index
 *   index of the object, value range 1..65535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODDisconnectEnvVar(dword index, dword subIndex, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Deletes the connection of an object to an environment variable.
 *
 * Deletes the connection between an object of the local object dictionary and an environment variable.
 *
 * @param envVar
 *   name of the environment variable
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODDisconnectEnvVar(char * envVar, dword * errCode);

}
