#include "coODAbortTransfer.h"

#include <iostream>

namespace capl
{

void coODAbortTransfer(dword abortCode, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
