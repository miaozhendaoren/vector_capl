#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the error code and text of the last function call.
 *
 * The function returns the error code and error text of the last function call.
 *
 * @param buffer
´*   buffer for the error text
 *
 * @param bufferSize
 *   size of the buffer in bytes
 *
 * @return
 *   error code
 */
dword coGetLastError(char * buffer, dword bufferSize);

}
