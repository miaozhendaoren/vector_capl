#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the data of an object.
 *
 * The function returns the data of an object in the event functions coOnUploadResponse or coOnDownloadIndication.
 * The call is only allowed in these event functions.
 * The size of the data can be determined with coThisGetSize.
 *
 * @param buffer
 *   buffer for the read data
 *
 * @param bufferSize
 *   size of the buffer in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coThisGetData(char * buffer, dword bufferSize, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the data of an object.
 *
 * The function returns the data of an object in the event functions coOnUploadResponse or coOnDownloadIndication.
 * The call is only allowed in these event functions.
 * The size of the data can be determined with coThisGetSize.
 *
 * @param buffer
 *   buffer for the read data
 *
 * @param bufferSize
 *   size of the buffer in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coThisGetData(byte * buffer, dword bufferSize, dword * errCode);

}
