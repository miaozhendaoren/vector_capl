#include "coGetNodeLabel.h"

#include <iostream>

namespace capl
{

void coGetNodeLabel(char * buffer, dword bufferSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
