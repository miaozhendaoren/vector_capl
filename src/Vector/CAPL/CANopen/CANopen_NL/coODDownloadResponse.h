#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Confirms a SDO download transfer.
 *
 * The function confirms a SDO download transfer, which was initiated on an object with access type 7.
 * It can be used, e.g. in the event function coOnDownloadIndication.
 * If the transfer should be aborted, then coODAbortTransfer must be used.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODDownloadResponse(dword * errCode);

}
