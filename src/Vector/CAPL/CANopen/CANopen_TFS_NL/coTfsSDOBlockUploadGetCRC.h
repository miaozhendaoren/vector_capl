#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Gets the CRC checksum of last successful SDO block upload.
 *
 * This function makes available the received CRC checksum of the last successful SDO block upload procedure.
 *
 * @param outValueBuf
 *   buffer to store the received CRC checksum
 *
 * @return
 *   error code
 */
long coTfsSDOBlockUploadGetCRC(dword outValueBuf[]);

}
