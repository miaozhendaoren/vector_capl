#include "coTfsSDOBlockInit.h"

#include <iostream>

namespace capl
{

long coTfsSDOBlockInit(dword index, dword subIndex, dword ccs, dword cs, dword crcSupported, dword  sizeIndicated, dword blkSize, dword size, dword threshold)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
