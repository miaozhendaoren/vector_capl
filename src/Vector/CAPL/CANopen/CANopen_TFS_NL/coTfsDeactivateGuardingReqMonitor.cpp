#include "coTfsDeactivateGuardingReqMonitor.h"

#include <iostream>

namespace capl
{

long coTfsDeactivateGuardingReqMonitor(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
