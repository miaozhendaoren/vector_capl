#include "coTfsSDOBlockUploadSegmentResponse.h"

#include <iostream>

namespace capl
{

long coTfsSDOBlockUploadSegmentResponse(dword ccs, dword cs, dword ackSeq, dword blkSize, dword expMsgNumber, dword startSeqNumber)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
