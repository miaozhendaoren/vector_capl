#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   NMT request
 *
 * This function sends a NMT request.
 *
 * For selection of the command parameter value 129 or 130 the function waits for the following boot-up message; otherwise the function attempts to check the device status of the DUT via the guarding or heartbeat mechanism.
 *
 * For this, there is first an attempt to configure a heartbeat producer in the DUT (object 0x1017). The heartbeat message contains the current device status. If the DUT should not make a heartbeat producer available, a remote guarding frame is sent to the DUT. The DUT responds with the corresponding guarding response. The procedure is repeated again. The second response is evaluated and contains the device status. The CANopen® specification provides that a CANopen® device supports at least one of the two modes.
 *
 * If the stop command (command=2) is sent, it is not checked if the DUT switches to stopped state.
 *
 * @param command
 *   - 1: start
 *   - 2: stop
 *   - 128: enter pre-operational
 *   - 129: reset node
 *   - 130: reset communication
 *
 * @return
 *   error code
 */
long coTfsNmtRequest(dword command);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   NMT request
 *
 * This function sends a NMT request.
 *
 * For selection of the command parameter value 129 or 130 the function waits for the following boot-up message; otherwise the function attempts to check the device status of the DUT via the guarding or heartbeat mechanism.
 *
 * For this, there is first an attempt to configure a heartbeat producer in the DUT (object 0x1017). The heartbeat message contains the current device status. If the DUT should not make a heartbeat producer available, a remote guarding frame is sent to the DUT. The DUT responds with the corresponding guarding response. The procedure is repeated again. The second response is evaluated and contains the device status. The CANopen® specification provides that a CANopen® device supports at least one of the two modes.
 *
 * If the stop command (command=2) is sent, it is not checked if the DUT switches to stopped state.
 *
 * @param command
 *   - 1: start
 *   - 2: stop
 *   - 128: enter pre-operational
 *   - 129: reset node
 *   - 130: reset communication
 *
 * @param broadcastFlag
 *   - !=0: NMT message is sent to all nodes (broadcast)
 *
 * @return
 *   error code
 */
long coTfsNmtRequest(dword command, dword broadcastFlag);

}
