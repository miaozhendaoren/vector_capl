#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the size of the received SDO data of an expedited/segmented/block upload.
 *
 * Provide the size of the transferred data after a SDO upload. This can be an expedited, segmented or block transfer.
 *
 * @return
 *   transfer size
 */
dword coTfsSDOGetUploadSize(void);

}
