#include "coTfsNMTEnterPreOperational.h"

#include <iostream>

namespace capl
{

long coTfsNmtEnterPreOperational(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsNmtEnterPreOperational(dword broadcastFlag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
