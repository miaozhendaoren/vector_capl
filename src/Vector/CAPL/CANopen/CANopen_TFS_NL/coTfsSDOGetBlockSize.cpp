#include "coTfsSDOGetBlockSize.h"

#include <iostream>

namespace capl
{

dword coTfsSDOGetBlockSize(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
