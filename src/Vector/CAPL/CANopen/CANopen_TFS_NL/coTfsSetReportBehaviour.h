#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the report behavior of the CANopen TFS node layer.
 *
 * With this function, the report behavior of the node layer can be set. In contrast to the function coTfsSetFailControl outputs were not interchanged but enabled/disabled selective.
 *
 * The parameter reportSetting is a bit field and sets the behavior of the node layer. Per default all outputs are enabled (reportSettings=0x3FFFDFFF)
 *
 * @param reportSetting
 *   bit field to set the report behavior:
 *   - bit 0: output of the initial message of each test
 *   - bit 1: output of the standard Test step messages
 *   - bit 2: output of the Test passed message
 *   - bit 3: output of the Test failed message
 *   - bit 4: output of warnings
 *   - bit 5: information messages
 *   - bit 6: expected messages, if a test fails the report contains a summary of expected messages
 *   - bit 7: message data (transferred data and used masks)
 *   - bit 8: all SDO abort codes that are not sent from the current DUT
 *   - bit 9: all received SDO abort codes including the DUT ones
 *   - bit 12: all received emergency codes
 *   - bit 13: SDO protocol specific control bits
 *   - bit 19: information about injected or replaced messages
 *   - bit 20: output of generic monitor messages
 *   - bit 21: output of generic monitor warning messages
 *   all other bits are reserved
 *
 * @return
 *   error code
 */
long coTfsSetReportBehaviour(dword reportSetting);

}
