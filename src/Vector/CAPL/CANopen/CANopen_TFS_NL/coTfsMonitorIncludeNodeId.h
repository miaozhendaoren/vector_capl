#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Includes a single node-ID for monitoring.
 *
 * This function includes a single node-ID for monitoring. To include a range of node-IDs use the function coTfsMonitorIncludeNodeIdRange.
 *
 * You can start the monitoring with coTfsMonitorActivate and stop it with coTfsMonitorDeactivate.
 *
 * To exclude node-IDs or ranges of node-IDs from monitoring you can use the functions coTfsMonitorExcludeNodeId and coTfsMonitorExcludeNodeIdRange. With coTfsMonitorSetNmtTimeout and coTfsMonitorSetSdoTimeout you can set NMT and SDO timeouts.
 *
 * @param nodeID
 *   node-ID to be monitored
 *
 * @return
 *   error code
 */
long coTfsMonitorIncludeNodeId(dword nodeId);

}
