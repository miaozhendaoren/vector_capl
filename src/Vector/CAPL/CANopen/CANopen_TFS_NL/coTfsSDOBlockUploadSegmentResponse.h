#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends the response to a SDO upload block request.
 *
 * This function waits for a SDO segmented upload request and sends the corresponding response. Before the call of this function, no time-delay functions should be inserted in order to prevent a loss of the request message.
 *
 * @param ccs (client command specifier)
 *   - 5: block upload
 *
 * @param cs (client subcommand)
 *   - 2: block upload response
 *
 * @param ackSeq
 *   sequence number of the message that should be confirmed
 *
 * @param blkSize
 *   block size
 *
 * @param expMsgNumber
 *   number of requests awaited (generally corresponds to the value ackSeq), deviations can be used in order to provoke protocol violations
 *
 * @param startSeqNumber
 *   SDO block number of the first block expected
 *
 * @return
 *   error code
 */
long coTfsSDOBlockUploadSegmentResponse(dword ccs, dword cs, dword ackSeq, dword blkSize, dword expMsgNumber, dword startSeqNumber);

}
