#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Switches off the checking of the SYNC messages.
 *
 * This function switches off the calling of the sync callbacks if these are switched on. Furthermore all active sync PDO checks are also disabled.
 *
 * The callbacks can be switched on with coTfsActivateSyncMonitor.
 *
 * @param canId
 *   CAN identifier
 *
 * @return
 *   error code
 */
long coTfsDeactivateSyncMonitor(dword canId);

}
