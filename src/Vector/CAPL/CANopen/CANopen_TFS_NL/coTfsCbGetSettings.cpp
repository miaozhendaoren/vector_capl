#include "coTfsCbGetSettings.h"

#include <iostream>

namespace capl
{

long coTfsCbGetSettings(dword type, dword nodeId, dword cycleTime[1], dword tolerance[1])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
