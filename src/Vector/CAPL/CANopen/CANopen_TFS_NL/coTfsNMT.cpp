#include "coTfsNMT.h"

#include <iostream>

namespace capl
{

long coTfsNmt(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
