#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the received SDO data of an expedited/segmented/block upload.
 *
 * This function makes available the data of the last successful SDO upload procedure. This can be an expedited, segmented or block transfer.
 *
 * The data field outValueBuf must always be able to accommodate the received data.
 *
 * @param outValueBuf
 *   received SDO upload data
 *   Take care that this data field is of appropriate size depending on the use case!
 *
 * @param valueBufSize
 *   buffer size in byte of outValueBuf
 *
 * @return
 *   - !0: number of received bytes
 *   - 0: data not available or data buffer outValueBuf too small
 */
long coTfsSDOGetUploadData(byte outValueBuf[], dword valueBufSize);

}
