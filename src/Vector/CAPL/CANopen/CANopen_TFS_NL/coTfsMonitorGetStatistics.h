#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns statistics information.
 *
 * This function returns the statistics information that are monitored between coTfsMonitorActivate and coTfsMonitorDeactivate.
 *
 * @param nodeId
 *   node-ID [1..127]
 *
 * @param requestType
 *   values to be monitored
 *   - 1: SDO time values
 *   - 2: NMT time values
 *   - 3: LSS time values
 *
 * @param minValue[1]
 *   minimum value in [ms]
 *
 * @param maxValue[1]
 *   maximum value in [ms]
 *
 * @param averageValue[1]
 *   average value in [ms]
 *
 * @param passedCounter[1]
 *   number of values within the allowed range
 *
 * @param failedCounter[1]
 *   number of values out of the allowed range
 *
 * @return
 *   error code
 */
long coTfsMonitorGetStatistics(dword nodeId, dword requestType, dword minValue[1], dword maxValue[1], dword averageValue[1], dword passedCounter[1] , dword failedCounter[1]);

}
