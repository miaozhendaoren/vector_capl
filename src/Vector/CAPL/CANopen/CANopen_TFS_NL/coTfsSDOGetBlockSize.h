#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the block size that is used for a block transfer.
 *
 * The function returns the block size of the last Initiate SDO Block Download or Download SDO Block Segment Response message.
 *
 * With a return value 0, the message is not detected or awaited. The block size can only be read out once after each individual block
 *
 * @return
 *   block size
 */
dword coTfsSDOGetBlockSize(void);

}
