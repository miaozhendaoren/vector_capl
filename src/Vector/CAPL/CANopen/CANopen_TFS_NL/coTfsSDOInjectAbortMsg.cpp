#include "coTfsSDOInjectAbortMsg.h"

#include <iostream>

namespace capl
{

long coTfsSDOInjectAbortMsg(dword canId, dword index, dword subIndex, dword abortCode, dword transmitCounter, dword replace)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
