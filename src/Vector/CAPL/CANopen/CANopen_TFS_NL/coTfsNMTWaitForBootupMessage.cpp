#include "coTfsNMTWaitForBootupMessage.h"

#include <iostream>

namespace capl
{

long coTfsNmtWaitForBootupMessage(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
