#include "coTfsGetApplProfile.h"

#include <iostream>

namespace capl
{

dword coTfsGetApplProfile(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
