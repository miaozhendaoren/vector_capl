#include "coTfsSetReportBehaviour.h"

#include <iostream>

namespace capl
{

long coTfsSetReportBehaviour(dword reportSetting)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
