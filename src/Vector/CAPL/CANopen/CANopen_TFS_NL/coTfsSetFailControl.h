#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Function that influences the report behavior of individual test functions.
 *
 * Determines the behavior of CANopen test feature set tests. The entries in the test report can be switched with this function, that is, a test that in the default setting 0 has passed also receives the note "passed" in the test protocol. Conversely, a failed test receives the note "failed".
 *
 * The call of this function with the parameter 1 reverses the behavior. If the device should pass a test, failed is in the test report and vice-versa. This behavior can be used to create a negative test.
 *
 * After measurement start, the setting 0 is always active.
 *
 * The return value of the test functions themselves in the CAPL program is never changed, only the protocol is adjusted.
 *
 * @param controlValue
 *   - 0: normal behavior (default)
 *   - 1: inverted behavior
 *   - 2: the test case is not marked with pass or fail in the test report, the user must ensure the corresponding entries himself
 *
 * @return
 *   error code
 */
long coTfsSetFailControl(dword controlValue);

}
