#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Starts a life guarding test.
 *
 * The test sets the guard time and retry factor objects in the DUT. After that, 20 guarding remote frames are sent to the target device which must respond to all queries within the guardTime+tolerance.
 *
 * Afterwards the sending of the remote frames is stopped. It is waited for the corresponding emergency message (EMCY code = 0x8130, Error Register = 0x11) before the values of the guard time and retry factor objects are reset.
 *
 * @param guardTime
 *   guard time in milliseconds
 *
 * @param retryFactor
 *   retry factor
 *
 * @param tolerance
 *   permitted time deviation of the target device in milliseconds
 *
 *   It is recommended that you use an even value. The tolerated time frame within which a message is still accepted is: x - (delta/2) <= x <= x + (delta/2)
 *
 * @return
 *   error code
 */
long coTfsLifeGuarding(dword guardTime, dword retryFactor, dword tolerance);

}
