#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets application profile.
 *
 * The function enables the support of an application profile. The function has to be called before any test functions are executed.
 *
 * @param profile
 *   number of profile which is to be set, the values 0 and 301 are saved as 301 and represent the default value
 *
 * @return
 *   error code
 */
long coTfsSetApplProfile(dword profile);

}
