#include "coTfsNodeGuarding.h"

#include <iostream>

namespace capl
{

long coTfsNodeGuarding(dword maxResponseTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
