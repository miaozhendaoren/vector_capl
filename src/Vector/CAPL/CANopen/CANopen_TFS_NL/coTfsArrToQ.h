#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Converts a byte array to a qword value.
 *
 * The function converts the content of a byte array into a qword value.
 *
 * @param inByteArray
 *   array containing the bytes to be converted, recommended array size: 8 byte
 *
 * @param arraysize
 *   size of the array
 *
 * @return
 *   converted value or 0 if parameters are invalid
 */
qword coTfsArrToQ(byte inByteArray[], dword arraysize);

}
