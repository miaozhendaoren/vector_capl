#include "coTfsMonitorGetStatistics.h"

#include <iostream>

namespace capl
{

long coTfsMonitorGetStatistics(dword nodeId, dword requestType, dword minValue[1], dword maxValue[1], dword averageValue[1], dword passedCounter[1] , dword failedCounter[1])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
