#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Switches off the checking of the SYNC PDO messages.
 *
 * This function switches off the calling of all switched on sync PDO callbacks.
 *
 * The callbacks can be switched on with coTfsActivateSyncPdoMonitor.
 *
 * @param pdoCanId
 *   CAN-ID of the PDO to be monitored
 *
 * @return
 *   error code
 */
long coTfsDeactivateSyncPdoMonitor(dword pdoCanId);

}
