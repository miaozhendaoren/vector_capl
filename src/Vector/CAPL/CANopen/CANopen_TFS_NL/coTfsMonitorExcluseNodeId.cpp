#include "coTfsMonitorExcluseNodeId.h"

#include <iostream>

namespace capl
{

long coTfsMonitorExcludeNodeId(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
