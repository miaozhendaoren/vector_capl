#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Calculates the CRC checksum for a block transfer.
 *
 * This function calculates the CRC checksum for a block transfer (SDO block upload or SDO block download)- necessary for the calculation of the CRC across several data ranges).
 *
 * @param inValueBuf
 *   In this data field are the data for which the CRC calculation is executed
 *
 * @param valueBufSize
 *   sbuffer size in byte of inValueBuf
 *
 * @return
 *   CRC checksum
 */
dword coTfsSDOCalcCrc(byte inValueBuf[], dword valueBufSize);

}
