#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Configures a TPDO as cyclic and starts a virtual sync producer.
 *   Checks the transmit performance of the test node.
 *
 * @todo
 */

}
