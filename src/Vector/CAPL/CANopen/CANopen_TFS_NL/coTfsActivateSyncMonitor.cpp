#include "coTfsActivateSyncMonitor.h"

#include <iostream>

namespace capl
{

long coTfsActivateSyncMonitor(dword canId, dword producerTime, dword tolerance, dword windowLength, dword maxCounter)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
