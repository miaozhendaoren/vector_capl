#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Excludes a range of node-IDs from monitoring.
 *
 * This function excludes the specified range of node-IDs from monitoring. To exclude a single node-ID use the function coTfsMonitorExcludeNodeId.
 *
 * You can start the monitoring with coTfsMonitorActivate and stop it with coTfsMonitorDeactivate.
 *
 * To include node-IDs or ranges of node-IDs for monitoring you can use the functions coTfsMonitorIncludeNodeId and coTfsMonitorIncludeNodeIdRange. With coTfsMonitorSetNmtTimeout and coTfsMonitorSetSdoTimeout you can set NMT and SDO timeouts.
 *
 * @param minNodeId
 *   lower limit
 *
 * @param maxNodeId
 *   upper limit
 *
 * @return
 *   error code
 */
long coTfsMonitorExcludeNodeIdRange(dword minNodeId, dword maxNodeId);

}
