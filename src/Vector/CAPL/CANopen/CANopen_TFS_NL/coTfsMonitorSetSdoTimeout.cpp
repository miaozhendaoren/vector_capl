#include "coTfsMonitorSetSdoTimeout.h"

#include <iostream>

namespace capl
{

long coTfsMonitorSetSdoTimeout(dword sdoTimeoutInMs)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
