#include "coTfsGuarding.h"

#include <iostream>

namespace capl
{

long coTfsGuarding(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
