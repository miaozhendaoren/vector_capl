#include "coTfsSDOInit.h"

#include <iostream>

namespace capl
{

long coTfsSDOInit(dword index, dword subIndex, dword ccs, dword expedited, dword sizeIndicated, dword notUsed, byte  inValueBuf[], dword valueBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
