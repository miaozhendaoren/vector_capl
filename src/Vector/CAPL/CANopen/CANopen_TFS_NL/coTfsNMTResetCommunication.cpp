#include "coTfsNMTResetCommunication.h"

#include <iostream>

namespace capl
{

long coTfsNmtResetCommunication(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsNmtResetCommunication(dword broadcastFlag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
