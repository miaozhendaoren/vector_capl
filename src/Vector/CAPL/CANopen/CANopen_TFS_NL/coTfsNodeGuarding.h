#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns received emergency messages (of a node).
 *
 * After that, 20 guarding remote frames are sent to the target device. The target device must respond to all queries within the maxResponseTime.
 *
 * @param maxResponseTime
 *   maximum permissible response time in milliseconds
 *
 * @return
 *   error code
 */
long coTfsNodeGuarding(dword maxResponseTime);

}
