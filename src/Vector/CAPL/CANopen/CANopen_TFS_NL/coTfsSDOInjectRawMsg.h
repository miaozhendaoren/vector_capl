#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Inserts a CAN message to the following command.
 *
 * This function inserts any CAN message during the following cotfs command. Only one message is supported and the insertion is only possible for the directly following command. The inserted message is sent directly after the x.message, whereas x is defined with transmitCounter.
 *
 * If less messages than expected are sent by the following command (transmitCounter > number of sent messages), the insertion of the message is canceled.
 *
 * @param canId
 *   CAN identifier, highest bit for extended and standard CAN identifier message
 *
 * @param dlc
 *   data length code
 *
 * @param buf
 *   CAN data bytes to be sent
 *
 * @param bufSize
 *   buffer size in byte of buf
 *
 * @param isRtr
 *   - 1: remote frame
 *   - 0: no remote frame
 *
 * @param transmitCounter
 *   specifies the number of cotfs commands after that the defined message will be sent or which message number will be replaced
 *   If not enough messages were sent by the following cotfs command, no message will be inserted/replaced at all (transmitCounter = 1..x).
 *
 * @param replace
 *   - 0: the message to be injected is sent after the regular planned CAN message
 *   - 1: the regular planned CAN message is replaced with the message to be injected
 *
 * @return
 *   error code
 */
long coTfsSDOInjectRawMsg(dword canId, dword dlc, byte buf[], dword bufSize, dword isRtr, dword transmitCounter, dword replace);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Inserts a CAN message to the following command.
 *
 * This function inserts any CAN message during the following cotfs command. Only one message is supported and the insertion is only possible for the directly following command. The inserted message is sent directly after the x.message, whereas x is defined with transmitCounter.
 *
 * If less messages than expected are sent by the following command (transmitCounter > number of sent messages), the insertion of the message is canceled.
 *
 * @param canId
 *   CAN identifier, highest bit for extended and standard CAN identifier message
 *
 * @param dlc
 *   data length code
 *
 * @param buf
 *   CAN data bytes to be sent
 *
 * @param isRtr
 *   - 1: remote frame
 *   - 0: no remote frame
 *
 * @param transmitCounter
 *   specifies the number of cotfs commands after that the defined message will be sent or which message number will be replaced
 *   If not enough messages were sent by the following cotfs command, no message will be inserted/replaced at all (transmitCounter = 1..x).
 *
 * @param replace
 *   - 0: the message to be injected is sent after the regular planned CAN message
 *   - 1: the regular planned CAN message is replaced with the message to be injected
 *
 * @return
 *   error code
 */
long coTfsSDOInjectRawMsg(dword canId, dword dlc, qword buf, dword isRtr, dword transmitCounter, dword replace);

}
