#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Resets the CANopen TFS node layer.
 *
 * This function resets the CANopen TFS node layer to the default values of measurement setup.
 */
void coTfsResetTfs(void);

}
