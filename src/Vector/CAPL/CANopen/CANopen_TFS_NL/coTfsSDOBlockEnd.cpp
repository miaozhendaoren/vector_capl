#include "coTfsSDOBlockEnd.h"

#include <iostream>

namespace capl
{

long coTfsSDOBlockEnd(dword ccs, dword cs, dword notUsed, dword crc)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsSDOBlockEnd(dword ccs, dword cs, dword notUsed, dword crc, dword crcSetting)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
