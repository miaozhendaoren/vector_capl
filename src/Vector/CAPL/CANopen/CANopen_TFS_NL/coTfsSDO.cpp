#include "coTfsSDO.h"

#include <iostream>

namespace capl
{

long coTfsSDO(dword index, dword subIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
