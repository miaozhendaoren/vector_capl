#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Gets the node-ID of the tester.
 *
 * The function gets the node-ID of the tester.
 *
 * @return
 *   node-ID of the tester
 */
dword coTfsGetTestModuleNodeId(void);

}
