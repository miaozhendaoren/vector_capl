#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an individual data segment of a segmented SDO transfer.
 *
 * This function sends a SDO segmented download or SDO segmented upload request/response and awaits the corresponding response. The DS-301 describes the precise protocol and all parameters.
 *
 * @param ccs (client command specifier)
 *    - 3: upload
 *    - 0: download, other values can cause unexpected behavior
 *
 * @param cont
 *    - 0: other segments will follow
 *    - 1: last segment
 *
 * @param toggleBit
 *    value of the toggle bit (0 or 1, start with 0)
 *
 * @param notUsed
 *    number of bytes that contain no reference data
 *
 * @param inValueBuf
 *    data field for the reference data
 *
 * @param valueBufSize
 *    buffer size in byte of inValueBuf
 *
 * @return
 *   error code
 */
long coTfsSDOSegmentRequest(dword ccs, dword cont, dword toggleBit, dword notUsed, byte inValueBuf[], dword valueBufSize);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an individual data segment of a segmented SDO transfer.
 *
 * This function sends a SDO segmented download or SDO segmented upload request/response and awaits the corresponding response. The DS-301 describes the precise protocol and all parameters.
 *
 * @param ccs (client command specifier)
 *    - 3: upload
 *    - 0: download, other values can cause unexpected behavior
 *
 * @param cont
 *    - 0: other segments will follow
 *    - 1: last segment
 *
 * @param toggleBit
 *    value of the toggle bit (0 or 1, start with 0)
 *
 * @param notUsed
 *    number of bytes that contain no reference data
 *
 * @param inValue
 *    reference data
 *
 * @return
 *   error code
 */
long coTfsSDOSegmentRequest(dword ccs, dword cont, dword toggleBit, dword notUsed, qword inValue);

}
