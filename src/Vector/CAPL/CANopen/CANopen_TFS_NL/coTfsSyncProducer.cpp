#include "coTfsSyncProducer.h"

#include <iostream>

namespace capl
{

long coTfsSyncProducer(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
