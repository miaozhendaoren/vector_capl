#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT waits for a NMT boot-up message.
 *
 * This function waits for the boot-up message.
 *
 * @return
 *   error code
 */
long coTfsNmtWaitForBootupMessage(void);

}
