#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT is set to state "Stopped".
 *
 * This call triggers a NMT message that sets the DUT into the stopped state. The new state is not checked because SDO transfers are not allowed in stopped mode.
 *
 * @return
 *   error code
 */
long coTfsNmtStopNode(void);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT is set to state "Stopped".
 *
 * This call triggers a NMT message that sets the DUT into the stopped state. The new state is not checked because SDO transfers are not allowed in stopped mode.
 *
 * @param broadcastFlag
 *   - !=0: NMT message is sent to all nodes (broadcast)
 *
 * @return
 *   error code
 */
long coTfsNmtStopNode(dword broadcastFlag);

}
