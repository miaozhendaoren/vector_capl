#include "coTfsConfigureGenericMonitor.h"

#include <iostream>

namespace capl
{

long coTfsConfigureGenericMonitor(dword canId, dword isRTR, dword dlc, dword msgData, qword msgMask, dword minOcc, dword maxOcc, char comment[], dword varContent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
