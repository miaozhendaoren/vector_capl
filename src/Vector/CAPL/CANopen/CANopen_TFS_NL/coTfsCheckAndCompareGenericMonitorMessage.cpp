#include "coTfsCheckAndCompareGenericMonitorMessage.h"

#include <iostream>

namespace capl
{

long coTfsCheckAndCompareGenericMonitorMessage(dword canId, dword dlc, qword msgData, qword msgMask, dword order)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
