#include "coTfsSDOAbort.h"

#include <iostream>

namespace capl
{

long coTfsSDOAbort(dword index, dword subIndex, dword abortCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
