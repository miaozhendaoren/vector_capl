#include "coTfsSDOBlockUploadGetCRC.h"

#include <iostream>

namespace capl
{

long coTfsSDOBlockUploadGetCRC(dword outValueBuf[])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
