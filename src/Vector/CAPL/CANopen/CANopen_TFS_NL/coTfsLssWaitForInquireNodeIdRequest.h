#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Waits for the Inquire node-ID request and sends the response.
 *
 * @todo
 */

}
