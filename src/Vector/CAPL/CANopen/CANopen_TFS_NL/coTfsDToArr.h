#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Converts a dword value to a byte array.
 *
 * The function converts a dword value into a byte array.
 *
 * @param dwordValue
 *   value to be converted
 *
 * @param outByteArray
 *   array containing the converted value, recommended array size: 4 byte
 *
 * @param arraysize
 *   size of the byte array
 *
 * @return
 *   error code
 */
long coTfsDToArr(dword dwordValue, byte outByteArray[], dword arraysize);

}
