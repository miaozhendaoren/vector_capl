#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Starts a SYNC producer test.
 *
 * The call of this function starts a sync test that lasts 5 s. The sync producer in the DUT generates a sync message every 100 ms. The tolerance is ±10 ms. At the end of the test, the successful switch-off of the sync producer is checked.
 *
 * This test does not support devices, which use the sync counter. Use the function coTfsSyncProducerDetail() instead.
 *
 * @return
 *   error code
 */
long coTfsSyncProducer(void);

}
