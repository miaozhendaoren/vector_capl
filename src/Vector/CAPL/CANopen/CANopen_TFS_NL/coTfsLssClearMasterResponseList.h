#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Clears the internal list of expected LSS Master requests and answers.
 *
 * @todo
 */

}
