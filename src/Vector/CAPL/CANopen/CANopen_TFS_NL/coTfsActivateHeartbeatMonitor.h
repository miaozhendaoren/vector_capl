#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks the temporally correct occurrence of the heartbeat producer message and calls the corresponding callback functions.
 *
 * This function checks whether the DUT as heartbeat producer makes the heartbeat message available within the defined times.
 *
 * For each correct occurrence, the callback void coTfsOnHeartbeatMsg (dword nodeId) is called. If the time is not adhered to, then the callback void coTfsOnHeartbeatFail(dword nodeId, dword cause) is called. After an error has occurred, the callback system is automatically disabled. The reason for the error is specified in the parameter cause:
 *   - 1: message distance too small
 *   - 2: message distance too large or message is missing
 *
 * Only one DUT can be monitored at a time. This check can be switched off again with coTfsDeactivateHeartbeatMonitor.
 *
 * @param nodeID
 *   node-ID of the DUT
 *
 * @param producerTime
 *   heartbeat time in us
 *
 * @param tolerance
 *   permitted time deviation of the target device in us
 *   It is recommended that you use an even value. The tolerated time frame within which a message is still accepted is: x - (delta/2) <= x <= x + (delta/2)
 *
 * @return
 *   error code
 */
long coTfsActivateHeartbeatMonitor(dword nodeID, dword producerTime, dword tolerance);

}
