#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Executes an user defined object dictionary test considering access type, data type and default values.
 *
 * @todo
 */

}
