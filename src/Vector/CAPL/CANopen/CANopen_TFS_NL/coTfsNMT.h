#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Executes a simple NMT test.
 *
 * This function attempts to set the following device states in the DUT sequentially: Stop, Reset, Operational, Pre-Operational, Reset Communication, Start, Pre-Operational and Reset.
 *
 * After this test, the DUT is in the pre-operational state.
 *
 * @return
 *   error code
 */
long coTfsNmt(void);

}
