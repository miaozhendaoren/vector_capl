#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the internally-stored node-ID.
 *
 * The internal node-ID for the simplified test functions is set.
 *
 * @param nodeID
 *   0 < node-ID < 128
 *
 * @return
 *   error code
 */
long coTfsSetNodeId(dword nodeID);

}
