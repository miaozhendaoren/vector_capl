#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Configures and activates a generic message monitor.
 *
 * This function configures and activates a generic message monitor. With the monitor you can check the occurrence count of a defined CAN message in the period between configuration and coTfsDeactivateGenericMonitor. The evaluation is done in the coTfsDeactivateGenericMonitor function.
 *
 * Between the two functions any other test functions can be used.
 *
 * The monitored message needn't sent and received cyclically, no time check is executed.
 *
 * Each received message with the correct CAN-ID and DLC and that fits to the mask, is stored (independent of the given mask) and can be compared with compare data (in function coTfsCheckAndCompareGenericMonitorMessage) at runtime or after a coTfsDeactivateGenericMonitor command.
 *
 * The user can specify a descriptive comment for the monitored message. This comment is written to the test report and provides a better overview.
 *
 * @param canId
 *   CAN-ID of the monitored message
 *
 * @param isRTR
 *   - 0: message is a data frame
 *   - 1: message is a remote frame
 *
 * @param dlc
 *   message length in byte, [0..8]
 *
 * @param msgData
 *   if data frame: expected message data
 *
 * @param msgMask
 *   if data frame: message data mask; set bits are compared and the message is presumed to be valid if the bits match with the data
 *
 * @param minOcc
 *   defines how often the expected message must be received at least
 *
 * @param maxOcc
 *   defines how often the expected message is allowed to be received at a maximum
 *
 * @param comment
 *   user defined comment that is written to the report (max. 200 characters)
 *
 * @param varContent
 *   - 0: the same messages is expected always
 *   - 1: the most significant bit of byte 0 of msgData is toggled after each successful reception of a message, the initial value is set by the value of the bit in msgData
 *   - other values: reserved
 *
 * @return
 *   error code
 */
long coTfsConfigureGenericMonitor(dword canId, dword isRTR, dword dlc, dword msgData, qword msgMask, dword minOcc, dword maxOcc, char comment[], dword varContent);

}
