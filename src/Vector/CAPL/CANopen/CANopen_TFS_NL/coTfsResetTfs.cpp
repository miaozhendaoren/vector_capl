#include "coTfsResetTfs.h"

#include <iostream>

namespace capl
{

void coTfsResetTfs(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
