#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Converts a word value to a byte array.
 *
 * The function converts a word value into a byte array.
 *
 * @param wordValue
 *   value to be converted
 *
 * @param outByteArray
 *   array containing the converted value, recommended array size: 2 byte
 *
 * @param arraysize
 *   size of the byte array
 *
 * @return
 *   error code
 */
long coTfsWToArr(word wordValue, byte outByteArray[], dword arraysize);

}
