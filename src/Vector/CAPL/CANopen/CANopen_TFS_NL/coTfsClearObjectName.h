#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Deletes stored object names for a specific node or all nodes.
 *
 * This function deletes stored object names that are load with coTfsLoadDeviceDescription.
 *
 * @param nodeId
 *   node-ID of the relevant node or 0 if all nodes are used
 *
 * @return
 *   error code
 */
long coTfsClearObjectName(dword nodeId);

}
