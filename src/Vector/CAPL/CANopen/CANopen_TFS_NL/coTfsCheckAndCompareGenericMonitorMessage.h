#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks if the expected message data are received during a generic message monitoring.
 *
 * This function checks if the expected message data are received.
 *
 * After monitoring is started with coTfsConfigureGenericMonitor the received messages can be checked with this function. The parameter order defines to which message the comparison is executed. The check is successfully passed if the message with the expected message number and the expected data is received.
 *
 * The function can be used during a monitoring started with coTfsConfigureGenericMonitor as well as after coTfsDeactivateGenericMonitor. The internal list of received messages is reset not before the monitoring is restarted with coTfsConfigureGenericMonitor.
 *
 * @param canId
 *   CAN-ID of the monitored message
 *
 * @param dlc
 *   message length in byte, [0..8]
 *
 * @param msgData
 *   expected message data
 *
 * @param msgMask
 *   message data mask, set bits are compared and the message is presumed to be valid if the bits match with the data
 *
 * @param order
 *   - 0: any receive message must contain the data specified in msgData/msgMask
 *   - 1..0xFFFFFFFE: the numeric specified message must contain the data specified in msgData/msgMask, 1 is the oldest message
 *   - 0xFFFFFFFF: the last received message must contain the data specified in msgData/msgMask
 *
 * @return
 *   error code
 */
long coTfsCheckAndCompareGenericMonitorMessage(dword canId, dword dlc, qword msgData, qword msgMask, dword order);

}
