#include "coTfsGetVersion.h"

#include <iostream>

namespace capl
{

dword coTfsGetVersion(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
