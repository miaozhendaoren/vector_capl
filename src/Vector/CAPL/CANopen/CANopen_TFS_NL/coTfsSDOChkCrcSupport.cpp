#include "coTfsSDOChkCrcSupport.h"

#include <iostream>

namespace capl
{

dword coTfsSDOChkCrcSupport(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
