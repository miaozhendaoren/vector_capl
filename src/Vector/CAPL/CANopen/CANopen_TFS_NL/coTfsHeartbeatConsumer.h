#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   A test is executed for a heartbeat consumer.
 *
 * A heartbeat consumer is configured on the DUT (object 0x1016, sub index 1). The tester then sends a single heartbeat message as heartbeat producer. Thus a heartbeat event should be triggered for the heartbeat consumer. The tester now waits for an emergency message with the error code 0x8130. The DUT is now reset with a NMT reset command. The test waits for the coming boot-up message.
 *
 * After this test, the device is in the pre-operational state.
 *
 * @param virtProducerId
 *   this "virtual" node emulates the necessary heartbeat producer, this node number may not be assigned in the system to be tested
 *
 * @param consumerTime
 *   heartbeat consumer time in ms
 *
 * @param tolerance
 *   permissible tolerance (x - (tolerance/2) <= x <= x + (tolerance/2)) for the receipt of the emergency message
 *
 * @return
 *   error code
 */
long coTfsHeartbeatConsumer(dword virtProducerId, dword consumerTime, dword tolerance);

}
