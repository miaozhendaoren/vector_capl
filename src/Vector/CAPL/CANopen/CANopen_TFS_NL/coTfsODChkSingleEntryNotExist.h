#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks if the given object does not exist in the object dictionary.
 *
 * @todo
 */

}
