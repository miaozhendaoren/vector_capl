#include "coTfsGuardingRequest.h"

#include <iostream>

namespace capl
{

long coTfsGuardingRequest(dword guardTime, dword retryFactor, dword tolerance, dword guardReqNumber, dword waitForEmcy)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
