#include "coTfsMonitorDeactivate.h"

#include <iostream>

namespace capl
{

long coTfsMonitorDeactivate(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
