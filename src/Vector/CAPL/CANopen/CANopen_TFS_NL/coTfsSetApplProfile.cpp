#include "coTfsSetApplProfile.h"

#include <iostream>

namespace capl
{

long coTfsSetApplProfile(dword profile)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
