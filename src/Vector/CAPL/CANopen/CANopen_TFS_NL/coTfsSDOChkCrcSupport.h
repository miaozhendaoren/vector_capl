#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the CRC support flag of a SDO init block download response.
 *
 * The call of this function after receipt of a SDO initiate upload/download response (coTfsSDOBlockInit) returns the information whether the SDO server supports block transfers with CRC checksums.
 *
 * @return
 *   - 0: CRC not supported
 *   - !=0: CRC supported
 */
dword coTfsSDOChkCrcSupport(void);

}
