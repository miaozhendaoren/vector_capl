#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Executes a complete heartbeat producer and heartbeat consumer test with different time settings.
 *
 * This function tests the heartbeat consumer and heartbeat producer of the DUT with two different time settings.
 *
 * First, the heartbeat producer is tested for regularity. The test duration is 10 seconds in each case. The heartbeat time is set to 1sec and after that, 200 ms.  The tolerance is 10 or 20%. After that, the heartbeat consumer is tested with a cycle time of 1sec and a tolerance of 10 % and 5%.
 *
 * The test uses internally the functions coTfsHeartbeatConsumer and coTfsHeartbeatProducer .
 *
 * After this test the target device is in the pre-operational state.
 *
 * @param virtProducerId
 *   virtual heartbeat producer, this node may not exist in the test setup
 *
 * @return
 *   error code
 */
long coTfsHeartbeat(dword virtProducerId);

}
