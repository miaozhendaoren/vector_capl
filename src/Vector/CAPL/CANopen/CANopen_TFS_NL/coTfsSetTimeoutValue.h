#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the general time-out for all test functions.
 *
 * This function sets the general time-out of all test functions, insofar as individual functions do not make a separate parameter available for this. The default value at measurement start is 2500 ms. If the value is set to 0, the function waits endlessly.
 *
 * @param timeout
 *   time-out in ms
 *
 * @return
 *   error code
 */
long coTfsSetTimeoutValue(dword timeout);

}
