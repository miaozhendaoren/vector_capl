#include "coTfsSetFailControl.h"

#include <iostream>

namespace capl
{

long coTfsSetFailControl(dword controlValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
