#include "coTfsSetTimeoutValue.h"

#include <iostream>

namespace capl
{

long coTfsSetTimeoutValue(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
