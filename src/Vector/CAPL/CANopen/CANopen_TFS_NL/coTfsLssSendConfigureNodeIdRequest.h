#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends a Configure LSS node-ID request and waits for the response.
 *
 * @todo
 */

}
