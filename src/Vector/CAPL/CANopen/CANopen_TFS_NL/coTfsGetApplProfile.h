#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Gets application profile.
 *
 * The function reads the currently set application profile.
 *
 * @return
 *   used application profile, 301 if not supported profile is used
 */
dword coTfsGetApplProfile(void);

}
