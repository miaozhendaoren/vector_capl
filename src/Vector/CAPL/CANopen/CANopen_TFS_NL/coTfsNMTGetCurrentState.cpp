#include "coTfsNMTGetCurrentState.h"

#include <iostream>

namespace capl
{

long coTfsNmtGetCurrentState(dword nmtState[1])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
