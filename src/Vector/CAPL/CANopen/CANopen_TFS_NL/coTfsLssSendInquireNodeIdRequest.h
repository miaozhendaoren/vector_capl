#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an Inquire node-ID request and waits for the response.
 *
 * @todo
 */

}
