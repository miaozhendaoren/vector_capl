#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Starts a SDO block up/download.
 *
 * This function starts a SDO block download or SDO block upload and waits for the corresponding response.
 *
 * @param index
 *   object index
 *
 * @param subIndex
 *   object sub index
 *
 * @param ccs (client command specifier)
 *   - 6: download
 *   - 5: upload, other values can cause unexpected behavior
 *
 * @param cs
 *   - 0: upload/download request
 *   - 3: start upload
 *
 * @param crcSupported
 *   - 0: client supports no CRC check
 *   - 1: client supports CRC check
 *
 * @param sizeIndicated
 *   - 0: size unknown
 *   - 1: size known (recommended value)
 *
 * @param blkSize
 *   number of segments per block (0 < blkSize < 128)
 *
 * @param size
 *   size of the data transmission in bytes
 *
 * @param threshold
 *   - 0: blocksize is static during the data transfer (recommended)
 *   - 1: blocksize can be changed
 *
 * @return
 *   error code
 */
long coTfsSDOBlockInit(dword index, dword subIndex, dword ccs, dword cs, dword crcSupported, dword  sizeIndicated, dword blkSize, dword size, dword threshold);

}
