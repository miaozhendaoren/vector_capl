#include "coTfsGetNodeId.h"

#include <iostream>

namespace capl
{

dword coTfsGetNodeId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
