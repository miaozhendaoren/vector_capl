#include "coTfsDeactivateSyncPdoMonitor.h"

#include <iostream>

namespace capl
{

long coTfsDeactivateSyncPdoMonitor(dword pdoCanId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
