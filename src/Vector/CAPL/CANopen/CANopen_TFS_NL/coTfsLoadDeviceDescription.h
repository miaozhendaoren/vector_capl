#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Reads an EDS, DCF or XML file to interpret objects index and sub index with object names in plain text.
 *
 * This function reads an EDS, DCF or XML file. This allows the interpretation of index and sub index during SDO accesses, so that object names can be displayed in plain text.
 *
 * The object names can be deleted with coTfsClearObjectName.
 *
 * @param nodeId
 *   This function reads an EDS, DCF or XML file. This allows the interpretation of index and sub index during SDO accesses, so that object names can be displayed in plain text.
 *   The object names can be deleted with coTfsClearObjectName.
 *
 * @param fileName
 *   name of the file to be read, relative to your CANoe configuration
 *
 * @return
 *   error code
 */
long coTfsLoadDeviceDescription(dword nodeId, char fileName[]);

}
