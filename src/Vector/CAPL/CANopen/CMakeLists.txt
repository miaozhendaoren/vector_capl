cmake_minimum_required(VERSION 2.8)

set(function_group CANopen)

set(source_files)

set(header_files
  CANopen.h)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_library(${PROJECT_NAME}_${function_group} OBJECT ${source_files} ${header_files})

if(OPTION_USE_GCOV)
  target_link_libraries(${PROJECT_NAME}_${function_group} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

add_subdirectory(CANopen_NL)
add_subdirectory(CANopen_TFS_NL)

install(
  FILES ${header_files}
  DESTINATION include/Vector/CAPL/${function_group})
