#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops all logging blocks immediately bypassing all logging trigger settings.
 *
 * Stops all logging blocks immediately bypassing all logging trigger settings.
 */
void stopLogging(void);

/**
 * @ingroup General
 *
 * @brief
 *   Stops all logging blocks immediately bypassing all logging trigger settings.
 *
 * Stops a logging block with name strLoggingBlockName immediately bypassing all logging
 * trigger settings.
 *
 * @param strLoggingBlockName
 *   Name of the logging block.
 */
void stopLogging(char * strLoggingBlockName);

/**
 * @ingroup General
 *
 * @brief
 *   Stops all logging blocks immediately bypassing all logging trigger settings.
 *
 *   Stops a logging block with name strLoggingBlockName bypassing all logging trigger
 *   settings.
 *
 *   Functions also sets a post-trigger time to a value of the postTriggerTime.
 *
 * @param strLoggingBlockName
 *   Name of the logging block.
 *
 * @param postTriggerTime
 *   Post-trigger time interval in ms.
 */
void stopLogging(char * strLoggingBlockName, long postTriggerTime);

}
