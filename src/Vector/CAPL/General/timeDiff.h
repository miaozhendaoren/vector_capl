#pragma once

#include "../DataTypes.h"
#include "../CAN/CAN.h"

namespace capl
{

#if 0
/**
 * @ingroup General
 *
 * @brief
 *   Time difference between messages or between a message and the current time in ms.
 *
 * Time difference between messages or between a message and the current time in ms
 * (msg2 - msg1 or now - msg1). Starting with CANalyzer 2.xx this difference can be
 * calculated directly (Units of 10 microseconds).
 *
 * @param m1
 *   Variable of type message
 *
 * @param NOW
 *   Variable of type now
 *
 * Time difference in ms.
 */
long timeDiff(Message m1, NOW);
#endif

/**
 * @ingroup General
 *
 * @brief
 *   Time difference between messages or between a message and the current time in ms.
 *
 * Time difference between messages or between a message and the current time in ms
 * (msg2 - msg1 or now - msg1). Starting with CANalyzer 2.xx this difference can be
 * calculated directly (Units of 10 microseconds).
 *
 * @param m1
 *   Variable of type message
 *
 * @param m2
 *   Variable of type message
 *
 * Time difference in ms.
 */
long timeDiff(message m1, message m2);

}
