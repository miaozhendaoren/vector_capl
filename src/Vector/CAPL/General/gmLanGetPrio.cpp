#include "gmLanGetPrio.h"

#include <iostream>

namespace capl
{

long gmLanGetPrio(gmLanMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
