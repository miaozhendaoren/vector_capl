#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Replaces all occurrences of a text in a string with another string. (form 1)
 *
 * Form 1: Replaces all occurrences of a text in a string with another string.
 *
 * Form 2: Replaces a part of a string with another string.
 *
 * @param s
 *   String to be modified.
 *
 * @param searched
 *   Text which shall be replaced.
 *
 * @param replacement
 *   Text which replaces the original characters.
 *
 * @return
 *   1 if successful, 0 if the resulting string would be too long for the buffer s.
 */
long str_replace(char * s, char * searched, char * replacement);

/**
 * @ingroup General
 *
 * @brief
 *   Replaces a part of a string with another string. (form 2)
 *
 * Form 1: Replaces all occurrences of a text in a string with another string.
 *
 * Form 2: Replaces a part of a string with another string.
 *
 * @param s
 *   String to be modified.
 *
 * @param startoffset
 *   Offset at which to start replacing characters.
 *
 * @param replacement
 *   Text which replaces the original characters.
 *
 * @param length
 *   Maximum number of characters to replace.
 *
 * @return
 *   1 if successful, 0 if the resulting string would be too long for the buffer s.
 */
long str_replace(char * s, long startoffset, char * replacement, long length);

}
