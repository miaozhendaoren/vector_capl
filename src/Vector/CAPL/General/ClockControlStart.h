#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Starts the Clock control designed as stop watch in the Panel Designer.
 *
 * Starts the Clock Control designed as stop watch in the Panel Designer (setting Mode
 * = StopWatch).
 *
 * The stop watch starts with 00:00:00 or 00:00 depending on the Panel Designer setting
 * Display Seconds. The start time cannot be changed.
 *
 * The stop watch is updated every second.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel
 * Designer.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the element. You can only access the control by its name. In the property
 *   dialog of the control it's name is assigned/displayed.
 *   If you want to use the name of a symbol (signal or environment/system variable) you
 *   have to ensure that the control has no name instead of the individual control's name.
 *   The name of the environment variable, system variable or signal could be specified as
 *   following.
 *   The form for signals is: "Signal:<signal name>".
 *   The form for environment variables is: "EnvVar:<environment variable name>".
 *   The form for system variables is: "SysVar:<name of system variable>". The name space
 *   must not be used.
 */
void ClockControlStart(char * panel, char * control);

}
