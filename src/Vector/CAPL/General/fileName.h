#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Output of the CAPL program in the Write window.
 *
 * Output of the CAPL program name in the Write window. Helpful for debugging purposes.
 */
void fileName(void);

}
