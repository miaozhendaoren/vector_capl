#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Cuts the connection between the node and the bus. (Form 1 obsolete)
 *
 * Cuts the connection between the node and the bus. Messages send from the node are not
 * passed through to the bus. The function canOnline() will restore the connection.
 *
 * If the node is set to offline, output instructions for sending messages in CAPL or
 * NodeLayer DLL are ignored (refers to a node locally only).
 * Regardless of the status, all messages are received in the CAPL program/NodeLayer.
 *
 * This form only has an effect on the CAPL-program.
 */
void canOffline(void);

/**
 * @ingroup General
 *
 * @brief
 *   Cuts the connection between the node and the bus. (Form 2)
 *
 * Cuts the connection between the node and the bus. Messages send from the node are not
 * passed through to the bus. The function canOnline() will restore the connection.
 *
 * If the node is set to offline, output instructions for sending messages in CAPL or
 * NodeLayer DLL are ignored (refers to a node locally only).
 * Regardless of the status, all messages are received in the CAPL program/NodeLayer.
 *
 * In this form you can choose between the CAPL-program and/or the Nodelayer-DLL.
 *
 * @param flags
 *   Indicates the deactivated part of the node.
 *   - 1: Deactivates the CAPL-program
 *   - 2: Deactivates the Nodelayer
 *   - 3: Deactivates the CAPL-program and the Nodelayer
 *
 * @return
 *   This form returns the part of the node being online before the function call. Equal to the
 *   flags.
 */
dword canOffline(dword flags);

}
