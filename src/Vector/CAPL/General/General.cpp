#include "General.h"

namespace capl
{

std::vector<void (*)(void)> on_preStart;
std::vector<void (*)(void)> on_preStop;
std::vector<void (*)(void)> on_Start;
std::vector<void (*)(void)> on_StopMeasurement;

}
