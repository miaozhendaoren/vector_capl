#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the time point relative to the measurement start at which the signal was last sent on the bus.
 *
 * @deprecated
 *   This function replaces the function getSignalTimeGM.
 *   Version 7.1: Replaced by getSignalTime.
 *
 * Gets the time point relative to the measurement start at which the signal was last sent on the bus.
 *
 * @param aSignal
 *   The signal to be polled.
 *
 * @param aTxNode
 *   Send node of the message whose signal should be polled.
 *
 * @return
 *   Time point or 0 if the signal was not sent yet.
 */
dword getSignalTimeByTxNode(char * aSignal, char * aTxNode);

}
