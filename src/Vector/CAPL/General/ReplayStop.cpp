#include "ReplayStop.h"

#include <iostream>

namespace capl
{

dword ReplayStop(char * pName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
