#include "isSimulated.h"

#include <iostream>

namespace capl
{

long isSimulated(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
