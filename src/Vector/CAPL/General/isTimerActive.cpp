#include "isTimerActive.h"

#include <iostream>

namespace capl
{

int isTimerActive(timer t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

int isTimerActive(msTimer t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
