#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Outputs a message to the Write window with the specified priority.
 *
 * Outputs a message to the write window with the specified priority. This function can be
 * used for debugging to vary the output to the write window. This function is especially
 * useful if nodelayer-DLL's are used. In this case the debug output can be controlled using a
 * global priority parameter.
 *
 * In the simulation tree the priority level can be set for every network node using the
 * setWriteDbgLevel function.
 *
 * @param priority
 *   Output priority from 0 to 15.
 *
 * @param format
 *   Format string, variables or expressions
 *   legal format expressions:
 *   - "%ld", "%d": decimal display
 *   - "%lx", "%x": hexadecimal display
 *   - "%lX", "%X": hexadecimal display (upper case)
 *   - "%lu", "%u": unsigned display
 *   - "%lo", "%o": octal display
 *   - "%s": display a string
 *   - "%g", "%lf": floating point display
 *   - "%c": display a character
 *   - "%%": display %-character
 *
 * @return
 *   -
 */
long writeDbgLevel(unsigned int priority, const char * format, ...);

}
