#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the foreground color of panel elements.
 *
 * Sets the foreground color of panel elements.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Designer /
 * Panel Editor.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the element. You can only activate/deactivate the control with its name. In the
 *   property dialog of the control it's name is assigned/displayed.
 *   If you want to use the name of a symbol (signal or environment/system variable) you have
 *   to ensure that the control has no name instead of the individual control's name. The name
 *   of the environment variable, system variable or signal could be specified as following.
 *   The form for signals is: "Signal:<signal name>".
 *   The form for environment variables is: "EnvVar:<environment variable name>".
 *   The form for system variables is: "SysVar:<name of system variable>". The name space
 *   must not be used.
 *
 * @param color
 *   Color value (e.g. calculated by MakeRGB)
 */
void SetControlForeColor(char * panel, char * control, long color);

}
