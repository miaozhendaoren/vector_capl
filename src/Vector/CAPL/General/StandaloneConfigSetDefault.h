#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
dword StandaloneConfigSetDefault(char * rtcfgFileName); // Changes the default configuration file.

}
