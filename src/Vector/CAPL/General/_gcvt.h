#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Converts a number to a string.
 *
 * The number val is converted to a string s containing a decimal point and a possible sign byte.
 *
 * @param val
 *   Number to be converted.
 *
 * @param digits
 *   Number of significant digits.
 *
 * @param s
 *   String, which contains the converted number. If the string size is too small, the string keeps unchanged.
 */
void _gcvt(double val, int digits, char * s);

}
