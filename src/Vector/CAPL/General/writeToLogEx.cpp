#include "writeToLogEx.h"

#include <iostream>

namespace capl
{

void writeToLogEx(const char * format, ...)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
