#include "sysExit.h"

#include <iostream>

namespace capl
{

void sysExit(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
