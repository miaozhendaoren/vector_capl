#include "fileReadInt.h"

#include <iostream>

namespace capl
{

long fileReadInt(char * section, char * entry, long def, char * file)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
