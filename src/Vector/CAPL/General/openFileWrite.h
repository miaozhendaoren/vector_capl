#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens the file for the write access.
 *
 * This function opens the file named filename for the write access.
 *
 * If mode=0 writing can be executed in ASCII mode;
 * if mode=1 writing can be executed in binary mode. An already existing file will be
 * overwritten.
 *
 * mode=2 to append data at the end of the file use for ASCII mode.
 * mode=3 to append data at the end of the file for binary mode.
 *
 * @param filename
 *   file name
 *
 * @param mode
 *   mode
 *
 * @return
 *   The return value is the file handle that must be used for write operations.
 *   If an error occurs the return value is 0.
 */
dword openFileWrite(char * filename, dword mode);

}
