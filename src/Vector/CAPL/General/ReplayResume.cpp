#include "ReplayResume.h"

#include <iostream>

namespace capl
{

dword ReplayResume(char * pName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
