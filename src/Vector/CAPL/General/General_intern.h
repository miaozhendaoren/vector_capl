#pragma once

#include <string>
#include <vector>

namespace caplIntern {

struct WriteCommand
{
    enum class Command {
        Write,
        WriteClear,
        WriteConfigure,
        WriteCreate,
        // WriteDbgLevel
        WriteDestroy,
        WriteEx,
        WriteLineEX,
        // WriteProfile*
        WriteTextBkgColor,
        WriteTextColor,
        // WriteToLog*
    } command;

    unsigned int sink;
    // -3: Trace
    // -2: Logging
    // -1: Reserved
    // 0: System
    // 1: CAPL
    // 4: Test

    unsigned int severity;
    // 0: Success
    // 1: Information
    // 2: Warning
    // 3: Error

    // for write, writeEx, writeLineEX
    std::string textMessage;

    // for writeConfigure
    unsigned int lines;
    bool logging;
    std::string filename;

    // for writeCreate
    std::string name;

    // for writeTextColor, writeTextBkgColor
    unsigned int red, green, blue;
};

extern std::vector<void (*)(struct WriteCommand)> on_write;

}
