#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the value of a signal.
 *
 * @deprecated
 *   This function replaces the function getSignalGM.
 *   Version 7.1: Replaced by getSignal.
 *
 * Gets the value of a signal.
 *
 * @param aSignal
 *   The signal to be polled.
 *
 * @param aTxNode
 *   Send node of the message whose signal should be polled.
 *
 * @return
 *   Value of the signal or 0 if the signal was not on the bus yet.
 */
double getSignalByTxNode(char * aSignal, char * aTxNode);

}
