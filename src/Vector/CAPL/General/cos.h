#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the cosine.
 *
 * Calculates cosine of x.
 *
 * @param x
 *   Value in radians whose cosine is to be calculated.
 *
 * @return
 *   Cosine of x.
 */
double cos(double x);

}
