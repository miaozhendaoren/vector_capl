#include "triggerEx.h"

#include <iostream>

namespace capl
{

void triggerEx(char * name)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
