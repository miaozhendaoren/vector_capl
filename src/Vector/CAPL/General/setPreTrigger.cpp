#include "setPreTrigger.h"

#include <iostream>

namespace capl
{

long setPreTrigger(long preTriggerTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
