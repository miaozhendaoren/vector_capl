#include "trigger.h"

#include <iostream>

namespace capl
{

void trigger(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
