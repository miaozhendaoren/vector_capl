#pragma once

#include "../DataTypes.h"
#include "General.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops an active timer
 *
 * Stops an active timer.
 *
 * @param t
 *   msTimer variable.
 */
void cancelTimer(msTimer t);

/**
 * @ingroup General
 *
 * @brief
 *   Stops an active timer
 *
 * Stops an active timer.
 *
 * @param t
 *   Timer variable.
 */
void cancelTimer(timer t);

}
