#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Selective activation and deactivation of special elements.
 *
 * Selective activation and deactivation of the following elements:
 * - control elements
 * - control and display elements
 *
 * If a control and display element is configured as a simple display, this command will have
 * no effect on the element in question.
 * The turned on or turned off state of an element remains intact at the start/end of the
 * measurement. Because of this, a defined state should be created for the beginning of the
 * measurement for the elements involved (for example in the CAPL program under System-
 * >Start).
 *
 * @param panel
 *   Name of the panel
 *   If an empty string is transferred, the action will affect all open panels.
 *
 * @param control
 *   Name of the element. You can only activate/deactivate the control with its name. In the
 *   property dialog of the control it's name is assigned/displayed.
 *   If you want to use the name of a symbol (signal or environment/system variable) you have
 *   to ensure that the control has no name instead of the individual control's name. The name
 *   of the environment variable, system variable or signal could be specified as following.
 *   The form for signals is: "Signal:<signal name>".
 *   The form for environment variables is: "EnvVar:<environment variable name>".
 *   The form for system variables is: "SysVar:<name of system variable>". The name space
 *   must not be used.
 *
 * @param enable
 *   enable
 *   - 0: turn off (disable)
 *   - 1: turn on (enable)
 */
void enableControl(const char * panel, const char * control, long enable);

}
