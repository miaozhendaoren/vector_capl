#include "writeTextBkgColor.h"
#include "General_intern.h"

namespace capl
{

void writeTextBkgColor(dword sink, dword red, dword green, dword blue)
{
    caplIntern::WriteCommand wc;
    wc.command = caplIntern::WriteCommand::Command::WriteTextBkgColor;
    wc.sink = sink;
    wc.red = red;
    wc.green = green;
    wc.blue = blue;
    for (auto fct: caplIntern::on_write)
        fct(wc);
}

}
