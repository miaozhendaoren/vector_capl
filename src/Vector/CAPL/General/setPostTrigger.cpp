#include "setPostTrigger.h"

#include <iostream>

namespace capl
{

long setPostTrigger(long preTriggerTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
