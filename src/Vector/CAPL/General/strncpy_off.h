#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Copies a string to another string.
 *
 * This function copies src to dest. max indicates the maximum length of src and dest.
 *
 * The function ensures that there is a terminating '\0'. Thus, a maximum of max-1-
 * destOffset characters are copied. Characters are overwritten in dest starting at
 * destOffset.
 *
 * @param dest
 *   Destination buffer
 *
 * @param destOffset
 *   Offset in destination buffer
 *
 * @param src
 *   Source string
 *
 * @param max
 *   Maximum number of characters in src and dest
 */
void strncpy_off(char * dest, long destOffset, char * src, long max);

}
