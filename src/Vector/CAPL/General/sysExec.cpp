#include "sysExec.h"

#include <iostream>

namespace capl
{

long sysExec(char * cmd, char * params)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysExec(char * cmd, char * params, char * directory)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
