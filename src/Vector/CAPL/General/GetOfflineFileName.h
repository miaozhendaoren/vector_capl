#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the complete path of the currently used offline source file.
 *
 * Returns the complete path of the currently used offline source file.
 *
 * @param buffer
 *   Space for the returned string.
 *
 * @param bufferSize
 *   Size of the buffer.
 *
 * @return
 *   - 0: If no error
 *   - 1: If buffer is too small
 *   - 2: 2: For other errors (e.g. not in offline mode)
 */
long GetOfflineFileName(char * buffer, dword bufferSize);

}
