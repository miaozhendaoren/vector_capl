#include "ClockControlStop.h"

#include <iostream>

namespace capl
{

void ClockControlStop(char * panel, char * control)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
