#include "openFileWrite.h"

#include <iostream>

namespace capl
{

dword openFileWrite(char * filename, dword mode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
