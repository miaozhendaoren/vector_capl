#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Executes an external command. (Form 1)
 *
 * Executes an external command. Does not wait until the command has completed its
 * execution.
 *
 * sysExec must be given an executable; sysExecCmd calls cmd.exe /K with the first
 * parameter, which opens a command window where the command is executed as if it was
 * entered directly.
 *
 * @param cmd
 *   The command to be executed. Either the full absolute path or a path relative to the
 *   current working directory must be given or the command must be in the system path.
 *
 * @param params
 *   Parameters to the command. A parameter which contains spaces must be enclosed in " ".
 *
 * @return
 *   1 if the command was successfully started, else 0.
 */
long sysExecCmd(char * cmd, char * params);

/**
 * @ingroup General
 *
 * @brief
 *   Executes an external command. (Form 2)
 *
 * Executes an external command. Does not wait until the command has completed its
 * execution.
 *
 * sysExec must be given an executable; sysExecCmd calls cmd.exe /K with the first
 * parameter, which opens a command window where the command is executed as if it was
 * entered directly.
 *
 * @param cmd
 *   The command to be executed. Either the full absolute path or a path relative to the
 *   current working directory must be given or the command must be in the system path.
 *
 * @param params
 *   Parameters to the command. A parameter which contains spaces must be enclosed in " ".
 *
 * @param directory
 *   Working directory for the command. Either an absolute path or a path relative to the
 *   current working directory must be given.
 *
 * @return
 *   1 if the command was successfully started, else 0.
 */
long sysExecCmd(char * cmd, char * params, char * directory);

}
