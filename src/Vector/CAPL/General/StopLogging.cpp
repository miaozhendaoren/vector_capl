#include "StopLogging.h"

#include <iostream>

namespace capl
{

void stopLogging(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void stopLogging(char * strLoggingBlockName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void stopLogging(char * strLoggingBlockName, long postTriggerTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
