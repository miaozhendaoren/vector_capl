#include "getJitterMin.h"

#include <iostream>

namespace capl
{

int getJitterMin(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
