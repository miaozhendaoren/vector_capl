#include "putValue.h"

#include <iostream>

namespace capl
{

void putValue(envvarInt & EnvVarName, int val)
{
    EnvVarName.value = val;
}

void putValue(envvarFloat & EnvVarName, double val)
{
    EnvVarName.value = val;
}

void putValue(envvarString & EnvVarName, char * val)
{
    EnvVarName.value.assign(val);
}

void putValue(envvarData & EnvVarName, byte * val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValue(envvarData & EnvVarName, byte * val, long vSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValue(char * name, int val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValue(char * name, double val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValue(char * name, char * val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValue(char * name, byte * val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValue(char * name, byte * val, long vSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
