#include "_round.h"

#include <iostream>

namespace capl
{

long _round(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
