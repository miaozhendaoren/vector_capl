#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens the file, searches the section and writes the variable.
 *
 * Opens the file filename, searches the section section and writes the variable entry with
 * the value value. If entry already exists the old value is overwritten.
 *
 * @param section
 *   Section of the file as a string.
 *
 * @param entry
 *   Variable name as a string.
 *
 * @param value
 *   Value as a float.
 *
 * @param filename
 *   File path as a string.
 *
 * @return
 *   The functional result is 0 in case of an error.
 */
long writeProfileFloat(char * section, char * entry, double value, char * filename);

}
