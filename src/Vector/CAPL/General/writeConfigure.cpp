#include "writeClear.h"
#include "General_intern.h"

namespace capl
{

void writeConfigure(dword sink, dword lines, dword logging, char * filename)
{
    caplIntern::WriteCommand wc;
    wc.sink = sink;
    wc.lines = lines;
    wc.logging = (logging != 0);
    wc.filename = std::string(filename);
    wc.command = caplIntern::WriteCommand::Command::WriteConfigure;
    for (auto fct: caplIntern::on_write)
        fct(wc);
}

}
