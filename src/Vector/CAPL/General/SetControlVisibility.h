#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
void SetControlVisibility(char * panel, char * control, long visible);

}
