#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Starts the Replay block after it is suspended.
 *
 * Starts the Replay block with the name pName after it is suspended by ReplaySuspend.
 *
 * @param pName
 *   Name of the Replay block.
 *
 * @return
 *   - 1: If successful
 *   - 0: If the Replay block does not exist or cannot be restarted
 */
dword ReplayResume(char * pName);

}
