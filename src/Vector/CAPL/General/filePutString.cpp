#include "filePutString.h"

#include <iostream>

namespace capl
{

long filePutString(char * buff, long buffsize, dword fileHandle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
