#include "GetBusNameContext.h"

#include <iostream>

namespace capl
{

dword GetBusNameContext(char * name)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
