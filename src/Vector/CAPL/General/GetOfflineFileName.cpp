#include "GetOfflineFileName.h"

#include <iostream>

namespace capl
{

long GetOfflineFileName(char * buffer, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
