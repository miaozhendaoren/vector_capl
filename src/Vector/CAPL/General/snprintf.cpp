#include "snprintf.h"

#include <iostream>

namespace capl
{

long snprintf(char * dest, long len, char * format, ...)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
