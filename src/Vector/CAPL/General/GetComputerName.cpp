#include "GetComputerName.h"

#include <iostream>

namespace capl
{

long GetComputerName(char * buffer, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
