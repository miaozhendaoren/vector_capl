#include "ConvertTimestampNS.h"

#include <iostream>

namespace capl
{

void ConvertTimestampNS(qword timestamp, dword& days, byte& hours, byte& minutes, byte& seconds, word& milliSeconds, word& microSeconds, word& nanoSeconds)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
