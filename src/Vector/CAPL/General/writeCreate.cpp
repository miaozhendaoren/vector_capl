#include "writeCreate.h"
#include "General_intern.h"

namespace capl
{

dword writeCreate(char * name)
{
    /* next sink identifier */
    static unsigned int writeNextSink = 10;

    /* define the command */
    caplIntern::WriteCommand wc;
    wc.command = caplIntern::WriteCommand::Command::WriteCreate;
    wc.sink = writeNextSink;
    ++writeNextSink;
    wc.name = std::string(name);
    for (auto fct: caplIntern::on_write)
        fct(wc);

    /* return sink identifier */
    return wc.sink;
}

}
