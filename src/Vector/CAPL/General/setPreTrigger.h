#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the pretrigger of the logging.
 *
 * Sets the pretrigger of the logging. The pretrigger set with this function is valid until the
 * end of the measurement or until the next call of this function.
 *
 * @param preTriggerTime
 *   New pretrigger value in milliseconds.
 *
 * @return
 *   1, if the pretrigger is set to the given value, 0 else.
 */
long setPreTrigger(long preTriggerTime);

}
