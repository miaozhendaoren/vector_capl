#include "writeProfileFloat.h"

#include <iostream>

namespace capl
{

long writeProfileFloat(char * section, char * entry, double value, char * filename)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
