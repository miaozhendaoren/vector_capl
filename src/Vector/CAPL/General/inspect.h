#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Causes an update of the variables displayed on the Inspect side of the Write window.
 *
 * @deprecated
 *   Command is ignored.
 *
 * This function causes an update of the variables displayed on the Inspect side of the Write
 * window.
 */
void inspect(void);

}
