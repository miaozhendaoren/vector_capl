#include "cos.h"

#include <complex>

namespace capl
{

double cos(double x)
{
    return std::cos(x);
}

}
