#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates arccosine of a value.
 *
 * Calculates arccosine of x.
 *
 * @param x
 *   Value between -1 and 1 whose arccosine is to be calculated. Values outside this range
 *   cause a CAPL runtime error.
 *
 * @return
 *   Arcus Cosine of x.
 */
double arccos(double x);

}
