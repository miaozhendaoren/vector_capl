#include "isStatisticAcquisitionRunning.h"

#include <iostream>

namespace capl
{

int isStatisticAcquisitionRunning(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
