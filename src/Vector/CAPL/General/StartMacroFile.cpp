#include "StartMacroFile.h"

#include <iostream>

namespace capl
{

dword StartMacroFile(char * fileName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
