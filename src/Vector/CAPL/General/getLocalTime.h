#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns details to the current date and time in an array of type long.
 *
 * Returns details to the current date and time in an array of type long.
 *
 * @param time
 *   An array of type long with at least 9 entries.
 *   The entries of the array will be filled with the following information:
 *   - 1: Seconds (0 - 60)
 *   - 2: Minutes (0 - 60)
 *   - 3: Hours (0 - 24)
 *   - 4: Day of month (1 - 31)
 *   - 5: Month (0 - 11)
 *   - 6: Year (starting with 1900)
 *   - 7: Day of week (0 - 7)
 *   - 8: Day of Year (0 - 365)
 *   - 9: Flag for daylight saving time (0 - 1, 1 = daylight saving time)
 */
void getLocalTime(long * time);

}
