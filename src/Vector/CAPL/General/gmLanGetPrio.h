#pragma once

#include "../DataTypes.h"
#include "../GMLAN/GMLAN.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the priority of the message.
 *
 * gmLanGetPrio gets the priority of the message.
 *
 * @param msg
 *   Message
 *
 * @return
 *   Priority
 */
long gmLanGetPrio(gmLanMessage msg);

}
