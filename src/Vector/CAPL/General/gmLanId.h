#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Creates a message ID for a GMLAN message.
 *
 * This function can be used to create a message ID for a GMLAN message. In addition to the
 * message, you must specify the transmission node or its source ID.
 *
 * The message ID returned can be used, for example, to wait for a specific GMLAN message
 * with the TestWaitForMessage function.
 *
 * @param aMessage
 *   GMLAN message for which a message ID is to be created.
 *
 * @param aSourceId
 *   Source ID of the node to be coded as the transmission node in the message ID.
 *
 * @return
 *   If the message is a GMLAN message, a GMLAN message ID will be returned containing the
 *   correct source address and priority. Otherwise, the message ID will be returned.
 */
dword gmLanId(char * aMessage, dword aSourceId);

/**
 * @ingroup General
 *
 * @brief
 *   Creates a message ID for a GMLAN message.
 *
 * This function can be used to create a message ID for a GMLAN message. In addition to the
 * message, you must specify the transmission node or its source ID.
 *
 * The message ID returned can be used, for example, to wait for a specific GMLAN message
 * with the TestWaitForMessage function.
 *
 * @param aMessage
 *   GMLAN message for which a message ID is to be created.
 *
 * @param aTxNode
 *   Node to be coded as the transmission node in the message ID.
 *
 * @return
 *   If the message is a GMLAN message, a GMLAN message ID will be returned containing the
 *   correct source address and priority. Otherwise, the message ID will be returned.
 */
dword gmLanId(char * aMessage, char * aTxNode);

}
