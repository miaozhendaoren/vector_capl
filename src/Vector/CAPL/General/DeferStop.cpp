#include "DeferStop.h"

#include <iostream>

namespace capl
{

void DeferStop(dword maxDeferTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
