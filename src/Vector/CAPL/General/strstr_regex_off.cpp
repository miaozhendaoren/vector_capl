#include "strstr_regex_off.h"

#include <iostream>

namespace capl
{

long strstr_regex_off(char * s, long offset, char * pattern)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
