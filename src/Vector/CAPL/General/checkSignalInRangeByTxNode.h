#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * @deprecated
 *   This function replaces the function checkSignalInRangeGM.
 *   Version 7.1: Replaced by checkSignalInRange.
 *
 * Checks the signal value against the condition:
 * aLowLimit <= Value <= aHighLimit
 *
 * @param aSignal
 *   The signal to be polled
 *
 * @param aTxNode
 *   Send node of the message whose signal should be polled
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - 1: If the condition is TRUE
 *   - 0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 */
long checkSignalInRangeByTxNode(char * aSignal, char * aTxNode, double aLowLimit, double aHighLimit);

}
