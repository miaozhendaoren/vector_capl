#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the value of the Start Delay for this network node.
 *
 * Sets the value of the Start Delay for this network node. This function can only be called in
 * the preStart event procedure. Afterwards the value of the Start Delay cannot be changed
 * any more.
 *
 * @param delay
 *   Integer for the Start Delay ms. This value may lie between 0 and 99999. If the value is
 *   greater or less than the limits of this range a warning appears in the Write window.
 */
void setStartdelay(int delay);

}
