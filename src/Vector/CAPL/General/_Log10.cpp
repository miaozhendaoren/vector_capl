#include "_Log10.h"

#include <iostream>

namespace capl
{

double _Log10(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
