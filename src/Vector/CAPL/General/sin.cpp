#include "sin.h"

#include <complex>

namespace capl
{

double sin(double x)
{
    return std::sin(x);
}

}
