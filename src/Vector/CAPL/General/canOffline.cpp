#include "canOffline.h"

#include <iostream>

namespace capl
{

void canOffline(void)
{
    std::cerr << "Obsolete function: " << __FUNCTION__ << std::endl;
    canOffline(1);
}

dword canOffline(dword flags)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
