#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches for a regular expression pattern in a string.
 *
 * Searches for a regular expression pattern in a string.
 *
 * @param s
 *   String to be searched.
 *
 * @param pattern
 *   Regular expression which is searched. For the regular expression, the same syntax is
 *   used as in the Perl programming language.
 *
 * @return
 *   The position in s where the pattern was found, or -1 if it wasn't found.
 */
long strstr_regex(char * s, char * pattern);

}
