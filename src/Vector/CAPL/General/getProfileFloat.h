#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads the value of the given variable from the specified section in the specified file.
 *
 * Searches the file filename under section section for the variable entry. If its value is a
 * number, this number is returned as the functional result. If the file or entry is not found,
 * or if entry does not contain a valid number, the default value def is returned as the
 * functional result.
 *
 * @param section
 *   Section of the file as a string.
 *
 * @param entry
 *   Variable name as a string.
 *
 * @param def
 *   Default value in case of error as a float.
 *
 * @param filename
 *   File path as a string.
 *
 * @return
 *   Float that was read in.
 */
double getProfileFloat(char * section, char * entry, long def, char * filename);

}
