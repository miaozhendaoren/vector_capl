#pragma once

#include "../DataTypes.h"
#include "envvar.h"

namespace capl
{

/**
 * Environment variable of type Data
 */
class envvarData : public envvar
{
public:
    envvarData();
    virtual ~envvarData();

    /* intern */
    byte * value;
    size_t size;

    virtual size_t valueSize();
};

}
