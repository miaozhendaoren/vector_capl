#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Plays back a sound predefined by the Windows system.
 *
 * The msgBeep function plays back a sound predefined by the Windows system. It replaces
 * the previous beep function.
 *
 * @param soundType
 *   Integer for the predefined sound. Specifically these are:
 *   - 0: MB_ICONASTERISK SystemAsterisk
 *   - 1: MB_ICONEXCLAMATION SystemExclamation
 *   - 2: MB_ICONHAND SystemHand
 *   - 3: MB_ICONQUESTION SystemQuestion
 *   - 4: MB_OK SystemDefault
 *   - 5: Standard beep using the PC speaker (default)
 */
void msgBeep(long soundType);

}
