#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the state of the Replay block.
 *
 * Returns the state of the Replay block with the name pName.
 *
 * @param pName
 *   Name of the Replay block
 *
 * @return
 *   - 0: Replay Block is stopped (state: stopped)
 *   - 1: Execution of the Replay file was started (state: running)
 *   - 2: Execution of the Replay file was stopped (state: suspended)
 *   - (dword)-1: when the Replay block doesn't exist
 */
dword ReplayState(char * pName);

}
