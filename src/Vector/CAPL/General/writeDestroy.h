#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Removes the specified page from the Write window.
 *
 * Removes the specified page from the Write window. Only pages that have been created
 * with the aid of the writeCreate function can be removed.
 *
 * @param sink
 *   Target identifier for the page to be removed.
 */
void writeDestroy(dword sink);

}
