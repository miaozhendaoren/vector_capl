#include "StopMacroFile.h"

#include <iostream>

namespace capl
{

void StopMacroFile(dword handle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
