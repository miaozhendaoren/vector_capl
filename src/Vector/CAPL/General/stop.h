#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Programmed interrupt of the ongoing measurement.
 *
 * Programmed interrupt of the ongoing measurement.
 */
void stop(void);

}
