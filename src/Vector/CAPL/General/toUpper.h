#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Transforms a character or string to upper case. (form 1)
 *
 * Transforms a character or string to upper case. Only characters a-z and A-Z are
 * supported.
 *
 * @param c
 *   Character to be transformed.
 *
 * @return
 *   Upper case of the character (form 1).
 */
char toUpper(char c);

/**
 * @ingroup General
 *
 * @brief
 *   Transforms a character or string to upper case. (form 2)
 *
 * Transforms a character or string to upper case. Only characters a-z and A-Z are
 * supported.
 *
 * @param dest
 *   Destination buffer for the transformed string.
 *
 * @param source
 *   String to be transformed.
 *
 * @param bufferSize
 *   Size of the destination buffer.
 */
void toUpper(char * dest, char * source, dword bufferSize);

}
