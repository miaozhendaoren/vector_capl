#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the information if CANoe is in simulated mode.
 *
 * This function is used to get the information if CANoe is in simulated mode.
 *
 * @return
 *   - 1: True, CANoe is in simulated mode.
 *   - 0: False, CANoe is in real mode.
 */
long isSimulated(void);

}
