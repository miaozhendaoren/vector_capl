#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads characters from the specified file in binary format.
 *
 * The function reads characters from the specified file in binary format.
 *
 * The source file must be opened in binary format.
 *
 * @param buff
 *   Buffer
 *
 * @param buffsize
 *   Maximum of buffsize characters
 *
 * @param fileHandle
 *   Handle to the file
 *
 * @return
 *   The function returns the number of characters read.
 */
long fileGetBinaryBlock(byte * buff, long buffsize, dword fileHandle);

}
