#include "toLower.h"

#include <iostream>

namespace capl
{

char toLower(char c)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

void toLower(char * dest, char * source, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
