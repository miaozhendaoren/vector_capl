#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Generates a new page in the Write window with the specified name.
 *
 * Generates a new page in the Write window with the specified name.
 *
 * The page is automatically deleted the next time a measurement starts.
 *
 * @param name
 *   Name of the page to be generated.
 *
 * @return
 *   Sink identifier that is valid for output to the new page.
 */
dword writeCreate(char * name);

}
