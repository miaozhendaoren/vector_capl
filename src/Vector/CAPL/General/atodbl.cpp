#include "atodbl.h"

#include <iostream>

namespace capl
{

double atodbl(char * s)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
