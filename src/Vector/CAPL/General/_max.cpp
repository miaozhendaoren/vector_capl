#include "_max.h"

namespace capl
{

long _max(long x, long y)
{
    return y > x ? y : x;
}

dword _max(dword x, dword y)
{
    return y > x ? y : x;
}

int64 _max(int64 x, int64 y)
{
    return y > x ? y : x;
}

qword _max(qword x, qword y)
{
    return y > x ? y : x;
}

double _max(double x, double y)
{
    return y > x ? y : x;
}

}
