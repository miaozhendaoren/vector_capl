#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches a string in another string.
 *
 * Searches in s1 for s2.
 *
 * @param s1
 *   First string
 *
 * @param s2
 *   Second string
 *
 * @return
 *   First position of s2 in s1, or -1 if s2 is not found in s1.
 */
long strstr(char * s1, char * s2);

}
