#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops the execution of the simulation.
 *
 * This function stops the execution of the simulation. The simulation can be continued with
 * < F9 >. The halt instruction is ignored in Real mode.
 * In addition, the halt instruction causes an update of the variables displayed on the Inspect
 * side of the Write window.
 */
void halt(void);

}
