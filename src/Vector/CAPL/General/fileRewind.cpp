#include "fileRewind.h"

#include <iostream>

namespace capl
{

long fileRewind(dword fileHandle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
