#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches for a variable entry in a file section.
 *
 * @deprecated
 *   Replaced by getProfileString
 *
 * Searches for the variable entry in the section section of the file filename.
 * Its content (value) is written to the buffer buffer.
 * Its length must be passed correctly to bufferlen.
 * If the file or entry is not found, the default value def is copied to buffer.
 *
 * @param section
 *   Section of file
 *
 * @param entry
 *   Name of variable
 *
 * @param def
 *   Value
 *
 * @param buffer
 *   Buffer for characters to be read
 *
 * @param bufferlen
 *   Size of buffer in byte
 *
 * @param filename
 *   Name of file
 *
 * @return
 *   Number of bytes read
 */
long fileReadString(char * section, char * entry, char * def, char * buffer, long bufferlen, char * filename);

}
