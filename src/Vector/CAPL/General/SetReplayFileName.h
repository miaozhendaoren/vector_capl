#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
dword SetReplayFileName(char * nameOfReplayBlock, char * fileName, char * fileType);

}
