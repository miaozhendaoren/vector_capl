#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the name of the logfile.
 *
 * Sets the name of the logfile.
 *
 * @param fileName
 *   The name may be an absolute path, a single filename or a relative path. If an absolute
 *   path or a relative path is supplied, all non existing directories of the path will be created.
 *   The logfile will be placed in the directory of the current configuration, if a single
 *   filename is supplied, or in the path relative to the configuration file if a relative path is
 *   supplied. The directories of the path must be separated by a backslash ('\'). The filename
 *   must not contain a file extension. The extension will be set automatically by the system.
 *   The new name will only be changed with a new setLogFileName call or by a
 *   corresponding entry in the configuration dialog of the logfile.
 *   If the logging block doesn't log (logging is not active) the name is changed immediately.
 *   If the logging block logs (logging is active) the new name will be taken over with the next
 *   trigger event or with a new measurement start.
 */
void setLogFileName(char * fileName);

}
