#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Exits the system (CANalyzer/CANoe) from within a CAPL program.
 *
 * Exits the system (CANalyzer/CANoe) from within a CAPL program.
 */
void sysExit(void);

}
