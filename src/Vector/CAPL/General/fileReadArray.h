#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches for a variable entry in a file section.
 *
 * @deprecated
 *   Replaced by getProfileArray
 *
 * Searches for the variable entry in the section section of the file file.
 * Its contents are interpreted as a list of byte values.
 * The number format is decimal or with the prefix 0x it is hexadecimal.
 * Numbers are separated by spaces, tabs, a comma, semi-colon or slash.
 * The buffer is filled up to a quantity of bufferlen bytes.
 *
 * @param section
 *   Section of file
 *
 * @param entry
 *   Name of variable
 *
 * @param buffer
 *   buffer for characters to be read
 *
 * @param bufferlen
 *   Size of buffer in byte
 *
 * @param filename
 *   Name of file
 *
 * @return
 *   Number of characters read.
 */
long fileReadArray(char * section, char * entry, char * buffer, long bufferlen, char * file);

}
