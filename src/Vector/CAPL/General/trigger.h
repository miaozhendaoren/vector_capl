#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Activates/Deactivates logging triggering of all logging and trigger blocks.
 *
 * Activates/Deactivates logging triggering of all logging and trigger blocks.
 */
void trigger(void);

}
