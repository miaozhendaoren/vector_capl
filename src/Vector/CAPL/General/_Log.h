#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the natural logarithm.
 *
 * Calculates the natural logarithm.
 *
 * @param x
 *   Value of which the logarithm shall be calculated.
 *
 * @return
 *   Logarithm of x (to base e).
 */
double _Log(double x);

}
