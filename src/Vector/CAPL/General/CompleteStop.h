#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Indicates completion of pre-stop activities after a measurement stop has been deferred.
 *
 * Indicates completion of pre-stop actions carried out in a certain node after a
 * measurement stop has been deferred by DeferStop.
 */
void CompleteStop(void);

}
