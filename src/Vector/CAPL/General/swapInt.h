#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Swaps bytes of parameters.
 *
 * Swaps bytes of parameters. CAPL arithmetics follows the "little-endian-format" (Intel).
 * The swap-functions serve for swapping bytes for the transition to and from the "bigendian-
 * format" (Motorola).
 *
 * @param x
 *   Value whose bits are to be swapped.
 *
 * @return
 *   Value with swapped bytes.
 */
int swapInt(int x);

}
