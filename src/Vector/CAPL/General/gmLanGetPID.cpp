#include "gmLanGetPID.h"

#include <iostream>

namespace capl
{

long gmLanGetPID(gmLanMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
