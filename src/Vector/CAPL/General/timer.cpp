#include "timer.h"

#include <iostream>

namespace capl
{

void timer::set(long duration)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void timer::set(long duration, long durationNanoSec)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void timer::cancel(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

int timer::isTimerActive(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long timer::timeToElapse(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
