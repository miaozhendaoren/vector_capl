#include "canOnline.h"

#include <iostream>

namespace capl
{

void canOnline(void)
{
    std::cerr << "Obsolete function: " << __FUNCTION__ << std::endl;
    canOnline(1);
}

dword canOnline(dword flags)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
