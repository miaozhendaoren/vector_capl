#include "SetControlProperty.h"

#include <iostream>

namespace capl
{

void SetControlProperty(char * panel, char * control, char * property, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void SetControlProperty(char * panel, char * control, char * property, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void SetControlProperty(char * panel, char * control, char * property, char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
