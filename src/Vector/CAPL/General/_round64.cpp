#include "_round64.h"

#include <iostream>

namespace capl
{

int64 _round64(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
