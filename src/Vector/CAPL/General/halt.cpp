#include "halt.h"

#include <iostream>

namespace capl
{

void halt(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
