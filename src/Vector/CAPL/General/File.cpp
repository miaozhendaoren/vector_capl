#include "File.h"

#include <iostream>

namespace capl {

void File::open(char * filename, dword access, dword mode)
{
    std::ios_base::openmode om;

    /* access: write/read */
    om |= (access = 0) ? std::ios_base::out : std::ios_base::in;

    /* mode: ascii/binary */
    if (mode == 1)
        om |= std::ios_base::binary;

    fs.open(filename, om);
}

long File::close(void)
{
    if (!fs.is_open())
        return 0;

    fs.close();

    return 1;
}

long File::getBinaryBlock(byte * buff, long buffsize)
{
    if (!fs.is_open())
        return 0;

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;

    return 0;
}

long File::getString(char * buff, long buffsize)
{
    if (!fs.is_open())
        return 0;

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;

    return 0;
}

long File::getStringSZ(char * buff, long buffsize)
{
    if (!fs.is_open())
        return 0;

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;

    return 0;
}

long File::putString(char * buff, long buffsize)
{
    if (!fs.is_open())
        return 0;

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;

    return 0;
}

long File::rewind(void)
{
    if (!fs.is_open())
        return 0;

    fs.seekg(0);
    fs.seekp(0);

    return 1;
}

long File::writeBinaryBlock(byte * buff, long buffsize)
{
    if (!fs.is_open())
        return 0;

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;

    return 0;
}

long File::writeString(char * buff, long buffsize)
{
    if (!fs.is_open())
        return 0;

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;

    return 0;
}

}
