#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Activates/Deactivates logging triggering of a special logging or trigger block.
 *
 * Activates/Deactivates logging triggering of a special logging or trigger block.
 *
 * If you enter no block name, triggering will be activated/deactivated for all blocks.
 *
 * @param name
 *   Name of the logging or trigger block you want to activate/deactivate triggering.
 */
void triggerEx(char * name);

}
