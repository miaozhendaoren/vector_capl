#pragma once

#include "../DataTypes.h"
#include "envvar.h"
#include <string>

namespace capl
{

/**
 * Environment variable of type String
 */
class envvarString : public envvar
{
public:
    envvarString();
    virtual ~envvarString();

    /* intern */
    std::string value;

    virtual size_t valueSize();
};

}
