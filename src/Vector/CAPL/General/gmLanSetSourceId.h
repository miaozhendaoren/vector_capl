#pragma once

#include "../DataTypes.h"
#include "../GMLAN/GMLAN.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the source address of the message.
 *
 * gmLanSetSourceId sets the source address of the message.
 *
 * @param msg
 *   Message
 *
 * @param v
 *   Parameter ID, source address or priority
 */
void gmLanSetSourceId(gmLanMessage msg, long v);

}
