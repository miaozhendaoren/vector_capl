#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops a started acquisition range.
 *
 * A started acquisition range is stopped with this function. If no acquisition range has been
 * started yet, this function has no effect.
 */
void stopStatisticAcquisition(void);

}
