#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Compares the number of characters of two strings. (Form 1)
 *
 * This function compares s1 with s2 for a maximum of len characters.
 *
 * If they are identical the functional result is 0.
 * If s1 is less than s2 the result is -1, else +1.
 *
 * Form 2 starts the comparison in s2 at the specified offset.
 *
 * @param s1
 *   First string
 *
 * @param s2
 *   Second string
 *
 * @param len
 *   Maximum number of characters to compare
 *
 * @return
 *   - -1: if s1 is less than s2.
 *   - 1: if s2 is less than s1.
 *   - -0: if the strings are equal.
 */
long strncmp(char * s1, char * s2, long len);

/**
 * @ingroup General
 *
 * @brief
 *   Compares the number of characters of two strings. (Form 2)
 *
 * This function compares s1 with s2 for a maximum of len characters.
 *
 * If they are identical the functional result is 0.
 * If s1 is less than s2 the result is -1, else +1.
 *
 * Form 2 starts the comparison in s2 at the specified offset.
 *
 * @param s1
 *   First string
 *
 * @param s2
 *   Second string
 *
 * @param s2offset
 *   Offset in s2
 *
 * @param len
 *   Maximum number of characters to compare
 *
 * @return
 *   - -1: if s1 is less than s2.
 *   - 1: if s2 is less than s1.
 *   - -0: if the strings are equal.
 */
long strncmp(char * s1, char * s2, long s2offset, long len);

}
