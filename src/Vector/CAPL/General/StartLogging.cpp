#include "StartLogging.h"

#include <iostream>

namespace capl
{

void startLogging(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void startLogging(char * strLoggingBlockName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void startLogging(char * strLoggingBlockName, long preTriggerTime)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
