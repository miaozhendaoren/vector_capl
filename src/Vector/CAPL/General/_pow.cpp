#include "_pow.h"

#include <iostream>

namespace capl
{

double _pow(double x, double y)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
