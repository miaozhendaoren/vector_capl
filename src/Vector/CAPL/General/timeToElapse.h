#pragma once

#include "../DataTypes.h"
#include "General.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns a value indicating how much more time will elapse before an on timer (Form 1)
 *
 * Returns a value indicating how much more time will elapse before an on timer event
 * procedure is called.
 *
 * For form 1, the time value is returned in seconds; for form 2, the time value is returned
 * in milliseconds.
 *
 * If the timer is not active, -1 is returned. This is also the case in the on timer event
 * procedure itself.
 *
 * @param t
 *   timer variable
 *
 * @return
 *   Time to go until the timer elapses and the event procedure is called.
 */
long timeToElapse(timer t);

/**
 * @ingroup General
 *
 * @brief
 *   Returns a value indicating how much more time will elapse before an on timer (Form 2)
 *
 * Returns a value indicating how much more time will elapse before an on timer event
 * procedure is called.
 *
 * For form 1, the time value is returned in seconds; for form 2, the time value is returned
 * in milliseconds.
 *
 * If the timer is not active, -1 is returned. This is also the case in the on timer event
 * procedure itself.
 *
 * @param t
 *   mstimer variable
 *
 * @return
 *   Time to go until the timer elapses and the event procedure is called.
 */
long timeToElapse(msTimer t);

}
