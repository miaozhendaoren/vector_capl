#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Copies a string to another string.
 *
 * This function copies src to dest. len indicates the maximum length of src and dest. The
 * function ensures that there is a terminating '\0'. Thus, a maximum of len-1 characters are
 * copied.
 *
 * @param dest
 *   Destination buffer
 *
 * @param src
 *   Source string
 *
 * @param len
 *   Maximum number of characters in src and dest
 */
void strncpy(char * dest, char * src, long len);

}
