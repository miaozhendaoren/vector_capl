#include "toUpper.h"

#include <iostream>

namespace capl
{

char toUpper(char c)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

void toUpper(char * dest, char * source, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
