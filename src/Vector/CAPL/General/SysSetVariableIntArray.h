#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the value of a variable of the int[] type. (form 1)
 *
 * @deprecated
 *   Replaced by SysSetVariableLongArray.
 *
 * Sets the value of a variable of the int[] type.
 *
 * @param namespace_
 *   Name of the name space (form 1)
 *
 * @param variable
 *   Name of the variable (form 1)
 *
 * @param values
 *   New values of the variable (both forms)
 *
 * @param arraySize
 *   Number of values in the array (both forms)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysSetVariableIntArray(char * namespace_, char * variable, int * values, long arraySize);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the value of a variable of the int[] type. (form 2)
 *
 * @deprecated
 *   Replaced by SysSetVariableLongArray.
 *
 * Sets the value of a variable of the int[] type.
 *
 * @param values
 *   New values of the variable (both forms)
 *
 * @param arraySize
 *   Number of values in the array (both forms)
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::". The name must be preceded by "sysVar::". (form 2)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysSetVariableIntArray(char * SysVarName, int * values, long arraySize);

}
