#include "strncmp.h"

#include <iostream>

namespace capl
{

long strncmp(char * s1, char * s2, long len)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long strncmp(char * s1, char * s2, long s2offset, long len)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
