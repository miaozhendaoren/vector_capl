#include "SetBusContext.h"

#include <iostream>

namespace capl
{

dword SetBusContext(dword context)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
