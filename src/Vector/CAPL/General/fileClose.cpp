#include "fileClose.h"

#include <iostream>

namespace capl
{

long fileClose(dword fileHandle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
