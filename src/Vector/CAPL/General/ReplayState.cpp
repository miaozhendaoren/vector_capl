#include "ReplayState.h"

#include <iostream>

namespace capl
{

dword ReplayState(char * pName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
