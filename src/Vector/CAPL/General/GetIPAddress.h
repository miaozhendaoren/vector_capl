#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Retrieves the default (first) IP address of the computer as a string.
 *
 * Retrieves the default (first) IP address of the computer as a string.
 *
 * @param buffer
 *   Space for the returned address.
 *
 * @param bufferSize
 *   Length of the buffer.
 *
 * @return
 *   0 if the function completed successfully, else unequal 0.
 */
long GetIPAddress(char * buffer, dword bufferSize);

}
