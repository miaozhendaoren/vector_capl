#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Replaces the media file of the Panel Designer Media Player element.
 *
 * Replaces the media file of the Panel Designer Media Player control during runtime.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel
 * Designer.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the panel element ("" - references all controls on the panel)
 *
 * @param mediafile
 *   Path and name of the media file.
 */
void SetMediaFile(char * panel, char * control, char * mediafile);

}
