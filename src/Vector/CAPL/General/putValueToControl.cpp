#include "putValueToControl.h"

#include <iostream>

namespace capl
{

void putValueToControl(char * panel, char * control, double val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValueToControl(char * panel, char * control, long val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValueToControl(char * panel, char * control, char * val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValueToControl(char * panel, char * control, beanMessage val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValueToControl(char * panel, char * control, message val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValueToControl(char * panel, char * control, pg val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void putValueToControl(char * panel, char * control, linMessage val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
