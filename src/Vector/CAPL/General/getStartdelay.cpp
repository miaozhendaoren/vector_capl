#include "getStartdelay.h"

#include <iostream>

namespace capl
{

int getStartdelay(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
