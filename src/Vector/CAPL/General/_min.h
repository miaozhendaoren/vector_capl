#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the minimum of the parameters. (Form 1)
 *
 * Returns the minimum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y < x ? y : x
 */
long _min(long x, long y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the minimum of the parameters. (Form 2)
 *
 * Returns the minimum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y < x ? y : x
 */
dword _min(dword x, dword y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the minimum of the parameters. (Form 3)
 *
 * Returns the minimum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y < x ? y : x
 */
int64 _min(int64 x, int64 y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the minimum of the parameters. (Form 4)
 *
 * Returns the minimum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y < x ? y : x
 */
qword _min(qword x, qword y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the minimum of the parameters. (Form 5)
 *
 * Returns the minimum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y < x ? y : x
 */
double _min(double x, double y);

}
