#include "fileReadString.h"

#include <iostream>

namespace capl
{

long fileReadString(char * section, char * entry, char * def, char * buffer, long bufferlen, char * filename)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
