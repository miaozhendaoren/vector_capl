#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the posttrigger of the logging.
 *
 * Sets the posttrigger of the logging. The posttrigger set with this function is valid until the
 * end of the measurement or until the next call of this function.
 *
 * @param preTriggerTime
 *   New posttrigger value in milliseconds.
 *   If a value of -1 is supplied, the prosttrigger will be set to infinity.
 *
 * @return
 *   1, if the posttrigger is set to the given value, 0 else.
 */
long setPostTrigger(long preTriggerTime);

}
