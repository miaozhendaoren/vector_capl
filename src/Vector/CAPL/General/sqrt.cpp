#include "sqrt.h"

#include <complex>

namespace capl
{

double sqrt(double x)
{
    return std::sqrt(x);
}

}
