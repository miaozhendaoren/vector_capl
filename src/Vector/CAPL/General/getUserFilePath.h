#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
long getUserFilePath(char * fileName, char * absPath, long absPathLen);

}
