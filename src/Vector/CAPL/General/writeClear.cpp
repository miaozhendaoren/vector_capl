#include "writeClear.h"
#include "General_intern.h"

namespace capl
{

void writeClear(dword sink)
{
    caplIntern::WriteCommand wc;
    wc.sink = sink;
    wc.command = caplIntern::WriteCommand::Command::WriteClear;
    for (auto fct: caplIntern::on_write)
        fct(wc);

}

}
