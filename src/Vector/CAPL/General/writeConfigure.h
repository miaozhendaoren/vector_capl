#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Configures the specified page in the Write window.
 *
 * Configures the specified page in the Write window.
 *
 * @param sink
 *   Target identifier for the page to be configured.
 *
 * @param lines
 *   The number of lines of the page.
 *
 * @param logging
 *   Enables the logging feature if non-zero.
 *
 * @param filename
 *   Name of the logging file.
 */
void writeConfigure(dword sink, dword lines, dword logging, char * filename);

}
