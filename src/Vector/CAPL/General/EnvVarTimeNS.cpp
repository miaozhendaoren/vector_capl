#include "EnvVarTimeNS.h"

#include <iostream>

namespace capl
{

double EnvVarTimeNS(envvar envVariable)
{
    return envVariable.TIME_NS;
}

}
