#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads a byte from the specified parallel port.
 *
 * @deprecated
 *   Replaced by inport
 *   Before you can use the CAPL functions outport, inport, outportLPT and inportLPT, you must install the generic I/O port driver for Windows.
 *   You will find this driver and its description (ReadMe.Txt) in the directory Exec32\GpIoDrv.
 *
 * Reads a byte from the specified parallel port.
 * This function changes the transmission mode of the parallel port automatically to input.
 * If you want to read from a parallel port the port has to be in a bi-directional mode (PS/2 or "Byte" Modus).
 * Please check this in the CMOS setup (BIOS).
 *
 * @param addr
 *   Port address or a predefined LPTx constant.
 *   Symbolic assignment:
 *   - LPT1 -> 0x378
 *   - LPT2 -> 0x278
 *   - LPT3 -> 0x3BC
 *
 * @return
 *   Byte that was read in
 */
byte inportLPT(word addr);

}
