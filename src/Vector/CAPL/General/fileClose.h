#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Closes the specified file.
 *
 * This function closes the specified file.
 *
 * @param fileHandle
 *   The integer contains the handle to the file.
 *
 * @return
 *   If an error occurs the return value is 0, else 1.
 */
long fileClose(dword fileHandle);

}
