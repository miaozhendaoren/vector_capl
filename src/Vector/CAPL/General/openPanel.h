#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens a panel.
 *
 * Opens a panel.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Designer /
 * Panel Editor.
 *
 * @param panelName
 *   Panel name
 */
void openPanel(char * panelName);

}
