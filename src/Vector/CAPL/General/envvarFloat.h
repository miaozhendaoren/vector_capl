#pragma once

#include "../DataTypes.h"
#include "envvar.h"
#include <cstddef>

namespace capl
{

/**
 * Environment variable of type Float
 */
class envvarFloat : public envvar
{
public:
    envvarFloat();
    virtual ~envvarFloat();

    /* intern */
    double value;

    virtual size_t valueSize();
};

}
