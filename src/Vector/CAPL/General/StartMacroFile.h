#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Starts playing the macro.
 *
 * Starts playing the macro with the fileName file name.
 *
 * @param fileName
 *   Macro file.
 *
 * @return
 *   The returned handle is required to stop the macro playback.
 */
dword StartMacroFile(char * fileName);

}
