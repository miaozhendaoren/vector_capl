#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops the replay file from playing.
 *
 * Stops the replay file from playing with the handle handle.
 *
 * @param handle
 *   Handle of the started replay file.
 *   The handle is returned from the StartReplayFile function when starting the macro.
 */
void StopReplayFile(dword handle);

}
