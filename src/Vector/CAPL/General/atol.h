#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Converts a string to a LONG number.
 *
 * This function converts the string s to a LONG number. The number base is decimal.
 * Starting with string 0x, base 16 is used. Leading blanks are not been read.
 *
 * @param s
 *   String to be converted.
 *
 * @return
 *   long integer
 */
long atol(char * s);

}
