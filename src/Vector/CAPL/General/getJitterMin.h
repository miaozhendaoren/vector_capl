#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the lower limit for the allowable deviation when Jitter is set.
 *
 * Determines the lower limit for the allowable deviation when Jitter is set.
 *
 * @return
 *   Lower deviation in parts per thousand.
 */
int getJitterMin(void);

}
