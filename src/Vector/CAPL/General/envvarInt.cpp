#include "envvarInt.h"

#include <iostream>

namespace capl
{

envvarInt::envvarInt() :
    envvar(),
    value()
{
}

envvarInt::~envvarInt()
{
}

size_t envvarInt::valueSize()
{
    return sizeof(value);
}

}
