#include "RegisterSignalDriverByTxNode.h"

#include <iostream>

namespace capl
{

long RegisterSignalDriver(char * aSignal, char * aTxNode, char * aCallbackFunction)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
