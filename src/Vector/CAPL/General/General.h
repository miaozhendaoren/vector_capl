#pragma once

/**
 * @defgroup General General CAPL Functions
 */

/* Objects */
#include "File.h"
#include "msTimer.h"
#include "timer.h"

/* General Functions */
#include "CompleteStop.h"
#include "DeferStop.h"
#include "FDXTriggerDataGroup.h"
#include "GetBusContext.h"
#include "GetBusNameContext.h"
#include "GetComputerName.h"
#include "GetEventSortingStatus.h"
#include "GetIPAddress.h"
#include "gmLanGetPID.h"
#include "gmLanGetPrio.h"
#include "gmLanGetSourceId.h"
#include "gmLanId.h"
#include "gmLanSetPID.h"
#include "gmLanSetPrio.h"
#include "gmLanSetSourceId.h"
#include "isStatisticAcquisitionRunning.h"
#include "SetBusContext.h"
#include "startStatisticAcquisition.h"
#include "stopStatisticAcquisition.h"
#include "traceSetEventColors.h"

/* Access to Hardware */
#include "InterfaceStatus.h"
#include "xlAcquireLED.h"
#include "xlReleaseLED.h"
#include "xlSetLED.h"

/* Access to CANoe Environment Variables and Panels */
#include "callAllOnEnvVar.h"
#include "ClockControlReset.h"
#include "ClockControlStart.h"
#include "ClockControlStop.h"
#include "closePanel.h"
#include "DeleteControlContent.h"
#include "enableControl.h"
#include "getValue.h"
#include "getValueSize.h"
#include "MakeRGB.h"
#include "openPanel.h"
#include "putValue.h"
#include "putValueToControl.h"
#include "SetClockControlTime.h"
#include "SetControlBackColor.h"
#include "SetControlColors.h"
#include "SetControlForeColor.h"
#include "SetControlProperty.h"
#include "SetControlVisibility.h"
#include "SetDefaultControlColors.h"
#include "SetMediaFile.h"
#include "SetPictureBoxImage.h"

/* Byte Swapping (Intel/Motorola) */
#include "swapDWord.h"
#include "swapInt.h"
#include "swapLong.h"
#include "swapWord.h"

/* File Functions */
#include "fileClose.h"
#include "fileGetBinaryBlock.h"
#include "fileGetString.h"
#include "fileGetStringSZ.h"
#include "filePutString.h"
#include "fileRewind.h"
#include "fileWriteBinaryBlock.h"
#include "GetOfflineFileName.h"
#include "getAbsFilePath.h"
#include "getProfileArray.h"
#include "getProfileFloat.h"
#include "getProfileInt.h"
#include "getProfileString.h"
#include "getUserFilePath.h"
#include "Open.h"
#include "openFileRead.h"
#include "openFileWrite.h"
#include "setFilePath.h"
#include "setWritePath.h"
#include "writeProfileFloat.h"
#include "writeProfileInt.h"
#include "writeProfileString.h"

/* Flow Control */
#include "canOffline.h"
#include "canOnline.h"
#include "getStartdelay.h"
#include "isOfflineMode.h"
#include "isSimulated.h"
#include "IsRunningOnRemoteKernel.h"
#include "setStartDelay.h"
#include "stop.h"

/* Language Support and Debugging */
#include "elCount.h"
#include "fileName.h"
#include "halt.h"
#include "runError.h"
#include "setWriteDbgLevel.h"
#include "writeDbgLevel.h"

/* Logging Functions */
#include "setLogFileName.h"
#include "setPostTrigger.h"
#include "setPreTrigger.h"
#include "StartLogging.h"
#include "StopLogging.h"
#include "trigger.h"
#include "triggerEx.h"
#include "writeToLog.h"
#include "writeToLogEx.h"

/* Replay Functions */
#include "ReplayResume.h"
#include "ReplayStart.h"
#include "ReplayState.h"
#include "ReplayStop.h"
#include "ReplaySuspend.h"
#include "SetReplayFileName.h"
#include "StartMacroFile.h"
#include "StartReplayFile.h"
#include "StopMacroFile.h"
#include "StopReplayFile.h"

/* Standalone Mode */
#include "StandaloneConfigOpen.h"
#include "StandaloneConfigSetDefault.h"

/* String Functions */
#include "_gcvt.h"
#include "atodbl.h"
#include "atol.h"
#include "ltoa.h"
#include "snprintf.h"
#include "strlen.h"
#include "strncat.h"
#include "strncmp.h"
#include "strncmp_off.h"
#include "strncpy.h"
#include "strncpy_off.h"
#include "strstr.h"
#include "strstr_off.h"
#include "substr_cpy.h"
#include "substr_cpy_off.h"
#include "str_match_regex.h"
#include "str_replace.h"
#include "str_replace_regex.h"
#include "strstr_regex.h"
#include "strstr_regex_off.h"
#include "toLower.h"
#include "toUpper.h"

/* Time Management */
#include "cancelTimer.h"
#include "ConvertTimestamp.h"
#include "ConvertTimestampNS.h"
#include "EnvVarTimeNS.h"
#include "getDrift.h"
#include "getJitterMax.h"
#include "getJitterMin.h"
#include "getLocalTime.h"
#include "getLocalTimeString.h"
#include "isTimerActive.h"
#include "MessageTimeNS.h"
#include "setDrift.h"
#include "setJitter.h"
#include "setTimer.h"
#include "setTimerCyclic.h"
#include "timeDiff.h"
#include "timeNow.h"
#include "timeNowFloat.h"
#include "timeNowInt64.h"
#include "timeNowNS.h"
#include "timeToElapse.h"

/* Trigonometric and Mathematical Functions */
#include "arccos.h"
#include "arcsin.h"
#include "arctan.h"
#include "_ceil.h"
#include "_floor.h"
#include "_Log.h"
#include "_Log10.h"
#include "_max.h"
#include "_min.h"
#include "_pow.h"
#include "_round.h"
#include "_round64.h"
#include "abs.h"
#include "cos.h"
#include "exp.h"
#include "random.h"
#include "sin.h"
#include "sqrt.h"

/* User Interactions */
#include "keypressed.h"
#include "msgBeep.h"
#include "sysExec.h"
#include "sysExecCmd.h"
#include "sysExit.h"
#include "sysMinimize.h"
#include "write.h"
#include "writeClear.h"
#include "writeConfigure.h"
#include "writeCreate.h"
#include "writeDestroy.h"
#include "writeEx.h"
#include "writeLineEX.h"
#include "writeTextBkgColor.h"
#include "writeTextColor.h"

/* Obsolete Functions */
#include "beep.h"
#include "checkSignalInRangeByTxNode.h"
#include "fileReadArray.h"
#include "fileReadFloat.h"
#include "fileReadInt.h"
#include "fileReadString.h"
#include "fileWriteFloat.h"
#include "fileWriteInt.h"
#include "fileWriteString.h"
#include "getSignalByTxNode.h"
#include "getSignalTimeByTxNode.h"
#include "inport.h"
#include "inportLPT.h"
#include "inspect.h"
#include "outport.h"
#include "outportLPT.h"
#include "RegisterSignalDriverByTxNode.h"
#include "SetSignalByTxNode.h"
#include "SysGetVariableIntArray.h"
#include "SysSetVariableIntArray.h"

/* LPT constants for outport, inport, outportLPT and inportLPT */
#define LPT1 0x378
#define LPT2 0x278
#define LPT3 0x3BC

/* Undocumented */
#include "envvar.h"
#include "envvarInt.h"
#include "envvarFloat.h"
#include "envvarString.h"
#include "envvarData.h"
#include "registerCAPLDLL.h"

#include <vector>

namespace capl
{

extern std::vector<void (*)(void)> on_preStart;
extern std::vector<void (*)(void)> on_preStop;
extern std::vector<void (*)(void)> on_Start;
extern std::vector<void (*)(void)> on_StopMeasurement;

}
