#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads a byte from the specified port.
 *
 * @deprecated
 *   The Parallel Port is only supported with Windows XP 32 bit.
 *   For simple control applications you can use the IOcab, the IOpiggy or a simple measurement hardware, e.g. the Meilhaus-RedLab series or an appropriate NI card.
 *
 * Reads a byte from the specified port. If you want to read from a parallel port the port has
 * to be in a bi-directional mode (PS/2 or "Byte" Modus). Please check this in the CMOS setup
 * (BIOS).
 *
 * @param addr
 *   Port address or a predefined LPTx constant.
 *   Symbolic assignment:
 *   - LPT1 -> 0x378
 *   - LPT2 -> 0x278
 *   - LPT3 -> 0x3BC
 *   The LPT constant could not be used in arithmetic expression!
 *
 * @return
 *   Byte that was read in.
 */
byte inport(word addr);

}
