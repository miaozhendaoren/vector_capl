#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the maximum of the parameters. (Form 1)
 *
 * Returns the maximum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y > x ? y : x
 */
long _max(long x, long y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the maximum of the parameters. (Form 2)
 *
 * Returns the maximum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y > x ? y : x
 */
dword _max(dword x, dword y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the maximum of the parameters. (Form 3)
 *
 * Returns the maximum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y > x ? y : x
 */
int64 _max(int64 x, int64 y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the maximum of the parameters. (Form 4)
 *
 * Returns the maximum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y > x ? y : x
 */
qword _max(qword x, qword y);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the maximum of the parameters. (Form 5)
 *
 * Returns the maximum of the parameters.
 *
 * @param x
 *   First operand
 *
 * @param y
 *   Second operand
 *
 * @return
 *   y > x ? y : x
 */
double _max(double x, double y);

}
