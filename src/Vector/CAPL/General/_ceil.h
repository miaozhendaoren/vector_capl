#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the ceiling of a value.
 *
 * Calculates the ceiling of a value, i.e. the smallest integer larger or equal to the value.
 *
 * @param x
 *   Value of which the ceiling shall be calculated.
 *
 * @return
 *   Ceiling of x.
 */
double _ceil(double x);

}
