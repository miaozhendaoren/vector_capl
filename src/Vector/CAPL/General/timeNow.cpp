#include "timeNow.h"

#include <iostream>

namespace capl
{

dword timeNow(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
