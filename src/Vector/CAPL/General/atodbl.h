#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Converts a string into a double number.
 *
 * The function converts the string s into a double number. Normally, the base is decimal
 * and must have the following format:
 * [Blank space] [Sign] [Digits] [.Digits] [ {d | D | e | E}[Sign]Digits]
 *
 * String parsing ceases at the first non-compliant character.
 *
 * If the string cannot be converted into a number, 0.0 is returned.
 *
 * If the string starts with 0x, the base used is 16. Only integer numbers can be read in.
 *
 * @param s
 *   Input string to be converted.
 *
 * @return
 *   Double number, the converted string.
 */
double atodbl(char * s);

}
