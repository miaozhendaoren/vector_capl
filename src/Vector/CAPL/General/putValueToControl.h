#pragma once

#include "../DataTypes.h"
#include "../BEAN/BEAN.h"
#include "../CAN/CAN.h"
#include "../J1939/J1939.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, double val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, long val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, char * val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, beanMessage val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, message val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, pg val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the multi display.
 *
 * Assigns the value val to the multi display with the name control. The multi display is
 * located on the panel with the title panel
 *
 * Different contents are displayed with the Control multi display. In addition to numbers
 * (float and integer) and texts (char *), in particular different messages (CAN, LIN, ... and
 * J1939 PGNs) can also be displayed.
 *
 * @param panel
 *   Title of the panel.
 *
 * @param control
 *   Name of the control.
 *
 * @param val
 *   Value to be displayed.
 */
void putValueToControl(char * panel, char * control, linMessage val);

}
