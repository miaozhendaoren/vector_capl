#include "msgBeep.h"

#include <iostream>

namespace capl
{

void msgBeep(long soundType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
