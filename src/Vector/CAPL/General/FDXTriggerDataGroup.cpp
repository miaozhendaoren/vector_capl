#include "FDXTriggerDataGroup.h"

#include <iostream>

namespace capl
{

long FDXTriggerDataGroup(word groupID)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
