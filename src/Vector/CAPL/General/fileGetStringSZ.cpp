#include "fileGetStringSZ.h"

#include <iostream>

namespace capl
{

long fileGetStringSZ(char * buff, long buffsize, dword fileHandle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
