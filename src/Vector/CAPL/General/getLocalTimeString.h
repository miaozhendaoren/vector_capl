#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Copies a printed representation of the current date and time.
 *
 * Copies a printed representation of the current date and time into the supplied character
 * buffer. The format of the string is ddd mmm dd hh:mm:ss jjjj (e.g. "Fri Aug 21 15:22:24
 * 1998").
 *
 * @brief timeBuffer
 *   The buffer the string will be written in.
 *   This buffer must be at least 26 characters long.
 */
void getLocalTimeString(char * timeBuffer);

}
