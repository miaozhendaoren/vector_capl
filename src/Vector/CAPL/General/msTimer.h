/**
 * This class is used to execute a specific function at regular intervals.
 */

#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
class msTimer
{
public:
    void set(long duration);
    void set(long duration, long durationNanoSec);
    void cancel(void);
    int isTimerActive(void);
    long timeToElapse(void);
};

}
