#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Converts a number to a string.
 *
 * The number val is converted to a string s. In this case, base indicates a number base
 * between 2 and 36. s must be large enough to accept the converted number!
 *
 * @param val
 *   Number to be converted.
 *
 * @param s
 *   String, which contains the converted number.
 *
 * @param base
 *   Number base.
 */
void ltoa(long val, char * s, long base);

}
