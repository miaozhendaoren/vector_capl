#include "SysSetVariableIntArray.h"

#include <iostream>

namespace capl
{

long SysSetVariableIntArray(char * namespace_, char * variable, int * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableIntArray(char * SysVarName, int * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
