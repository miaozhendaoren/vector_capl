#include "StopReplayFile.h"

#include <iostream>

namespace capl
{

void StopReplayFile(dword handle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
