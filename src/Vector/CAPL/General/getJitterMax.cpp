#include "getJitterMax.h"

#include <iostream>

namespace capl
{

int getJitterMax(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
