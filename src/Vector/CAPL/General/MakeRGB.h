#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the color value from the three primary color components.
 *
 * Calculates the color value from the three primary color components.
 *
 * @param Red
 *   Red color component (0 - 255)
 *
 * @param Green
 *   Green color component (0 - 255)
 *
 * @param Blue
 *   Blue color component (0 - 255)
 *
 * @return
 *   Color value (Type: long)
 */
long MakeRGB(long Red, long Green, long Blue);

}
