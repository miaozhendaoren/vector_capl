#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads the value of the given variable from the specified section in the specified file.
 *
 * Searches the file filename under section section for the variable entry. Its contents
 * (value) are written to the buffer buff. Its length must be passed correctly in buffsize. If
 * the file or entry is not found, the default value def is copied to buffer.
 *
 * @param section
 *   Section of the file as a string.
 *
 * @param entry
 *   Variable name as a string.
 *
 * @param def
 *   Default value in case of error as a string.
 *
 * @param buff
 *   Buffer for the read-in character as a string.
 *
 * @param buffsize
 *   Size of buff in bytes.
 *
 * @param filename
 *   File name as a string.
 *
 * @return
 *   Number of characters that were read.
 */
long getProfileString(char * section, char * entry, char * def, char * buff, long buffsize, char * filename);

}
