#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Starts a new acquisition range.
 *
 * A new acquisition range is started with this function. If an acquisition range has already
 * been started, the function has no effect since it cannot influence the currently active
 * range.
 */
void startStatisticAcquisition(void);

}
