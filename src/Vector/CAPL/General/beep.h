#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Tone output.
 *
 * @deprecated
 *   Replaced by msgBeep
 *
 * Tone output. In the Windows Version the parameter duration has no effect.
 *
 * @param freq
 *   Integer for tone pitch.
 *   In the Windows version, the parameters freq defines the tone output.
 *   Different sounds are defined in the section [SOUND] in the file WIN.ini:
 *   - freq = 0x0000 (SystemDefault)
 *   - freq = 0x0010 (SystemHand)
 *   - freq = 0x0020 (SystemQuestion)
 *   - freq = 0x0030 (SystemExclamation)
 *   - freq = 0x0040 (SystemAsterisk)
 *   - freq = 0xFFFF Standard Beep
 *
 * @param duration
 *   integer for tone length
 */
void beep(int freq, int duration);

}
