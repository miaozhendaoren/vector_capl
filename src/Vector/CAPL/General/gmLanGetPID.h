#pragma once

#include "../DataTypes.h"
#include "../GMLAN/GMLAN.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the priority of the message.
 *
 * gmLanGetPID gets the parameter ID of the message.
 *
 * @param msg
 *   Message
 *
 * @return
 *   Parameter ID
 */
long gmLanGetPID(gmLanMessage msg);

}
