#include "inspect.h"

#include <iostream>

namespace capl
{

void inspect(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
