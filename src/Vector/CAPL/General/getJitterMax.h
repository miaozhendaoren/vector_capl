#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the upper limit for the allowable deviation when Jitter is set.
 *
 * Determines the upper limit for the allowable deviation when Jitter is set.
 *
 * @return
 *   Upper deviation in parts per thousand.
 */
int getJitterMax(void);

}
