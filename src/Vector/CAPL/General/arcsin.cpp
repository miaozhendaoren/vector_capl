#include "arcsin.h"

#include <iostream>

namespace capl
{

double arcsin(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
