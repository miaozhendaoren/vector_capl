#include "fileWriteFloat.h"

#include <iostream>

namespace capl
{

long fileWriteFloat(char * section, char * entry, double def, char * file)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
