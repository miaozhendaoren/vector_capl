#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Copies a substring to another string.
 *
 * This function copies a substring of src to dest. max indicates the maximum length of src
 * and dest.
 *
 * The function ensures that there is a terminating '\0'. Thus, a maximum of max-1-
 * destOffset characters are copied.
 *
 * @param dest
 *   Destination buffer
 *
 * @param destOffset
 *   Offset in destination buffer
 *
 * @param src
 *   Source string
 *
 * @param srcStart
 *   Start index in src of substring
 *
 * @param len
 *   Length of the substring, or -1 to copy the string until the end
 *
 * @param max
 *   Maximum number of characters in src and dest
 *
 */
void substr_cpy_off(char * dest, long destOffset, char * src, long srcStart, long len, long max);

}
