#pragma once

#include "../DataTypes.h"
#include "General.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets a cyclical timer. (Form 1)
 *
 * Sets a cyclical timer.
 *
 * With form 2, firstDuration is implicitly the same as period, i.e. the timer runs precisely
 * according to period the first time.
 *
 * @param t
 *   The timer to be set.
 *
 * @param firstDuration
 *   Time in milliseconds until the timer runs out for the first time.
 *
 * @param period
 *   Time in milliseconds in which the timer is restarted in case of expiration.
 */
void setTimerCyclic(msTimer t, long firstDuration, long period);

/**
 * @ingroup General
 *
 * @brief
 *   Sets a cyclical timer. (Form 2)
 *
 * Sets a cyclical timer.
 *
 * With form 2, firstDuration is implicitly the same as period, i.e. the timer runs precisely
 * according to period the first time.
 *
 * @param t
 *   The timer to be set.
 *
 * @param period
 *   Time in milliseconds in which the timer is restarted in case of expiration.
 */
void setTimerCyclic(msTimer t, long period);

}
