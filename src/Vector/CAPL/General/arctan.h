#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates arctangent of a value.
 *
 * @param x
 *   Value whose arctangent is to be calculated.
 *
 * @return
 *   Arcus Tangent of x.
 */
double arctan(double x);

}
