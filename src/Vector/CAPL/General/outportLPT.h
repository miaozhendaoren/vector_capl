#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Writes a byte to the specified parallel port.
 *
 * @deprecated
 *   Replaced by outport
 *
 * Writes a byte to the specified parallel port.
 * This function changes the transmission mode of the parallel port automatically to output.
 *
 * @param addr
 *   Port address or a predefined LPTx constant
 *
 * @param value
 *   Output value
 *   Symbolic assignment:
 *   - LPT1 -> 0x378
 *   - LPT2 -> 0x278
 *   - LPT3 -> 0x3BC
 */
void outportLPT(word addr, byte value);

}
