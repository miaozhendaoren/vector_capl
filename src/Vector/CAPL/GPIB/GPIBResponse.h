#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Is called when the query returns.
 *
 * This function is called when the query returns. Users must implement this function and
 * receive the original query string, the result as a string and the device ID.
 *
 * @param deviceDescriptor
 *   The Device Descriptor of the device, that transmitted the response.
 *
 * @param queryString
 *   The data transmitted in the GPIBQuery or GPIBQueryEx call, respectively.
 *
 * @param resultString
 *   The device response.
 */
void GPIBResponse(long deviceDescriptor, char * queryString, char * resultString);

}
