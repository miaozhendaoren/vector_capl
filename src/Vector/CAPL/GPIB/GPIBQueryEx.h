#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Query to the device.
 *
 * Query to the device.
 *
 * The cmdStr is put to a queue, and then the function returns immediately. When the
 * cmdStr has been sent to the device, and a response has been received, the user-supplied
 * function GPIBResponse is called.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @param cmdStr
 *   Query string
 *
 * @param size
 *   Maximum expected size of the data to be read.
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available, or if no GPIBResponse function was supplied
 */
long GPIBQueryEx(long deviceDescriptor, char * cmdStr, long size);

}
