#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Resets a device.
 *
 * Resets a device.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBDevClear(long deviceDescriptor);

}
