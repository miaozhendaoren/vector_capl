#include "GPIBDevOnline.h"

#include <iostream>

namespace capl
{

long GPIBDevOnline(long deviceDescriptor, long mode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
