#pragma once

/**
 * @defgroup GPIB GPIB CAPL Functions
 */

#include "GPIBDevChangePrimAddr.h"
#include "GPIBDevChangeSecAddr.h"
#include "GPIBDevClear.h"
#include "GPIBDevGotoLocal.h"
#include "GPIBDevOnline.h"
#include "GPIBDevOpen.h"
#include "GPIBGetCnt.h"
#include "GPIBGetCtrlLineStatus.h"
#include "GPIBGetError.h"
#include "GPIBGetFloatResult.h"
#include "GPIBGetIntResult.h"
#include "GPIBGetStatus.h"
#include "GPIBOnError.h"
#include "GPIBQuery.h"
#include "GPIBQueryEx.h"
#include "GPIBReqRelSysCtrl.h"
#include "GPIBResponse.h"
#include "GPIBWriteNum.h"
#include "GPIBWriteStr.h"
