#include "GPIBDevChangeSecAddr.h"

#include <iostream>

namespace capl
{

long GPIBDevChangeSecAddr(long deviceDescriptor, long newAddr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
