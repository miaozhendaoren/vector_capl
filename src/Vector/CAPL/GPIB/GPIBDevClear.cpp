#include "GPIBDevClear.h"

#include <iostream>

namespace capl
{

long GPIBDevClear(long deviceDescriptor)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
