#include "GPIBReqRelSysCtrl.h"

#include <iostream>

namespace capl
{

long GPIBReqRelSysCtrl(long boardIdx, long mode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
