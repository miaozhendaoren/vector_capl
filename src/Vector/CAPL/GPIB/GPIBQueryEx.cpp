#include "GPIBQueryEx.h"

#include <iostream>

namespace capl
{

long GPIBQueryEx(long deviceDescriptor, char * cmdStr, long size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
