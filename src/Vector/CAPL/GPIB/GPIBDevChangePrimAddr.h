#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Changes the primary address of a device.
 *
 * Changes the primary address of a device.
 *
 * @param deviceDescriptor
 *   Changes the primary address of a device.
 *
 * @param newAddr
 *   New secondary address
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBDevChangePrimAddr(long deviceDescriptor, long newAddr);

}
