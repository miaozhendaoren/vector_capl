#include "GPIBQuery.h"

#include <iostream>

namespace capl
{

long GPIBQuery(long deviceDescriptor, char * cmdStr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
