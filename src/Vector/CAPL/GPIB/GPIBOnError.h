#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Is called if a query or a write operation raised an error condition.
 *
 * This optional function is called if a query or a write operation (GPIBWriteStr or
 * GPIBWriteNum) raised an error condition.
 *
 * Users may implement this function and then receive the original query string or the data
 * to be written, respectively. The result string, if any, is also returned, as well as the GPIB
 * status word, the error code and the device descriptor.
 *
 * @param deviceDescriptor
 *   The descriptor of the device that transmitted the response.
 *
 * @param query
 *   The data transmitted in the failed GPIBQuery... or GPIBWrite... call, respectively
 *
 * @param response
 *   The data received from the device so far. The empty string if a write operation failure is
 *   reported.
 *
 * @param status
 *   The GPIB status word.
 *
 * @param error
 *   The GPIB error code.
 */
void GPIBOnError(long deviceDescriptor, char * query, char * response, long status, long error);

}
