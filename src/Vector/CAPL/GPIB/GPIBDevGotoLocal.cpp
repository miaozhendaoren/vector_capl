#include "GPIBDevGotoLocal.h"

#include <iostream>

namespace capl
{

long GPIBDevGotoLocal(long deviceDescriptor)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
