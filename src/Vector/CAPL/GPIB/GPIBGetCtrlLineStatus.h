#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Returns the status of the GPIB control lines.
 *
 * Returns the status of the GPIB control lines.
 *
 * @param boardIdx
 *   GPIB Board ID
 *
 * @return
 *   Line Status Bytes
 *   The value consists of 2 bytes. The low-order byte (bits 0 through 7) contains a mask
 *   indicating the capability of the GPIB interface to sense the status of each GPIB control
 *   line. The high-order byte (bits 8 through 15) contains the GPIB control line state
 *   information. If a bit is set (1), the corresponding control line can be monitored by the
 *   interface or is asserted, respectively. The following is a pattern of each byte.
 *   Bit 7..0: EOI ATN SRQ REN IFC NRFd NDAC DAV
 *   -1 (all bits set in 4 bytes) is returned on error, if no GPIB driver is available.
 */
long GPIBGetCtrlLineStatus(long boardIdx);

}
