#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Returns the error variable.
 *
 * Returns the error variable. It is meaningful only when the ERR bit in the status word is set
 *
 * @return
 *   Current error status
 *   28: on error, if no GPIB driver is available
 */
long GPIBGetError(void);

}
