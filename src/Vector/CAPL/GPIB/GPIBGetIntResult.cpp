#include "GPIBGetIntResult.h"

#include <iostream>

namespace capl
{

long GPIBGetIntResult(char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
