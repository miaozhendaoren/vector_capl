#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Writes GPIB command and numeric value to the device.
 *
 * Writes cmdStr + numeric value to the device.
 *
 * The function returns immediately and does not wait for the end of the command
 * transmission.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @param cmdStr
 *   GPIB command
 *
 * @param value
 *   Numeric value of the parameter
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBWriteNum(long deviceDescriptor, char * cmdStr, double value);

}
