#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Sets or deletes the permission to send interface clear (IFC) or remote enabler (REN).
 *
 * Sets or deletes the permission to send interface clear (IFC) or remote enable (REN). If
 * mode is "0," the GPIB board surrenders system control and all controller-specific
 * commands are not allowed. If mode = "1," then controller-specific commands are allowed.
 *
 * @param boardIdx
 *   GPIB Board ID
 *
 * @param mode
 *   (0..1)
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBReqRelSysCtrl(long boardIdx, long mode);

}
