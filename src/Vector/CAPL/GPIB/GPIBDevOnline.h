#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Activates/deactivates the device.
 *
 * Activates/deactivates the device.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @param mode
 *   mode
 *   - 1: online
 *   - 0: offline
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBDevOnline(long deviceDescriptor, long mode);

}
