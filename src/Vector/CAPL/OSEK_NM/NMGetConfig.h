#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * The elements in the fields flags are set to 1, if the node corresponding to the
 * element index participates in the NM, to 0 otherwise. The parameter startIndex
 * denotes the node index corresponding to field element 0, i.e. flags[n] is
 * set to the state of node (n + startIndex). The parameter startIndex can be
 * omitted and 0 is assumed.
 * The number of active nodes is returned by this function, therefore it is possible
 * to query the number of active nodes.
 */
long NMGetConfig(unsigned long arraySize, byte * flags, unsigned long startIndex = 0);

}
