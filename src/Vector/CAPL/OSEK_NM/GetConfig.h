#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMGetConfig
 *
 * Current configuration shows which ECUs are actively participating in Network
 * Management. The ECU No. 31 has the most significant bit (31 ... 0).
 */
unsigned long GetConfig(void);

}
