#include "GetConfig.h"

#include <iostream>

namespace capl
{

unsigned long GetConfig(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
