#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMBusSleepInd
 *
 * This function communicates to Network Management that the application is
 * ready to sleep.
 */
void GotoMode_BusSleep(void);

}
