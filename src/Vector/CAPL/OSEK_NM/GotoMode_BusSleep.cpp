#include "GotoMode_BusSleep.h"

#include <iostream>

namespace capl
{

void GotoMode_BusSleep(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
