#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMAwake
 *
 * The application can cancel its readiness to sleep with this function while
 * Network Management is running.
 * If the sleep state has already been entered, this function must be called to
 * initialize Network Management. This function is then generally called by the
 * function ApplCanWakeUp() (cf. CALLBACK Function 4).
 *
 */
void GotoMode_Awake(void);

}
