#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMSilent
 *
 * After this call Network Management does not participate in the ring any more
 * and also does not observe incoming messages.
 */
void SilentNM(void);

}
