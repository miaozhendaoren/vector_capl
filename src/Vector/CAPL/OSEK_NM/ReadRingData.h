#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * Any ring data that has been received previously could be read at any time.
 * The only pre-condition is a stable ring (means, that the token has been
 * passed one time to every node on the bus without any alive or limphome
 * message).
 * The ringdata will be copied into a CAPL-internal data buffer, where Array
 * (the 2nd parameter) points to. This requires that enough space should be allocated
 * for this array. The first parameter (ArraySize) gives the number of data
 * that could be read.
 * If ring is not stable, which means that access to ring data are not allowed, the
 * function returns 0. Otherwise, it returns the number of data written into the array.
 */
unsigned int ReadRingData(unsigned long ArraySize, byte * Array);

}
