#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMGetStatus
 *
 * The internal status of Network Management is retrieved with this function.
 * The lower eight bits have the following structure:
 * - Bit 1: Config. Stable
 * - Bit 2: CAN-Drv Active
 * - Bit 5: NM in Bus Sleep
 * - Bit 6: NM in LimpHome
 * - Bit 7: NM On
 * - Bit 8: Sleep Ready On
 */
unsigned long NMGetStatus(void);

}
