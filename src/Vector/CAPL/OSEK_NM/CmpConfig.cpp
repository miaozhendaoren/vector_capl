#include "CmpConfig.h"

#include <iostream>

namespace capl
{

unsigned long CmpConfig(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
