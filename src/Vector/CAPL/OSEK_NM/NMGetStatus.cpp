#include "NMGetStatus.h"

#include <iostream>

namespace capl
{

unsigned long NMGetStatus(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
