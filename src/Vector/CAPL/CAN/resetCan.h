#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Resets the CAN controller.
 *
 * Resets the CAN controller. Can be used to reset the CAN controller after a BUSOFF or to
 * activate configuration changes. Since execution of the function takes some time and the
 * CAN controller is disconnected from the bus briefly, messages can be lost when this is
 * performed.
 */
void VECTOR_CAPL_EXPORT resetCan(void);

}
