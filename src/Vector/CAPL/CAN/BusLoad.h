#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current busload of a channel.
 *
 * Returns the current busload of a channel.
 *
 * @return
 *   Current busload of channel in percent.
 */
long VECTOR_CAPL_EXPORT BusLoad(void);

}
