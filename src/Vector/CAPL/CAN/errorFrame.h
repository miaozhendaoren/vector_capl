#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl {

/**
 * @ingroup CAN
 *
 * CAN Error Frame
 */
class VECTOR_CAPL_EXPORT errorFrame
{
public:
    errorFrame();
    virtual ~errorFrame();

    /**
     * Time stamp
     * Unit: nanosecond
     */
    int64 Time_ns;

    /**
     * Time stamp
     * Unit: 10µs
     */
    dword Time;

    /**
     * Error Capture Code
     */
    byte ECC;

    union {
        /**
         * Transmission channel or channel through which the frame error has been received.
         * Value range: 1..32
         */
        word CAN;

        /**
         * Transmission channel or channel through which the frame error has been received.
         * Value range: 1..32
         */
        word MsgChannel;
    };

    /**
     * Start-of-Frame time stamp in ns.
     *
     * For some CAN hardware this value is not available (value 0). In such a case a software calculation is required (see CANoe Options dialog).
     */
    int64 SOF;

    /**
     * Bit index where an error has been detected by the controller.
     * Thereby the first bit in frame has index 0.
     *
     * Only valid for CANCardXLe and VNxxxx interfaces
     */
    long ErrorPosition_Bit;

    /**
     * Time interval from SOF time stamp to the first error bit.
     * Unit: nanoseconds
     *
     * Only valid for CANCardXLe and VNxxxx interfaces
     */
    long ErrorPosition_Time;

    /**
     * Extended Data Length
     *
     * - 0 = CAN message
     * - 1 = CAN FD message
     */
    char EDL;

    /**
     * Bit Rate Switch
     */
    char BRS;

    /**
     * Error State Indicator
     *
     * - 0 = ESI not set
     * - 1 = ESI set
     * - 2 = CAN controller sets ESI automatically depending on the state of the controller
     */
    char ESI;

    /**
     * The selector Phase gives information on whether the error occurred during the arbitration phase or the during data phase:
     *
     * - 0 = arbitration phase
     * - 1 = data phase
     */
    char Phase;
};

}
