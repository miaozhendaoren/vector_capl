#include "ExtendedRemoteFrameRate.h"

#include <iostream>

namespace capl
{

long ExtendedRemoteFrameRate(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
