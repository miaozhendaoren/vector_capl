#pragma once

#include "../DataTypes.h"
#include "CAN.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the value of a message identifier independent of its type.
 *
 * Returns the value of a message identifier independent of its type.
 *
 * Identifier as long value.
 *
 * @param id
 *   Id portion of a message.
 *
 * @return
 *   Identifier as long value.
 */
long VECTOR_CAPL_EXPORT valOfId(dword id);

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the value of a message identifier independent of its type.
 *
 * Returns the value of a message identifier independent of its type.
 *
 * Identifier as long value.
 *
 * @param m
 *   Variable of the type message.
 *
 * @return
 *   Identifier as long value.
 */
long VECTOR_CAPL_EXPORT valOfId(message m);

}
