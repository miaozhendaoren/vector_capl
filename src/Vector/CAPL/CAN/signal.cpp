#include "signal.h"

#include <cstring>
#include <limits>

/* '..' : This function or variable may be unsafe. Consider using ... instead. To disable deprecation, use _CRT_SECURE_NO_WARNINGS. See online help for details. */
#pragma warning (disable: 4996)

namespace capl
{

template<typename RawType>
signal<RawType>::signal(message * parent) :
    bitstart(0),
    bitcount(0),
    bigEndian(false),
    offset(0.0),
    factor(1.0),
    unit(nullptr),
    minimum(0.0),
    maximum(0.0),
    dbtype(nullptr),
    parent(parent),
    rawValue(0)
{
}

template<typename RawType>
signal<RawType>::signal(const signal<RawType> & rhs)
{
    if (this != &rhs) {
        bitstart = rhs.bitstart;
        bitcount = rhs.bitcount;
        bigEndian = rhs.bigEndian;
        offset = rhs.offset;
        factor = rhs.factor;
        unit = new char[strlen(rhs.unit)+1];
        strcpy(unit, rhs.unit);
        minimum = rhs.minimum;
        maximum = rhs.maximum;
        dbtype = rhs.dbtype;
        parent = rhs.parent;
        rawValue = rhs.rawValue;
    }
}

template<typename RawType>
signal<RawType>::~signal()
{
    delete unit;
}

template<typename RawType>
signal<RawType> & signal<RawType>::operator =(const signal<RawType> & rhs)
{
    if (this != &rhs) {
        bitstart = rhs.bitstart;
        bitcount = rhs.bitcount;
        bigEndian = rhs.bigEndian;
        offset = rhs.offset;
        factor = rhs.factor;
        unit = new char[strlen(rhs.unit)+1];
        strcpy(unit, rhs.unit);
        minimum = rhs.minimum;
        maximum = rhs.maximum;
        dbtype = rhs.dbtype;
        parent = rhs.parent;
        rawValue = rhs.rawValue;
    }

    return *this;
}

template<typename RawType>
bool signal<RawType>::messageUpdated()
{
    /* safety check */
    if (parent == nullptr)
        return false;

    /* target data union */
    union {
        RawType value1;
        uint64_t value2 : sizeof(RawType);
    };

    /* copy bits */
    unsigned int msgBit = bitstart;
    unsigned int size = bitcount;
    if (bigEndian) {
        /* start with MSB */
        unsigned int sigBit = size - 1;
        while(size > 0) {
            /* copy bit */
            if (parent->data[msgBit/8] & (1<<(msgBit%8))) {
                value2 |= (1ULL<<sigBit);
            }

            /* calculate next position */
            if ((msgBit % 8) == 0) {
                msgBit += 15;
            } else {
                --msgBit;
            }
            --sigBit;
            --size;
        }
    } else {
        /* start with LSB */
        unsigned int sigBit = 0;
        while(size > 0) {
            /* copy bit */
            if (parent->data[msgBit/8] & (1<<(msgBit%8))) {
                value2 |= (1ULL<<sigBit);
            }

            /* calculate next position */
            ++msgBit;
            ++sigBit;
            --size;
        }
    }

    /* if signed, then fill all bits above MSB with 1 */
    if (std::numeric_limits<RawType>::is_signed) {
        unsigned int msb = (value2 >> (size - 1)) & 1;
        if (msb) {
            for (unsigned int i = size; i < 8*sizeof(RawType); ++i) {
                value2 |= (1ULL<<i);
            }
        }
    }

    /* check if value has changed */
    bool valueChanged = (value1 != rawValue);
    rawValue = value1;
    return valueChanged;
}

template<typename RawType>
signal<RawType>::operator RawType() const
{
    return rawValue;
}

template<typename RawType>
signal<RawType> & signal<RawType>::operator=(const RawType newValue)
{
    rawValue = newValue;

    /* safety check */
    if (parent == nullptr)
        return *this;

    /* target data union */
    union {
        RawType value1;
        uint64_t value2 : sizeof(RawType);
    };
    value1 = rawValue;

    /* copy bits */
    unsigned int msgBit = bitstart;
    unsigned int size = bitcount;
    if (bigEndian) {
        /* start with MSB */
        unsigned int sigBit = size - 1;
        while(size > 0) {
            /* copy bit */
            if (value2 & (1ULL<<sigBit)) {
                /* set the bit */
                parent->data[msgBit/8] |= (1<<(msgBit%8));
            } else {
                /* clear the bit */
                parent->data[msgBit/8] &= ~(1<<(msgBit%8));
            }

            /* calculate next position */
            if ((msgBit % 8) == 0) {
                msgBit += 15;
            } else {
                --msgBit;
            }
            --sigBit;
            --size;
        }
    } else {
        /* start with LSB */
        unsigned int sigBit = 0;
        while(size > 0) {
            /* copy bit */
            if (value2 & (1ULL<<sigBit)) {
                /* set the bit */
                parent->data[msgBit/8] |= (1ULL<<sigBit);
            } else {
                /* clear the bit */
                parent->data[msgBit/8] &= ~(1ULL<<sigBit);
            }

            /* calculate next position */
            ++msgBit;
            ++sigBit;
            --size;
        }
    }

    return *this;
}

template<typename RawType>
double signal<RawType>::getPhys()
{
    double phys = ((double) rawValue * factor) + offset;
    return phys;
}

template<typename RawType>
void signal<RawType>::setPhys(double newValue)
{
    double newRawValue = (newValue - offset) / factor;
    operator=((RawType) newRawValue);
}

/* Instantiate signal for the supported template type parameters */
template class signal<unsigned int>;
template class signal<signed int>;
template class signal<float>;
template class signal<double>;

}
