#include "canGetDataLength.h"

#include <iostream>

namespace capl
{

long canGetDataLength(message obj)
{
    return obj.DataLength;
}

}
