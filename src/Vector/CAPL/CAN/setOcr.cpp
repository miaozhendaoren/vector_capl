#include "setOcr.h"

#include <iostream>

namespace capl
{

long setOcr(long channel, byte ocr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
