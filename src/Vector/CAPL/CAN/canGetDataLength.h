#pragma once

#include "../DataTypes.h"
#include "CAN.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the valid payload length of the can message.
 *
 * Returns the valid payload length of the can message.
 *
 * @param obj
 *   CAN message
 *
 * @return
 *   0 - 64
 */
long VECTOR_CAPL_EXPORT canGetDataLength(message obj);

}
