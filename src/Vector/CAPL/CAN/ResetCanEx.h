#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Resets the CAN controller for one specific CAN channel.
 *
 * Resets the CAN controller for one specific CAN channel. Can be used to reset the CAN
 * controller after a BUSOFF or to activate configuration changes. Since execution of the
 * function takes a certain amount of time and the CAN controller is disconnected from the
 * bus for a brief period messages may be lost.
 *
 * @param channel
 *   CAN channel
 */
void VECTOR_CAPL_EXPORT ResetCanEx(long channel);

}
