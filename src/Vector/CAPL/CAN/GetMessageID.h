#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Finds out the message ID. (Form 1)
 *
 * Finds out the message ID.
 *
 * @param messageName
 *   Name of the message.
 *
 * @return
 *   Message ID, or (dword)-1 if the message is not found
 */
dword VECTOR_CAPL_EXPORT GetMessageID(char * messageName);

/**
 * @ingroup CAN
 *
 * @brief
 *   Finds out the message ID. (Form 2)
 *
 * Finds out the message ID.
 *
 * @param dbName
 *   Name of the database, needed if the message name is used in more than one database.
 *
 * @param messageName
 *   Name of the message.
 *
 * @return
 *   Message ID, or (dword)-1 if the message is not found
 */
dword VECTOR_CAPL_EXPORT GetMessageID(char * dbName, char * messageName);

}
