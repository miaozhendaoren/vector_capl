#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the number of standard remote CAN frames.
 *
 * @deprecated
 *   Replaced by StandardRemoteFrameCount.
 *
 * Returns the number of standard remote CAN frames on the specified channel since start of measurement.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Number of standard remote CAN frames on the specified channel since start of measurement.
 */
long VECTOR_CAPL_EXPORT canGetStdRemote(long channel);

}
