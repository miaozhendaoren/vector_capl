#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the number of CAN overload frames on a channel since start of measurement.
 *
 * Returns the number of CAN overload frames on a channel since start of measurement.
 *
 * @return
 *   Number of CAN overload frames on a channel since start of measurement.
 */
long VECTOR_CAPL_EXPORT OverloadFrameCount(void);

}
