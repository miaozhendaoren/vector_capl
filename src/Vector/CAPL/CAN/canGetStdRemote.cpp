#include "canGetStdRemote.h"

#include <iostream>

namespace capl
{

long canGetStdRemote(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
