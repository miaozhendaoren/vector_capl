#include "StandardFrameRate.h"

#include <iostream>

namespace capl
{

long StandardFrameRate(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
