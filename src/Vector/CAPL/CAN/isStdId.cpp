#include "isStdId.h"

#include <iostream>

namespace capl
{

long isStdId(dword id)
{
    return (id & (1 << 31)) == 0;
}

long isStdId(message m)
{
    return (m.ID & (1 << 31)) == 0;
}

}
