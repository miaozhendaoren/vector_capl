#include "canGetExtDataRate.h"

#include <iostream>

namespace capl
{

long canGetExtDataRate(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
