#include "canResetStatistics.h"

#include <iostream>

namespace capl
{

void canResetStatistics(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void canResetStatistics(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
