#include "setCanCabsMode.h"

#include <iostream>

namespace capl
{

long setCanCabsMode(long ntype, long nchannel, long nmode, long nflags)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
