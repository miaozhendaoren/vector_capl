#include "canGetTxErrorCount.h"

#include <iostream>

namespace capl
{

long canGetTxErrorCount(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
