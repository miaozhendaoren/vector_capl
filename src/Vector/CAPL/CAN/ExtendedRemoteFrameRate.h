#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the number of extended remote CAN messages on a channel since start of measurement.
 *
 * Returns the number of extended remote CAN messages on a channel since start of measurement.
 *
 * @return
 *   Current rate of extended remote CAN messages on a channel in frames per second.
 */
long VECTOR_CAPL_EXPORT ExtendedRemoteFrameRate(void);

}
