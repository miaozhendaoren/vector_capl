#include "valOfId.h"

#include <iostream>

namespace capl
{

long valOfId(dword id)
{
    return (id & ~(1<<31));
}

long valOfId(message m)
{
    return (m.ID & ~(1<<31));
}

}
