#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current rate of extended CAN frames on a channel.
 *
 * Returns the current rate of extended CAN frames on a channel.
 *
 * @return
 *   Current rate of extended CAN frames on a channel in messages per second.
 */
long VECTOR_CAPL_EXPORT ExtendedFrameRate(void);

}
