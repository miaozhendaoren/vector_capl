#include "BusLoad.h"

#include <iostream>

namespace capl
{

long BusLoad(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
