#include "ErrorFrameCount.h"

#include <iostream>

namespace capl
{

long ErrorFrameCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
