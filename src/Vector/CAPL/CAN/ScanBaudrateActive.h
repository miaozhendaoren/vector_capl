#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Determines the baud rate for the given channel.
 *
 * The function determines the baud rate for the given channel. Result of the function is
 * written into the Write window.
 *
 * The baud rate scanner checks different baud rates and tries to send a message through
 * the given channel. The function is finished if the message was sent successfully and the
 * baud rate was determined. If a wrong baud rate is present, the other power supply takers
 * cannot receive the message. CANoe as the transmitter does not receive an acknowledge
 * and sends an ErrorFrame. In this case the next baud rate of the baud rate range is
 * checked.
 *
 * @param channel
 *   Channel number. (1,..,32)
 *
 * @param messageID
 *   ID of the message that the scanner will send to detect the baudrate. The DLC of the
 *   message is always 8.
 *
 * @param firstBaudrate, lastBaudrate
 *   Baud rate range to scan.
 *   - If both values are set to zero the scanner checks the most commonly used baudrates:
 *     33.333, 50.0, 83.333, 100.0, 125.0, 250.0, 500.0, 1000.0 [kBaud]
 *   - If both values are the same but not zero the scanner multiplies the baudrate with a
 *     given factor (Value range 0.25-5.0). The factor is changed with steps of 0.25.
 *   - If both values are different, all possible baudrate values in the ranged are scanned.
 *     The incremental step in the range is 1.5%.
 *
 * @param timeout
 *   Period of time [ms] the scanner waits when the message is sent.
 *
 * @return
 *   Returns 0 if the scan function was successfully started. Otherwise the return value is nonzero.
 */
long VECTOR_CAPL_EXPORT ScanBaudrateActive(dword channel, dword messageID, double firstBaudrate, double lastBaudrate, dword timeout);

}
