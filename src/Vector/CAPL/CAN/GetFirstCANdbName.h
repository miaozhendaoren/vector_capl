#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Finds out the name of the first assigned database.
 *
 * Finds out the name of the first assigned database.
 *
 * @param buffer
 *   Buffer in which the database name is written.
 *
 * @param size
 *   Size of the buffer in Byte.
 *
 * @return
 *   If successful unequal 0, otherwise 0.
 */
dword VECTOR_CAPL_EXPORT GetFirstCANdbName(char * buffer, dword size);

}
