#include "StandardRemoteFrameCount.h"

#include <iostream>

namespace capl
{

long StandardRemoteFrameCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
