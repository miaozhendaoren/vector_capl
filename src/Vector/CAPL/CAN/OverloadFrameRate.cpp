#include "OverloadFrameRate.h"

#include <iostream>

namespace capl
{

long OverloadFrameRate(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
