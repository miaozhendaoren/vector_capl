#include "GetMessageName.h"

#include <iostream>

namespace capl
{

dword GetMessageName(dword id, dword context, char * buffer, dword size)
{
    word channel = context & 0xffff;
    switch (context >> 16) {
    case 1: // CAN
        break;
    case 5: // LIN
        break;
    case 6: // MOST
        break;
    case 7: // FlexRay
        break;
    case 8: // BEAN
        break;
    case 9: // J1708
        break;
    }

    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
