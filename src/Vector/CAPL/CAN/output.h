#pragma once

#include "../DataTypes.h"
#include "CAN.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Outputs a message frame from the program block.
 *
 * Outputs a message frame from the program block.
 *
 * @param msg
 *   Variable of type message.
 */
void VECTOR_CAPL_EXPORT output(message msg);

/**
 * @ingroup CAN
 *
 * @brief
 *   Outputs an error frame from the program block.
 *
 * Outputs an error frame from the program block.
 *
 * @param msg
 *   Variable of type errorFrame.
 */
void VECTOR_CAPL_EXPORT output(errorFrame msg);

}
