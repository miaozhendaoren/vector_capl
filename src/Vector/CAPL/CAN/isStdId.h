#pragma once

#include "../DataTypes.h"
#include "CAN.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Checks parameter for standard identifier.
 *
 * Checks parameter for extended identifier (29 bit) or standard identifier (11 Bit).
 *
 * @param id
 *   Id part of a message
 *
 * @return
 *   1 if check was successful, else 0
 */
long VECTOR_CAPL_EXPORT isStdId(dword id);

/**
 * @ingroup CAN
 *
 * @brief
 *   Checks parameter for standard identifier.
 *
 * Checks parameter for extended identifier (29 bit) or standard identifier (11 Bit).
 *
 * @param m
 *   Variable of type message.
 *
 * @return
 *   1 if check was successful, else 0
 */
long VECTOR_CAPL_EXPORT isStdId(message m);

}
