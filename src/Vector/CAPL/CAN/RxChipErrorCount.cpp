#include "RxChipErrorCount.h"

#include <iostream>

namespace capl
{

long RxChipErrorCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
