#include "ExtendedFrameCount.h"

#include <iostream>

namespace capl
{

long ExtendedFrameCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
