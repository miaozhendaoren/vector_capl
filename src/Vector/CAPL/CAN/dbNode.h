#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * Node definitions from the database
 */
class VECTOR_CAPL_EXPORT dbNode
{
public:
    dbNode();
    virtual ~dbNode();
};

}
