#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Activates/deactivates the TxReq of the CAN controller.
 *
 * Activates/deactivates the TxReq and Tx of the CAN controller. This function does nothing
 * with the Ack bit.
 *
 * @param channel
 *   CAN channel
 *
 * @param gtx
 *   gtx
 *   - 0: tx off
 *   - 1: tx on
 *
 * @param gtxreq
 *   gtxreq
 *   - 0: gtxreq off
 *   - 1: gtxreq on
 *
 * @return
 *   - 0: ok
 *   - !=0: error
 */
long VECTOR_CAPL_EXPORT canSetChannelMode(long channel, long gtx, long gtxreq);

}
