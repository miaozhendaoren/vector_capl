#include "canGetStdData.h"

#include <iostream>

namespace capl
{

long canGetStdData(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
