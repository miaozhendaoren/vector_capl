#include "canSetChannelMode.h"

#include <iostream>

namespace capl
{

long canSetChannelMode(long channel, long gtx, long gtxreq)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
