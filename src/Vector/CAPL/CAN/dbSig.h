#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * Signal definitions from the database
 */
class VECTOR_CAPL_EXPORT dbSig
{
public:
    dbSig();
    virtual ~dbSig();
};

typedef dbSig dbSignal;

}
