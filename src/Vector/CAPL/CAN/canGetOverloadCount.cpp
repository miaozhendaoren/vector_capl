#include "canGetOverloadCount.h"

#include <iostream>

namespace capl
{

long canGetOverloadCount(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
