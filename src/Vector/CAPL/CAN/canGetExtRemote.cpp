#include "canGetExtRemote.h"

#include <iostream>

namespace capl
{

long canGetExtRemote(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
