#include "ResetCanEx.h"

#include <iostream>

namespace capl
{

void ResetCanEx(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
