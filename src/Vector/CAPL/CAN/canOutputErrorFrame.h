#pragma once

#include "../DataTypes.h"
#include "CAN.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Outputs an Error Frame to the CAN bus.
 *
 * Outputs an Error Frame to the CAN bus. The number of dominant bits and the number of trailing recessive bits are given as arguments.
 *
 * @param msg
 *   Variable of type errorFrame.
 *
 * @param dominant
 *   Number of dominant bits.
 *
 * @param recessive
 *   Number of recessive bits.
 *
 * @return
 *   - 1: OK
 *   - 0: error, e.g. not supported by driver
 */
long VECTOR_CAPL_EXPORT canOutputErrorFrame(errorFrame msg, long dominant, long recessive);

}
