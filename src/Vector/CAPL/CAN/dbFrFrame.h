#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * FlexRay frame definitions from the database
 */
class VECTOR_CAPL_EXPORT dbFrFrame
{
public:
    dbFrFrame();
    virtual ~dbFrFrame();
};

}
