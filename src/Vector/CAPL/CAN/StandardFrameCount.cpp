#include "StandardFrameCount.h"

#include <iostream>

namespace capl
{

long StandardFrameCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
