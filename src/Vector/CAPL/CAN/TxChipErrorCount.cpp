#include "TxChipErrorCount.h"

#include <iostream>

namespace capl
{

long TxChipErrorCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
