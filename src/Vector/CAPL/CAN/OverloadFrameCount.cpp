#include "OverloadFrameCount.h"

#include <iostream>

namespace capl
{

long OverloadFrameCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
