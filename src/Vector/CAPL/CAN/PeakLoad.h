#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the peakload of a channel.
 *
 * Returns the peakload of a channel.
 *
 * @return
 *   Peakload of a channel in percent.
 */
long VECTOR_CAPL_EXPORT PeakLoad(void);

}
