#include "canOutputErrorFrame.h"

#include <iostream>

namespace capl
{

long canOutputErrorFrame(errorFrame msg, long dominant, long recessive)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
