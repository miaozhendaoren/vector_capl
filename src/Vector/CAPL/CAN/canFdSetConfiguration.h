#pragma once

#include "../DataTypes.h"
#include "CANSettings.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   The CAN controller parameters for arbitration and data phase can be set.
 *
 * The CAN controller parameters for arbitration and data phase can be set.
 *
 * canFdSetConfiguration performs an automatic reset of the CAN controller.
 *
 * @param channel
 *   The CAN channel.
 *
 * @param abrSettings
 *   Arbitration baudrate settings
 *
 * @param dbrSettings
 *   Data phase baudrate settings
 *
 * @return
 *   - 1 = success
 *   - 0 = error
 */
long VECTOR_CAPL_EXPORT canFdSetConfiguration(int channel, CANSettings abrSettings, CANSettings dbrSettings);

}
