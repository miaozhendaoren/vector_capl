#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Sends received messages through to CANoe or CANalyzer via an acceptance filter.
 *
 * Via an acceptance filter the CAN controllers control which received messages are sent
 * through to CANoe or CANalyzer.
 * Some controller chips, such as the SJA 1000, expect partitioning into acceptance mask
 * and acceptance code.
 *
 * @param channel
 *   CAN channel
 *
 * @param code
 *   Acceptance code for ID filtering
 *
 * @param mask
 *   Acceptance mask for ID filtering
 *
 * @return
 *   - 0: ok
 *   - !=0: error
 */
long VECTOR_CAPL_EXPORT canSetChannelAcc(long channel, dword code, dword mask);

}
