#include "canGetBusLoad.h"

#include <iostream>

namespace capl
{

long canGetBusLoad(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
