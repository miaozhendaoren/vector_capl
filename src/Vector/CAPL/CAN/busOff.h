#pragma once

#include "../DataTypes.h"
#include <vector>

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 */
class VECTOR_CAPL_EXPORT busOff
{
public:
    busOff();
    virtual ~busOff();

    /**
     * Receive error counter
     */
    unsigned int errorCountRX;

    /**
     * Transmit error counter
     */
    unsigned int errorCountTX;

    /**
     * Assign the channel
     */
    unsigned int can;
};

extern std::vector<void (*)(class busOff)> on_busOff;

}
