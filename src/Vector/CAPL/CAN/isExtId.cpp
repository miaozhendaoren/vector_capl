#include "isExtId.h"

#include <iostream>

namespace capl
{

long isExtId(dword id)
{
    return (id & (1 << 31)) != 0;
}

long isExtId(message m)
{
    return (m.ID & (1 << 31)) != 0;
}

}
