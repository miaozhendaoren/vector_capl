#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

class VECTOR_CAPL_EXPORT dbNetwork
{
public:
    dbNetwork();
    virtual ~dbNetwork();
};

}
