#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current rate of extended remote CAN messages on a channel.
 *
 * Returns the number of extended remote CAN messages on a channel since start of
 * measurement.
 *
 * @return
 *   Number of extended remote CAN messages on a channel since start of measurement.
 */
long VECTOR_CAPL_EXPORT ExtendedRemoteFrameCount(void);

}
