#pragma once

#include "../DataTypes.h"
#include "CAN.h"
#include "../FlexRay/FlexRay.h"
#include "../J1939/J1939.h"
#include "../J1587/J1587.h"
#include "../LIN/LIN.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 1)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param msg
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(message msg);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 2)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param msg
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(multiplexed_message msg);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 3)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param frame
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(FRFrame frame);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 4)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param frame
 *   Objects where the signals shall be set.
 *
 * @param uninitializedData
 *   Value to which the bytes in the frame / PDU shall be set which are not used by signals.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(FRFrame frame, byte uninitializedData);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 5)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param pdu
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(FrPDU pdu);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 6)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param pdu
 *   Objects where the signals shall be set.
 *
 * @param uninitializedData
 *   Value to which the bytes in the frame / PDU shall be set which are not used by signals.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(FrPDU pdu, byte uninitializedData);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 7)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param paramGroup
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(pg paramGroup);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 8)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param parameter
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(J1587Param parameter);

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the values of the signals in the parameter to the start values defined in the database. (form 9)
 *
 * Sets the values of the signals in the parameter to the start values defined in the
 * database.
 *
 * @param msg
 *   Objects where the signals shall be set.
 *
 * @return
 *   - 0: function was successful
 *   - 1: message / frame / PDU / paramGroup wasn't found in the database
 *   - 2: at least one signal start value didn't fit into the signal in the message
 */
long VECTOR_CAPL_EXPORT setSignalStartValues(linMessage msg);

}
