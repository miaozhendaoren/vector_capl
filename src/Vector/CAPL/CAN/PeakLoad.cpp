#include "PeakLoad.h"

#include <iostream>

namespace capl
{

long PeakLoad(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
