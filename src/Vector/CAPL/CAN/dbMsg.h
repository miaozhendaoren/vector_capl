#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * Message definitions from the database
 */
class VECTOR_CAPL_EXPORT dbMsg
{
public:
    dbMsg();
    virtual ~dbMsg();
};

}
