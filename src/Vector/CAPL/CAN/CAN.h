#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

/**
 * @defgroup CAN CAN CAPL Functions
 */

/* Objects / Selectors / Event Procedures */
#include "message.h" // on message
#include "errorFrame.h" // on errorFrame
#include "errorActive.h" // on errorActive
#include "errorPassive.h" // on errorPassive
#include "warningLimit.h" // on warningLimit
#include "busOff.h" // on busOff
#include "multiplexed_message.h"

/* General Functions */
#include "canGetDataLength.h"
#include "canOutputErrorFrame.h"
#include "isStdId.h"
#include "isExtId.h"
#include "mkExtId.h"
#include "output.h"
#include "valOfId.h"

/* CANdb API */
#include "GetFirstCANdbName.h"
#include "GetMessageAttrInt.h"
#include "GetMessageID.h"
#include "GetMessageName.h"
#include "GetNextCANdbName.h"
#include "setSignalStartValues.h"

/* Hardware API */
#include "canFdSetConfiguration.h"
#include "canGetConfiguration.h"
#include "canSetChannelAcc.h"
#include "canSetChannelMode.h"
#include "canSetChannelOutput.h"
#include "canSetConfiguration.h"
#include "getCardType.h"
#include "getCardTypeEx.h"
#include "resetCan.h"
#include "ResetCanEx.h"
#include "ScanBaudrateActive.h"
#include "ScanBaudratePassive.h"
#include "setBtr.h"
#include "setCanCabsMode.h"
#include "setOcr.h"

/* Statistics API */
#include "canResetStatistics.h"
#include "BusLoad.h"
#include "ChipState.h"
#include "ErrorFrameCount.h"
#include "ErrorFrameRate.h"
#include "ExtendedFrameCount.h"
#include "ExtendedFrameRate.h"
#include "ExtendedRemoteFrameCount.h"
#include "ExtendedRemoteFrameRate.h"
#include "OverloadFrameCount.h"
#include "OverloadFrameRate.h"
#include "PeakLoad.h"
#include "RxChipErrorCount.h"
#include "StandardFrameCount.h"
#include "StandardFrameRate.h"
#include "StandardRemoteFrameCount.h"
#include "StandardRemoteFrameRate.h"
#include "TxChipErrorCount.h"

/* Obsolete Functions */
#include "canGetBusLoad.h"
#include "canGetChipState.h"
#include "canGetErrorCount.h"
#include "canGetErrorRate.h"
#include "canGetExtData.h"
#include "canGetExtDataRate.h"
#include "canGetExtRemote.h"
#include "canGetExtRemoteRate.h"
#include "canGetOverloadCount.h"
#include "canGetOverloadRate.h"
#include "canGetPeakLoad.h"
#include "canGetRxErrorCount.h"
#include "canGetStdData.h"
#include "canGetStdDataRate.h"
#include "canGetStdRemote.h"
#include "canGetStdRemoteRate.h"
#include "canGetTxErrorCount.h"

/* Undocumented */
#include "signal.h"
#include "dbNetwork.h"
#include "dbNode.h"
#include "dbFrFrame.h"
#include "dbMsg.h"
#include "dbSig.h"

namespace capl {

/**
 * @ingroup CAN
 *
 * @brief Class: CAN
 *
 * The CAN objects (CAN1, CAN2, ...) provide access to channel specific statistics.
 *
 * @note
 *   The methods are linked to the description of the corresponding CAPL function.
 */
class VECTOR_CAPL_EXPORT CAN
{
public:
    CAN();
    virtual ~CAN();

    /**
     * @brief
     *   Returns the current busload of a channel.
     *
     * Returns the current busload of a channel.
     *
     * @return
     *   Current busload of channel in percent.
     */
    long BusLoad(void);

    /**
     * @brief
     *   Returns the current chip state of the CAN controller.
     *
     * Returns the current chip state of the CAN controller.
     *
     * @return
     *   Chip state of the CAN controller. See the following table for a description of the return
     *   values.
     *   - 0: Value not available
     *   - 1: Simulated
     *   - 2: Active
     *   - 3: Error Active
     *   - 4: Warning Level
     *   - 5: Error Passive
     *   - 6: Bus Off
     */
    long ChipState(void);

    /**
     * @brief
     *   Returns the number of error frames on a channel since start of measurement.
     *
     * Returns the number of error frames on a channel since start of measurement.
     *
     * @return
     *   Number of error frames on a channel since start of measurement.
     */
    long ErrorFrameCount(void);

    /**
     * @brief
     *   Returns the current rate of CAN error messages of a channel.
     *
     * Returns the current rate of CAN error messages of a channel.
     *
     * @return
     *   Current rate of CAN error messages on a channel in messages per second.
     */
    long ErrorFrameRate(void);

    /**
     * @brief
     *   Returns the number of extended CAN frames on a channel since start of measurement.
     *
     * Returns the number of extended CAN frames on a channel since start of measurement.
     *
     * @return
     *   Number of extended CAN frames on a channel since start of measurement
     */
    long ExtendedFrameCount(void);

    /**
     * @brief
     *   Returns the current rate of extended CAN frames on a channel.
     *
     * Returns the current rate of extended CAN frames on a channel.
     *
     * @return
     *   Current rate of extended CAN frames on a channel in messages per second.
     */
    long ExtendedFrameRate(void);

    /**
     * @brief
     *   Returns the current rate of extended remote CAN messages on a channel.
     *
     * Returns the number of extended remote CAN messages on a channel since start of
     * measurement.
     *
     * @return
     *   Number of extended remote CAN messages on a channel since start of measurement.
     */
    long ExtendedRemoteFrameCount(void);

    /**
     * @brief
     *   Returns the number of extended remote CAN messages on a channel since start of measurement.
     *
     * Returns the number of extended remote CAN messages on a channel since start of measurement.
     *
     * @return
     *   Current rate of extended remote CAN messages on a channel in frames per second.
     */
    long ExtendedRemoteFrameRate(void);

    /**
     * @brief
     *   Returns the number of CAN overload frames on a channel since start of measurement.
     *
     * Returns the number of CAN overload frames on a channel since start of measurement.
     *
     * @return
     *   Number of CAN overload frames on a channel since start of measurement.
     */
    long OverloadFrameCount(void);

    /**
     * @brief
     *   Returns the current rate of CAN overload frames on a channel.
     *
     * Returns the current rate of CAN overload frames on a channel.
     *
     * @return
     *   Current rate of CAN overload frames on a channel in messages per second.
     */
    long OverloadFrameRate(void);

    /**
     * @brief
     *   Returns the peakload of a channel.
     *
     * Returns the peakload of a channel.
     *
     * @return
     *   Peakload of a channel in percent.
     */
    long PeakLoad(void);

    /**
     * @brief
     *   Returns the current Rx error count in the receiver of a channel.
     *
     * Returns the current Rx error count in the receiver of a channel.
     *
     * @return
     *   Current error count in the receiver of a channel.
     */
    long RxChipErrorCount(void);

    /**
     * @brief
     *   Returns the number of standard CAN frames on a channel since start of measurement.
     *
     * Returns the number of standard CAN frames on a channel since start of measurement.
     *
     * @return
     *   Number of standard CAN frames on a channel since start of measurement.
     */
    long StandardFrameCount(void);

    /**
     * @brief
     *   Returns the current rate of standard CAN frames on a channel.
     *
     * Returns the current rate of standard CAN frames on a channel.
     *
     * @return
     *   Current rate of standard CAN frames on a channel in messages per second.
     */
    long StandardFrameRate(void);

    /**
     * @brief
     *   Returns the number of standard remote CAN frames on a channel since start of measurement.
     *
     * Returns the number of standard remote CAN frames on a channel since start of measurement.
     *
     * @return
     *   Number of standard remote CAN frames on a channel since start of measurement.
     */
    long StandardRemoteFrameCount(void);

    /**
     * @brief
     *   Returns the current rate of standard CAN frames on a channel.
     *
     * Returns the current rate of standard CAN frames on a channel.
     *
     * @return
     *   Current rate of standard remote CAN frames of a channel in messages per second.
     */
    long StandardRemoteFrameRate(void);

    /**
     * @brief
     *   Returns the current number of Tx errors in the CAN receiver of a channel.
     *
     * Returns the current number of Tx errors in the CAN receiver of a channel.
     *
     * @return
     *   Current number of errors in the CAN receiver of a channel.
     */
    long TxChipErrorCount(void);
};

}
