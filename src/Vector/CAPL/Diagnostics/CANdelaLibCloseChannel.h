#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Closes the communication channel to the control unit.
 *
 * Closes the communication channel to the control unit,
 * thereby terminating the sending of 'Tester Present' on the selected diagnostic channel
 * for the selected diagnostic descriptions.
 *
 * If necessary in the context of sending requests from the Diagnostic Console,
 * the Fault Memory window and diagnostic test modules
 * which do not implement a dedicated callback interface for the transport protocol layer,
 * the channel can be restored automatically.
 *
 * This function can be used, for example, to ensure
 * that a 'Tester Present' sent cyclically does not prevent the bus switching to sleep mode
 * in the context of network management when the Diagnostic Console is in use.
 *
 * @param ECUqualifier
 *   Qualifier associated with the diagnostic description
 *   whose channel is to be closed and/or for which
 *   no more 'Tester Present' requests are to be sent.
 */
void CANdelaLibCloseChannel(char * ECUqualifier);

}
