#include "DiagGetRespPrimitiveByte.h"

#include <iostream>

namespace capl
{

long DiagGetRespPrimitiveByte(DiagRequest request, dword bytePos)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
