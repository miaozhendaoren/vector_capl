#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Retrieves the qualifier of the variant that has been identified by the last successful run of the automatic variant identification algorithm for given target, or the current target if no target is given.
 *
 * Retrieve the qualifier of the variant that has been identified by the last successful run of the automatic variant identification algorithm for given target, or the current target if no target is given.
 * The function can also be used to determine whether the algorithm has finished.
 *
 * @param identifiedVariantOut
 *   Buffer for the qualifier
 *
 * @param bufferLen
 *   Length of the buffer
 *
 * @return
 *   - >=0: Number of characters written into the buffer
 *   - <0: Error code
 *   Especially:
 *   - -99: The algorithm is still running, try again later
 *   - -98: No variant was identified, e.g. the algorithm was not started or failed
 */
long DiagGetIdentifiedVariant(char * identifiedVariantOut, dword bufferLen);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Retrieves the qualifier of the variant that has been identified by the last successful run of the automatic variant identification algorithm for given target, or the current target if no target is given.
 *
 * Retrieve the qualifier of the variant that has been identified by the last successful run of the automatic variant identification algorithm for given target, or the current target if no target is given.
 * The function can also be used to determine whether the algorithm has finished.
 *
 * @param ecuQualifier
 *   ECU the identification algorithm should run for, not the currently selected one. If given, DiagSetTarget does not have to be called.
 *
 * @param identifiedVariantOut
 *   Buffer for the qualifier
 *
 * @param bufferLen
 *   Length of the buffer
 *
 * @return
 *   - >=0: Number of characters written into the buffer
 *   - <0: Error code
 *   Especially:
 *   - -99: The algorithm is still running, try again later
 *   - -98: No variant was identified, e.g. the algorithm was not started or failed
 */
long DiagGetIdentifiedVariant(char * ecuQualifier, char * identifiedVariantOut, dword bufferLen);

}
