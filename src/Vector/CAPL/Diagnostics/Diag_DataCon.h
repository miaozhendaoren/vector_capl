#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Tells the diagnostic layer, via the count parameter, how many bytes of data were transmitted successfully.
 *
 * Tells the diagnostic layer, via the count parameter, how many bytes of data
 * were transmitted successfully.
 *
 * This function is typically called within a transport layer callback. Once the
 * diagnostic layer has initiated the transmission of a message via
 * _Diag_DataRequest() and the transport layer has sent this message in its
 * entirety, using several message segments if needed, the diagnostic layer's
 * transport layer uses this function to indicate that the message was sent
 * successfully.
 *
 * @param count
 *   Number of bytes transmitted successfully.
 */
void Diag_DataCon(long count);

}
