#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns != 0 if the parameter in the object has its default value.
 *
 *
 * @todo
 */

}
