#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Retrieves the qualifier of the variant that is configured for the current or given target in the Diagnostics configuration dialog.
 *
 * Retrieves the qualifier of the variant that is configured for the current target.
 *
 * Retrieve the qualifier of the variant that is configured for the current or given target in the Diagnostics configuration dialog.
 * It is possible to provide this qualifier to the automatic variant identification algorithm.
 *
 * @param configuredVariantOut
 *   Buffer for the qualifier
 *
 * @param bufferLen
 *   Length of the buffer
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 *   Especially:
 *   - -94: No target was selected
 */
long DiagGetConfiguredVariant(char * configuredVariantOut, dword bufferLen);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Retrieves the qualifier of the variant that is configured for the current or given target in the Diagnostics configuration dialog.
 *
 * Retrieves the qualifier of the variant that is configured for the current target.
 *
 * Retrieve the qualifier of the variant that is configured for the current or given target in the Diagnostics configuration dialog.
 * It is possible to provide this qualifier to the automatic variant identification algorithm.
 *
 * @param ecuQualifier
 *   ECU the identification algorithm should run for, not the currently selected one. If given, DiagSetTarget does not have to be called.
 *
 * @param configuredVariantOut
 *   Buffer for the qualifier
 *
 * @param bufferLen
 *   Length of the buffer
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 *   Especially:
 *   - -94: No target was selected
 */
long DiagGetConfiguredVariant(char * ecuQualifier, char * configuredVariantOut, dword bufferLen);

}
