#include "Diag_DataCon.h"

#include <iostream>

namespace capl
{

void Diag_DataCon(long count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
