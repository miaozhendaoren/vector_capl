#include "DiagGetCommParameter.h"

#include <iostream>

namespace capl
{

long DiagGetCommParameter(const char * qualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetCommParameter(const char * qualifier, dword index, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
