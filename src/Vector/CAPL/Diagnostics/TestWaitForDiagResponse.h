#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits for arrival of the response to the given request.
 *
 * Waits for the arrival of the response to the given request.
 *
 * The function will return immediately after a positive or negative reponse - other than "responsePending" - was received within the configured protocol (P2/P2*) timings.
 *
 * Intermediate "responsePending" NRCs from the target ECU will automatically prolong the wait timer of the tester in P2* increments until maximally <timeout> [ms].
 * If by then no response has been received from the ECU target, the function will return with value =0 (timeout reached).
 *
 * In case the tester node implements a "CAPL callback interface for diagnostics" (CCI), but has no further provisions to handle NRCs on its own right, the behaviour is equivalent to the one described above (automated "responsePending" handling).
 *
 * If the tester implements a CCI and handles "responsePending" messages by itself - calling DiagSetP2Extended(-1) beforehand on the CCI - then this function will also treat "reponsePending" as a regular negative reponse code and will return immediately after having received such a negative response.
 *
 * @param request
 *   Sent request
 *
 * @param timeout
 *   Maximum wait time [ms]
 *
 * @return
 *   - <0: An internal error occurred, e.g. a protocol error or a faulty configuration of the diagnostic layer.
 *   - 0: The timeout was reached, i.e. the event of interest did not occur within the specified time.
 *   - 1: The event occurred.
 */
long TestWaitForDiagResponse(DiagRequest request, dword timeout);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits for arrival of the response to the given request.
 *
 * Waits for the arrival of the response to the given request.
 *
 * The function will return immediately after a positive or negative reponse - other than "responsePending" - was received within the configured protocol (P2/P2*) timings.
 *
 * Intermediate "responsePending" NRCs from the target ECU will automatically prolong the wait timer of the tester in P2* increments until maximally <timeout> [ms].
 * If by then no response has been received from the ECU target, the function will return with value =0 (timeout reached).
 *
 * In case the tester node implements a "CAPL callback interface for diagnostics" (CCI), but has no further provisions to handle NRCs on its own right, the behaviour is equivalent to the one described above (automated "responsePending" handling).
 *
 * If the tester implements a CCI and handles "responsePending" messages by itself - calling DiagSetP2Extended(-1) beforehand on the CCI - then this function will also treat "reponsePending" as a regular negative reponse code and will return immediately after having received such a negative response.
 *
 * @param timeout
 *   Maximum wait time [ms]
 *
 * @return
 *   - <0: An internal error occurred, e.g. a protocol error or a faulty configuration of the diagnostic layer.
 *   - 0: The timeout was reached, i.e. the event of interest did not occur within the specified time.
 *   - 1: The event occurred.
 */
long TestWaitForDiagResponse(dword timeout);


}
