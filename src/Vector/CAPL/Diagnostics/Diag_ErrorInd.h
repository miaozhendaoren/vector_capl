#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reports errors to the diagnostic layer.
 *
 * Reports errors to the diagnostic layer.
 *
 * This function is typically called within a transport layer callback. The
 * transport layer uses it to report errors to the diagnostic layer.
 *
 * If, for example, TestWaitForDiagResponse() is used in a test module to wait
 * for receipt of a response and Diag_ErrorInd() reports an error, the test
 * function returns an error and stops waiting.
 *
 * @param error
 *   Error number.
 *   This number is valid only in the context of the concrete transport
 *   protocol implementation used. It is recommended to forward error numbers
 *   reported by the protocol layer. For example, the OSEKTL API for the ISO
 *   TP on CAN implementation found in OSEK_TP.DLL reports errors in the
 *   callback function OSEKTL_ErrorInd.
 *   The error number reported here can simply be forwarded to the diagnostic
 *   layer.
 */
void Diag_ErrorInd(long error);

}
