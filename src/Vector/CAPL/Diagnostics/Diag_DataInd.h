#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Passes count bytes of the transmitter address sender from the rxBuffer buffer to the diagnostic layer.
 *
 * Passes count bytes of the transmitter address sender from the rxBuffer to the
 * diagnostic layer.
 *
 * This function is typically called in a transport layer callback after a
 * message, which may be segmented, has been received in its entirety. The
 * transport layer removed all protocol information (segmentation, flow control,
 * etc.) and forwards only the payload data (e.g. the ECU's diagnostic response
 * starting at the service ID and including all response parameters) to the
 * diagnostic layer.
 *
 * @param rxBuffer
 *   Byte buffer to be passed to the diagnostic layer.
 *
 * @param sender
 *   Address of the transmitter from which the diagnostic message was received.
 *
 * @param count
 *   Number of bytes to be passed.
 */
void Diag_DataInd(byte * rxBuffer, long count, long sender);

}
