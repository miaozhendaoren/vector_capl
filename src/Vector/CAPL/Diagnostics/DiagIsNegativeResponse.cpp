#include "DiagIsNegativeResponse.h"

#include <iostream>

namespace capl
{

long DiagIsNegativeResponse(DiagResponse obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
