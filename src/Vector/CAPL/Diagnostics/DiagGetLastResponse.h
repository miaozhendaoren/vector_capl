#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Saves the last response received (for the specified request) in the output object.
 *
 * Saves the last response received (for the specified request) in the output object.
 *
 * @brief req
 *   Request
 *
 * @brief respOut
 *   Output object for the received response
 *
 * @brief
 *   Error code
 */
long DiagGetLastResponse(DiagRequest req, DiagResponse respOut);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Saves the last response received (for the specified request) in the output object.
 *
 * Saves the last response received (for the specified request) in the output object.
 *
 * @brief respOut
 *   Output object for the received response
 *
 * @brief
 *   Error code
 */
long DiagGetLastResponse(DiagResponse respOut);

}
