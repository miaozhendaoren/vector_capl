#include "DiagGetPrimitiveData.h"

#include <iostream>

namespace capl
{

long DiagGetPrimitiveData(DiagResponse obj, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetPrimitiveData(DiagRequest obj, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagSetPrimitiveData(DiagResponse obj, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagSetPrimitiveData(DiagRequest obj, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
