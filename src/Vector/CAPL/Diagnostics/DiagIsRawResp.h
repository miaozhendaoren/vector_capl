#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns if the response stored for the request is stored as raw data or can be interpreted.
 *
 * Returns if the response stored for the request is stored as raw data or can be interpreted.
 *
 * @param request
 *   Request
 *
 * @return
 *   - 1: Response is stored as raw data.
 *   - 0: Response can be interpreted.
 *   - <0: Error code
 */
long DiagIsRawResp(DiagRequest request);

}
