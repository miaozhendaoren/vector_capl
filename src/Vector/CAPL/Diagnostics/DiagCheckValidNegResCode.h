#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given negative response code is defined for the object.
 *
 * The functions return 1 if the given negative response code is defined for the object.
 * It returns 0 if the code is not valid, and <0 for an error.
 *
 * In the one-argument form, the response object has to be a negative response.
 *
 * @param obj
 *   Diagnostics object to check the response code for.
 *
 * @param negResCode
 *   Negative response code like 0x11, "serviceNotSupported" (KWP 2000).
 *
 * @return
 *   - 1: Code is defined for the object in the CDD.
 *   - 0: Code is not defined for the object in the CDD.
 *   - Error code
 */
long DiagCheckValidNegResCode(DiagRequest obj, dword negResCode);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given negative response code is defined for the object.
 *
 * The functions return 1 if the given negative response code is defined for the object.
 * It returns 0 if the code is not valid, and <0 for an error.
 *
 * In the one-argument form, the response object has to be a negative response.
 *
 * @param obj
 *   Diagnostics object to check the response code for.
 *
 * @param negResCode
 *   Negative response code like 0x11, "serviceNotSupported" (KWP 2000).
 *
 * @return
 *   - 1: Code is defined for the object in the CDD.
 *   - 0: Code is not defined for the object in the CDD.
 *   - Error code
 */
long DiagCheckValidNegResCode(DiagResponse obj, dword negResCode);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given negative response code is defined for the object.
 *
 * The functions return 1 if the given negative response code is defined for the object.
 * It returns 0 if the code is not valid, and <0 for an error.
 *
 * In the one-argument form, the response object has to be a negative response.
 *
 * @param obj
 *   Diagnostics object to check the response code for.
 *
 * @return
 *   - 1: Code is defined for the object in the CDD.
 *   - 0: Code is not defined for the object in the CDD.
 *   - Error code
 */
long DiagCheckValidNegResCode(DiagResponse obj);

}
