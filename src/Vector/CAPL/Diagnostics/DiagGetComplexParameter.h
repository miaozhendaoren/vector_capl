#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   The behavior of this CAPL function depends on the used parameters.
 *     - Retrieving numeric sub-parameter from a parameter iteration.
 *     - Returns the symbolic value of a complex parameter.
 *
 * @todo
 */

}
