/**
 * This class represents a diagnostic request.
 */

#pragma once

#include "../DataTypes.h"
#include "DiagResponse.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @todo
 */
class DiagRequest
{
public:
    long CheckValidNegResCode(dword negResCode);
    long CheckValidPrimitive(void);
    long CheckValidPrimitive(dword * reasonOut);
    long CheckValidRespPrimitive(void);
    long CheckValidRespPrimitive(dword * reasonOut);
    long CheckValidRespPrimitive(char * primitiveQualifier);
    long CheckValidRespPrimitive(char * primitiveQualifier, dword * reasonOut);
    long GetComplexParameter(char * parameterName, dword iteration, char * subParameter, double * output);
    double GetComplexParameter(char * parameterName, dword iteration, char * subParameter);
    long GetComplexParameter(long mode, char * parameterName, dword iteration, char * subParameter, double * output);
    double GetComplexParameter(long mode, char * parameterName, dword iteration, char * subParameter);
    long GetComplexParameter(char * parameterName, dword iteration, char * subParameter, char * buffer, dword buffersize);
    //double GetComplexParameter(char * parameterName, dword iteration, char * subParameter);
    long GetComplexParameterRaw(char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize);
    long SetComplexParameterRaw(char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize);
    long GetComplexRespParameter(char * parameterName, dword iteration, char * subParameter, double * output);
    long GetComplexRespParameter(long mode, char * parameterName, dword iteration, char * subParameter, double * output);
    long GetComplexRespParameter(char * parameterName, dword iteration, char * subParameter, byte * buffer, dword bufferLen);
    double GetComplexRespParameter(char * parameterName, dword iteration, char * subParameter);
    double GetComplexRespParameter(long mode, char * parameterName, dword iteration, char * subParameter);
    long GetComplexRespParameterRaw(char * parameterName, dword iteration, char * subParameter, byte * buffer, dword bufferLen);
    long GetLastResponse(DiagResponse * respOut);
    long GetLastResponseCode(void);
    long GetObjectName(char * nameBufferOut, dword nameBufferLen);
    long GetObjectPath(char * buffer, dword buffersize);
    long GetParameter(char * parameterName, double * output);
    double GetParameter(char * parameterName);
    long GetParameter(long mode, char * parameterName, double * output);
    double GetParameter(long mode, char * parameterName);
    long GetParameter(char * parameterName, char * buffer, dword buffersize);
    //double GetParameter(char * parameterName);
    long GetParameterName(dword paramNo, char * buffer, dword buffersize);
    long GetParameterPath(dword paramNo, char * buffer, dword bufferSize);
    long GetRespParameterPath(dword paramNo, char * buffer, dword bufferSize);
    long SetParameterRaw(char * parameterName, byte * buffer, dword buffersize);
    long GetParameterRaw(char * parameterName, byte * buffer, dword buffersize);
    long GetParameterSize(char * parameterName);
    long GetParameterType(char * qualifier, char * buffer, dword bufferSize);
    long GetRespParameterType(char * qualifier, char * buffer, dword bufferSize);
    long GetParameterUnit(char * parameterName, char * buffer, dword buffersize);
    long GetPrimitiveByte(dword bytePos);
    long GetPrimitiveData(byte * buffer, dword buffersize);
    long SetPrimitiveData(byte * buffer, dword buffersize);
    long GetPrimitiveSize(void);
    long GetRespParameter(char * parameterName, double * output);
    long GetRespParameter(long mode, char * parameterName, double * output);
    long GetRespParameter(char * parameterName, char * buffer, dword bufferLen);
    double GetRespParameter(char * parameterName);
    double GetRespParameter(long mode, char * parameterName);
    long GetRespParameterRaw(char * parameterName, byte * buffer, dword bufferLen);
    long GetRespPrimitiveByte(dword bytePos);
    long GetRespPrimitiveSize(void);
    long GetSuppressResp(void);
    long SetSuppressResp(long flag);
    long IsParameterConstant(char * qualifier);
    long IsRespParameterConstant(char * qualifier);
    long IsParameterDefault(char * parameterName);
    long IsRaw(void);
    long IsRawResp(void);
    long ResetParameter(char * parameterName);
    long Resize(void);
    long Resize(dword length);
    // long Resize(dword byteCount);
    long SendMarked(void);
    long SendNegativeResponse(dword code);
    long SendNetwork(void);
    long SendRequest(void);
    long SetComplexParameter(char * parameterName, dword iteration, char * subParameter, double numValue);
    long SetComplexParameter(char * parameterName, dword iteration, char * subParameter, char * symbValue);
    long SetComplexParameter(long mode, char * parameterName, dword iteration, char * subParameter, char * valueIn);
    long SetParameter(char * parameterName, double newValue);
    long SetParameter(long mode, char * parameterName, double newValue);
    long SetParameter(char * parameterName, char * newValue);
    long SetPrimitiveByte(dword bytePos, dword newValue);
    long SetRespPrimitiveByte(dword bytePos, dword newValue);
    long SetTimeoutHandler(char * callbackName);
};

}
