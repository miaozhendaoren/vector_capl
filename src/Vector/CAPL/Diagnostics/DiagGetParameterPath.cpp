#include "DiagGetParameterPath.h"

#include <iostream>

namespace capl
{

long DiagGetParameterPath(DiagResponse object, dword paramNo, char * buffer, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetParameterPath(DiagRequest object, dword paramNo, char * buffer, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
