#include "_Diag_ChannelStateChanged.h"

#include <iostream>

namespace capl
{

void _Diag_ChannelStateChanged(long newState)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
