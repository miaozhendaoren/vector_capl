#include "DiagInterpretRespAs.h"

#include <iostream>

namespace capl
{

long DiagInterpretRespAs(DiagRequest response, char * primitiveQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
