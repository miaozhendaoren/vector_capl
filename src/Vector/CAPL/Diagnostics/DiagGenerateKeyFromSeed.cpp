#include "DiagGenerateKeyFromSeed.h"

#include <iostream>

namespace capl
{

long DiagGenerateKeyFromSeed(byte * seedArray, dword seedArraySize, dword securityLevel, char * variant, char * ipOption, byte * keyArray, dword maxKeyArraySize, dword & keyActualSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
