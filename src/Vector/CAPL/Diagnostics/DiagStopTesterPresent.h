#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Stops sending autonomous/cyclical Tester Present requests from CANoe to the specified or current ECU.
 *
 * @todo
 */

}
