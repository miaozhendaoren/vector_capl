#include "TestReportWriteDiagObject.h"

#include <iostream>

namespace capl
{

long TestReportWriteDiagObject(DiagRequest req)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestReportWriteDiagObject(DiagResponse resp)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestReportWriteDiagResponse(DiagRequest req)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
