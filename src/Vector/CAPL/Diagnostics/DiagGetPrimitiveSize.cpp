#include "DiagGetPrimitiveSize.h"

#include <iostream>

namespace capl
{

long DiagGetPrimitiveSize(DiagRequest request)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetPrimitiveSize(DiagResponse response)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
