#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Indicates the start of a diagnostic data reception to the diagnostic layer.
 *
 * Indicated the start of a diagnostic data reception to the diagnostic layer.
 *
 * This function is typically called from a transport layer callback. For
 * example, the ISO TP protocol on CAN indicates the reception of a "First
 * Frame" to the application.
 *
 * In the diagnostic layer of a tester, a call to this function will stop the
 * timer started after the request has been sent, i.e. even a very long data
 * reception will not time out. If the tester has called
 * TestWaitForDiagResponseStart, that call will now be continued.
 *
 * @param source
 *   Identifies the sender node
 *
 * @param target
 *   Identifies the receiver
 *
 * @param length
 *   Number of bytes announced
 */
void Diag_FirstFrameInd(long source, long target, long length);

}
