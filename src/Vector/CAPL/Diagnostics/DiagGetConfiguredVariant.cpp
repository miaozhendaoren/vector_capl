#include "DiagGetConfiguredVariant.h"

#include <iostream>

namespace capl
{

long DiagGetConfiguredVariant(char * configuredVariantOut, dword bufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetConfiguredVariant(char * ecuQualifier, char * configuredVariantOut, dword bufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
