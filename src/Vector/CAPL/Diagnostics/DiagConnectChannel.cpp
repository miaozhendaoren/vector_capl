#include "DiagConnectChannel.h"

#include <iostream>

namespace capl
{

long DiagConnectChannel()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
