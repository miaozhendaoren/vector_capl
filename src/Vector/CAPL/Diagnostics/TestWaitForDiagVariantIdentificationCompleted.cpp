#include "TestWaitForDiagVariantIdentificationCompleted.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagVariantIdentificationCompleted()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestWaitForDiagVariantIdentificationCompleted(char * expectedVariant)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
