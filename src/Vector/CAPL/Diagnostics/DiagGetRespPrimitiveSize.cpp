#include "DiagGetRespPrimitiveSize.h"

#include <iostream>

namespace capl
{

long DiagGetRespPrimitiveSize(DiagRequest request)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
