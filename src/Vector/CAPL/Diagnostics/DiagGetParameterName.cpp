#include "DiagGetParameterName.h"

#include <iostream>

namespace capl
{

long DiagGetParameterName(DiagResponse obj, dword paramNo, char * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetParameterName(DiagRequest obj, dword paramNo, char * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
