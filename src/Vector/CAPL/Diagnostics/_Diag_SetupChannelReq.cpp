#include "_Diag_SetupChannelReq.h"

#include <iostream>

namespace capl
{

void _Diag_SetupChannelReq(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
