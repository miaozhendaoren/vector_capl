#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Determines the CAN ID mask in order to be able to filter out CAN messages sent by the diagnostic tester as functional requests.
 *
 * @todo
 */

}
