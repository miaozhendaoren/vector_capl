#include "Diag_ErrorInd.h"

#include <iostream>

namespace capl
{

void Diag_ErrorInd(long error)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
