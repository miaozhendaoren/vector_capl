#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sends diagnostic requests to the currently selected diagnostic target and writes the responses to the report file.
 *
 * Sends diagnostic requests to the currently selected diagnostic target and writes the responses to the report file.
 * It considers all requests below a diagnostic class that have constant parameters only.
 *
 * @param class_
 *   The qualifier of the diagnostic class.
 *
 * @return
 *   Error code
 */
long TestCollectDiagEcuInformation(char * class_);

}
