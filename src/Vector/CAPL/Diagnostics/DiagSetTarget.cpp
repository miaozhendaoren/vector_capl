#include "DiagSetTarget.h"

#include <iostream>

namespace capl
{

long DiagSetTarget(char * ecuName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
