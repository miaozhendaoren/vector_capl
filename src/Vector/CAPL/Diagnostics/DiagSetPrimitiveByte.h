#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes one byte of a diagnostic object.
 *
 * Writes one byte of a diagnostic object.
 *
 * @param request
 *   Request
 *
 * @param response
 *   Response
 *
 * @param bytePos
 *   Position of the byte in the object
 *
 * @param newValue
 *   New value of the accessed byte
 *
 * @return
 *   - >= 0: Requested value or "no error"
 *   - <0: Error code
 */
long DiagSetPrimitiveByte(DiagRequest request, dword bytePos, dword newValue);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes one byte of a diagnostic object.
 *
 * Writes one byte of a diagnostic object.
 *
 * @param request
 *   Request
 *
 * @param response
 *   Response
 *
 * @param bytePos
 *   Position of the byte in the object
 *
 * @param newValue
 *   New value of the accessed byte
 *
 * @return
 *   - >= 0: Requested value or "no error"
 *   - <0: Error code
 */
long DiagSetPrimitiveByte(DiagResponse response, dword bytePos, dword newValue);

}
