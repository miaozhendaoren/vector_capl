#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes one byte of the response stored for the request.
 *
 * Writes one byte of the response stored for the request.
 *
 * @param request
 *   Request
 *
 * @param bytePos
 *   Position of the byte in the object.
 *
 * @param newValue
 *   New value of the accessed byte.
 *
 * @return
 *   - >= 0: Requested value or "no error"
 *   - <0: Error code
 */
long DiagSetRespPrimitiveByte(DiagRequest request, dword bytePos, dword newValue);

}
