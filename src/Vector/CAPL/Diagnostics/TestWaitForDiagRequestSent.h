#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits until the previously sent request has been sent to the ECU.
 *
 * Waits until the previously sent request has been sent to the ECU.
 * This might be triggered by a call of the function Diag_DataCon() at the CAPL Callback Interface.
 *
 * @param request
 *   Sent request
 *
 * @param timeout
 *   Maximum wait time [ms]
 *
 * @return
 *   - <0: An internal error occurred, e.g. a faulty configuration of the Diagnostic Layer.
 *   - 0: The timeout was reached, i.e. the event of interest did not occur within the specified time.
 *   - 1: The event occurred.
 */
long TestWaitForDiagRequestSent(DiagRequest request, dword timeout);

}
