#include "DiagGetObjectName.h"

#include <iostream>

namespace capl
{

long DiagGetObjectName(DiagResponse obj, char * nameBufferOut, dword nameBufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetObjectName(DiagRequest obj, char * nameBufferOut, dword nameBufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
