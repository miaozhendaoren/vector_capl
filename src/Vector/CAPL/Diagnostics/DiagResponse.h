/**
 * This class represents a diagnostic response.
 */

#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @todo
 */
class DiagResponse
{
public:
    long CheckValidNegResCode(dword negResCode);
    long CheckValidNegResCode(void);
    long CheckValidPrimitive(void);
    long CheckValidPrimitive(dword * reasonOut);
    long CheckValidPrimitive(char * primitiveQualifier);
    long CheckValidPrimitive(char * primitiveQualifier, dword * reasonOut);
    long GetComplexParameter(char* parameterName, dword iteration, char * subParameter, double * output);
    double GetComplexParameter(char * parameterName, dword iteration, char * subParameter);
    long GetComplexParameter(long mode, char * parameterName, dword iteration, char * subParameter, double * output);
    double GetComplexParameter(long mode, char * parameterName, dword iteration, char * subParameter);
    long GetComplexParameter(char * parameterName, dword iteration, char * subParameter, char * buffer, dword buffersize);
    //double GetComplexParameter(char * parameterName, dword iteration, char * subParameter);
    long GetComplexParameterRaw(char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize);
    long SetComplexParameterRaw(char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize);
    long GetLastResponse(void);
    long GetObjectName(char * nameBufferOut, dword nameBufferLen);
    long GetObjectPath(char * buffer, dword buffersize);
    long GetParameter(char * parameterName, double * output);
    double GetParameter(char * parameterName);
    long GetParameter(long mode, char * parameterName, double * output);
    double GetParameter(long mode, char * parameterName);
    long GetParameter(char * parameterName, char * buffer, dword buffersize);
    //double GetParameter(char * parameterName);
    long GetParameterName(dword paramNo, char * buffer, dword buffersize);
    long GetParameterPath(dword paramNo, char * buffer, dword bufferSize);
    long SetParameterRaw(char * parameterName, byte * buffer, dword buffersize);
    long GetParameterRaw(char * parameterName, byte * buffer, dword buffersize);
    long GetParameterSize(char * parameterName);
    long GetParameterType(char * qualifier, char * buffer, dword bufferSize);
    long GetParameterUnit(char * parameterName, char * buffer, dword buffersize);
    long GetPrimitiveByte(dword bytePos);
    long GetPrimitiveData(byte * buffer, dword buffersize);
    long SetPrimitiveData(byte * buffer, dword buffersize);
    long GetPrimitiveSize(void);
    long GetResponseCode(void);
    long GetSendingMode(void);
    long IsNegativeResponse(void);
    long IsParameterConstant(char * qualifier);
    long IsParameterDefault(char * parameterName);
    long IsPositiveResponse(void);
    long IsRaw(void);
    long ResetParameter(char * parameterName);
    long Resize(void);
    long Resize(dword length);
    // long Resize(dword byteCount);
    long SendNegativeResponse(dword code);
    long SendPositiveResponse(void);
    long SendResponse(void);
    long SetComplexParameter(char * parameterName, dword iteration, char * subParameter, double numValue);
    long SetComplexParameter(char * parameterName, dword iteration, char * subParameter, char * symbValue);
    long SetComplexParameter(long mode, char * parameterName, dword iteration, char * subParameter, char * valueIn);
    long SetParameter(char * parameterName, double newValue);
    long SetParameter(long mode, char * parameterName, double newValue);
    long SetParameter(char * parameterName, char * newValue);
    long SetPrimitiveByte(dword bytePos, dword newValue);
};

}
