#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Specifies the value of a (complex) parameter directly via uncoded data bytes.
 *   Offers access to parameters contained in a received response object.
 *
 * @todo
 */

}
