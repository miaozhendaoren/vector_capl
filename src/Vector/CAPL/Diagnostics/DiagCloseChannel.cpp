#include "DiagCloseChannel.h"

#include <iostream>

namespace capl
{

long DiagCloseChannel()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
