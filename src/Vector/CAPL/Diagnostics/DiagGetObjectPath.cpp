#include "DiagGetObjectPath.h"

#include <iostream>

namespace capl
{

long DiagGetObjectPath(DiagResponse obj, char * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetObjectPath(DiagRequest obj, char * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
