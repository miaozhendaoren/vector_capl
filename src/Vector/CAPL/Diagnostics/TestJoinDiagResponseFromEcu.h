#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Adds an event to the set of joined events.
 *   Events: Arrival of a diagnostic response from a specific or any ECU.
 *
 * Adds an event of type diagnostic response reception to the set of joined events. This is a non-blocking function.
 *
 * @return
 *   - -3: Error while adding the specified event to the set of joined events
 *   - -1: General error, for example, functionality is not available
 *   - >0: Number of the newly joined event
 */
long TestJoinDiagResponseFromEcu();

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Adds an event to the set of joined events.
 *   Events: Arrival of a diagnostic response from a specific or any ECU.
 *
 * Adds an event of type diagnostic response reception to the set of joined events. This is a non-blocking function.
 *
 * @param ecuQualifier
 *   If given, only a response from the indicated ECU will fire the event.
 *
 * @return
 *   - -3: Error while adding the specified event to the set of joined events
 *   - -1: General error, for example, functionality is not available
 *   - >0: Number of the newly joined event
 */
long TestJoinDiagResponseFromEcu(char * ecuQualifier);

}
