#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Tries to unlock an ECU.
 *
 * This functions tries to unlock an ECU.
 * It requests a seed, calculates the key with the Seed & Key DLL and sends it to the ECU.
 * The Seed & Key DLL must be configured in the diagnostic configuration dialog.
 *
 * The message exchange can be monitored with on DiagRequest/on DiagResponse event handler.
 *
 * @param securityLevel
 *   The required security level.
 *
 * @return
 *   Error code
 */
long TestWaitForUnlockEcu(long securityLevel);

}
