#include "DiagInitialize.h"

#include <iostream>

namespace capl
{

long DiagInitialize(DiagResponse object, char * serviceQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagInitialize(DiagResponse object, char * serviceQualifier, char * primitiveQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagInitialize(DiagRequest object, char * serviceQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
