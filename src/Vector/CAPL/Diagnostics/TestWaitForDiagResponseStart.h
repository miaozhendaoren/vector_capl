#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits for the arrival of the response to a sent request, e.g. the so-called "First Frame" in ISO TP transmissions.
 *
 * Waits for the arrival of the response to a sent request, e.g. the so-called "First Frame" in ISO TP transmissions.
 * One way this function might be triggered is by Diag_FirstFrameInd() at the CAPL Callback Interface, but only if this has been implemented suitably.
 * When other protocols or interfaces are used this call might be omitted.
 * Then the function rolls back after the response has been fully received.
 *
 * @param request
 *   Sent request
 *
 * @param timeout
 *   Maximum wait time [ms]
 *
 * @return
 *   - <0: An internal error occurred, e.g. a faulty configuration of the Diagnostic Layer.
 *   - 0: The timeout was reached, i.e. the event of interest did not occur within the specified time.
 *   - 1: The event occurred.
 */
long TestWaitForDiagResponseStart(DiagRequest request, dword timeout);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits for the arrival of the response to a sent request, e.g. the so-called "First Frame" in ISO TP transmissions.
 *
 * Waits for the arrival of the response to a sent request, e.g. the so-called "First Frame" in ISO TP transmissions.
 * One way this function might be triggered is by Diag_FirstFrameInd() at the CAPL Callback Interface, but only if this has been implemented suitably.
 * When other protocols or interfaces are used this call might be omitted.
 * Then the function rolls back after the response has been fully received.
 *
 * @param timeout
 *   Maximum wait time [ms]
 *
 * @return
 *   - <0: An internal error occurred, e.g. a faulty configuration of the Diagnostic Layer.
 *   - 0: The timeout was reached, i.e. the event of interest did not occur within the specified time.
 *   - 1: The event occurred.
 */
long TestWaitForDiagResponseStart(dword timeout);

}
