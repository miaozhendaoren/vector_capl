#include "_Diag_DataRequest.h"

#include <iostream>

namespace capl
{

void _Diag_DataRequest(byte * data, dword count, long furtherSegments)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
