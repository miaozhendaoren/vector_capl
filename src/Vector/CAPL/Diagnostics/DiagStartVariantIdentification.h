#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Starts the automatic variant identification algorithm for the given target, or the currently selected one if none is given.
 *
 * Start the automatic variant identification algorithm for the given target, or the currently selected one if none is given.
 * The algorithm will communicate via the CANdelaLib channel, i.e. the CAPL callback functions are not used for the identification.
 *
 * A tester module may wait for the completion of the algorithm with TestWaitForDiagVariantIdentificationCompleted, or query the status of the identification with DiagGetIdentifiedVariant.
 *
 * @return
 *   - 0: No error, algorithm was started
 *   - <0: Error code, especially:
 *   - -98: No communication channel could be found for the target
 *   - -97: Automatic variant identification is not defined in the diagnostic description
 *   - -91: The variant identification algorithm is already running
 */
long DiagStartVariantIdentification();

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Starts the automatic variant identification algorithm for the given target, or the currently selected one if none is given.
 *
 * Start the automatic variant identification algorithm for the given target, or the currently selected one if none is given.
 * The algorithm will communicate via the CANdelaLib channel, i.e. the CAPL callback functions are not used for the identification.
 *
 * A tester module may wait for the completion of the algorithm with TestWaitForDiagVariantIdentificationCompleted, or query the status of the identification with DiagGetIdentifiedVariant.
 *
 * @param ecuQualifier
 *   ECU the identification algorithm should run for, not the currently selected one. If given, DiagSetTarget does not have to be called.
 *
 * @return
 *   - 0: No error, algorithm was started
 *   - <0: Error code, especially:
 *   - -98: No communication channel could be found for the target
 *   - -97: Automatic variant identification is not defined in the diagnostic description
 *   - -91: The variant identification algorithm is already running
 */
long DiagStartVariantIdentification(char * ecuQualifier);

}
