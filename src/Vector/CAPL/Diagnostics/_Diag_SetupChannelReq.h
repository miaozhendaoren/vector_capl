#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   With this function the CAPL program of a tester implementation will be requested to open a channel to the ECU.
 *
 * With this function the CAPL program of a tester implementation will be
 * requested to open a channel to the ECU. After opening the channel the CAPL
 * program can call the function Diag_SetupChannelCon.
 */
void _Diag_SetupChannelReq(void);

}
