#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Return the qualifier path of the parameter at the given position in the primitive.
 *
 * Return the qualifier path of the parameter at the given position in the primitive.
 *
 * @param object
 *   Diagnostics object
 *
 * @param paramNo
 *   Which parameter in the object, beginning with 0.
 *
 * @param buffer
 *   Input/output buffer
 *
 * @param bufferSize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetParameterPath(DiagResponse object, dword paramNo, char * buffer, dword bufferSize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Return the qualifier path of the parameter at the given position in the primitive.
 *
 * Return the qualifier path of the parameter at the given position in the primitive.
 *
 * @param object
 *   Diagnostics object
 *
 * @param paramNo
 *   Which parameter in the object, beginning with 0.
 *
 * @param buffer
 *   Input/output buffer
 *
 * @param bufferSize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetParameterPath(DiagRequest object, dword paramNo, char * buffer, dword bufferSize);

}
