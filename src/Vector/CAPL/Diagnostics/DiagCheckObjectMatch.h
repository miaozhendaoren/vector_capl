#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the response is a valid (positive or negative) response for the request.
 *
 * Checks if the response service id is a valid (positive or negative) response for the request.
 *
 * @param request
 *   Diagnostics request
 *
 * @param response
 *   Diagnostics response
 *
 * @return
 *   - 1: Objects match.
 *   - 0: Objects do not match.
 *   - Error code
 */
long DiagCheckObjectMatch(DiagRequest request, DiagResponse response);

}
