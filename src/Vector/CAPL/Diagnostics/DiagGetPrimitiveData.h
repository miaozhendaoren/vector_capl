#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads the raw data of the complete service primitive.
 *
 * Reads/sets the raw data of the complete service primitive (all data that is transmitted via the transport protocol).
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief buffer
 *   Input/output buffer
 *
 * @brief buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetPrimitiveData(DiagResponse obj, byte * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads the raw data of the complete service primitive.
 *
 * Reads/sets the raw data of the complete service primitive (all data that is transmitted via the transport protocol).
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief buffer
 *   Input/output buffer
 *
 * @brief buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetPrimitiveData(DiagRequest obj, byte * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads the raw data of the complete service primitive.
 *
 * Reads/sets the raw data of the complete service primitive (all data that is transmitted via the transport protocol).
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief buffer
 *   Input/output buffer
 *
 * @brief buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagSetPrimitiveData(DiagResponse obj, byte * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads the raw data of the complete service primitive.
 *
 * Reads/sets the raw data of the complete service primitive (all data that is transmitted via the transport protocol).
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief buffer
 *   Input/output buffer
 *
 * @brief buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagSetPrimitiveData(DiagRequest obj, byte * buffer, dword buffersize);

}
