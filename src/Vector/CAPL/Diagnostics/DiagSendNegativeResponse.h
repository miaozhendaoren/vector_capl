#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sends a negative response to the tester, whereby the specified value is transmitted as error code.
 *
 * @todo
 */

}
