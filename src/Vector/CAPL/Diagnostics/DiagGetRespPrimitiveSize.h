#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the byte length of the response stored for the request.
 *
 * Returns the byte length of the response stored for the request.
 *
 * @param request
 *   Request
 *
 * @return
 *   - >0: Number of bytes
 *   - <0: Error code
 */
long DiagGetRespPrimitiveSize(DiagRequest request);

}
