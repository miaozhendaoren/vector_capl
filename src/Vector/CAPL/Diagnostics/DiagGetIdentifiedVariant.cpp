#include "DiagGetIdentifiedVariant.h"

#include <iostream>

namespace capl
{

long DiagGetIdentifiedVariant(char * identifiedVariantOut, dword bufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetIdentifiedVariant(char * ecuQualifier, char * identifiedVariantOut, dword bufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
