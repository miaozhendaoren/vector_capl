#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sets the ECU with which the tester should communicate from now on. May not be called in an ECU simulation node.
 *
 * Sets the target ECU with which the tester shall communicate from now on and
 * configures/establishes the communication channel to that ECU.
 *
 * After calling the function in the tester, the following actions are
 * executed:
 * - From now on the tester will interpret and use all diagnostic objects in
 *   the CAPL code of this node as defined in the respective diagnostic
 *   description file.
 * - From now on the tester will use the SeedKey DLL defined for this target in
 *   the diagnostic configuration dialog when computing a security key with
 *   DiagGenerateKeyFromSeed().
 * - All diagnostic objects (requests/responses) defined in the CAPL code are
 *   re-initialized for the given target. If DiagSetTarget() was called for the
 *   same or a different target before, the request/response objects will no
 *   longer keep modifications made in a previous session with a previos target
 *   ECU.
 * - DiagSetTarget shall not be called in the "on prestart" handler of a test
 *   module, since it would be called only once when the measurement starts. It
 *   is recommended to call it in the "preparation" phase of an XML test
 *   module, or to make sure programmatically that it is called before any test
 *   case using diagnostics is executed.
 * - The communication layer is configured:
 *   - In case of a CAPL Callback interface (CCI) being used, the function
 *     _Diag_SetChannelParameters is called to configure the parameters of the
 *     underlaying TP nodelayer DLL.
 *   - If no CCI is implemented, a built-in diagnostic channel is configured
 *     according to the network type to which the diagnostic database is
 *     attached and according to the communication parameters of the selected
 *     diagnostic interface. For connection oriented transport protocols
 *     (like e.g. VW TP 2.0) furthermore not only the parameters of the
 *     connection are configured, but a communication channel is actually
 *     established between tester and ECU.
 *
 * @param ecuName
 *   Qualifier of the ECU as set in the diagnostic configuration dialog for
 *   the respective diagnostic description (CDD/ODX).
 *
 * @return
 *   Error code
 */
long DiagSetTarget(char * ecuName);

}
