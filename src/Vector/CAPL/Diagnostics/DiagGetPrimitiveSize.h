#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the byte length of the object.
 *
 * Returns the byte length of the object.
 *
 * @param request
 *   Request
 *
 * @param response
 *   Response
 *
 * @return
 *   - >0: Number of bytes
 *   - <0: Error code
 */
long DiagGetPrimitiveSize(DiagRequest request);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the byte length of the object.
 *
 * Returns the byte length of the object.
 *
 * @param request
 *   Request
 *
 * @param response
 *   Response
 *
 * @return
 *   - >0: Number of bytes
 *   - <0: Error code
 */
long DiagGetPrimitiveSize(DiagResponse response);

}
