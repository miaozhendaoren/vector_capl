#include "DiagGetComplexRespParameterRaw.h"

#include <iostream>

namespace capl
{

long DiagGetComplexRespParameterRaw(DiagRequest req, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword bufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
