#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sets one of the sub-parameters within a complex parameter to the specified (numeric or symbolic) value.
 *
 * @todo
 */

}
