#include "TestCollectDiagEcuInformation.h"

#include <iostream>

namespace capl
{

long TestCollectDiagEcuInformation(char * class_)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
