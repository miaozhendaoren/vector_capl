#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes the language dependent name of the diagnostic object into the buffer.
 *
 * Writes the language dependent name of the diagnostics object into the buffer. For ODX descriptions, this is the "long name".
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief nameBufferOut
 *   Output buffer
 *
 * @brief nameBufferLen
 *   Output buffer size
 *
 * @return
 *   Error code
 */
long DiagGetObjectName(DiagResponse obj, char * nameBufferOut, dword nameBufferLen);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes the language dependent name of the diagnostic object into the buffer.
 *
 * Writes the language dependent name of the diagnostics object into the buffer. For ODX descriptions, this is the "long name".
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief nameBufferOut
 *   Output buffer
 *
 * @brief nameBufferLen
 *   Output buffer size
 *
 * @return
 *   Error code
 */
long DiagGetObjectName(DiagRequest obj, char * nameBufferOut, dword nameBufferLen);


}
