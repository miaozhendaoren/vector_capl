#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   The behavior of this CAPL function depends on the used parameters.
 *     - Returns the value of the numeric parameter.
 *     - Returns the symbolic value of the parameter.
 *     - Retrieves numeric parameter directly.
 *
 * @todo
 */

}
