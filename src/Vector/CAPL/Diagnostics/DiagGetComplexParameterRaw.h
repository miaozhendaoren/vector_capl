#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads the complex parameter to the specified raw value.
 *
 * Reads the complex parameter to the specified raw value.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param parameterName
 *   Parameter qualifier
 *
 * @param iteration
 *   Iteration (beginning with 0)
 *
 * @param subParameter
 *   Sub parameter qualifier
 *
 * @param buffer
 *   Output buffer
 *
 * @param buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetComplexParameterRaw(DiagResponse obj, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads the complex parameter to the specified raw value.
 *
 * Reads the complex parameter to the specified raw value.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param parameterName
 *   Parameter qualifier
 *
 * @param iteration
 *   Iteration (beginning with 0)
 *
 * @param subParameter
 *   Sub parameter qualifier
 *
 * @param buffer
 *   Output buffer
 *
 * @param buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetComplexParameterRaw(DiagRequest obj, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize);

}
