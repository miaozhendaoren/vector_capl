#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   No more attempt is made to send Network Requests to the specified or current ECU.
 *
 * @todo
 */

}
