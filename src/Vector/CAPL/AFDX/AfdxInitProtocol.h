#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Initializes a protocol for an AFDX packet.
 *
 * The function initializes the protocol for a packet. If necessary further needed lower protocols are initialized, e.g. IPv4. Already initialized higher protocols are deleted.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol
 *
 * @return
 *   0 or error code
 */
long AfdxInitProtocol(long packet, char * protocolDesignator);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Initializes a protocol for an AFDX packet.
 *
 * The function initializes the protocol for a packet. If necessary further needed lower protocols are initialized, e.g. IPv4. Already initialized higher protocols are deleted.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol
 *
 * @param packetTypeDesignator
 *   type of the packet
 *
 * @return
 *   0 or error code
 */
long AfdxInitProtocol(long packet, char * protocolDesignator, char * packetTypeDesignator);

}
