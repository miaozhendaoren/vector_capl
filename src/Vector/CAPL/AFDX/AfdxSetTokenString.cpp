#include "AfdxSetTokenString.h"

#include <iostream>

namespace capl
{

long AfdxSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
