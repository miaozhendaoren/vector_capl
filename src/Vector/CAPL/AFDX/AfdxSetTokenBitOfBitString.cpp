#include "AfdxSetTokenBitOfBitString.h"

#include <iostream>

namespace capl
{

long AfdxSetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, char *namedBit, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, long bitPosition, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
