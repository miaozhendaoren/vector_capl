#include "AfdxGetSignalInt.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalInt(long packet, ulong offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalInt(long packet, const char * sigName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
