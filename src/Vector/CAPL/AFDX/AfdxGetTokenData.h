#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param bufferStruct
 *   struct in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * bufferStruct);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param bufferStruct
 *   struct in which the data are copied
 *
 * @return
 *   number of copied bytes or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * bufferStruct);

}
