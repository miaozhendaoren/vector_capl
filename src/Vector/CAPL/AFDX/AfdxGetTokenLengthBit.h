#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the length of a token in bit.
 *
 * The function returns the length of a token in bit.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "header"
 *
 * @return
 *   length of the token in bit
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenLengthBit(long packet, char * protocolDesignator, char * tokenDesignator);

}
