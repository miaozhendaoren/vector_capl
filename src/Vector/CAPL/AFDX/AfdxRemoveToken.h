#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Removes a token from a protocol header.
 *
 * The function removes a token from a protocol header
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol
 *
 * @param tokenDesignator
 *   name of the token
 *
 * @return
 *   0 or error code
 */
long AfdxRemoveToken(long packet, char * protocolDesignator, char * tokenDesignator);

}
