#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the value of a bit in a bit string.
 *
 * This function sets a bit of a bit string that is specified either by its name (1) or by its position (2) to a new value.
 *
 * If the bit string does not yet contain the specified bit the bit string is resized up to the specified bit. On resizing the new bits before the specified bit are set to 0.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param namedBit
 *   name of the bit
 *
 * @param value
 *   new value of the bit
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, char * namedBit, long value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the value of a bit in a bit string.
 *
 * This function sets a bit of a bit string that is specified either by its name (1) or by its position (2) to a new value.
 *
 * If the bit string does not yet contain the specified bit the bit string is resized up to the specified bit. On resizing the new bits before the specified bit are set to 0.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param bitPosition
 *   zero based index of the bit
 *
 * @param value
 *   new value of the bit
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, long bitPosition, long value);

}
