#include "AfdxSetSignalOpaque.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalOpaque(long packet, ulong offset, ulong length, const char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalOpaque(long packet, const char * sigName, ulong length, const char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalOpaque(long packet, const char * sigName, ulong length, const char * value, long fdsStatus)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
