#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Checks if a token is part of a packet.
 *
 * The function checks, if the specified token is part of the packet.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol
 *
 * @param tokenDesignator
 *   name of the token
 *
 * @return
 *   - 1 - token is part of the packet
 *   - 0 - token is not part of the packet
 */
long AfdxIsTokenAvailable(long packet, char * protocolDesignator, char * tokenDesignator);

}
