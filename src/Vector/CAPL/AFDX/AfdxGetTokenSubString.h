#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets a part of a string value of a token.
 *
 * The function copies a specified number of characters from a given position inside the token and adds a terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of characters to be copied
 *
 * @param buffer
 *   buffer in which the characters are copied
 *   The function adds a terminating "\0". Thus, the size of the buffer must be at least one byte larger than length.
 *
 * @return
 *   number of copied characters or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenSubString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * buffer);

}
