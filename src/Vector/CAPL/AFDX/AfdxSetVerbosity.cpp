#include "AfdxSetVerbosity.h"

#include <iostream>

namespace capl
{

long AfdxSetVerbosity(long verbosity)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
