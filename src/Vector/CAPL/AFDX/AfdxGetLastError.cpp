#include "AfdxGetLastError.h"

#include <iostream>

namespace capl
{

long AfdxGetLastError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
