#include "OnAfdxPacket.h"

#include <iostream>

namespace capl
{

void OnAfdxPacket(long dir, long line, int64 timestamp, long bag, long afdxFlags, long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
