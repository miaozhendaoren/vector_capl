#include "AfdxCompletePacket.h"

#include <iostream>

namespace capl
{

long AfdxCompletePacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
