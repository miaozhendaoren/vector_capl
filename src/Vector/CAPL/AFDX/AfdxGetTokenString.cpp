#include "AfdxGetTokenString.h"

#include <iostream>

namespace capl
{

long AfdxGetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
