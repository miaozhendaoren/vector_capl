#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the string value of a token.
 *
 * The function copies characters from the token and adds a terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param bufferSize
 *   size of buffer in byte
 *   The function adds a terminating "\0". Thus the maximum number of copied characters is bufferSize-1.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied characters or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long bufferSize, char * buffer);

}
