#include "AfdxInitProtocol.h"

#include <iostream>

namespace capl
{

long AfdxInitProtocol(long packet, char * protocolDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitProtocol(long packet, char * protocolDesignator, char * packetTypeDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
