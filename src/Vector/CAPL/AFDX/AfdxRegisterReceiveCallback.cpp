#include "AfdxRegisterReceiveCallback.h"

#include <iostream>

namespace capl
{

long AfdxRegisterReceiveCallback(char * onPacketCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxRegisterReceiveCallback(char * onPacketCallback, long direction, long dropped, long type)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
