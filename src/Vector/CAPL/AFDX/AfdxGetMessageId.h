#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the ID of a message.
 *
 * This function gets the message ID (from VL ID and UDP port). DBC is not necessary.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @return
 *   - >0: message ID
 *   - 0: error (invalid packet or no UDP)
 */
long AfdxGetMessageId(long packet);

}
