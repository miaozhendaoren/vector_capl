#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(void);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param protocolDesignator
 *   designator of the protocol, which should be used for initialization
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(char * protocolDesignator);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param protocolDesignator
 *   designator of the protocol, which should be used for initialization
 *
 * @param packetTypeDesignator
 *   designator of the packet type
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(char * protocolDesignator, char * packetTypeDesignator);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param packetToCopy
 *   handle of a packet which was created with AfdxInitPacket before or handle of a packet which has been received within a callback function (OnAfdxPacket)
 *   The header and the data of this packet are copied to the new created packet.
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(long packetToCopy);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawData
 *   raw data of an AFDX packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(long rawDataLength, byte * rawData);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawData
 *   raw data of an AFDX packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(long rawDataLength, char * rawData);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawDataStruct
 *   raw data of an AFDX packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(long rawDataLength, void * rawDataStruct);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param srcIP
 *   source IP address as defined by ARINC-664p7
 *
 * @param dstIP
 *   destination IP address as defined by ARINC-664p7
 *
 * @param srcUdpPort
 *   source UDP port
 *
 * @param dstUdpPort
 *   destination UDP port
 *
 * @param virtualLinkId
 *   VirtualLink identifier as defined by ARINC 664p7
 *
 * @param payloadSize
 *   size of requested UDP payload
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(long srcIP, long dstIP, long srcUdpPort, long dstUdpPort, long virtualLinkId, long payloadSize);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Creates a new AFDX packet.
 *
 * This function creates a new AFDX packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param msgID
 *   messageID as per DBC (vlID * 0x10000 + udpPort or 0 to use msgName)
 *
 * @param msgName
 *   message name as per DBC or NULL to force use of msgID
 *
 * @param initSignals
 *   specifies if signals should be created with default setting (0=no)
 *
 * @return
 *   handle of the created packet or 0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxInitPacket(long msgID, char * msgName, long initSignals);

}
