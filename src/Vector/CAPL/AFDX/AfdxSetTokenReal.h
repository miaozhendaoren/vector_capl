#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the float or double value of a token.
 *
 * The function sets the float or double value of a token depending on the length parameter.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "udp"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   length of the integer value, must be 4 or 8 byte
 *
 * @param value
 *   new value
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenReal(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, double value);

}
