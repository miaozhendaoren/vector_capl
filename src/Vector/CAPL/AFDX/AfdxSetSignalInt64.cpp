#include "AfdxSetSignalInt64.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalInt64(long packet, ulong offset, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalInt64(long packet, const char * sigName, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalInt64(long packet, const char * sigName, int64 value, long fdsStatus)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
