#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Completes an AFDX packet for sending.
 *
 * The function completes a packet before sending it with AfdxOutputPacket. It calculates the fields that are marked with CompleteProtocol in the protocol overview, e.g. checksum, lengths, etc.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @return
 *   0 or error code
 */
long AfdxCompletePacket(long packet);

}
