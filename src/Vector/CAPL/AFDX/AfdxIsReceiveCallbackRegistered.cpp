#include "AfdxIsReceiveCallbackRegistered.h"

#include <iostream>

namespace capl
{

long AfdxIsReceiveCallbackRegistered(char * onPacketCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
