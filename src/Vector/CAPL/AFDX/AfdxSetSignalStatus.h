#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX FDS status.
 *
 * This function sets the content of an AFDX FDS status within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param value
 *   status value according to AFDX specification
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalStatus(long packet, ulong offset, long value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX FDS status.
 *
 * This function sets the content of an AFDX FDS status within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param value
 *   status value according to AFDX specification
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalStatus(long packet, const char * sigName, long value);

}
