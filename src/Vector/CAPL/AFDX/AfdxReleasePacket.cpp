#include "AfdxReleasePacket.h"

#include <iostream>

namespace capl
{

long AfdxReleasePacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
