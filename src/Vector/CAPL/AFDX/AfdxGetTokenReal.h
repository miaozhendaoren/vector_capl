#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the float or double value of a token.
 *
 * The function requests the float or double value of a token depending on the length parameter.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket or from within a callback function
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "udp"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   length of the integer value, must be 4 or 8 byte
 *
 * @return
 *   value of the token or 0.0
 *   With AfdxGetLastError you can check if the function has been processed successfully.
 */
long AfdxGetTokenReal(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length);

}
