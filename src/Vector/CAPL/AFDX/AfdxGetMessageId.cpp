#include "AfdxGetMessageId.h"

#include <iostream>

namespace capl
{

long AfdxGetMessageId(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
