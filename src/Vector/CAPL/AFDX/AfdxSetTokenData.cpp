#include "AfdxSetTokenData.h"

#include <iostream>

namespace capl
{

long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * dataStruct)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * data)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * dataStruct)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
