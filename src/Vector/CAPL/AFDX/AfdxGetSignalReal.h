#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX real signal.
 *
 * This function gets the content of an AFDX real signal (float or double, i.e. four or eight byte) within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param isDouble
 *   - 0: set four byte value
 *   - !0: set eight byte value
 *
 * @return
 *   read value as double or 0 if error (diagnostics with AfdxGetLastError)
 */
long AfdxGetSignalReal(long packet, ulong offset, long isDouble);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX real signal.
 *
 * This function gets the content of an AFDX real signal (float or double, i.e. four or eight byte) within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @return
 *   read value as double or 0 if error (diagnostics with AfdxGetLastError)
 */
long AfdxGetSignalReal(long packet, const char * sigName);

}
