#include "AfdxGetSignalStatus.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalStatus(long packet, ulong offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalStatus(long packet, const char * sigName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
