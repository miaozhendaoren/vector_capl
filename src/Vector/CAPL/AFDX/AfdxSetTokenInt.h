#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the integer value of a token.
 *
 * The function sets the integer value of a token.
 *
 * Variant (2) with byte offset sets a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "lpvSpeed"
 *
 * @param value
 *   new integer value
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator, long value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the integer value of a token.
 *
 * The function sets the integer value of a token.
 *
 * Variant (2) with byte offset sets a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "lpvSpeed"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   length of the integer value, must be 1, 2, 3 or 4 byte
 *
 * @param networkByteOrder
 *   - 0 = INTEL (little-endian)
 *   - 1 = MOTOROLA (big-endian)
 *
 * @param value
 *   new integer value
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder, long value);

}
