#include "AfdxResizeToken.h"

#include <iostream>

namespace capl
{

long AfdxResizeToken(long packet, char * protocolDesignator, char * tokenDesignator, long newBitLength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
