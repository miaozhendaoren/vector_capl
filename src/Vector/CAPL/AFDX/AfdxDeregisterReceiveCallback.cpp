#include "AfdxDeregisterReceiveCallback.h"

#include <iostream>

namespace capl
{

long AfdxDeregisterReceiveCallback(char * onPacketCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
