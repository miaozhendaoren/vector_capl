#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX boolean signal.
 *
 * This function gets the content of an AFDX boolean signal within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param bitPos
 *   position of the bit within the 32 bit according to ICD (1..32)
 *
 * @return
 *   0 or 1, other values are error codes
 */
long AfdxGetSignalBool(long packet, ulong offset, long bitPos);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX boolean signal.
 *
 * This function gets the content of an AFDX boolean signal within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @return
 *   0 or 1, other values are error codes
 */
long AfdxGetSignalBool(long packet, const char * sigName);

}
