#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX string signal.
 *
 * This function sets the content of an AFDX string signal within a packet, with or without DBC information, inclusive two byte for length information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param value
 *   new signal value as zero terminated ASCII string
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalString(long packet, ulong offset, const char * value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX string signal.
 *
 * This function sets the content of an AFDX string signal within a packet, with or without DBC information, inclusive two byte for length information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param value
 *   new signal value as zero terminated ASCII string
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalString(long packet, const char * sigName, const char * value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX string signal.
 *
 * This function sets the content of an AFDX string signal within a packet, with or without DBC information, inclusive two byte for length information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param value
 *   new signal value as zero terminated ASCII string
 *
 * @param fdsStatus
 *   FDS status according to ARINC 664 part 7
 *   - 0: ND (no data): no valid data in data set, this would include Fail Warn and other conditions where the contents are meaningless
 *   - 3: NO (normal operation): valid data, normal operating conditions
 *   - 12: FT (functional test): equipment test conditions
 *   - 48: NCD (no computed data): Invalid data, equipment is in normal operating conditions but unable to compute reliable data
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalString(long packet, const char * sigName, const char * value, long fdsStatus);

}
