#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the data of a token.
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use AfdxResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * data);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the data of a token.
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use AfdxResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * data);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the data of a token.
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use AfdxResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param dataStruct
 *   struct containing the data
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * dataStruct);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the data of a token.
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use AfdxResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * data);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the data of a token.
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use AfdxResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * data);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the data of a token.
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use AfdxResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param dataStruct
 *   struct containing the data
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * dataStruct);

}
