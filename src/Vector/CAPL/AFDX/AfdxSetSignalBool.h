#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX boolean signal.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param bitPos
 *   position of the bit within the 32 bit according to ICD (1..32))
 *
 * @param value
 *   new signal value
 *   - 0: false
 *   - !0: true
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalBool(long packet, ulong offset, long bitPos, long value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX boolean signal.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param value
 *   new signal value
 *   - 0: false
 *   - !0: true
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalBool(long packet, const char * sigName, long value);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the content of an AFDX boolean signal.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param value
 *   new signal value
 *   - 0: false
 *   - !0: true
 *
 * @param fdsStatus
 *   FDS status according to ARINC 664 part 7
 *   - 0: ND (no data): no valid data in data set, this would include Fail Warn and other conditions where the contents are meaningless
 *   - 3: NO (normal operation): valid data, normal operating conditions
 *   - 12: FT (functional test): equipment test conditions
 *   - 48: NCD (no computed data): Invalid data, equipment is in normal operating conditions but unable to compute reliable data
 *
 * @return
 *   0 or error code
 */
long AfdxSetSignalBool(long packet, const char * sigName, long value, long fdsStatus);

}
