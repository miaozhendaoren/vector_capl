#include "AfdxGetSignalOpaque.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalOpaque(long packet, ulong offset, ulong bufSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalOpaque(long packet, ulong offset, ulong numBytes, ulong bufSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalOpaque(long packet, const char * sigName, ulong bufSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
