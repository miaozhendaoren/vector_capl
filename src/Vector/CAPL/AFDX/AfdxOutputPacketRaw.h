#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sends a raw AFDX packet.
 *
 * The function sends a raw AFDX packet.
 *
 * An AfdxOutputPacketRaw( packet ) call is equivalent to AfdxOutputPacket( packet, SingleShot, NoIPFragmentation, NoAFDXVLScheduling, NoAFDXSeqNoManagement, MACIFRelated ).
 *
 * @param packet
 *   handle of the packet to send that has been created with AfdxInitPacket.
 *
 * @return
 *   0 or error code
 */
long AfdxOutputPacketRaw(long packet);

}
