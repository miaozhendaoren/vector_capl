#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX integer (64 bit) signal.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @return
 *   read value as integer (64 bit) or 0 if error (diagnostics with AfdxGetLastError)
 */
long AfdxGetSignalInt64(long packet, ulong offset);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX integer (64 bit) signal.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @return
 *   read value as integer (64 bit) or 0 if error (diagnostics with AfdxGetLastError)
 */
long AfdxGetSignalInt64(long packet, const char * sigName);

}
