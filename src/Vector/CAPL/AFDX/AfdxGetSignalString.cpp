#include "AfdxGetSignalString.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalString(long packet, ulong offset, ulong bufSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalString(long packet, const char * sigName, ulong bufSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
