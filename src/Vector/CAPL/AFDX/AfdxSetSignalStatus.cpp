#include "AfdxSetSignalStatus.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalStatus(long packet, ulong offset, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalStatus(long packet, const char * sigName, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
