#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the name of a message.
 *
 * This function gets the message name if the message is defined in DBC.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param bufferSize
 *   size of buffer in byte
 *
 * @param buffer
 *   buffer to store the message name
 *
 * @return
 *   0 if message name could be read, else error code
 */
long AfdxGetMessageName(long packet, long bufferSize, char * buffer);

}
