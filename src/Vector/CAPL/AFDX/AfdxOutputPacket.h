#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sends an AFDX packet.
 *
 * This function sends an AFDX packet.
 *
 * An AfdxOutputPacket( packet ) call is equivalent to AfdxOutputPacket( packet, SingleShot, DoIPFragmentation, DoAFDXVLScheduling, DoAFDXSeqNoManagement, DBRelated ).
 *
 * If one of the following parameters is set to the listed value
 * - cyclicTransmission = CyclicOn
 * - ipFragmentation = DoIPFragmentation (true)
 * - vlScheduling = DoAFDXVLScheduling (true)
 * - redundancyManagement = DBRelated
 * the message to send needs to be described in the database.
 *
 * @param packet
 *   handle of the packet to send that has been created with AfdxInitPacket.
 *
 * @return
 *   0 or error code
 */
long AfdxOutputPacket(long packet);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sends an AFDX packet.
 *
 * This function sends an AFDX packet.
 *
 * An AfdxOutputPacket( packet ) call is equivalent to AfdxOutputPacket( packet, SingleShot, DoIPFragmentation, DoAFDXVLScheduling, DoAFDXSeqNoManagement, DBRelated ).
 *
 * If one of the following parameters is set to the listed value
 * - cyclicTransmission = CyclicOn
 * - ipFragmentation = DoIPFragmentation (true)
 * - vlScheduling = DoAFDXVLScheduling (true)
 * - redundancyManagement = DBRelated
 * the message to send needs to be described in the database.
 *
 * @param packet
 *   handle of the packet to send that has been created with AfdxInitPacket.
 *
 * @param cyclicTransmission
 *   - 0: CyclicOn (Switches on cyclic transmission of the passed message)
 *   - 1: CyclicOff (Switches off cyclic transmission of the passed message)
 *   - 2: SingleShot (Leads to a transmission of the passed message independent of the current cyclic transmission state)
 *
 * @param ipFragmentation
 *   - 0: NoIPFragmentation (false, IP fragmentation is switched off for the passed message)
 *   - 1: DoIPFragmentation (true, IP fragmentation is switched on for the passed message)
 *
 * @param vlScheduling
 *   - 0: NoAFDXVLScheduling (false, VL scheduling for the passed message will be omitted)
 *   - 1: DoAFDXVLScheduling (true, VL scheduling for the passed message will be done)
 *
 * @param seqNoManagement
 *   - 0: NoAFDXSeqNoManagement (false, sequence number will be transmitted as defined within the passed message)
 *   - 1: DoAFDXSeqNoManagement (true, sequence number will be set by the underlying sequence number management system)
 *
 * @param redundancyManagement
 *   - 0: Short Name: DBRelated; Description: Redundancy and line according to DBC
 *   - 1: Short Name: ForceA; Description: Force MAC interface ID A on line A
 *   - 2: Short Name: ForceB; Description: Force MAC interface ID B on line B
 *   - 3: Short Name: ForceAB; Description: Force MAC interface ID A on line A and B on B
 *   - 4: Short Name: ForceAonB; Description: Force MAC interface ID A on line B
 *   - 5: Short Name: ForceBonA; Description: Force MAC interface ID B on line A
 *   - 6: Short Name: ForceABonBA; Description: Force MAC interface ID A on line B and B on A
 *   - 7: Short Name: ForceLineA; Description: Force line A
 *   - 8: Short Name: ForceLineB; Description: Force line B
 *   - 9: Short Name: ForceLineAB; Description: Force line AB
 *   - 10: Short Name: MACIFRelated; Description: Line according MAC interface ID
 *
 * @return
 *   0 or error code
 */
long AfdxOutputPacket(long packet, long cyclicTransmission, long ipFragmentation, long vlScheduling, long seqNoManagement, long redundancyManagement);

}
