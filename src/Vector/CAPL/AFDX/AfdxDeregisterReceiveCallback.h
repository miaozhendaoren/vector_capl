#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Deregisters a previous installed CAPL callback function.
 *
 * Use this function to deregister a previous installed CAPL callback function.
 *
 * @param onPacketCallback
 *   name of CAPL callback function to be deregistered
 *
 * @return
 *   0 or error code
 */
long AfdxDeregisterReceiveCallback(char * onPacketCallback);

}
