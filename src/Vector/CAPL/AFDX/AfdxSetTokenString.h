#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the string value of a token.
 *
 * The function copies the string value to the token without terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param data
 *   data as string
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, char * data);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the string value of a token.
 *
 * The function copies the string value to the token without terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param data
 *   data as string
 *
 * @return
 *   0 or error code
 */
long AfdxSetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, char * data);

}
