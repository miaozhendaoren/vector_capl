#include "AfdxIsPacketValid.h"

#include <iostream>

namespace capl
{

long AfdxIsPacketValid(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
