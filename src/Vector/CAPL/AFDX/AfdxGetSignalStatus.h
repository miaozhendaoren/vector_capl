#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX FDS status.
 *
 * This function gets the content of an AFDX FDS status within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @return
 *   - >=0: FDS status
 *   - <0: indicates an error code
 */
long AfdxGetSignalStatus(long packet, ulong offset);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX FDS status.
 *
 * This function gets the content of an AFDX FDS status within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @return
 *   - >=0: FDS status
 *   - <0: indicates an error code
 */
long AfdxGetSignalStatus(long packet, const char * sigName);

}
