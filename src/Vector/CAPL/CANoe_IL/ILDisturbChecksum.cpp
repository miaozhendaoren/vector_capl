#include "ILDisturbChecksum.h"

#include <iostream>

namespace capl
{

long ILDisturbChecksum(char * pduName, long checksumType, long disturbanceMode, long disturbanceCount, long disturbanceValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long ILDisturbChecksum(char * pduName, char * sigGroupName, long checksumType, long disturbanceMode, long disturbanceCount, long disturbanceValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
