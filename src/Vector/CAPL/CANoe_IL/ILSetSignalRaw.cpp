#include "ILSetSignalRaw.h"

#include <iostream>

namespace capl
{

long ILSetSignalRaw(char * sig, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
