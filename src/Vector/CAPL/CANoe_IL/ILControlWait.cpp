#include "ILControlWait.h"

#include <iostream>

namespace capl
{

long ILControlWait(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
