#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Enables the sending of the message.
 *
 * Enables the sending of the message.
 *
 * @param msg
 *   Message that should be re-enabled after having disabled it with ILFaultInjectionDisableMsg.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 */
long ILFaultInjectionEnableMsg(char * msg);

}
