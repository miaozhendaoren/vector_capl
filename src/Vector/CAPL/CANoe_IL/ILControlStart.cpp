#include "ILControlStart.h"

#include <iostream>

namespace capl
{

long ILControlStart(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
