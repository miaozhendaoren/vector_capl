#include "ILFaultInjectionDisableMsg.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionDisableMsg(char * msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
