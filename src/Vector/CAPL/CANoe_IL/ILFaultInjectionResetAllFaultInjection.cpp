#include "ILFaultInjectionResetAllFaultInjection.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionResetAllFaultInjections(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
