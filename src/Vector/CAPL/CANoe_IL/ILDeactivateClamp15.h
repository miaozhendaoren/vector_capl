#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Forwards the appropriate state of clamp 15.
 *
 * Forwards the appropriate state (active/deactive) of clamp 15 to the NM simulation (if present).
 *
 * @return
 *   - 0: No error
 *   - -1: The IL is not initialized properly. The function is ignored.
 */
long ILDeactivateClamp15(void);

}
