#include "ILFaultInjectionSetMsgCycleTime.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionSetMsgCycleTime(char * msg, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
