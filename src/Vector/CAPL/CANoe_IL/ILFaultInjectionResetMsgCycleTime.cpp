#include "ILFaultInjectionResetMsgCycleTime.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionResetMsgCycleTime(char * msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
