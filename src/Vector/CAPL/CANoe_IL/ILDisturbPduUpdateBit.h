#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Disturbs the PDU Update Bit with constant 0 or 1.
 *
 * This function modifies the update bit of a PDU. Different fault injections are possible.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param pduName
 *   Name of the node that should be modified.
 *
 * @param disturbanceCount
 *   - -1: Infinite.
 *   - 0: Stops the disturbance, e.g.a infinite disturbance.
 *   - n: Number of disturbances.
 *
 * @param updateBit
 *   Desired disturbance value (0,1).
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILDisturbPduUpdateBit(char * pduName, int disturbanceCount, int updateBit);

}
