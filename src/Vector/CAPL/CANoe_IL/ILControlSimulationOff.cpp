#include "ILControlSimulationOff.h"

#include <iostream>

namespace capl
{

long ILControlSimulationOff(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
