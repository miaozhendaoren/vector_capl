#include "ILSetSignal.h"

#include <iostream>

namespace capl
{

long ILSetSignal(char * sig, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
