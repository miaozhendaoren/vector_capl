#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Initialization of CANoe IL.
 *
 * Initialization of CANoe IL.
 *
 * This function may only be used in on preStart to prevent the IL autostart function.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this query.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 */
long ILControlInit(void);

}
