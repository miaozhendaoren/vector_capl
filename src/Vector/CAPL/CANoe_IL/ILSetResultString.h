#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Converts the transferred error code to text.
 *
 * Converts the transferred error code to text.
 *
 * @param aResult
 *   Return value of a CANoe IL API function.
 *
 * @param aText
 *   Char array.
 *
 * @param aLenText
 *   Length of the char array.
 *
 * @return
 *   - 0: No error.
 *   - -103: Invalid value was passed.
 */
long ILSetResultString(long aResult, char * aText, long aLenText);

}
