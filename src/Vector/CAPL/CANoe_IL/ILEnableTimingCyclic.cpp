#include "ILEnableTimingCyclic.h"

#include <iostream>

namespace capl
{

long ILEnableTimingCyclic(char * pduName, int enable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
