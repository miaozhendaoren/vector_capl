#include "ILSetResultString.h"

#include <iostream>

namespace capl
{

long ILSetResultString(long aResult, char * aText, long aLenText)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
