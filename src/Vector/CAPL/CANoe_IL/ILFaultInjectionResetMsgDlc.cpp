#include "ILFaultInjectionResetMsgDlc.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionResetMsgDlc(char * msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
