#include "ILSetEvent.h"

#include <iostream>

namespace capl
{

long ILSetEvent(char * sig)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
