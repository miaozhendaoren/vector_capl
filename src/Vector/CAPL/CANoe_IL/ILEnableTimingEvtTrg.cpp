#include "ILEnableTimingEvtTrg.h"

#include <iostream>

namespace capl
{

long ILEnableTimingEvtTrg(char * pduName, int enable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
