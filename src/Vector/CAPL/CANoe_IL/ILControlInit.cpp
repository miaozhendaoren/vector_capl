#include "ILControlInit.h"

#include <iostream>

namespace capl
{

long ILControlInit(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
