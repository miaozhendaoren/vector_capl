#include "ILSetSignalRawField.h"

#include <iostream>

namespace capl
{

long ILSetSignalRawField(char * sig, const char * pData, long aDataLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
