#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Sets the transferred signal to the provided value.
 *
 * Sets the transferred signal to the provided value.
 *
 * Use for integer signals > 32 bit.
 *
 * @param sig
 *   Signal that should be set. Specify as follows 'Message::Signal'.
 *
 * @param pData
 *   Byte array, which contains the raw data.
 *
 * @param aDataLen
 *   Length of the byte array.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this query.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 *   - -101: The maximum possible value range was exceeded (no sending on the bus).
 *   - -103: Invalid value was passed.
 */
long ILSetSignalRawField(char * sig, const char * pData, long aDataLen);

}
