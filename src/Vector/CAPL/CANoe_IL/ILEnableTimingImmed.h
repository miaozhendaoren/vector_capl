#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Controls the immediate timing of PDUs. The immediate timing can be enabled/disabled.
 *
 * Controls the immediate timing of PDUs. The immediate timing can be enabled/disabled.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param pduName
 *   Name of the PDU that should be modified.
 *
 * @param enable
 *   - 0 = disable
 *   - 1 = enable
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILEnableTimingImmed(char * pduName, int enable);

}
