#include "ILDisturbSignalUpdateBit.h"

#include <iostream>

namespace capl
{

long ILDisturbSignalUpdateBit(char * aPduName, char * aSignalName, long disturbanceMode, long disturbanceCount, long disturbanceValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
