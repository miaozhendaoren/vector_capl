#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Disturbs the counter with a configurable value.
 *
 * This function modifies the counter. Different fault injections are possible.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param pduName
 *   Name of the node that should be modified.
 *
 * @param counterType
 *   The possible values are described in the corresponding OEM Package manual.
 *
 * @param disturbanceMode
 *   Identifies the disturbance mode:
 *   - 0: Value. The disturbance uses the value in disturbanceValue as counter.
 *   - 1: Freeze. The current counter value is transmitted.
 *   - 2: Random. A random value is transmitted as counter.
 *   - 3: Offset. The counter is incremented with the value in disturbanceValue.
 *
 * @param disturbanceCount
 *   - -1. Infinite.
 *   - 0. Stops the disturbance, e.g.a infinite disturbance.
 *   - n. Number of disturbances.
 *
 * @param disturbanceValue
 *   This value is used in combination with the disturbanceMode.
 *
 * @param continueMode
 *   Defines the behavior of the counter after the disturbances are finished.
 *   - 0: CorrectCounter.
 *        The counter will be incremented with counter value + number of disturbances.
 *   - 1: LastValidCounter.
 *        The counters next value bases on the last value before the disturbance has started.
 *   - 2: LastValue.
 *        The counter increments the last counter value (last disturbance value).
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILDisturbCounter(char * pduName, long counterType, long disturbanceMode, long disturbanceCount, long disturbanceValue, long continueMode);

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Disturbs the counter with a configurable value.
 *
 * This function modifies the counter. Different fault injections are possible.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param pduName
 *   Name of the node that should be modified.
 *
 * @param sigGroupName
 *   Some systems assign a counter to signal group.
 *   With this variant you can apply the disturbance to a dedicated signal group within a PDU.
 *
 * @param counterType
 *   The possible values are described in the corresponding OEM Package manual.
 *
 * @param disturbanceMode
 *   Identifies the disturbance mode:
 *   - 0: Value. The disturbance uses the value in disturbanceValue as counter.
 *   - 1: Freeze. The current counter value is transmitted.
 *   - 2: Random. A random value is transmitted as counter.
 *   - 3: Offset. The counter is incremented with the value in disturbanceValue.
 *
 * @param disturbanceCount
 *   - -1. Infinite.
 *   - 0. Stops the disturbance, e.g.a infinite disturbance.
 *   - n. Number of disturbances.
 *
 * @param disturbanceValue
 *   This value is used in combination with the disturbanceMode.
 *
 * @param continueMode
 *   Defines the behavior of the counter after the disturbances are finished.
 *   - 0: CorrectCounter.
 *        The counter will be incremented with counter value + number of disturbances.
 *   - 1: LastValidCounter.
 *        The counters next value bases on the last value before the disturbance has started.
 *   - 2: LastValue.
 *        The counter increments the last counter value (last disturbance value).
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILDisturbCounter(char * pduName, char * sigGroupName, long counterType, long disturbanceMode, long disturbanceCount, long disturbanceValue, long continueMode);

}
