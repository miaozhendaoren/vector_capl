#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Assigns a new cycle time to the message.
 *
 * Assigns a new cycle time to the message.
 * To set the cycle time back to its original value, use ILFaultInjectionResetMsgCycleTime.
 *
 * @param msg
 *   Message that should get a new cycle time.
 *
 * @param value
 *   New cycle time of the message in milliseconds.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 */
long ILFaultInjectionSetMsgCycleTime(char * msg, double value);

}
