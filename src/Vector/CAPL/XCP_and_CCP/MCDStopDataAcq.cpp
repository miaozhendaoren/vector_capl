#include "MCDStopDataAcq.h"

#include <iostream>

namespace capl
{

long MCDStopDataAcq(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
