#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Checks, if a parameter is valid.
 *
 * Checks, if a parameter is valid. The value has to be from the MCD communication.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter.
 *
 * @return
 *   - 1: Valid in the meaning of the functional description
 *   - 0: Not valid, e.g. default value
 */
long MCDParamIsValid(char * moduleName, char * parameterName);

}
