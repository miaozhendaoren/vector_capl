#include "MCDQuitCANape.h"

#include <iostream>

namespace capl
{

long MCDQuitCANape(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
