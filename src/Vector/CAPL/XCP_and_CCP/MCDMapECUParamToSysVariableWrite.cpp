#include "MCDMapECUParamToSysVariableWrite.h"

#include <iostream>

namespace capl
{

long MCDMapECUParamToSysVariableWrite(char * moduleName, char * parameterName, char * SysVarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
