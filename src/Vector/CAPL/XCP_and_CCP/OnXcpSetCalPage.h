#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Is called when the ECU switched the calibration data page.
 *
 * The callback returns that the ECU switched the calibration data page.
 *
 * @param ecuQualifier
 *   Name of the device, configured within the XCP/CCP Configuration dialog.
 */
void OnXcpSetCalPage(char * ecuQualifier);

}
