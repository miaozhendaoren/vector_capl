#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   If the XCP slave device supports calibration data page switching, this command sets the current page and access mode.
 *
 * If the XCP slave device supports calibration data page switching, this command sets the current page and access mode.
 *
 * The callback returns that the ECU switched the calibration data page.
 *
 * @param ecuQualifier
 *   Name of the device, configured within the XCP/CCP Configuration dialog.
 *
 * @param mode
 *   Bit field defined in the XCP specification.
 *   - Bit 7: Use for all segments, i.e. parameter segmentNr is ignored.
 *   - Bit 1: The slave device XCP driver will access the given page.
 *   - Bit 0: The given page will be used by the slave device application.
 *
 * @param segmentNr
 *   Logical data segment number.
 *   Ignored if bit 7 of the mode parameter is set to 1.
 *
 * @param pageNr
 *   Logical data page number.
 *   Usually 0 identifies the page in RAM and 1 the page in FLASH memory.
 *   CANoe.XCP can only work on parameters stored in RAM.
 */
void xcpSetCalPage(char * ecuQualifier, byte mode, byte segmentNr, byte pageNr);

}
