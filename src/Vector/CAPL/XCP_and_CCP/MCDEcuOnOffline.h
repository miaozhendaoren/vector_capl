#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Sets a controller into online or offline mode.
 *
 * Sets a controller into online or offline mode.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param ecuState
 *   - 0: Sets the controller offline
 *   - 1: Sets the controller online
 *
 * @return
 *   - 1: OK
 *   - 0: Error
 */
long MCDEcuOnOffline(char * moduleName, long ecuState);

}
