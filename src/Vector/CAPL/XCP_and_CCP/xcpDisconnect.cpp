#include "xcpDisconnect.h"

#include <iostream>

namespace capl
{

long xcpDisconnect(char * ecuQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
