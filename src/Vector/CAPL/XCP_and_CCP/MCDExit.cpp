#include "MCDExit.h"

#include <iostream>

namespace capl
{

long MCDExit(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
