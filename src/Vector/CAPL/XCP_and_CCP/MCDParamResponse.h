#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Callback handler on a MCDParamRead request.
 *
 * Callback handler on a MCDGetECUParam request.
 * This function must be defined in the CAPL program to get a response on a read request.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter.
 *
 * @param value
 *   Value of the parameter.
 */
void MCDParamResponse(char * moduleName, char * parameterName, double value);

}
