#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Disconnects from a XCP/CCP device.
 *
 * Disconnects from a XCP/CCP device.
 *
 * Use xcpIsConnected to be aware of the disconnection.
 *
 * @param ecuQualifier
 *   Name of the device – configured within the XCP/CCP configuration dialog.
 *
 * @return
 *   - 0: OK
 *   - -1: System variable was not found
 *   - -2: Operation not allowed
 */
long xcpDisconnect(char * ecuQualifier);

}
