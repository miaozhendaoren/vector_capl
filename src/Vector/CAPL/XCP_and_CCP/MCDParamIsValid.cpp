#include "MCDParamIsValid.h"

#include <iostream>

namespace capl
{

long MCDParamIsValid(char * moduleName, char * parameterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
