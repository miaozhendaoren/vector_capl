#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * This function must only be called during the initialisation phase of the measurement (MeasurementInit).
 *
 * @return
 *   - 0: OK
 *   - -1: Error
 */
long MCDInit(void);

}
