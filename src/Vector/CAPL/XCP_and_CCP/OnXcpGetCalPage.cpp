#include "OnXcpGetCalPage.h"

#include <iostream>

namespace capl
{

void OnXcpGetCalPage(char * ecuQualifier, byte reserved0, byte reserved1, byte pageNr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
