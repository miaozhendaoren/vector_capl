#include "xcpConnect.h"

#include <iostream>

namespace capl
{

long xcpConnect(char * ecuQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
