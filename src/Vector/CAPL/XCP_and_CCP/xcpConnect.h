#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Establishes a connection to the XCP/CCP device and starts the configured DAQ measurement.
 *
 * Establishes a connection to the XCP/CCP device and starts the configured DAQ measurement.
 *
 * If the connection is successfully depends on the response of the device.
 *
 * Use xcpIsConnected to be aware of the connection.
 *
 * After establishing the connection and before start of the DAQ measurement the callback function OnXcpConnect is called.
 *
 * @param ecuQualifier
 *   Name of the device – configured within the XCP/CCP configuration dialog.
 *
 * @return
 *   - 0: OK
 *   - -1: Device with this name is not existing
 *   - -2: Operation not allowed – already connected
 */
long xcpConnect(char * ecuQualifier);

}
