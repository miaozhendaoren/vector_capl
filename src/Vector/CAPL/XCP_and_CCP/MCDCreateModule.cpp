#include "MCDCreateModule.h"

#include <iostream>

namespace capl
{

long MCDCreateModule(char * moduleName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
