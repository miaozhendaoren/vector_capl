#include "MCDStartDataAcq.h"

#include <iostream>

namespace capl
{

long MCDStartDataAcq(char * moduleName, int taskId, int pollingRate, char * parameters)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
