#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Executes implicit a MCDInitEx and closes CANape.
 *
 * Executes implicit a MCDInitEx and closes CANape.
 *
 * @return
 *   - 0: OK
 */
long MCDQuitCANape(void);

}
