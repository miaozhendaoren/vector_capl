#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   If the XCP slave device supports calibration data page switching, this command gets the currently active page and access mode.
 *
 * If the XCP slave device supports calibration data page switching, this command gets the currently active page and access mode.
 *
 * @param ecuQualifier
 *   Name of the device, configured within the XCP/CCP Configuration dialog.
 *
 * @param mode
 *   Access mode of the currently active page.
 *   The values may be 0x01 (ECU access) or 0x02 (XCP access).
 *   All other values are invalid.
 *
 * @param segmentNr
 *   Logical data segment number.
 */
void xcpGetCalPage(char * ecuQualifier, byte mode, byte segmentNr);

}
