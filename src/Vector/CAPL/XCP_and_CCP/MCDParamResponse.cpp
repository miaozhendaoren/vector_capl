#include "MCDParamResponse.h"

#include <iostream>

namespace capl
{

void MCDParamResponse(char * moduleName, char * parameterName, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
