#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Reading of a parameter.
 *
 * Reading of a parameter. The read parameter value will be available in the MCDParamResponse callback handler.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter to be read.
 *
 * @param format
 *   Read out format:
 *   - 0 – hex, ECU intern
 *   - 1 – physical representation
 *
 * @return
 *   - 0: OK
 */
long MCDGetECUParam(char * moduleName, char * parameterName, int format);

}
