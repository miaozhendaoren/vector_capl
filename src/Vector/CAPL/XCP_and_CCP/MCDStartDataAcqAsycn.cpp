#include "MCDStartDataAcqAsycn.h"

#include <iostream>

namespace capl
{

long MCDStartDataAcqAsync(char * moduleName, int taskId, int pollingRate, char * parameters)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
