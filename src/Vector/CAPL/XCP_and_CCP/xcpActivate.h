#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Activates a a2l signal for upload, download and DAQ measurement.
 *
 * Activates a a2l signal for upload, download and DAQ measurement.
 *
 * @param namespace_
 *   Namespace of the corresponding system variable.
 *
 * @param variable
 *   Name of the corresponding system variable.
 *
 * @return
 *   - 0: OK
 *   - -1: System variable was not found
 *   - -2: Operation not allowed
 */
long xcpActivate(char * namespace_, char * variable);

/**
 * @ingroup XCP
 *
 * @brief
 *   Activates a a2l signal for upload, download and DAQ measurement.
 *
 * Activates a a2l signal for upload, download and DAQ measurement.
 *
 * @param sysvar
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::".
 *   The name must be preceded by "sysVar::"
 *
 * @return
 *   - 0: OK
 *   - -1: System variable was not found
 *   - -2: Operation not allowed
 */
long xcpActivate(char * sysvar);

}
