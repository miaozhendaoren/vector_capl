#include "xcpActivate.h"

#include <iostream>

namespace capl
{

long xcpActivate(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long xcpActivate(char * sysvar)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
