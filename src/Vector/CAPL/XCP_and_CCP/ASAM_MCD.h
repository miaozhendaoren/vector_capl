#pragma once

/**
 * @ingroup XCP_and_CCP
 * @{
 * @defgroup ASAM_MCD ASAM-MCD CAPL Functions
 * @}
 */

#include "MCDCreateModule.h"
#include "MCDEcuOnOffline.h"
#include "MCDExit.h"
#include "MCDGetCurrentValue.h"
#include "MCDGetECUParam.h"
#include "MCDGetLastError.h"
#include "MCDInit.h"
#include "MCDInitEx.h"
#include "MCDMapECUParamToSysVariableRead.h"
#include "MCDMapECUParamToSysVariableWrite.h"
#include "MCDParamIsValid.h"
#include "MCDParamResponse.h"
#include "MCDQuitCANape.h"
#include "MCDSetECUParam.h"
#include "MCDStartDataAcq.h"
#include "MCDStartDataAcqAsycn.h"
#include "MCDStatusIndication.h"
#include "MCDStopDataAcq.h"

/* Error Code */
enum {
    AEC_CMD_NOT_SUP = 1, /**< Command not supported. */
    AEC_INTERFACE_NOTSUPPORTED = 2, /**< Interface type not supported. */
    AEC_CREATE_MEM_MAPPED_FILE = 3, /**< Error creating memory mapped file. */
    AEC_WRITE_CMD = 4, /**< Error writing data to memory mapped file. */
    AEC_READ_RESPONSE = 5, /**< Error reading response from memory mapped file. */
    AEC_ASAP2_FILE_NOT_FOUND = 6, /**< ASAP2 file not found. */
    AEC_INVALID_MODULE_HDL = 7, /**< Invalid module handle. */
    AEC_ERR_OPEN_FILE = 8, /**< Open file error. */
    AEC_UNKNOWN_OBJECT = 9, /**< Unknown object name. */
    AEC_NO_DATABASE = 10, /**< No database assigned. */
    AEC_PAR_SIZE_OVERFLOW = 11, /**< Parameter 'size' too large. */
    AEC_NOT_WRITE_ACCESS = 12, /**< Object has no write access. */
    AEC_OBJECT_TYPE_DOESNT_MATCH = 13, /**< Object type does not match. */
    AEC_NO_TASKS_OVERFLOW = 14, /**< Number of tasks overflow. */
    AEC_CCP_RESPONSE_SIZE_INVALID = 15, /**< Invalid CCP response size. */
    AEC_TIMEOUT_RESPONSE = 16, /**< Timeout reading response from memory mapped file. */
    AEC_NO_VALUES_SAMPLED = 17, /**< FIFO does not contain any values. */
    AEC_ACQ_CHNL_OVERRUN = 18, /**< Too much channels defined relating to single raster. */
    AEC_NO_RASTER_OVERFLOW = 19, /**< Too much raster selected for data acquisition (overflow of internal parameter). */
    AEC_CANAPE_CREATE_PROC_FAILED = 20, /**< CreateProcess of CANape failed. */
    AEC_EXIT_DENIED_WHILE_ACQU = 21, /**< Asap3Exit denied because data acquisition is still running. */
    AEC_WRITE_DATA_FAILED = 22, /**< Error writing data to application RAM. */
    AEC_NO_RESPONSE_FROM_ECU = 23, /**< No response from ECU (attach Asap2 failed). */
    AEC_ACQUIS_ALREADY_RUNNING = 24, /**< Asap3StartDataAcquisition denied: data acquisition already running. */
    AEC_ACQUIS_NOT_STARTED = 25, /**< Asap3StopAcquisition denied: data acquisition not started. */
    // 26
    AEC_NO_AXIS_PTS_NOT_VALID = 27, /**< Invalid number of axis points (see following note). */
    AEC_SCRIPT_CMD_TO_LARGE = 28, /**< Script command size overflow. */
    AEC_SCRIPT_CMD_INVALID = 29, /**< Invalid/unknown script command. */
    AEC_UNKNOWN_MODULE_NAME = 30, /**< Unknown module. */
    AEC_FIFO_INTERNAL_ERROR = 31, /**< CANape internal error concerning FIFO management. */
    AEC_VERSION_ERROR = 32, /**< Access denied: incompatible CANape version. */
    AEC_ILLEGAL_DRIVER = 33, /**< Illegal driver type. */
    AEC_CALOBJ_READ_FAILED = 34, /**< Read of calibration object failed. */
    AEC_ACQ_STP_INIT_FAILED = 35, /**< Initialization of data acquisition failed. */
    AEC_ACQ_STP_PROC_FAILED = 36, /**< Data acquisition failed. */
    AEC_ACQ_STP_OVERFLOW = 37, /**< Buffer overflow at data acquisition. */
    AEC_ACQ_STP_TIME_OVER = 38, /**< Data acquisition stopped because selected time is elapsed. */
    // 39
    AEC_NOSERVER_ERRCODE = 40, /**< No Server application available. */
    AEC_ERR_OPEN_DATADESCFILE = 41, /**< Unable to open data description file, maybe not exist. */
    AEC_ERR_OPEN_DATAVERSFILE = 42, /**< Unable to open a data file. */
    AEC_TO_MUCH_DISPLAYS_OPEN = 43, /**< Maximal count of displays are opened. */
    AEC_INTERNAL_CANAPE_ERROR = 44, /**< Attempt to create a module was failed. */
    AEC_CANT_OPEN_DISPLAY = 45, /**< Unable to open a display. */
    AEC_ERR_NO_PATTERNFILE_DEFINED = 46, /**< No parameterfilename. */
    AEC_ERR_OPEN_PATTERNFILE = 47, /**< Unable to open patternfile. */
    AEC_ERR_CANT_RELEASE_MUTEX = 48, /**< Release of a mutex failed. */
    AEC_WRONG_CANAPE_VERSION = 49, /**< Canape do not fit dll version. */
    AEC_TCP_SERV_CONNECT_FAILED = 50, /**< Connect to ASAP3 server failed. */
    AEC_TCP_MISSING_CFG = 51, /**< Missing CANape TCP Server configuration. */
    AEC_TCP_SERV_NOT_CONNECTED = 52, /**< Connection between ASAP3 Server and TCP CANapeAPI is not active. */
    AEC_TCP_EXIT_NOTCLOSED = 53,
    AEC_FIFO_ALREADY_INIT = 54, /**< The FIFO Memory was already created. Close all connection to reconfigure. */
    AEC_ILLEGAL_OPERATION = 55, /**< It's not possible to operate this command. */
    AEC_WRONG_TYPE = 56, /**< The given type is not supported. */
    AEC_NO_CANAPE_LICENSE = 57, /**< CANape is not licensed. */
    AEC_REG_OPEN_KEY_FAILED = 58, /**< Key "HKEY_LOCAL_MACHINE\\SOFTWARE\\VECTOR\\CANape" missing at Windows Registry, maybe CANape setup has not been correctly performed. */
    AEC_REG_QUERY_VALUE_FAILED = 59, /**< Value "Path" missing at Windows Registry, maybe CANape setup has not been correctly performed. */
    AEC_WORKDIR_ACCESS_FAILED = 60, /**< CreateProcess of CANape failed: working directory not accessible/exists. */
    AEC_INIT_COM_FAILED = 61, /**< Internal error: Asap3InitCom() failed */
    AEC_INIT_CMD_FAILED = 62, /**< Negative Response from CANape: Init() failed. */
    AEC_CANAPE_INVALID_PRG_PATH = 63, /**< CreateProcess of CANape failed: program directory not accessible/exists. */
    AEC_INVALID_ASAP3_HDL = 64, /**< Invalid asap3 handle. */
    AEC_LOADING_FILE = 65, /**< File loading failed. */
    AEC_SAVING_FILE = 66, /**< File saving failed. */
    AEC_UPLOAD = 67, /**< Upload failed. */
    AEC_WRITE_VALUE_ERROR = 68, /**< Value could not be written. */
    AEC_TMTF_NOT_FINSHED = 69, /**< Other file transmission in process. */
    AEC_TMTF_SEQUENCE_ERROR = 70, /**< TransmitFile: sequence error (internal error). */
    AEC_LAST_ERRCODE = 71
};
