#include "MCDInit.h"

#include <iostream>

namespace capl
{

long MCDInit(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
