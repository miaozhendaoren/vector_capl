#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Callback handler for different MCD status.
 *
 * Callback handler for different MCD status.
 *
 * The function provides the status of successful/ not successful data acquisition starts.
 *
 * @param moduleName
 *   Module name configured in the global option dialog. External Programs|MCD 3D Server.
 *
 * @param operation
 *   Operation ID, which shows the status.
 *   - 1 – Start Data Acq
 *
 * @param status
 *   - 1: Successful
 *   - 0: Fail
 */
void MCDStatusIndication(char * moduleName, long operation, long status);

}
