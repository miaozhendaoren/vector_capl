#include "xcpIsConnected.h"

#include <iostream>

namespace capl
{

long xcpIsConnected(char * ecuQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
