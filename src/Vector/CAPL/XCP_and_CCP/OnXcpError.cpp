#include "OnXcpError.h"

#include <iostream>

namespace capl
{

void OnXcpError(char * ecuQualifier, byte xcpCmd, byte xcpErrorCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
