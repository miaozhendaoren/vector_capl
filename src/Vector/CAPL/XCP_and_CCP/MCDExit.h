#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Ends the connection to the MCD server.
 *
 * Ends the connection to the MCD server.
 *
 * @return
 *   - 0: OK
 */
long MCDExit(void);

}
