/**
 * CAPL basic data types
 */

#pragma once

namespace capl
{

/* unsigned 8 bit */
// char
typedef unsigned char byte;

/* unsigned 16 bit */
typedef unsigned short word;

/* unsigned 32 bit */
typedef unsigned long dword;
typedef unsigned long ulong;

/* unsigned 64 bit */
typedef unsigned long long qword;

/* signed 8 bit */
// char

/* signed 16 bit */
// int

/* signed 32 bit */
// long
typedef long int int32;

/* signed 64 bit */
typedef long long int64;

}
