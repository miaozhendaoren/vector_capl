#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Initialize K-Line ECU communication on given channel.
 *
 * Initialize K-Line ECU communication on given channel.
 *
 * @param channel
 *   Number of the hardware channel.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_CreateECURepresentation(dword channel);

}
