#include "DiagInitEcuSimulation.h"

#include <iostream>

namespace capl
{

long DiagInitEcuSimulation(char * ecuQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
