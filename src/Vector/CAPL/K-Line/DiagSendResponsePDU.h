#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sends a raw byte buffer.
 *
 * Sends a raw byte buffer. E.g. this allows sending responses with invalid protocol headers.
 *
 * @param rawPDULength
 *   Length of the byte buffer.
 *
 * @param rawPDU
 *   Byte buffer to be transmitted.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long DiagSendResponsePDU(word rawPDULength, byte * rawPDU);

}
