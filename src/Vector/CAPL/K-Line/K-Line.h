#pragma once

/** @todo to be implemented */

/**
 * @defgroup K-Line K-Line CAPL Functions
 */

/* Callbacks */
// @todo _Diag_ChannelStateChanged
// @todo _Diag_PreSend
// @todo _KLine_ByteReceptionInd
// @todo _KLine_ByteTransmissionCon
// @todo _KLine_DataCon
// @todo _KLine_DataInd
// @todo _KLine_Error
// @todo _KLine_FastInitPatternReceived
// @todo _KLine_FrameReceptionInd
// @todo _KLine_FrameTransmissionCon

/* Controlling a Diagnostic Channel */
#include "DiagCloseChannel.h"
#include "DiagConnectChannel.h"
#include "DiagDisconnectChannel.h"
#include "DiagIsChannelConnected.h"

/* Configuring and Controlling a K-Line Tester */
#include "DiagSendRequestPDU.h"
#include "KLine_EnableSegmentedResponses.h"
#include "KLine_GetMeasuredInitParameter.h"
#include "KLine_Set5BaudAddressTester.h"
#include "KLine_SetBaudrate.h"
#include "KLine_SetECUAddress.h"
#include "KLine_SetHeaderFormat.h"
#include "KLine_SetInitType.h"
#include "KLine_SetP3Tester.h"
#include "KLine_SetP4Tester.h"
#include "KLine_SetTesterAddress.h"
#include "KLine_SetW4Tester.h"
#include "KLine_SetW5Tester.h"
#include "KLine_SetWakeUpTimesTester.h"
#include "KLine_SuppressAutomaticStopCommunication.h"
#include "KLine_UseDefaultHeader.h"

/* Configuring and Controlling a K-Line ECU Simulation */
#include "DiagInitEcuSimulation.h"
#include "DiagSendResponsePDU.h"
#include "KLine_CreateECURepresentation.h"
#include "KLine_ResetECUConnection.h"
#include "KLine_SendFrame.h"
#include "KLine_SetP1ECU.h"

/* Test Feature Set for K-Line */
#include "TestGetWaitEventKLineByte.h"
#include "TestGetWaitEventKLineFrame.h"
#include "TestWaitForDiagChannelClosed.h"
#include "TestWaitForDiagChannelConnected.h"
#include "TestWaitForDiagKLineByteReceived.h"
#include "TestWaitForDiagKLineByteTransmitted.h"
#include "TestWaitForDiagKLineFrameReceived.h"
#include "TestWaitForDiagKLineFrameTransmitted.h"
