#include "TestWaitForDiagKLineByteReceived.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagKLineByteReceived(dword timeout_ms)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
