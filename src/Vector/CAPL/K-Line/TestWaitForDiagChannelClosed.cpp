#include "TestWaitForDiagChannelClosed.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagChannelClosed(dword timeout_ms)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
