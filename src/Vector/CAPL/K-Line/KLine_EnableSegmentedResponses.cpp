#include "KLine_EnableSegmentedResponses.h"

#include <iostream>

namespace capl
{

long KLine_EnableSegmentedResponses(long enable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
