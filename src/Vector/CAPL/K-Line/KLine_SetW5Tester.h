#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the W5 timing befor the tester starts to transmit the address byte.
 *
 * Sets the W5 timing befor the tester starts to transmit the address byte.
 *
 * @param W5_us
 *   W5 tim in us.
 *   Value range: 0 – 10 000 000
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetW5Tester(dword W5_us);

}
