#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sends a raw byte buffer.
 *
 * Sends a raw byte buffer. Allows also to send invalid requests.
 *
 * @param rawPDULength
 *   Length of the raw buffer
 *
 * @param rawPDU
 *   PDU Byte buffer.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long DiagSendRequestPDU(byte * rawPDU, word rawPDULength);

}
