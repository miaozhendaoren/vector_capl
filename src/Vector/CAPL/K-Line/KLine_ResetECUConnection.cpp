#include "KLine_ResetECUConnection.h"

#include <iostream>

namespace capl
{

long KLine_ResetECUConnection()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
