#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Waits for the occurrence of a received valid message.
 *
 * Waits for the occurrence of a received valid message.
 * Should the message not occur before the expiration of the time timeout_ms, the wait condition is resolved nevertheless.
 *
 * @param timeout_ms
 *   Maximum time that should be waited [ms]
 *   (Transmission of 0: no timeout controlling)
 *
 * @return
 *   - -2: Resume due to constraint violation
 *   - -1: General error, for example, functionality is not available
 *   - 0: Resume due to timeout
 *   - 1: Resume due to event occurred
 */
long TestWaitForDiagKLineFrameReceived(dword timeout_ms);

}
