#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Resets the connection state of the simulated ECU.
 *
 * Resets the connection state of the simulated ECU.
 * After calling this function an initialization pattern can be received again.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_ResetECUConnection();

}
