#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Send data on the active K-Line communication channel.
 *
 * Send data on the active K-Line communication channel.
 *
 * The K-Line header is generated automatically due to header settings.
 *
 * @param data
 *   Data buffer
 *
 * @param count
 *   Length of data buffer
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SendFrame(byte * data, dword count);

}
