#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Enables receiving segmented responses.
 *
 * Enables receiving segmented responses.
 *
 * @param enable
 *   - 1: enable receiving of segmented responses.
 *   - 0: disable receiving of segmented responses.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_EnableSegmentedResponses(long enable);

}
