#include "KLine_SetTesterAddress.h"

#include <iostream>

namespace capl
{

long KLine_SetTesterAddress(byte testerAddress)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
