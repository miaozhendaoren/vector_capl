#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the address of the ECU.
 *
 * Sets the address of the ECU.
 *
 * @param ecuAddress
 *   Address of the ECU.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetECUAddress(byte ecuAddress);

}
