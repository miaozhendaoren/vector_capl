#include "KLine_SendFrame.h"

#include <iostream>

namespace capl
{

long KLine_SendFrame(byte * data, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
