#include "DiagSendRequestPDU.h"

#include <iostream>

namespace capl
{

long DiagSendRequestPDU(byte * rawPDU, word rawPDULength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
