#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   The header format specified in the diagnostic description file will be used.
 *
 * The header format specified in the diagnostic description file will be used.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_UseDefaultHeader();

}
