#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Configures the used initialization pattern.
 *
 * Configures the used initialization pattern.
 *
 * Restriction:
 * It is only possible to change the init type between the pre-configured init type an no init pattern.
 * It is not possible to change between fast init and 5 baud init or vice versa.
 *
 * @param initType
 *   Initialization pattern
 *   - 1 = 5 baud init
 *   - 2 = fast init
 *   - 3 = no init pattern
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetInitType(long initType);

}
