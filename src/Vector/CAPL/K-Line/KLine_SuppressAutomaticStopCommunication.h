#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   For a fast init ECU, automatic sending of a Stop communication command will be suppressed after closing the channel or a S3 timeout.
 *
 * For a fast init ECU, automatic sending of a Stop communication command will be suppressed after closing the channel or a S3 timeout.
 *
 * @param doSuppress
 *   - 1: Suppress sending of stop communication command
 *   - 0: do not suppress sending of stop communication command
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SuppressAutomaticStopCommunication(long doSuppress);

}
